import {useEffect} from 'react'

import axios from 'axios'

export const GetLabList = ({labs,setLabsData}) => {
  useEffect(() => {
    // const config = {
    //     headers: {
    //         Authorization: 'Bearer ' + localStorage.getItem('auth'),
    //     }
    // }
    axios.get(`/api/labs`).then(res => {
        const labsData = res.data;
        setLabsData(labsData);
    });
}, []);
}



export const SearchLabData = ({ inputChange, setLabsData }) => {  
  const searchLab = ()=>{
    const config = {
      headers: {
          Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
  }
  axios.get(`/api/stats/getLabList?lab_id=${inputChange.current}?&offset=0&count=10`, config).then(res => {
      const labsData = res.data;
      setLabsData(labsData);
    });   
  }
  return searchLab
  }

  
  export const RefreshData = ({ setLabsData }) => {  
    const refreshLab = ()=>{
      const config = {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
        }
    }
    axios.get(`/api/stats/getLabList?lab_id?&offset=0&count=10000`, config).then(res => {
      const labsData = res.data;
      setLabsData(labsData);
      });   
    }
    return refreshLab
    }
  