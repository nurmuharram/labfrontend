import { CButton } from "@coreui/react";
import axios from "axios";

export const PostBooking = ({ startdate, starttime, bookingName,labName, setAddItemModal }) => {

    const convertTime = new Date(startdate + ' ' + starttime)
    const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')
    // const startingdate = convertTime
    // const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')
   
    // const refreshBooking = () => {
    //     const config = {
    //         headers: {
    //             Authorization: 'Bearer ' + localStorage.getItem('auth'),
    //         }
    //     }
    //     axios.get(`/api/Booking/getAllBooking`, config).then(res => {
    //         const items = res.data;
    //         setBookingList(items);
    //     });
    // }

    console.log(startingdate);
    
    const addBooking = (e) => {
        e.preventDefault()
        // const FormData = require('form-data');

        var data = JSON.stringify({
            "name": bookingName,
            "date": startingdate,
            "lab_id": parseInt(labName)
          });

        // const data = new FormData();
        // data.append('name', bookingName)
        // data.append('date', startingdate)
        // data.append('lab_id', labName)

        const config = {
            method: 'POST',
            url: `/api/bookings`,
            headers: { 
                'Content-Type': 'application/json'
              },
            data: data
        };

                axios(config)
        .then(function (response) {
        console.log(JSON.stringify(response.data));
        alert('Booking  Added!')
        })
        .catch(function (error) {
        console.log(error);
        alert('Failed to Add!')
        });
        console.log(data);
        // axios(config)
        //     .then((response) => {
        //         console.log(JSON.stringify(response.data));
        //         alert('Booking  Added!')
        //         setAddItemModal(false)
        //     })
        //     .catch((error) => {
        //         console.log(error);
        //         alert('Failed to Add!')
        //     });
        // setTimeout(refreshBooking, 100)

    }

    return (
        <><CButton color='primary' size='md' onClick={addBooking}>Booking Lab</CButton></>
    )
}
