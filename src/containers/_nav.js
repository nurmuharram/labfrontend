import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,

  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Features']
  },

  {
    _tag: 'CSidebarNavItem',
    name: 'Lab List',
    to: '/lablist',
    icon: 'cil-pencil',
  },
]

export default _nav
