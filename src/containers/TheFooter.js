import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
        <a href="https://indonesiadefenceservices.com/" target="_blank" rel="noopener noreferrer">IDS</a>
        <span className="ml-1">&copy; 2022 Tes IDS</span>
      </div>
      <div className="mfs-auto">
        <span className="mr-1">Powered by</span>
        <a href="https://indonesiadefenceservices.com/" target="_blank" rel="noopener noreferrer">IDS</a>
      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
