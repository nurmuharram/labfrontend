import React, { useState, useRef } from "react";

import { useHistory } from "react-router";

import axios from "axios";
import { GetLabList } from "src/api/labListApi/LabListGetApi";
import { RefreshData } from "src/api/labListApi/LabListGetApi";
import DataTable from "src/components/table/DataTable";
import Collapse from "src/components/collapse/Collapse";
import { toggleDetails } from "src/services/utils/toggleDetails";
import AddBooking from "./Booking";
function LabList() {
  const [details, setDetails] = useState([]);
  const [deleteItemModal, setDeleteItemModal] = useState(false);
  const history = useHistory();
  const [labsData, setLabsData] = useState([]);
  // const [changeData, setChangedata] = useState('')
  const [expiredate, setExpiredate] = useState("2019-10-10");
  const [expiretime, setExpiretime] = useState("00:00");
  const convertCreatedAt = new Date(expiredate + " " + expiretime + ":" + "00");
  const created_at = convertCreatedAt
    .toISOString()
    .split("T")
    .join(" ")
    .replace(".000Z", "");
  const [addItemModal, setAddItemModal] = useState(false);
  // const inputChange = useRef();
  // inputChange.current = changeData;

  // SearchRoomData({inputChange, setLabsData})

  // useEffect(() => {
  //     const config = {
  //         headers: {
  //             Authorization: 'Bearer ' + localStorage.getItem('auth'),
  //         }
  //     }
  //     axios.get(`/api/stats/getRoomList?room_id?&offset=0&count=1000`, config).then(res => {
  //         const labsData = res.data;
  //         setLabsData(labsData);
  //     });
  // }, []);

  // const searchroom = () => {
  //     const config = {
  //         headers: {
  //             Authorization: 'Bearer ' + localStorage.getItem('auth'),
  //         }
  //     }
  //     axios.get(`/api/stats/getRoomList?room_id=${inputChange.current}?&offset=0&count=10`, config).then(res => {
  //         const labsData = res.data;
  //         setLabsData(labsData);
  //     });
  // }

  GetLabList({ setLabsData });
  RefreshData({ setLabsData });

  // const refreshData = () => {
  //     const config = {
  //         headers: {
  //             Authorization: 'Bearer ' + localStorage.getItem('auth'),
  //         }
  //     }
  //     axios.get(`/api/stats/getRoomList?room_id?&offset=0&count=10000`, config).then(res => {
  //         const labsData = res.data;
  //         setLabsData(labsData);
  //     });
  // }

  const fields = [
    { key: "id", _style: { width: "10%" }, sorter: false },
    { key: "lab_name", _style: { width: "10%" } },
    { key: "price", _style: { width: "20%" } },
    { key: "description", _style: { width: "15%" } },
    { key: "created_at", _style: { width: "20%" } },
  ];

  return (
    <div>
      <div className="col" align="center">
        {/* <div className="card" style={{ background: '#9bf54c' }}>

                    <h4>
                        <b>Lab Data</b>
                    </h4>
                    <h6>
                        <i>View Lab Data</i>
                    </h6>

                </div> */}
      </div>

      <br />
      <div className="container">
        <div className="col">
          <div className="card card-accent-primary">
            <div className="card-header">
              <h5>Lab Data List</h5>
            </div>

            <div className="card-body">
              <div className="col">
                <div className="col" align="center">
                  <AddBooking
                    addItemModal={addItemModal}
                    setAddItemModal={setAddItemModal}
                  />
                </div>
              </div>

              {labsData === null ? (
                <></>
              ) : (
                <>
                  {/*                                 
                                <DataTable
                                    items={labsData}
                                    fields={fields}
                                    itemsPerPageSelect
                                    clickableRows
                                    onRowClick={(item) => history.push(`/labdetail/${item.id}`)}
                                    itemsPerPage={5}
                                    hover
                                    sorter
                                    pagination
                                    scopedSlots={{
                                        "created_at":
                                            (item, index) => {
                                                const date = new Date((item.created_at && item.created_at.split(' ').join('T')) )
                                                return (
                                                    <td>
                                                        {date.toString()}
                                                    </td>
                                                )
                                            }
                                    }}
                                /> */}

                  <DataTable
                    items={labsData}
                    fields={[
                      { key: "id" },
                      { key: "lab_name" },
                      { key: "price" },
                      { key: "created_date" },
                      {
                        key: "show_details",
                        label: "",
                        _style: { width: "10%" },
                        sorter: false,
                        filter: false,
                      },
                    ]}
                    itemsPerPage={10}
                    pagination
                    sorter
                    hover
                    itemsPerPageSelect
                    scopedSlots={{
                      show_details: (item, index) => {
                        return (
                          <td className="py-2">
                            <button
                              className="btn btn-outline-primary btn-sm"
                              color="primary"
                              variant="outline"
                              shape="round"
                              size="sm"
                              onClick={() => {
                                toggleDetails(index, { details, setDetails });
                              }}
                            >
                              {details.includes(index) ? "Close" : "Detail"}
                            </button>
                            
                            &nbsp;

                          <td className="py-1">
{/* 
                          <button 
                            className="btn btn-outline-primary btn-sm"
                            color="primary"
                            variant="outline"
                            shape="round"
                            size="sm"
                            onClick={() => setAddItemModal(!addItemModal)}
                            >
                              Booking
                              </button> */}
                          </td>    
                          </td>

                        );
                      },
                      details: (item, index) => {
                        const id = item.lab_name;
                        return (
                          <Collapse show={details.includes(index)}>
                            <div className="card-body">
                              <div className="row">
                                <div className="col" align="left">
                                  <h4>Description of Laboratory</h4>
                                  Laboratory Name :&nbsp;
                                  <b>
                                    <i>{item.lab_name}</i>
                                  </b>
                                  <div className="" align="left">
                                    Price : &nbsp;
                                    <b>
                                      <i>{item.price} $</i>
                                    </b>
                                  </div>
                                </div>
                              </div>
                              <hr />
                              <div className="row">
                                <div className="col" align="justify">
                                  Description :
                                  <div className="col">
                                    <i>{item.description}</i>
                                  </div>
                                </div>

                                <div
                                  className="col-md-3 col-lg-5"
                                  align="center"
                                >
                                  <img
                                    className=" img-responsive img-fluid"
                                    src={`${process.env.PUBLIC_URL}/assets/img/logoids.png`}
                                    width="50%"
                                  />
                                </div>
                              </div>
                            </div>
                          </Collapse>
                        );
                      },
                      start_date: (item, index) => {
                        const date = new Date(
                          (item.start_date &&
                            item.start_date.split(" ").join("T")) + ".000Z"
                        );
                        return <td>{date.toString()}</td>;
                      },
                      created_date: (item, index) => {
                        const date = new Date(
                          (item.created_date &&
                            item.created_date.split(" ").join("T")) + ".000Z"
                        );
                        return <td>{date.toString()}</td>;
                      },
                    }}
                  />
                </>
              )}
            </div>
            
          </div>
        </div>
      </div>
    </div>
  );
}

export default LabList;
