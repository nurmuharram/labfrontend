import React, { useState } from "react";
import { PostBooking } from "src/api/bookingLab/bookingPostApi";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from "src/components/modal";
import { Form } from "react-bootstrap";
import { GetLabList } from "src/api/labListApi/LabListGetApi";


function AddBooking({ data }) {
  const [addItemModal, setAddItemModal] = useState(false);
  const [startdate, setStartdate] = useState("2019-10-10");
  const [starttime, setStarttime] = useState("00:00:00");

//   const [startdate, setStartdate] = useState("");
//   const [starttime, setStarttime] = useState("");
  const [bookingName, setBookingName] = useState("");
//   const [labName, setLabName] = useState([]);

//   const [labsData, setLabsData] = useState([]);


  const [labName, setLabName] = useState([]);

  const [labsData, setLabsData] = useState([]);


  console.log(labName);
  console.log(bookingName);
  console.log(startdate);
  console.log(starttime);
  
  const dropdownLablist = labsData.map((item, index) => {
    return (
      <option value={item.id}>
        {item.id} - {item.lab_name}
      </option>
    );
  });

  GetLabList({ setLabsData });
  return (
    <div>
      <button
        className="btn btn-primary"
        color="primary"
        onClick={() => setAddItemModal(!addItemModal)}
      >
        Booking Lab
      </button>
      <Modal
        show={addItemModal}
        onClose={() => setAddItemModal(!addItemModal)}
        color="primary"
        size="lg"
      >
        <ModalHeader>
          <ModalTitle>Booking Lab</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <div className="col" align="left">
            <Form>
              <Form.Group className="mb-3" controlId="bookingName">
                <div className="row">
                  <div className="col-md-3">
                    <Form.Label>
                      <b>Name</b>
                    </Form.Label>
                  </div>
                  :
                  <div className="col-md-8">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Set Booking Name"
                      onChange={(e) => setBookingName(e.target.value)}
                    />
                  </div>
                </div>
              </Form.Group>

              <Form.Group className="mb-3" controlId="bookingstart">
                <div className="row">
                  <div className="col-md-3">
                    <Form.Label>
                      <b>Booking Time</b>
                    </Form.Label>
                  </div>
                  :
                  <div className="col-md-4">
                    <input
                      className="form-control"
                      type="date"
                      onKeyDown={(e) => e.preventDefault()}
                      onChange={(e) => setStartdate(e.target.value)}
                    />
                  </div>
                  <div className="col-md-4">
                    <input
                      className="form-control"
                      type="time"
                      onChange={(e) => setStarttime(e.target.value)}
                    />
                  </div>
                </div>
              </Form.Group>

              <Form.Group className="mb-3" controlId="bookingstart">
                <div className="row">
                  <div className="col-md-3">
                    <Form.Label>
                      <b>Lab</b>
                    </Form.Label>
                  </div>
                  :
                  <div className="col-md-8">
                    <Form.Select
                      className="col-md-8"
                      aria-label="Select"
                      type="text"
                      onChange={(e) => setLabName(e.target.value)}
                    >
                      {dropdownLablist}
                    </Form.Select>
                  </div>
                </div>
              </Form.Group>
            </Form>
          </div>
        </ModalBody>
        <ModalFooter>
          {startdate === "2019-10-10" ||
          starttime === "00:00:00" ||
          labName === "" ||
          bookingName === "" ? (
            <button className="btn btn-md btn-primary" disabled>
              Booking
            </button>
          ) : (
            <>
              {PostBooking({
                data,
                startdate,
                starttime,
                bookingName,
                labName,
                setAddItemModal,
                addItemModal,
              })}
            </>
          )}
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default AddBooking;
