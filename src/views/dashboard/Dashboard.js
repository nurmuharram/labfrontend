import React  from 'react'

const Dashboard = () => {


  return (
    <div className='container-fluid'>
      <div className='align-items-center'>
        <div className='col'>
          <figure style={{ position: 'static' }}>
            <img className=' img-responsive img-fluid' src={`${process.env.PUBLIC_URL}/assets/img/logoids.png`} width='100%' />
            <figcaption style={{
              position: 'absolute',
              width: '100%',
              'line-height': '5%',
              'text-align': 'left',
              paddingTop: '5%',
              paddingLeft: '3%',
              left: 0,
              top: 0,
              'text-shadow': '0 0 12px grey',
            }}>
              <h1 className='mb-0' style={{'fontSize' : '20px', 'font-family' : 'tahoma'}}><b>WELCOME TO</b></h1>
              <h2 className='mb-0' style={{'fontSize' : '46px', 'font-family' : 'tahoma'}}><b>Indonesian Defense Services</b></h2>
              
            </figcaption>
          </figure>
        </div>
      </div>
    </div>
  )
}

export default Dashboard; 
