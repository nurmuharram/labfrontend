## Installation

### Clone repo

``` bash
# clone the repo
$ git clone git@gitlab.com:nurmuharram/labfrontend.git

# go into app's directory
$ cd labfrontend

# checkout stable version
$ git checkout main

# install app's dependencies
$ npm install

### Start
# build for production with minification
$ npm start

### Build

Run `build` to build the project. The build artifacts will be stored in the `build/` directory.

```bash
# build for production with minification
$ npm run build
```

