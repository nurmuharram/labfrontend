import React, { useState } from "react";
import { DeleteRotationPeriod } from "src/api/ksaRotationApi/ksaRotationDeleteRequest";
import { AddKsaRotation } from "src/api/ksaRotationApi/ksaRotationPostRequest";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from "src/components/modal";
import { KsaRotationForm } from "../form/KsaRotationForm";

export const KsaRotationModal = ({
  addItemModal,
  setAddItemModal,
  setKsaRotation,
  ksatriyas,
}) => {
  const [arrayData, setArrayData] = useState([]);
  const [startDate, setStartDate] = useState("2019-10-10");
  const [startTime, setStartTime] = useState("00:00");
  const [endDate, setEndDate] = useState("2019-10-10");
  const [endTime, setEndTime] = useState("00:00");

  const [ksaPageNumber, setKsaPageNumber] = useState(0);
  const ksaPerPage = 12;
  const ksaPagesVisited = ksaPageNumber * ksaPerPage;

  const pageCount = Math.ceil(ksatriyas.length / ksaPerPage);
  const changePage = ({ selected }) => {
    setKsaPageNumber(selected);
  };
  return (
    <div>
      <Modal
        show={addItemModal}
        onClose={() => setAddItemModal(!addItemModal)}
        color="primary"
        size="lg"
      >
        <ModalHeader closeButton>Add KSA Rotation</ModalHeader>
        <ModalBody>
          <div className="col" align="left">
            <KsaRotationForm
              ksatriyas={ksatriyas}
              changePage={changePage}
              pageCount={pageCount}
              ksaPerPage={ksaPerPage}
              arrayData={arrayData}
              setArrayData={setArrayData}
              setEndDate={setEndDate}
              setEndTime={setEndTime}
              setStartDate={setStartDate}
              setStartTime={setStartTime}
              ksaPagesVisited={ksaPagesVisited}
            />
          </div>
        </ModalBody>
        <ModalFooter>
          <AddKsaRotation
            startDate={startDate}
            startTime={startTime}
            endDate={endDate}
            endTime={endTime}
            arrayData={arrayData}
            setKsaRotation={setKsaRotation}
          />
          <button
            className="btn btn-secondary"
            onClick={() => setAddItemModal(!addItemModal)}
          >
            Cancel
          </button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export const DeleteKsaRotationModal = ({
  setDeleteItemModal,
  deleteItemModal,
  id,
  setKsaRotation
}) => {
  return (
    <Modal
      show={deleteItemModal}
      onClose={() => setDeleteItemModal(!deleteItemModal)}
      color="danger"
      size="md"
    >
      <ModalHeader closeButton>
        <ModalTitle>
          <b>Delete this item?</b>
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="col" align="center">
          <h5>Are you sure?</h5>
        </div>
      </ModalBody>
      <ModalFooter>
        <div className="row">
          <div className="col" align="center">
            <DeleteRotationPeriod
              setKsaRotation={setKsaRotation}
              setDeleteItemModal={setDeleteItemModal}
              id={id}
            />
            &nbsp;
            <button
              className="btn btn-secondary btn-sm"
              onClick={() => setDeleteItemModal(!deleteItemModal)}
            >
              <b>Cancel</b>
            </button>
          </div>
        </div>
      </ModalFooter>
    </Modal>
  );
};
