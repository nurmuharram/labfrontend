import React, { useState } from "react";
import { DeleteRotationPeriod } from "src/api/ksaRotationApi/ksaRotationDeleteRequest";
import Collapse from "src/components/collapse/Collapse";
import DataTable from "src/components/table/DataTable";
import { toggleDetails } from "src/services/utils/toggleDetails";
import { UpdateKsaRotationForm } from "../form/KsaRotationForm";
import { DeleteKsaRotationModal } from "../modal/KsaRotationModal";

export const KsaRotationDataTable = ({ ksatriyas, setKsaRotation, ksaRotation }) => {
  
  const [details, setDetails] = useState([]);
  const [deleteItemModal, setDeleteItemModal] = useState(false);

  return (
    <div>
      <DataTable
        items={ksaRotation}
        fields={[
          { key: "ksatriya_rotation_id" },
          { key: "ksatriya_id" },
          { key: "start_date" },
          { key: "end_date" },
          {
            key: "show_details",
            label: "",
            _style: { width: "10%" },
            sorter: false,
            filter: false,
          },
        ]}
        itemsPerPage={10}
        pagination
        sorter
        sorterValue={{ column: "start_date", asc: false }}
        scopedSlots={{
          show_details: (item, index) => {
            return (
              <td className="py-2">
                <button
                  className="btn btn-outline-primary btn-sm"
                  color="primary"
                  variant="outline"
                  shape="round"
                  size="sm"
                  onClick={() => {
                    toggleDetails(index, { details, setDetails });
                  }}
                >
                  {details.includes(index) ? "Close" : "Edit Item"}
                </button>
              </td>
            );
          },
          details: (item, index) => {
            const id = item.ksatriya_rotation_id;
            return (
              <Collapse show={details.includes(index)}>
                <div className="card-body">
                  <div className="row">
                    <div className="col" align="left">
                      <h4>
                        <i>
                          Edit/Update (
                          <small>
                            KSA Rotation ID : {item.ksatriya_rotation_id}
                          </small>
                          )
                        </i>
                      </h4>
                    </div>
                    <div className="col-md-3 col-lg-5" align="left">
                      <button
                        className="btn btn-sm btn-outline-danger"
                        onClick={() => setDeleteItemModal(!deleteItemModal)}
                      >
                        Delete Item
                      </button>
                      <DeleteKsaRotationModal
                        setDeleteItemModal={setDeleteItemModal}
                        deleteItemModal={deleteItemModal}
                        id={id}
                        setKsaRotation={setKsaRotation}
                      />
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col">
                      <UpdateKsaRotationForm
                        ksatriyas={ksatriyas}
                        setKsaRotation={setKsaRotation}
                        id={id}
                      />
                    </div>
                  </div>
                </div>
              </Collapse>
            );
          },
          start_date: (item, index) => {
            const date = new Date(
              (item.start_date && item.start_date.split(" ").join("T")) +
                ".000Z"
            );
            return <td>{date.toString()}</td>;
          },
          end_date: (item, index) => {
            const date = new Date(
              (item.end_date && item.end_date.split(" ").join("T")) + ".000Z"
            );
            return <td>{date.toString()}</td>;
          },
        }}
      />
    </div>
  );
};
