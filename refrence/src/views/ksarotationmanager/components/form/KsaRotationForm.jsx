import React, { useState } from "react";
import { Form } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import { UpdateKsaRotation } from "src/api/ksaRotationApi/ksaRotationPutRequest";
import {
  KsatriyaList,
  showSelectedKsatriyas,
} from "src/services/ksaRotation.services/ksaRotation.function";

export const KsaRotationForm = ({
  ksatriyas,
  changePage,
  pageCount,
  ksaPerPage,
  arrayData,
  setArrayData,
  setEndDate,
  setEndTime,
  setStartDate,
  setStartTime,
  ksaPagesVisited,
}) => {
  return (
    <div>
      <Form>
        <Form.Group className="mb-3">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Start date</b>
              </Form.Label>
            </div>
            :
            <div className="col-xs-12 col-md-4">
              <Form.Control
                type="date"
                onKeyDown={(e) => e.preventDefault()}
                onChange={(e) => setStartDate(e.target.value)}
              />
            </div>
            <div className="col-xs-12 col-md-4">
              <Form.Control
                type="time"
                onChange={(e) => setStartTime(e.target.value)}
              />
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>End date</b>
              </Form.Label>
            </div>
            :
            <div className="col-xs-12 col-md-4">
              <Form.Control
                type="date"
                onKeyDown={(e) => e.preventDefault()}
                onChange={(e) => setEndDate(e.target.value)}
              />
            </div>
            <div className="col-xs-12 col-md-4">
              <Form.Control
                type="time"
                onChange={(e) => setEndTime(e.target.value)}
              />
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3">
          <div className="row">
            <div className="col-md-4 col-lg-3">
              <Form.Label htmlFor="input">
                <b>Selected Ksatriyas</b>
              </Form.Label>
            </div>
            :
            <div className="col">
              {showSelectedKsatriyas({ arrayData, setArrayData })}
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3">
          <div className="col-xs-12 col-md-11" align="center">
            <div className="card">
              <div className="card-body">
                <br />
                <div className="row">
                  <KsatriyaList
                    ksatriyas={ksatriyas}
                    ksaPagesVisited={ksaPagesVisited}
                    arrayData={arrayData}
                    setArrayData={setArrayData}
                    ksaPerPage={ksaPerPage}
                  />
                </div>
                <br />
                <div className="col" align="center">
                  <ReactPaginate
                    previousLabel={"Prev"}
                    nextLabel={"Next"}
                    pageCount={pageCount}
                    onPageChange={changePage}
                    containerClassName={"paginationBttns"}
                    previousLinkClassName={"PreviousBttn"}
                    nextLinkClassName={"nextBttn"}
                    disabledClassName={"paginationDisabled"}
                    activeClassName={"paginationActive"}
                  />
                </div>
              </div>
            </div>
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};

export const UpdateKsaRotationForm = ({ksatriyas, setKsaRotation}) => {

  const [ksatriyaId, setKsatriyaId] = useState(1);
  const [newStartDate, setNewstartdate] = useState("2019-10-10");
  const [newStartTime, setNewStartTime] = useState("00:00");

  const [newEndDate, setNewEndDate] = useState("2019-10-10");
  const [newEndTime, setNewEndTime] = useState("00:00");
  const ksatriyaList =
    ksatriyas &&
    ksatriyas.map((ksa, index) => {
      return (
        <option key={ksa.ksatriya_id} value={ksa.ksatriya_id}>
          {ksa.ksatriya_id} - {ksa.ksatriya_name}
        </option>
      );
    });
  return (
    <div>
      <Form>
        <Form.Group className="mb-3">
          <div className="row">
          <div className="col-lg-2 col-md-3">
            <Form.Label htmlFor="input">
              <b>Select Ksatriya</b>
            </Form.Label>
          </div>
          :
          <div className="col-lg-4 col-md-6">
            <select className="custom-select" aria-label=".form-select-lg example" onChange={(e) => setKsatriyaId(e.target.value)}>
              {ksatriyaList}
            </select>
          </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3">
          <div className="row">
          <div className="col-lg-2 col-md-3">
            <Form.Label htmlFor="input">
              <b>Start date</b>
            </Form.Label>
          </div>
          :
          <div className="col-lg-2 col-md-3">
            <Form.Control
              type="date"
              onKeyDown={(e) => e.preventDefault()}
              onChange={(e) => setNewstartdate(e.target.value)}
            />
          </div>
          <div className="col-lg-2 col-md-3">
            <Form.Control
              type="time"
              onChange={(e) => setNewStartTime(e.target.value)}
            />
          </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3">
          <div className="row">
          <div className="col-lg-2 col-md-3">
            <Form.Label htmlFor="input">
              <b>End date</b>
            </Form.Label>
          </div>
          :
          <div className="col-lg-2 col-md-3">
            <Form.Control
              type="date"
              onKeyDown={(e) => e.preventDefault()}
              onChange={(e) => setNewEndDate(e.target.value)}
            />
          </div>
          <div className="col-lg-2 col-md-3">
            <Form.Control
              type="time"
              onChange={(e) => setNewEndTime(e.target.value)}
            />
          </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3">
          <div className="row">
          <div className="col-md-9 col-lg-6" align="right">
            <UpdateKsaRotation
              setKsaRotation={setKsaRotation}
              newStartDate={newStartDate}
              newStartTime={newStartTime}
              newEndDate={newEndDate}
              newEndTime={newEndTime}
              ksatriyaId={ksatriyaId}
            />
          </div>
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};
