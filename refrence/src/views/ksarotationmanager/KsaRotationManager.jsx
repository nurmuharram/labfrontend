import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CContainer,
  CDataTable,
  CFormGroup,
  CInput,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CRow,
  CCollapse,
  CModalTitle,
  CSelect,
} from "@coreui/react";

import { useSelector, useDispatch } from "react-redux";
import { getKsatriyas } from "src/redux/action/mailAction/getKsatriyasAction";
import { KsaRotationModal } from "./components/modal/KsaRotationModal";
import { KsaRotationDataTable } from "./components/dataTable/KsaRotationDataTable";
import { GetKsaRotationList } from "src/api/ksaRotationApi/ksaRotationGetRequest";

function KsaRotationManager() {
  const dispatch = useDispatch();
  const [addItemModal, setAddItemModal] = useState(false);

  const [ksaRotation, setKsaRotation] = useState([]);

  GetKsaRotationList({ setKsaRotation });

  /* GET Ksatriyas - Get Request */
  const ksatriyas = useSelector((state) => state.ksatriyas.ksatriyas);
  useEffect(() => {
    dispatch(getKsatriyas());
  }, []);

  return (
    <div>
      <CContainer fluid>
        <CCard accentColor="primary">
          <CCardHeader>
            <CCol align="center">
              <h5>
                <b>KSA Rotation Manager</b>
              </h5>
            </CCol>
          </CCardHeader>
          <CCardBody>
            <CCol align="right">
              <CButton color="primary" onClick={() => setAddItemModal(!addItemModal)}>
                + Add KSA Rotation
              </CButton>
              <KsaRotationModal
                addItemModal={addItemModal}
                setAddItemModal={setAddItemModal}
                setKsaRotation={setKsaRotation}
                ksatriyas={ksatriyas}
              />
            </CCol>
            <br />
            <KsaRotationDataTable
              ksatriyas={ksatriyas}
              ksaRotation={ksaRotation}
              setKsaRotation={setKsaRotation}
            />
          </CCardBody>
        </CCard>
      </CContainer>
    </div>
  );
}

export default KsaRotationManager;
