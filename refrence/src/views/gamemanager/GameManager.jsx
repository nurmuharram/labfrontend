import React from 'react'
import GameManagerTabs from 'src/views/gamemanager/components/tabs/GameManagerTabs'

function GameManager() {
    return (
        <div>
            <div>
      <div className="col" align="center">
        <div className="card" style={{ background: "#9bf54c" }}>
          <h4>
            <b>Game Manager</b>
          </h4>
        </div>
      </div>
      <br />
      <div className="container-fluid">
        <div className="card card-accent-primary">
          <div className="card-header">
          </div>
          <div className="card-body">
            <GameManagerTabs/>
          </div>
        </div>
      </div>
    </div>
        </div>
    )
}

export default GameManager
