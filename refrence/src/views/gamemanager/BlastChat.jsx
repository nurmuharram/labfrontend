import React from "react";
import { useState } from "react";
import axios from "axios";

function BlastChat() {
  const [message, setMessage] = useState("");
  const postmessage = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();
    data.append("message", message);

    const config = {
      method: "POST",
      url: `/api/chat/broadcast`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Message Sent!");
      })
      .catch((error) => {
        console.log(error);
        alert("Failed to Send!");
      });
  };
  return (
    <div className="col" align="center">
      <div align="center">
        <b>
          <u>Blast Chat</u>
        </b>
      </div>
      <br />
      <div align="center" className="col-md-10 col-lg-6">
        <input
          className="form-control"
          type="text"
          placeholder="Write a Blast Chat Here..."
          onChange={(e) => setMessage(e.target.value)}
        />
      </div>
      <br />
      <div align="center">
        <button className="btn btn-primary btn-md" onClick={postmessage}>
          Blast Chat
        </button>
      </div>
    </div>
  );
}

export default BlastChat;
