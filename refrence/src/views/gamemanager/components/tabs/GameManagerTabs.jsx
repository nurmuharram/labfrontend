import React, { useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import DailyRewardManagement from "src/views/dailyrewardmanagement/DailyRewardManagement";
import BlastChat from "../../BlastChat";
import FortuneOfTheNorth from "src/views/fortuneofthenorth/FortuneoftheNorth";
import SeasonEnd from "src/views/seasonend/SeasonEnd";
import MaintenanceManager from "src/views/maintenancemanagement/MTManagement";
import axios from "axios";
import { GetCurrentInfo } from "src/api/currentinfo/currentInfoandPermissionGetRequests";
import { GetCurrentPermission } from "src/api/currentinfo/currentInfoandPermissionGetRequests";
import EventEnergyMain from "src/views/event/EventMainPage";
import ForceUpdateMain from "src/views/forceupdate/ForceUpdateMain";
import KsaRotationManager from "src/views/ksarotationmanager/KsaRotationManager";
function GameManagerTabs() {
  const [key, setKey] = useState("BlastChat");
  const [currentInfo, setCurrentInfo] = useState([]);
  const [permission, setPermission] = useState([]);

  GetCurrentInfo({ setCurrentInfo });
  GetCurrentPermission({ setPermission, currentInfo });

  const _permission =
    permission &&
    permission
      .map((item) => item.permission_id)
      .filter((value, index, self) => self.indexOf(value) === index);

  return (
    <div>
      <Tabs
        id="controlled-tab"
        activeKey={key}
        onSelect={(k) => setKey(k)}
        className="mb-3"
        variant="pills"
        color="red"
      >
        <Tab eventKey="BlastChat" title="Blast Chat (BroadCast)">
          <BlastChat />
        </Tab>
        <Tab eventKey="ForceUpdate" title="Force Update">
          <ForceUpdateMain />
        </Tab>
        {_permission && _permission.includes(16) === true ? (
          <Tab eventKey="maintenancemanagement" title="Maintenance Manager">
            <MaintenanceManager />
          </Tab>
        ) : (
          <></>
        )}
        {_permission && _permission.includes(17) === true ? (
          <Tab eventKey="dailyrewards" title="Daily Reward">
            <DailyRewardManagement />
          </Tab>
        ) : (
          <></>
        )}
        {_permission && _permission.includes(6) === true ? (
          <Tab eventKey="ksarotationmanager" title="KSA Rotation">
            <KsaRotationManager/>
          </Tab>
        ) : (
          <></>
        )}
        {_permission && _permission.includes(2) === true ? (
          <Tab eventKey="fortuneofthenorth" title="Fortune Of The North">
            <FortuneOfTheNorth />
          </Tab>
        ) : (
          <></>
        )}

        {_permission && _permission.includes(12) === true ? (
          <Tab eventKey="seasonend" title="Season End">
            <SeasonEnd />
          </Tab>
        ) : (
          <></>
        )}
        {/* {_permission && _permission.includes(15) === true ? (
          <Tab eventKey="eventmanager" title="Event Manager">
           <EventEnergyMain/>
          </Tab>
        ) : (
          <></>
        )} */}
      </Tabs>
    </div>
  );
}

export default GameManagerTabs;
