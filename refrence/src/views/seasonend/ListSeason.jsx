import React, { useState } from "react";
import { GetSeasonEndList } from "src/api/seasonend/SeasonEndGetRequest";
import { ListSeasonDatatable } from "./components/datatable/ListSeasonDatatable";
import { ListSeasonModals } from "./components/modals/ListSeasonModals";

function ListSeason() {
  const [addItemModal, setAddItemModal] = useState(false);
  const [seasonEndList, setSeasonEndList] = useState([]);

  const [startdate, setStartdate] = useState("2019-10-10");
  const [starttime, setStarttime] = useState("00:00");
  const [enddate, setEnddate] = useState("2019-10-10");
  const [endtime, setEndtime] = useState("00:00");

  const [newstartdate, setNewstartdate] = useState("2019-10-10");
  const [newstarttime, setNewstarttime] = useState("00:00");
  const [newenddate, setNewenddate] = useState("2019-10-10");
  const [newendtime, setNewendtime] = useState("00:00");

  GetSeasonEndList({ setSeasonEndList });

  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-header">
              <button
                type="button"
                className="btn btn-primary"
                onClick={() => setAddItemModal(!addItemModal)}
              >
                + Add Season Period
              </button>
              <ListSeasonModals
                addItemModal={addItemModal}
                setAddItemModal={setAddItemModal}
                setStartdate={setStartdate}
                setStarttime={setStarttime}
                setEnddate={setEnddate}
                setEndtime={setEndtime}
                startdate={startdate}
                enddate={enddate}
                starttime={starttime}
                endtime={endtime}
                setSeasonEndList={setSeasonEndList}
              />
            </div>
            <div className="card-body">
              <ListSeasonDatatable
                seasonEndList={seasonEndList}
                setNewstartdate={setNewstartdate}
                setNewstarttime={setNewstarttime}
                setNewenddate={setNewenddate}
                setNewendtime={setNewendtime}
                newstartdate={newstartdate}
                newstarttime={newstarttime}
                newenddate={newenddate}
                newendtime={newendtime}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ListSeason;
