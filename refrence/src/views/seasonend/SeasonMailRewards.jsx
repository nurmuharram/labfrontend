import React, { useState } from "react";
import { GetSeasonEndList } from "src/api/seasonend/SeasonEndGetRequest";
import { GetMails } from "src/api/seasonend/SeasonEndGetRequest";
import { GetSeasonMailRewards } from "src/api/seasonend/SeasonEndGetRequest";
import { SeasonMailDatatable } from "./components/datatable/SeasonMailRewardsDatatable";
import { SeasonMailRewardsModal } from "./components/modals/SeasonMailRewardModals";

function SeasonMailRewards() {
  const [addItemModal, setAddItemModal] = useState(false);
  const [seasonMailReward, setSeasonMailReward] = useState([]);
  const [seasonEndList, setSeasonEndList] = useState([]);
  const [mails, setMails] = useState([]);

  const [seasonid, setSeasonid] = useState(0);
  const [rank, setRank] = useState(1);
  const [mailtemplate, setMailtemplate] = useState(1);

  const [maildata, setMaildata] = useState([]);

  const [itemtype, setItemtype] = useState();
  const [itemid, setItemid] = useState(1);
  const [amount, setAmount] = useState();
  const [newseasonid, setNewseasonid] = useState(0)

  GetSeasonMailRewards({ setSeasonMailReward });
  GetSeasonEndList({ setSeasonEndList });
  GetMails({ setMails });

  return (
    <div>
      <div>
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-header">
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => setAddItemModal(!addItemModal)}
                >
                  + Add Season Mail Reward
                </button>
                <SeasonMailRewardsModal
                  addItemModal={addItemModal}
                  setAddItemModal={setAddItemModal}
                  seasonEndList={seasonEndList}
                  setSeasonid={setSeasonid}
                  setRank={setRank}
                  setMailtemplate={setMailtemplate}
                  mails={mails}
                  setMaildata={setMaildata}
                  rank={rank}
                  mailtemplate={mailtemplate}
                  maildata={maildata}
                  seasonid={seasonid}
                  setSeasonMailReward={setSeasonMailReward}
                />
              </div>
              <div className="card-body">
                <SeasonMailDatatable
                  seasonMailReward={seasonMailReward}
                  setSeasonMailReward={setSeasonMailReward}
                  setItemtype={setItemtype}
                  setItemid={setItemid}
                  setAmount={setAmount}
                  setNewseasonid={setNewseasonid}
                  itemtype={itemtype}
                  itemid={itemid}
                  amount={amount}
                  newseasonid={newseasonid}
                  seasonEndList={seasonEndList}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SeasonMailRewards;
