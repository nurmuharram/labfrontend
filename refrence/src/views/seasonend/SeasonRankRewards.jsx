import React, { useState } from "react";
import { GetSeasonEndList } from "src/api/seasonend/SeasonEndGetRequest";
import { GetSeasonRankRewards } from "src/api/seasonend/SeasonEndGetRequest";
import { SeasonRankRewardDatatable } from "./components/datatable/SeasonRankRewardDatatable";
import { SeasonRankRewardsModal } from "./components/modals/SeasonRankModals";

function SeasonRankRewards() {
  const [addItemModal, setAddItemModal] = useState(false);
  const [seasonRankReward, setSeasonRankReward] = useState([]);
  const [seasonEndList, setSeasonEndList] = useState([]);

  const [itemtype, setItemtype] = useState();
  const [itemid, setItemid] = useState(1);
  const [amount, setAmount] = useState();
  const [seasonid, setSeasonid] = useState(0)
  const [rank, setRank] = useState(1)

  const [newitemtype, setNewitemtype] = useState();
  const [newitemid, setNewitemid] = useState(1);
  const [newamount, setNewamount] = useState();
  const [newseasonid, setNewseasonid] = useState(0)
  const [newrank, setNewrank] = useState()

  GetSeasonRankRewards({ setSeasonRankReward });
  GetSeasonEndList({ setSeasonEndList });

  return (
    <div>
      <div>
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-header">
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => setAddItemModal(!addItemModal)}
                >
                  + Add Season Rank Reward
                </button>
                <SeasonRankRewardsModal
                  addItemModal={addItemModal}
                  setAddItemModal={setAddItemModal}
                  seasonEndList={seasonEndList}
                  setItemtype={setItemtype}
                  setItemid={setItemid}
                  setAmount={setAmount}
                  itemtype={itemtype}
                  itemid={itemid}
                  amount={amount}
                  setSeasonRankReward={setSeasonRankReward}
                  rank={rank}
                  setRank={setRank}
                  seasonid={seasonid}
                  setSeasonid={setSeasonid}
                />
              </div>
              <div className="card-body">
                <SeasonRankRewardDatatable
                  seasonRankReward={seasonRankReward}
                  newitemtype={newitemtype}
                  setNewitemtype={setNewitemtype}
                  newitemid={newitemid}
                  setNewitemid={setNewitemid}
                  newamount={newamount}
                  setNewamount={setNewamount}
                  newseasonid={newseasonid}
                  setNewseasonid={setNewseasonid}
                  newrank={newrank}
                  setNewrank={setNewrank}
                  seasonEndList={seasonEndList}
                  setSeasonRankReward={setSeasonRankReward}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SeasonRankRewards;
