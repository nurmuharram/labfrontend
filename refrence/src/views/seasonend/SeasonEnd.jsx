import React from "react";
import SeasonEndTabs from "./components/tabs/SeasonEndTabs";

function SeasonEnd() {
  return (
    <div>
      <div className="container-fluid">
        <div className="card card-accent-primary">
          <div className="card-header">
            <div className="col" align="center">
              <h5>
                <b>Season End Settings</b>
              </h5>
            </div>
          </div>
          <div className="card-body">
            <SeasonEndTabs />
          </div>
        </div>
      </div>
    </div>
  );
}

export default SeasonEnd;
