import { useState } from "react";
import { Collapse } from "react-bootstrap";
import DataTable from "src/components/table/DataTable";
import { toggleDetails } from "src/services/seasonEnd.services/ListSeasonPeriod.function";
import { UpdateSeasonPeriodForms } from "../forms/ListSeasonForms";
import { DeleteSeasonPeriodModal } from "../modals/ListSeasonModals";

export const ListSeasonDatatable = ({
  seasonEndList,
  setSeasonEndList,
  setNewstartdate,
  setNewstarttime,
  setNewenddate,
  setNewendtime,
  newstartdate,
  newstarttime,
  newenddate,
  newendtime,
}) => {
  const [details, setDetails] = useState([]);
  const [deleteItemModal, setDeleteItemModal] = useState(false);
  const fields = [
    { key: "season_id", _style: { width: "10%" } },
    { key: "start_date" },
    { key: "end_date" },
    {
      key: "show_details",
      label: "",
      _style: { width: "14%" },
      sorter: false,
      filter: false,
    },
  ];

  return (
    <DataTable
      items={seasonEndList}
      fields={fields}
      sorter
      scopedSlots={{
        start_date: (item, index) => {
          const date = new Date(
            (item.start_date && item.start_date.split(" ").join("T")) + ".000Z"
          );
          return <td>{date.toString()}</td>;
        },
        end_date: (item, index) => {
          const date = new Date(
            (item.end_date && item.end_date.split(" ").join("T")) + ".000Z"
          );
          return <td>{date.toString()}</td>;
        },
        show_details: (item, index) => {
          return (
            <td className="py-2">
              <button
                className="btn btn-outline-primary btn-sm"
                type="button"
                onClick={() => {
                  toggleDetails(index, { setDetails, details });
                }}
              >
                {details.includes(index) ? "Hide" : "Edit/Update"}
              </button>
            </td>
          );
        },
        details: (item, index) => {
          const id = item.season_id;
          const end_date = item.end_date;
          const start_date = item.start_date;
          return (
            <Collapse in={details.includes(index)}>
              <div className="card-body">
                <div className="row">
                  <div className="col" align="left">
                    <h4>
                      <i>
                        Edit/Update Item (<small>ID : {id}</small>)
                      </i>
                    </h4>
                  </div>
                  <div className="col-lg-5 col-md-4" align="left">
                    <button
                      className="btn btn-outline-danger"
                      type="button"
                      onClick={() => setDeleteItemModal(!deleteItemModal)}
                    >
                      Delete Item
                    </button>
                    <DeleteSeasonPeriodModal
                      id={id}
                      setDeleteItemModal={setDeleteItemModal}
                      setSeasonEndList={setSeasonEndList}
                      deleteItemModal={deleteItemModal}
                    />
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col" align="left">
                    <UpdateSeasonPeriodForms
                      setNewstartdate={setNewstartdate}
                      setNewstarttime={setNewstarttime}
                      setNewenddate={setNewenddate}
                      setNewendtime={setNewendtime}
                      newstartdate={newstartdate}
                      newstarttime={newstarttime}
                      newenddate={newenddate}
                      newendtime={newendtime}
                      id={id}
                      end_date={end_date}
                      start_date={start_date}
                      setSeasonEndList={setSeasonEndList}
                    />
                  </div>
                </div>
              </div>
            </Collapse>
          );
        },
      }}
    />
  );
};
