import { useState } from "react";
import { Collapse } from "react-bootstrap";
import DataTable from "src/components/table/DataTable";
import { toggleDetails } from "src/services/seasonEnd.services/SeasonReward.function";
import { UpdateSeasonRewardForm } from "../forms/SeasonRewardForms";
import { DeleteSeasonRewardItemModal } from "../modals/SeasonRewardModals";

export const SeasonRewardDataTable = ({
  seasonReward,
  newitemtype,
  setNewitemtype,
  newitemid,
  setNewitemid,
  newamount,
  setNewamount,
  newseasonid,
  setNeweasonid,
  seasonEndList,
  setSeasonEndList,
  setSeasonReward
}) => {
  const [details, setDetails] = useState([]);
  const [deleteItemModal, setDeleteItemModal] = useState(false);
  const fields = [
    { key: "season_reward_id", _style: { width: "5%" } },
    { key: "season_id" },
    { key: "start_date" },
    { key: "end_date" },
    { key: "item_type_name" },
    { key: "item_name" },
    { key: "amount" },
    {
      key: "show_details",
      label: "",
      _style: { width: "14%" },
      sorter: false,
      filter: false,
    },
  ];
  return (
    <DataTable
      items={seasonReward}
      fields={fields}
      pagination
      itemsPerPage={5}
      itemsPerPageSelect
      scopedSlots={{
        start_date: (item, index) => {
          const date = new Date(
            (item.start_date && item.start_date.split(" ").join("T")) + ".000Z"
          );
          return <td>{date.toString()}</td>;
        },
        end_date: (item, index) => {
          const date = new Date(
            (item.end_date && item.end_date.split(" ").join("T")) + ".000Z"
          );
          return <td>{date.toString()}</td>;
        },
        show_details: (item, index) => {
          return (
            <td className="py-2">
              <button
                className="btn btn-outline-primary btn-sm"
                type="button"
                onClick={() => {
                  toggleDetails(index, { setDetails, details });
                }}
              >
                {details.includes(index) ? "Hide" : "Edit/Update"}
              </button>
            </td>
          );
        },
        details: (item, index) => {
          const id = item.season_reward_id;

          return (
            <Collapse in={details.includes(index)}>
              <div className="card-body">
                <div className="row">
                  <div className="col" align="left">
                    <h4>
                      <i>
                        Edit/Update Item (<small>ID : {id}</small>)
                      </i>
                    </h4>
                  </div>
                  <div className="col-md-5" align="left">
                    <button
                      className="btn btn-outline-danger"
                      type="button"
                      onClick={() => setDeleteItemModal(!deleteItemModal)}
                    >
                      Delete Item
                    </button>
                    <DeleteSeasonRewardItemModal
                      id={id}
                      setDeleteItemModal={setDeleteItemModal}
                      deleteItemModal={deleteItemModal}
                      setSeasonReward={setSeasonReward}
                    />
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col" align="left">
                    <UpdateSeasonRewardForm
                      newitemtype={newitemtype}
                      setNewitemtype={setNewitemtype}
                      newitemid={newitemid}
                      setNewitemid={setNewitemid}
                      newamount={newamount}
                      setNewamount={setNewamount}
                      newseasonid={newseasonid}
                      setNeweasonid={setNeweasonid}
                      seasonEndList={seasonEndList}
                      setSeasonEndList={setSeasonEndList}
                      id={id}
                      setSeasonReward={setSeasonReward}
                    />
                  </div>
                </div>
              </div>
            </Collapse>
          );
        },
      }}
    />
  );
};
