import { useState } from "react";
import { Collapse } from "react-bootstrap";
import DataTable from "src/components/table/DataTable";
import { toggleDetails } from "src/services/seasonEnd.services/SeasonMail.Function";
import { UpdateSeasonMail } from "../forms/SeasonMailRewardForms";
import { DeleteSeasonMailModal } from "../modals/SeasonMailRewardModals";

export const SeasonMailDatatable = ({
  seasonMailReward,
  setSeasonMailReward,
  setItemtype,
  setItemid,
  setAmount,
  setNewseasonid,
  itemtype,
  itemid,
  amount,
  newseasonid,
  seasonEndList,
}) => {
  const [details, setDetails] = useState([]);
  const [deleteItemModal, setDeleteItemModal] = useState(false);
  const fields = [
    { key: "mail_id", _style: { width: "7%" } },
    { key: "season_id" },
    { key: "start_date" },
    { key: "end_date" },
    { key: "rank_desc" },
    { key: "mail_template" },
    { key: "mail_subject" },
    {
      key: "show_details",
      label: "",
      _style: { width: "14%" },
      sorter: false,
      filter: false,
    },
  ];
  return (
    <DataTable
      items={seasonMailReward}
      fields={fields}
      pagination
      itemsPerPage={5}
      itemsPerPageSelect
      scopedSlots={{
        start_date: (item, index) => {
          const date = new Date(
            (item.start_date && item.start_date.split(" ").join("T")) + ".000Z"
          );
          return <td>{date.toString()}</td>;
        },
        end_date: (item, index) => {
          const date = new Date(
            (item.end_date && item.end_date.split(" ").join("T")) + ".000Z"
          );
          return <td>{date.toString()}</td>;
        },
        show_details: (item, index) => {
          return (
            <td className="py-2">
              <button
                className="btn btn-outline-primary btn-sm"
                type="button"
                onClick={() => {
                  toggleDetails(index, { setDetails, details });
                }}
              >
                {details.includes(index) ? "Hide" : "Edit/Update"}
              </button>
            </td>
          );
        },
        details: (item, index) => {
          const id = item.mail_id;

          return (
            <Collapse in={details.includes(index)}>
              <div className="card-body">
                <div className="row">
                  <div className="col" align="left">
                    <h4>
                      <i>
                        Edit/Update Item (<small>ID : {id}</small>)
                      </i>
                    </h4>
                  </div>
                  <div className="col-md-5" align="left">
                    <button
                      className="btn btn-outline-danger"
                      type="button"
                      onClick={() => setDeleteItemModal(!deleteItemModal)}
                    >
                      Delete Item
                    </button>
                    <DeleteSeasonMailModal
                      id={id}
                      setSeasonMailReward={setSeasonMailReward}
                      setDeleteItemModal={setDeleteItemModal}
                      deleteItemModal={deleteItemModal}
                    />
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col" align="left">
                    <UpdateSeasonMail
                      setSeasonMailReward={setSeasonMailReward}
                      setItemtype={setItemtype}
                      setItemid={setItemid}
                      setAmount={setAmount}
                      setNewseasonid={setNewseasonid}
                      itemtype={itemtype}
                      itemid={itemid}
                      amount={amount}
                      newseasonid={newseasonid}
                      seasonEndList={seasonEndList}
                      id={id}
                    />
                  </div>
                </div>
              </div>
            </Collapse>
          );
        },
      }}
    />
  );
};
