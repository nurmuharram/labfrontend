import { useState } from "react";
import { Collapse } from "react-bootstrap";
import DataTable from "src/components/table/DataTable";
import { toggleDetails } from "src/services/seasonEnd.services/SeasonRankRewards.function";
import { UpdateSeasonRankRewardForm } from "../forms/SeasonRankRewardForms";
import { DeleteSeasonRankRewardItemModal } from "../modals/SeasonRankModals";

export const SeasonRankRewardDatatable = ({
  seasonRankReward,
  newitemtype,
  setNewitemtype,
  newitemid,
  setNewitemid,
  newamount,
  setNewamount,
  newseasonid,
  setNewseasonid,
  seasonEndList,
  setSeasonEndList,
  id,
  newrank,
  setNewrank,
  setSeasonRankReward,
}) => {
  const [details, setDetails] = useState([]);
  const [deleteItemModal, setDeleteItemModal] = useState(false);
  const fields = [
    { key: "season_rank_reward_id", _style: { width: "10%" } },
    { key: "season_id" },
    { key: "start_date" },
    { key: "end_date" },
    { key: "rank_desc" },
    { key: "item_type_name" },
    { key: "item_name" },
    { key: "amount" },
    {
      key: "show_details",
      label: "",
      _style: { width: "14%" },
      sorter: false,
      filter: false,
    },
  ];
  return (
    <DataTable
      items={seasonRankReward}
      fields={fields}
      pagination
      itemsPerPage={5}
      itemsPerPageSelect
      scopedSlots={{
        start_date: (item, index) => {
          const date = new Date(
            (item.start_date && item.start_date.split(" ").join("T")) + ".000Z"
          );
          return <td>{date.toString()}</td>;
        },
        end_date: (item, index) => {
          const date = new Date(
            (item.end_date && item.end_date.split(" ").join("T")) + ".000Z"
          );
          return <td>{date.toString()}</td>;
        },
        show_details: (item, index) => {
          return (
            <td className="py-2">
              <button
                className="btn btn-outline-primary btn-sm"
                type="button"
                onClick={() => {
                  toggleDetails(index, { setDetails, details });
                }}
              >
                {details.includes(index) ? "Hide" : "Edit/Update"}
              </button>
            </td>
          );
        },
        details: (item, index) => {
          const id = item.season_rank_reward_id;

          return (
            <Collapse in={details.includes(index)}>
              <div className="card-body">
                <div className="row">
                  <div className="col" align="left">
                    <h4>
                      <i>
                        Edit/Update Item (<small>ID : {id}</small>)
                      </i>
                    </h4>
                  </div>
                  <div className="col-md-5" align="left">
                    <button
                      className="btn btn-outline-danger"
                      type="button"
                      onClick={() => setDeleteItemModal(!deleteItemModal)}
                    >
                      Delete Item
                    </button>
                    <DeleteSeasonRankRewardItemModal
                      id={id}
                      setSeasonRankReward={setSeasonRankReward}
                      setDeleteItemModal={setDeleteItemModal}
                      deleteItemModal={deleteItemModal}
                    />
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col" align="left">
                    <UpdateSeasonRankRewardForm
                      newitemtype={newitemtype}
                      setNewitemtype={setNewitemtype}
                      newitemid={newitemid}
                      setNewitemid={setNewitemid}
                      newamount={newamount}
                      setNewamount={setNewamount}
                      newseasonid={newseasonid}
                      setNewseasonid={setNewseasonid}
                      seasonEndList={seasonEndList}
                      setSeasonEndList={setSeasonEndList}
                      id={id}
                      newrank={newrank}
                      setNewrank={setNewrank}
                    />
                  </div>
                </div>
              </div>
            </Collapse>
          );
        },
      }}
    />
  );
};
