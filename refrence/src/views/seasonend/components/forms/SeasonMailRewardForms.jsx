import { Form } from "react-bootstrap";
import { UpdateMail } from "src/api/seasonend/SeasonEndPutRequest";
import { Maildata } from "src/services/seasonEnd.services/SeasonMail.Function";
import { SeasonRewardFunction } from "src/services/seasonEnd.services/SeasonReward.function";

export const AddSeasonMailForm = ({
  seasonEndList,
  setSeasonid,
  setRank,
  setMailtemplate,
  mails,
  setMaildata,
  rank,
  mailtemplate,
  maildata,
}) => {
  const dropdownSeasonid = seasonEndList.map((item, index) => {
    return (
      <option value={item.season_id}>
        {item.season_id} - ({item.start_date})
      </option>
    );
  });

  const dropdownMailid = mails.map((item, index) => {
    return (
      <option value={item.template_id}>
        {item.template_id} - ({item.subject})
      </option>
    );
  });

  return (
    <Form aria-required>
      <Form.Group className="mb-3" controlId="formSeasonID">
        <div className="row">
          <div className="col-md-4">
            <Form.Label>
              <b>Season ID</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-7">
            <Form.Select
              className="col-md-10"
              aria-label="Select"
              onChange={(e) => setSeasonid(e.target.value)}
            >
              {dropdownSeasonid}
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <hr />
      <Form.Group className="mb-3" controlId="formSeasonRank">
        <div className="row">
          <div className="col-md-4">
            <Form.Label>
              <b>Rank</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-7">
            <Form.Select
              className="col-md-10"
              aria-label="Select"
              onChange={(e) => setRank(e.target.value)}
            >
              <option value={1}>Ardha-rathi</option>
              <option value={2}>Rathi</option>
              <option value={3}>Ekarathi</option>
              <option value={4}>Atirathi</option>
              <option value={5}>Maharathi</option>
              <option value={6}>Atimaharati</option>
              <option value={7}>Mahamaharathi</option>
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonMailID">
        <div className="row">
          <div className="col-md-4">
            <Form.Label>
              <b>Mail Template</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-7">
            <Form.Select
              className="col-md-10"
              aria-label="Select"
              onChange={(e) => setMailtemplate(e.target.value)}
            >
              {dropdownMailid}
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="buttonAddRankMail">
        <div className="row">
          <div className="col-md-10" align="right">
            <button
              type="button"
              className="btn btn-info"
              onClick={() =>
                setMaildata([
                  ...maildata,
                  {
                    rank: parseInt(rank),
                    mail_template: parseInt(mailtemplate),
                  },
                ])
              }
            >
              + Add
            </button>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formRankMail">
        <div className="row">
          <div className="col-md-4">
            <Form.Label>
              <b>Rank/Mail Template ID</b>
            </Form.Label>
          </div>
          :<div className="col-md-7">{Maildata({ maildata, setMaildata })}</div>
        </div>
      </Form.Group>
    </Form>
  );
};

export const UpdateSeasonMail = ({
  setItemtype,
  setAmount,
  setItemid,
  amount,
  itemtype,
  itemid,
  seasonEndList,
  setSeasonid,
  setNewseasonid,
  id,
  newseasonid,
  setSeasonMailReward
}) => {
  const dropdownSeasonid = seasonEndList.map((item, index) => {
    return (
      <option value={item.season_id}>
        {item.season_id} - ({item.start_date})
      </option>
    );
  });
  return (
    <Form>
      <Form.Group className="mb-3" controlId="formSeasonID">
        <div className="row">
          <div className="col-md-2">
            <Form.Label>
              <b>Season ID</b>
            </Form.Label>
          </div>
          :
          <div className="col-lg-7 col-md-9">
            <Form.Select
              aria-label="Select"
              onChange={(e) => setNewseasonid(e.target.value)}
            >
              {dropdownSeasonid}
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonItemType">
        <div className="row">
          <div className="col-md-2">
            <Form.Label>
              <b>Item Type</b>
            </Form.Label>
          </div>
          :
          <div className="col-lg-7 col-md-9">
            <Form.Select
              className="col-md-5"
              aria-label="Select"
              onChange={(e) => setItemtype(e.target.value)}
            >
              <option value={""}>Choose Item Type</option>
              <option value={1}>Currency</option>
              <option value={6}>Box</option>
              <option value={5}>Items</option>
              <option value={2}>Ksatriya</option>
              <option value={3}>KSA Skins</option>
              <option value={4}>Rune</option>
              <option value={11}>Frame</option>
              <option value={12}>Avatar</option>
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonItemID">
        <div className="row">
          <div className="col-md-2">
            <Form.Label>
              <b>Item ID</b>
            </Form.Label>
          </div>
          :
          <div className="col-lg-7 col-md-9">
            <Form.Select
              className="col-md-5"
              aria-label="Select"
              onChange={(e) => setItemid(e.target.value)}
            >
              {SeasonRewardFunction({ itemtype, itemid })}
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonItemAmount">
        <div className="row">
          <div className="col-md-2">
            <Form.Label>
              <b>Amount</b>
            </Form.Label>
          </div>
          :
          <div className="col-lg-3 col-md-4">
            <Form.Control
              type="number"
              placeholder="Set Item Amount..."
              onChange={(e) => setAmount(e.target.value)}
            />
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonItemAmount">
        <div className="row">
          <div className="col-lg-5 col-md-6" align="right">
            {UpdateMail({
               setItemtype,
               setAmount,
               setItemid,
               amount,
               itemtype,
               itemid,
               seasonEndList,
               setSeasonid,
               setNewseasonid,
               id,
               setSeasonMailReward,
               newseasonid
            })}
          </div>
        </div>
      </Form.Group>
    </Form>
  );
};
