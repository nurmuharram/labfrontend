import { Form } from "react-bootstrap";
import { UpdateSeasonReward } from "src/api/seasonend/SeasonEndPutRequest";
import { SeasonRewardItemUpdate } from "src/services/seasonEnd.services/SeasonReward.function";
import { SeasonRewardFunction } from "src/services/seasonEnd.services/SeasonReward.function";

export const AddSeasonRewardForm = ({
  setItemtype,
  setAmount,
  setItemid,
  amount,
  itemtype,
  itemid,
  seasonEndList,
  setSeasonid,
}) => {
  const dropdownSeasonid = seasonEndList.map((item, index) => {
    return (
      <option value={item.season_id}>
        {item.season_id} - ({item.start_date})
      </option>
    );
  });
  return (
    <Form>
      <Form.Group className="mb-3" controlId="formSeasonID">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Season ID</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-8">
            <Form.Select
              className="col-md-9"
              aria-label="Select"
              onChange={(e) => setSeasonid(e.target.value)}
            >
              {dropdownSeasonid}
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonItemType">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Item Type</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-8">
            <Form.Select
              className="col-md-9"
              aria-label="Select"
              onChange={(e) => setItemtype(e.target.value)}
            >
              <option value={""}>Choose Item Type</option>
              <option value={1}>Currency</option>
              <option value={6}>Box</option>
              <option value={5}>Items</option>
              <option value={2}>Ksatriya</option>
              <option value={3}>KSA Skins</option>
              <option value={4}>Rune</option>
              <option value={11}>Frame</option>
              <option value={12}>Avatar</option>
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonItemID">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Item ID</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-8">
            <Form.Select
              className="col-md-9"
              aria-label="Select"
              onChange={(e) => setItemid(e.target.value)}
            >
              {SeasonRewardFunction({ itemtype, itemid })}
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonItemAmount">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Amount</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-6">
            <Form.Control
              type="number"
              placeholder="Set Item Amount..."
              onChange={(e) => setAmount(e.target.value)}
            />
          </div>
        </div>
      </Form.Group>
    </Form>
  );
};

export const UpdateSeasonRewardForm = ({
  newitemtype,
  setNewitemtype,
  newitemid,
  setNewitemid,
  newamount,
  setNewamount,
  newseasonid,
  setNeweasonid,
  seasonEndList,
  setSeasonEndList,
  id,
  setSeasonReward
}) => {
  const dropdownSeasonid = seasonEndList.map((item, index) => {
    return (
      <option value={item.season_id}>
        {item.season_id} - ({item.start_date})
      </option>
    );
  });
  return (
    <Form>
      <Form.Group className="mb-3" controlId="formSeasonID">
        <div className="row">
          <div className="col-md-2">
            <Form.Label>
              <b>Season ID</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-5">
            <Form.Select
              className="col-md-9"
              aria-label="Select"
              onChange={(e) => setNeweasonid(e.target.value)}
            >
              {dropdownSeasonid}
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonItemType">
        <div className="row">
          <div className="col-md-2">
            <Form.Label>
              <b>Item Type</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-5">
            <Form.Select
              className="col-md-9"
              aria-label="Select"
              onChange={(e) => setNewitemtype(e.target.value)}
            >
              <option value={""}>Choose Item Type</option>
              <option value={1}>Currency</option>
              <option value={6}>Box</option>
              <option value={5}>Items</option>
              <option value={2}>Ksatriya</option>
              <option value={3}>KSA Skins</option>
              <option value={4}>Rune</option>
              <option value={11}>Frame</option>
              <option value={12}>Avatar</option>
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonItemID">
        <div className="row">
          <div className="col-md-2">
            <Form.Label>
              <b>Item ID</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-5">
            <Form.Select
              className="col-md-9"
              aria-label="Select"
              onChange={(e) => setNewitemid(e.target.value)}
            >
              {SeasonRewardItemUpdate({ newitemtype, newitemid })}
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonItemAmount">
        <div className="row">
          <div className="col-md-2">
            <Form.Label>
              <b>Amount</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-4">
            <Form.Control
              type="number"
              placeholder="Set Item Amount..."
              onChange={(e) => setNewamount(e.target.value)}
            />
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonItemAmount">
        <div className="row">
          <div className="col-md-6" align='right'>
            {UpdateSeasonReward({
              newitemtype,
              setNewitemtype,
              newitemid,
              setNewitemid,
              newamount,
              setNewamount,
              newseasonid,
              setNeweasonid,
              setSeasonEndList,
              id,
              setSeasonReward
            })}
          </div>
        </div>
      </Form.Group>
    </Form>
  );
};
