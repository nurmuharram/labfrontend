import { Form } from "react-bootstrap";
import { UpdateSeasonEnddate } from "src/api/seasonend/SeasonEndPutRequest";
import { UpdateSeasonStartdate } from "src/api/seasonend/SeasonEndPutRequest";

export const AddSeasonPeriodForms = ({
  setStartdate,
  setStarttime,
  setEnddate,
  setEndtime,
}) => {
  return (
    <Form>
      <Form.Group className="mb-3" controlId="formSeasonStartDate">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Start Date</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-4">
            <Form.Control
              type="date"
              onKeyDown={(e) => e.preventDefault()}
              onChange={(e) => setStartdate(e.target.value)}
            />
          </div>
          <div className="col-md-4">
            <Form.Control
              type="time"
              onChange={(e) => setStarttime(e.target.value)}
            />
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonEndDate">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>End Date</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-4">
            <Form.Control
              type="date"
              onKeyDown={(e) => e.preventDefault()}
              onChange={(e) => setEnddate(e.target.value)}
            />
          </div>
          <div className="col-md-4">
            <Form.Control
              type="time"
              onChange={(e) => setEndtime(e.target.value)}
            />
          </div>
        </div>
      </Form.Group>
    </Form>
  );
};

export const UpdateSeasonPeriodForms = ({
  setNewstartdate,
  setNewstarttime,
  setNewenddate,
  setNewendtime,
  newstartdate,
  newstarttime,
  newenddate,
  newendtime,
  id,
  end_date,
  start_date,
  setSeasonEndList,
}) => {
  return (
    <Form>
      <Form.Group className="mb-3" controlId="formSeasonStartDate">
        <div className="row">
          <div className="col-lg-3 col-md-3">
            <Form.Label>
              <b>New Start Date</b>
            </Form.Label>
          </div>
          :
          <div className="col-lg-2 col-md-3">
            <Form.Control
              type="date"
              onKeyDown={(e) => e.preventDefault()}
              onChange={(e) => setNewstartdate(e.target.value)}
            />
          </div>
          <div className="col-lg-2 col-md-3">
            <Form.Control
              type="time"
              onChange={(e) => setNewstarttime(e.target.value)}
            />
          </div>
          <div className="col-lg-2 col-md-2">
            {UpdateSeasonStartdate({
              newstartdate,
              newstarttime,
              newenddate,
              newendtime,
              id,
              end_date,
              start_date,
              setSeasonEndList,
            })}
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formSeasonEndDate">
        <div className="row">
          <div className="col-lg-3 col-md-3">
            <Form.Label>
              <b>New End Date</b>
            </Form.Label>
          </div>
          :
          <div className="col-lg-2 col-md-3">
            <Form.Control
              type="date"
              onKeyDown={(e) => e.preventDefault()}
              onChange={(e) => setNewenddate(e.target.value)}
            />
          </div>
          <div className="col-lg-2 col-md-3">
            <Form.Control
              type="time"
              onChange={(e) => setNewendtime(e.target.value)}
            />
          </div>
          <div className="col-lg-2 col-md-2">
            {UpdateSeasonEnddate({
              newstartdate,
              newstarttime,
              newenddate,
              newendtime,
              id,
              end_date,
              start_date,
              setSeasonEndList,
            })}
          </div>
        </div>
      </Form.Group>
    </Form>
  );
};
