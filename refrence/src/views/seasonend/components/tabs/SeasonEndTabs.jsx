import React, { useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import ListSeason from "../../ListSeason";
import SeasonMailRewards from "../../SeasonMailRewards";
import SeasonRankRewards from "../../SeasonRankRewards";
import SeasonReward from "../../SeasonReward";

function SeasonEndTabs() {
  const [key, setKey] = useState('listseason');

  return (
    <Tabs
      id="controlled-tab"
      activeKey={key}
      onSelect={(k) => setKey(k)}
      className="mb-3"
      variant="pills"
      color="red"
    >
      <Tab eventKey="listseason" title="Season Period List">
       <ListSeason/>
      </Tab>
      <Tab eventKey="seasonreward" title="Season Reward">
        <SeasonReward/>
      </Tab>
      <Tab eventKey="seasonrankreward" title="Season Rank Reward">
        <SeasonRankRewards/>
      </Tab>
      <Tab eventKey="seasonmailreward" title="Season Mail Reward">
        <SeasonMailRewards/>
      </Tab>
    </Tabs>
  );
}

export default SeasonEndTabs;
