import { Modal } from "react-bootstrap";
import { DeleteSeasonRewardItem } from "src/api/seasonend/SeasonEndDeleteRequest";
import { AddSeasonReward } from "src/api/seasonend/SeasonEndPostRequest";
import { AddSeasonRewardForm } from "../forms/SeasonRewardForms";

export const SeasonRewardsModal = ({
  addItemModal,
  setAddItemModal,
  setItemtype,
  setAmount,
  setItemid,
  amount,
  itemtype,
  itemid,
  seasonEndList,
  setSeasonid,
  seasonid,
  setSeasonReward
}) => {
  return (
    <Modal show={addItemModal} onHide={() => setAddItemModal(!addItemModal)}>
      <Modal.Header className="bg-primary" closeButton>
        <Modal.Title>Add Season Reward</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <AddSeasonRewardForm
          setItemtype={setItemtype}
          setAmount={setAmount}
          setItemid={setItemid}
          amount={amount}
          itemtype={itemtype}
          itemid={itemid}
          seasonEndList={seasonEndList}
          setSeasonid={setSeasonid}
        />
      </Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => setAddItemModal(!addItemModal)}
        >
          Cancel
        </button>
        {AddSeasonReward({ seasonid, amount, itemtype, itemid, setSeasonReward })}
      </Modal.Footer>
    </Modal>
  );
};

export const DeleteSeasonRewardItemModal = ({
  id,
  setDeleteItemModal,
  deleteItemModal,
  setSeasonReward
}) => {
  return (
    <Modal
      show={deleteItemModal}
      onHide={() => setDeleteItemModal(!deleteItemModal)}
    >
      <Modal.Header className="bg-danger" closeButton>
        <Modal.Title>You're About To Delete this Item</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="col" align="center">
          <h4>Are You Sure?</h4>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => setDeleteItemModal(!deleteItemModal)}
        >
          Cancel
        </button>
        {DeleteSeasonRewardItem({ setDeleteItemModal, id, setSeasonReward })}
      </Modal.Footer>
    </Modal>
  );
};
