import { Modal } from "react-bootstrap";
import { DeleteSeasonMail } from "src/api/seasonend/SeasonEndDeleteRequest";
import { SendSeasonMail } from "src/api/seasonend/SeasonEndPostRequest";
import { AddSeasonMailForm } from "../forms/SeasonMailRewardForms";

export const SeasonMailRewardsModal = ({
  addItemModal,
  setAddItemModal,
  seasonEndList,
  setSeasonid,
  setRank,
  setMailtemplate,
  mails,
  setMaildata,
  rank,
  mailtemplate,
  maildata,
  seasonid,
  setSeasonMailReward,
}) => {
  return (
    <Modal size="lg" show={addItemModal} onHide={() => setAddItemModal(!addItemModal)}>
      <Modal.Header className="bg-primary" closeButton>
        <Modal.Title>Send Season Mail Reward</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <AddSeasonMailForm
          seasonEndList={seasonEndList}
          setSeasonid={setSeasonid}
          setRank={setRank}
          mails={mails}
          setMaildata={setMaildata}
          rank={rank}
          mailtemplate={mailtemplate}
          maildata={maildata}
          setMailtemplate={setMailtemplate}
          setSeasonMailReward={setSeasonMailReward}
        />
      </Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => setAddItemModal(!addItemModal)}
        >
          Cancel
        </button>
        <SendSeasonMail
          seasonid={seasonid}
          maildata={maildata}
          setSeasonMailReward={setSeasonMailReward}
          setAddItemModal={setAddItemModal}
        />
      </Modal.Footer>
    </Modal>
  );
};

export const DeleteSeasonMailModal = ({
  id,
  setDeleteItemModal,
  deleteItemModal,
  setSeasonMailReward
}) => {
  return (
    <Modal
      show={deleteItemModal}
      onHide={() => setDeleteItemModal(!deleteItemModal)}
    >
      <Modal.Header className="bg-danger" closeButton>
        <Modal.Title>You're About To Delete this Item</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="col" align="center">
          <h4>Are You Sure?</h4>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => setDeleteItemModal(!deleteItemModal)}
        >
          Cancel
        </button>
        {DeleteSeasonMail({ setDeleteItemModal, id, setSeasonMailReward })}
      </Modal.Footer>
    </Modal>
  );
};