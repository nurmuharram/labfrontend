import { Modal } from "react-bootstrap";
import { DeleteSeasonPeriod } from "src/api/seasonend/SeasonEndDeleteRequest";
import { AddSeasonPeriod } from "src/api/seasonend/SeasonEndPostRequest";
import { AddSeasonPeriodForms } from "../forms/ListSeasonForms";

export const ListSeasonModals = ({
  addItemModal,
  setAddItemModal,
  setStartdate,
  setStarttime,
  setEnddate,
  setEndtime,
  setSeasonEndList,
  startdate,
  enddate,
  starttime,
  endtime,
}) => {
  return (
    <Modal show={addItemModal} onHide={() => setAddItemModal(!addItemModal)}>
      <Modal.Header className="bg-primary" closeButton>
        <Modal.Title>Add Season Period</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <AddSeasonPeriodForms
          setStartdate={setStartdate}
          setStarttime={setStarttime}
          setEnddate={setEnddate}
          setEndtime={setEndtime}
        />
      </Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => setAddItemModal(!addItemModal)}
        >
          Cancel
        </button>
        <AddSeasonPeriod
          setAddItemModal={setAddItemModal}
          startdate={startdate}
          enddate={enddate}
          starttime={starttime}
          endtime={endtime}
          setSeasonEndList={setSeasonEndList}
        />
      </Modal.Footer>
    </Modal>
  );
};

export const DeleteSeasonPeriodModal = ({
  id,
  setDeleteItemModal,
  setSeasonEndList,
  deleteItemModal,
}) => {
  return (
    <Modal
      show={deleteItemModal}
      onHide={() => setDeleteItemModal(!deleteItemModal)}
    >
      <Modal.Header className="bg-danger" closeButton>
        <Modal.Title>You're About To Delete this Item</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="col" align="center">
          <h4>Are You Sure?</h4>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => setDeleteItemModal(!deleteItemModal)}
        >
          Cancel
        </button>
        {DeleteSeasonPeriod({ setDeleteItemModal, id, setSeasonEndList })}
      </Modal.Footer>
    </Modal>
  );
};
