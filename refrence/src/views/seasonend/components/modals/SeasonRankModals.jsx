import { Modal } from "react-bootstrap";
import { DeleteSeasonRankRewardItem } from "src/api/seasonend/SeasonEndDeleteRequest";
import { AddSeasonRankRewards } from "src/api/seasonend/SeasonEndPostRequest";
import { AddSeasonRankReward } from "../forms/SeasonRankRewardForms";

export const SeasonRankRewardsModal = ({
  addItemModal,
  setAddItemModal,
  seasonEndList,
  setItemtype,
  setItemid,
  setAmount,
  itemtype,
  itemid,
  amount,
  setSeasonRankReward,
  rank,
  setRank,
  seasonid,
  setSeasonid,
}) => {
  return (
    <Modal show={addItemModal} onHide={() => setAddItemModal(!addItemModal)}>
      <Modal.Header className="bg-primary" closeButton>
        <Modal.Title>Add Season Rank Reward</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <AddSeasonRankReward
          setAddItemModal={setAddItemModal}
          seasonEndList={seasonEndList}
          setItemtype={setItemtype}
          setItemid={setItemid}
          setAmount={setAmount}
          itemtype={itemtype}
          itemid={itemid}
          amount={amount}
          setSeasonRankReward={setSeasonRankReward}
          rank={rank}
          setRank={setRank}
          seasonid={seasonid}
          setSeasonid={setSeasonid}
        />
      </Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => setAddItemModal(!addItemModal)}
        >
          Cancel
        </button>
        {AddSeasonRankRewards({
          seasonid,
          amount,
          itemid,
          itemtype,
          rank,
          setSeasonRankReward,
          setAddItemModal,
        })}
      </Modal.Footer>
    </Modal>
  );
};

export const DeleteSeasonRankRewardItemModal = ({
  id,
  setDeleteItemModal,
  deleteItemModal,
  setSeasonRankReward
}) => {
  return (
    <Modal
      show={deleteItemModal}
      onHide={() => setDeleteItemModal(!deleteItemModal)}
    >
      <Modal.Header className="bg-danger" closeButton>
        <Modal.Title>You're About To Delete this Item</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="col" align="center">
          <h4>Are You Sure?</h4>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => setDeleteItemModal(!deleteItemModal)}
        >
          Cancel
        </button>
        {DeleteSeasonRankRewardItem({ setDeleteItemModal, id, setSeasonRankReward })}
      </Modal.Footer>
    </Modal>
  );
};