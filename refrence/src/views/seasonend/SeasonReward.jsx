import React, { useState } from "react";
import { GetSeasonEndList } from "src/api/seasonend/SeasonEndGetRequest";
import { GetSeasonRewards } from "src/api/seasonend/SeasonEndGetRequest";
import { SeasonRewardDataTable } from "./components/datatable/SeasonRewardDatatable";
import { SeasonRewardsModal } from "./components/modals/SeasonRewardModals";

function SeasonReward() {
  const [addItemModal, setAddItemModal] = useState(false);
  const [seasonReward, setSeasonReward] = useState([]);
  const [seasonEndList, setSeasonEndList] = useState([]);

  const [itemtype, setItemtype] = useState();
  const [itemid, setItemid] = useState(1);
  const [amount, setAmount] = useState();
  const [seasonid, setSeasonid] = useState(0)

  const [newitemtype, setNewitemtype] = useState();
  const [newitemid, setNewitemid] = useState(1);
  const [newamount, setNewamount] = useState();
  const [newseasonid, setNeweasonid] = useState(0)

  GetSeasonRewards({ setSeasonReward });
  GetSeasonEndList({ setSeasonEndList });

  return (
    <div>
      <div>
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-header">
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => setAddItemModal(!addItemModal)}
                >
                  + Add Season Reward
                </button>
                <SeasonRewardsModal
                  addItemModal={addItemModal}
                  setAddItemModal={setAddItemModal}
                  setItemtype={setItemtype}
                  setItemid={setItemid}
                  setAmount={setAmount}
                  itemtype={itemtype}
                  itemid={itemid}
                  amount={amount}
                  seasonEndList={seasonEndList}
                  setSeasonid={setSeasonid}
                  seasonid={seasonid}
                  setSeasonReward={setSeasonReward}
                />
              </div>
              <div className="card-body">
                <SeasonRewardDataTable
                  seasonReward={seasonReward}
                  setItemtype={setItemtype}
                  setItemid={setItemid}
                  setAmount={setAmount}
                  itemtype={itemtype}
                  itemid={itemid}
                  amount={amount}
                  newitemtype={newitemtype}
                  setNewitemtype={setNewitemtype}
                  newitemid={newitemid}
                  setNewitemid={setNewitemid}
                  newamount={newamount}
                  setNewamount={setNewamount}
                  newseasonid={newseasonid}
                  setNeweasonid={setNeweasonid}
                  setSeasonEndList={setSeasonEndList}
                  seasonEndList={seasonEndList}
                  setSeasonReward={setSeasonReward}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SeasonReward;
