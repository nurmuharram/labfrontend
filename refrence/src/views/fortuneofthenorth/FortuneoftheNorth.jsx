import React from 'react'

import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CContainer,
    CNav,
    CNavItem,
    CNavLink,
    CTabContent,
    CTabPane,
    CTabs,
} from '@coreui/react'
import GachaList from './GachaList'
import GachaFeature from './GachaFeature'
import GachaItem from './GachaItem'
import GachaLoot from './GachaLoot'

function FortuneoftheNorth() {
    return (
        <div>
            <CContainer fluid>
                <CCard accentColor='primary'>
                    <CCardHeader>
                        <CCol align='center'>
                            <h5>
                                <b>Fortune Of The North (Gacha) Manager</b>
                            </h5>
                        </CCol>
                    </CCardHeader>
                    <CCardBody>
                        <CTabs>
                            <CNav variant='tabs'>
                                <CNavItem>
                                    <CNavLink>
                                        Gacha List
                                    </CNavLink>
                                </CNavItem>
                                <CNavItem>
                                    <CNavLink>
                                        Gacha Featured
                                    </CNavLink>
                                </CNavItem>
                                <CNavItem>
                                    <CNavLink>
                                        Gacha Item
                                    </CNavLink>
                                </CNavItem>
                                <CNavItem>
                                    <CNavLink>
                                        Gacha Loot
                                    </CNavLink>
                                </CNavItem>
                            </CNav>
                            <CTabContent>
                                <CTabPane>
                                    <GachaList />
                                </CTabPane>
                                <CTabPane>
                                    <GachaFeature />
                                </CTabPane>
                                <CTabPane>
                                    <GachaItem />
                                </CTabPane>
                                <CTabPane>
                                    <GachaLoot />
                                </CTabPane>
                            </CTabContent>
                        </CTabs>
                    </CCardBody>
                </CCard>
            </CContainer>
        </div>
    )
}

export default FortuneoftheNorth