import React, { useState } from 'react'

import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CCollapse,
    CDataTable,
    CFormGroup,
    CInput,
    CLabel,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    CRow,
    CSelect
} from '@coreui/react'
import { GetGachaItems } from 'src/api/GachaAPI/gachaGetRequest'
import { GachaItemFunction, toggleDetails } from '../../services/fortuneofthenorth.services/GachaItem.Function'
import { PostItemstoGacha } from 'src/api/GachaAPI/gachaPostRequest'
import { DeleteGachaItem } from 'src/api/GachaAPI/gachaDeleteRequest'
import { UpdateGachaItemAmount } from 'src/api/GachaAPI/gachaPutRequest'

function GachaItem() {

    const [addItemModal, setAddItemModal] = useState(false)
    const [deleteItemModal, setDeleteItemModal] = useState(false)
    const [gachaItems, setGachaItems] = useState([])

    const [details, setDetails] = useState([])

    const [itemtype, setItemtype] = useState('')
    const [itemID, setItemID] = useState(1)
    const [amount, setAmount] = useState()

    const [newamount, setNewamount] = useState()

    GetGachaItems({ setGachaItems })

    const fields = [
        { key: 'gacha_item_id', _style: { width: '15%' } },
        { key: 'item_type_name' },
        { key: 'item_name' },
        { key: 'amount' },
        {
            key: 'show_details',
            label: '',
            _style: { width: '14%' },
            sorter: false,
            filter: false
        }
    ]

    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader align='right'>
                            <CButton color='primary' onClick={() => setAddItemModal(!addItemModal)}>+ Add Items to Gacha</CButton>
                            <CModal
                                show={addItemModal}
                                onClose={() => setAddItemModal(!addItemModal)}
                                color="primary"
                                size="md"
                            >
                                <CModalHeader>
                                    <CModalTitle>
                                        Add Items to Gacha
                                    </CModalTitle>
                                </CModalHeader>
                                <CModalBody>
                                    <CCol>
                                        <CFormGroup row>
                                            <CCol md="3" align='left'>
                                                <CLabel htmlFor="input"><b>Item Type</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setItemtype(e.target.value)}>
                                                    <option value=''>Choose Item Type</option>
                                                    <option value={1}  >Currency</option>
                                                    <option value={6}  >Box</option>
                                                    <option value={5}  >Items</option>
                                                    <option value={2}  >Ksatriya</option>
                                                    <option value={3}  >KSA Skins</option>
                                                    <option value={4}  >Rune</option>
                                                    <option value={11} >Frame</option>
                                                    <option value={12} >Avatar</option>
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3" align='left'>
                                                <CLabel htmlFor="input"><b>Item ID</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setItemID(e.target.value)}>
                                                    {GachaItemFunction({ itemtype })}
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3" align='left'>
                                                <CLabel htmlFor="input"><b>Amount</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CInput type='number' placeholder='Set Amount...' onChange={(e) => setAmount(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                    </CCol>
                                </CModalBody>
                                <CModalFooter>
                                    {PostItemstoGacha({ itemtype, itemID, amount, addItemModal, setAddItemModal, setGachaItems })}
                                </CModalFooter>
                            </CModal>
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={gachaItems}
                                fields={fields}
                                sorter
                                pagination
                                itemsPerPage={5}
                                scopedSlots={{
                                    'show_details':
                                        (item, index) => {
                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        color="primary"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(index, { setDetails, details }) }}
                                                    >
                                                        {details.includes(index) ? 'Hide' : 'Edit/Update'}
                                                    </CButton>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (item, index) => {
                                            const id = item.gacha_item_id
                                            return (
                                                <CCollapse show={details.includes(index)}>
                                                    <CCardBody>
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <h4>
                                                                    <i>Edit/Update Item (<small>ID : {id}</small>)</i>
                                                                </h4>
                                                            </CCol>
                                                            <CCol align='left' md='2'>
                                                                <CButton size="sm" color="danger" variant='outline' shape='square' onClick={() => setDeleteItemModal(!deleteItemModal)}>
                                                                    Delete Item</CButton>
                                                                <CModal
                                                                    show={deleteItemModal}
                                                                    onClose={() => setDeleteItemModal(!deleteItemModal)}
                                                                    color="danger"
                                                                    size="md"
                                                                >
                                                                    <CModalHeader closeButton>
                                                                        <CModalTitle>
                                                                            <b>Delete this item?</b>
                                                                        </CModalTitle>
                                                                    </CModalHeader>
                                                                    <CModalBody>

                                                                        <CCol align='center'>
                                                                            <h5>Are you sure?</h5>
                                                                        </CCol>

                                                                    </CModalBody>
                                                                    <CModalFooter>
                                                                        <CRow>
                                                                            <CCol align='center'>
                                                                                {DeleteGachaItem({ setGachaItems, setDeleteItemModal, id })}&nbsp;
                                                                                <CButton type="send" size="sm" color="secondary" onClick={() => setDeleteItemModal(!deleteItemModal)} ><b>Cancel</b></CButton>
                                                                            </CCol>
                                                                        </CRow>
                                                                    </CModalFooter>
                                                                </CModal>
                                                            </CCol>
                                                        </CRow>
                                                        <hr />
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <CFormGroup row>
                                                                    <CCol md="2" align='left'>
                                                                        <CLabel htmlFor="input"><b>Amount</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CInput type='number' placeholder='Set New Amount...' onChange={(e) => setNewamount(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol xs="12" md="2" >
                                                                        {UpdateGachaItemAmount({ setGachaItems, newamount, id })}
                                                                    </CCol>
                                                                </CFormGroup>
                                                            </CCol>
                                                        </CRow>
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        }
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default GachaItem