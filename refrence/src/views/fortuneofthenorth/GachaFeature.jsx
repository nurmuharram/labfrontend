import React, { useState } from 'react'

import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CCollapse,
    CDataTable,
    CFormGroup,
    CInput,
    CLabel,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    CRow,
    CSelect,
} from '@coreui/react'
import { GetGachaFeatured, GetGachaItems, GetGachaPeriod } from 'src/api/GachaAPI/gachaGetRequest'
import { PostGachaFeatured } from 'src/api/GachaAPI/gachaPostRequest'
import { toggleDetails } from 'src/services/fortuneofthenorth.services/GachaFeature.Function'
import { DeleteGachaFeatured } from 'src/api/GachaAPI/gachaDeleteRequest'
import { UpdateGachaIdFeatured, UpdateGachaItemIdFeatured, UpdateGachaPriorityFeatured } from 'src/api/GachaAPI/gachaPutRequest'

function GachaFeature() {

    const [addItemModal, setAddItemModal] = useState(false)
    const [deleteItemModal, setDeleteItemModal] = useState(false)
    const [gachaFeaturedList, setGachaFeaturedList] = useState([])
    const [gachaItems, setGachaItems] = useState([])
    const [gachaList, setGachaList] = useState([])

    const [details, setDetails] = useState([])

    const [gacha_id, setGacha_id] = useState(1)
    const [gacha_item_id, setGacha_item_id] = useState(1)
    const [priority, setPriority] = useState()

    const [newgacha_id, setNewgacha_id] = useState(1)
    const [newgacha_item_id, setNewgacha_item_id] = useState(1)
    const [newpriority, setNewpriority] = useState()

    GetGachaPeriod({ setGachaList })
    GetGachaFeatured({ setGachaFeaturedList })
    GetGachaItems({ setGachaItems })

    const fields = [
        { key: 'gacha_feature_id', _style: { width: '15%' } },
        { key: 'gacha_id' },
        { key: 'gacha_item_id' },
        { key: 'priority' },
        {
            key: 'show_details',
            label: '',
            _style: { width: '14%' },
            sorter: false,
            filter: false
        }
    ]

    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader align='right'>
                            <CButton color='primary' onClick={() => setAddItemModal(!addItemModal)}>+ Add Featured Gacha</CButton>
                            <CModal
                                show={addItemModal}
                                onClose={() => setAddItemModal(!addItemModal)}
                                color="primary"
                                size="lg"
                            >
                                <CModalHeader>
                                    <CModalTitle>
                                        Add Featured Gacha
                                    </CModalTitle>
                                </CModalHeader>
                                <CModalBody>
                                    <CCol>
                                        <CFormGroup row>
                                            <CCol md="4" align='left'>
                                                <CLabel htmlFor="input"><b>Gacha ID</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="7">
                                                <CSelect custom size='md' onChange={(e) => setGacha_id(e.target.value)} >
                                                    {
                                                        gachaList && gachaList.map((item) =>
                                                            <option key={item.gacha_id} value={item.gacha_id}>
                                                                {item.gacha_id} - ({item.start_date} - {item.End_date})
                                                            </option>)
                                                    }
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="4" align='left'>
                                                <CLabel htmlFor="input"><b>Gacha Item ID</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="7">
                                                <CSelect custom size='md' onChange={(e) => setGacha_item_id(e.target.value)}>
                                                    {
                                                        gachaItems && gachaItems.map((item) =>
                                                            <option key={item.gacha_item_id} value={item.gacha_item_id}>
                                                                {item.gacha_item_id} - {item.item_type_name} {item.item_name}
                                                            </option>)
                                                    }
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="4" align='left'>
                                                <CLabel htmlFor="input"><b>Priority</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="7">
                                                <CInput type='number' placeholder='Set Priority...' onChange={(e) => setPriority(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                    </CCol>
                                </CModalBody>
                                <CModalFooter>
                                    {PostGachaFeatured({ gacha_id, gacha_item_id, priority, addItemModal, setAddItemModal, setGachaFeaturedList })}
                                </CModalFooter>
                            </CModal>
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={gachaFeaturedList}
                                fields={fields}
                                sorter
                                pagination
                                itemsPerPage={5}
                                scopedSlots={{
                                    'show_details':
                                        (item, index) => {
                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        color="primary"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(index, { setDetails, details }) }}
                                                    >
                                                        {details.includes(index) ? 'Hide' : 'Edit/Update'}
                                                    </CButton>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (item, index) => {
                                            const id = item.gacha_feature_id
                                            const gachaID = item.gacha_id
                                            const gachaitemID = item.gacha_item_id
                                            const _priority = item.priority
                                            return (
                                                <CCollapse show={details.includes(index)}>
                                                    <CCardBody>
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <h4>
                                                                    <i>Edit/Update Item (<small>ID : {id}</small>)</i>
                                                                </h4>
                                                            </CCol>
                                                            <CCol align='left' md='2'>
                                                                <CButton size="sm" color="danger" variant='outline' shape='square' onClick={() => setDeleteItemModal(!deleteItemModal)}>
                                                                    Delete Item</CButton>
                                                                <CModal
                                                                    show={deleteItemModal}
                                                                    onClose={() => setDeleteItemModal(!deleteItemModal)}
                                                                    color="danger"
                                                                    size="md"
                                                                >
                                                                    <CModalHeader closeButton>
                                                                        <CModalTitle>
                                                                            <b>Delete this item?</b>
                                                                        </CModalTitle>
                                                                    </CModalHeader>
                                                                    <CModalBody>

                                                                        <CCol align='center'>
                                                                            <h5>Are you sure?</h5>
                                                                        </CCol>

                                                                    </CModalBody>
                                                                    <CModalFooter>
                                                                        <CRow>
                                                                            <CCol align='center'>
                                                                                {DeleteGachaFeatured({ setGachaFeaturedList, setDeleteItemModal, id })}&nbsp;
                                                                                <CButton type="send" size="sm" color="secondary" onClick={() => setDeleteItemModal(!deleteItemModal)} ><b>Cancel</b></CButton>
                                                                            </CCol>
                                                                        </CRow>
                                                                    </CModalFooter>
                                                                </CModal>
                                                            </CCol>
                                                        </CRow>
                                                        <hr />
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <CFormGroup row>
                                                                    <CCol md="2" align='left'>
                                                                        <CLabel htmlFor="input"><b>Gacha ID</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="5">
                                                                        <CSelect custom size='md' onChange={(e) => setNewgacha_id(e.target.value)} >
                                                                            {
                                                                                gachaList && gachaList.map((item) =>
                                                                                    <option key={item.gacha_id} value={item.gacha_id}>
                                                                                        {item.gacha_id} - ({item.start_date} - {item.End_date})
                                                                                    </option>)
                                                                            }
                                                                        </CSelect>
                                                                    </CCol>
                                                                    <CCol xs="12" md="2" >
                                                                        {UpdateGachaIdFeatured({ setGachaFeaturedList, newgacha_id, gachaitemID, _priority, id })}
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="2" align='left'>
                                                                        <CLabel htmlFor="input"><b>Gacha Item ID</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="5">
                                                                        <CSelect custom size='md' onChange={(e) => setNewgacha_item_id(e.target.value)}>
                                                                            {
                                                                                gachaItems && gachaItems.map((item) =>
                                                                                    <option key={item.gacha_item_id} value={item.gacha_item_id}>
                                                                                        {item.gacha_item_id} - {item.item_type_name} {item.item_name}
                                                                                    </option>)
                                                                            }
                                                                        </CSelect>
                                                                    </CCol>
                                                                    <CCol xs="12" md="2" >
                                                                        {UpdateGachaItemIdFeatured({ setGachaFeaturedList, gachaID, newgacha_item_id, _priority, id })}
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="2" align='left'>
                                                                        <CLabel htmlFor="input"><b>Priority</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="5">
                                                                        <CInput type='number' placeholder='Set New Priority...' onChange={(e) => setNewpriority(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol xs="12" md="2" >
                                                                        {UpdateGachaPriorityFeatured({ setGachaFeaturedList, gachaID, gachaitemID, newpriority, id })}
                                                                    </CCol>
                                                                </CFormGroup>
                                                            </CCol>
                                                        </CRow>
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        }
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default GachaFeature