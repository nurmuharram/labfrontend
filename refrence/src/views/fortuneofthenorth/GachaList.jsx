import React, { useState } from 'react'

import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CCollapse,
    CDataTable,
    CFormGroup,
    CInput,
    CLabel,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    CRow,
    CSelect
} from '@coreui/react'

import { GetGachaPeriod } from 'src/api/GachaAPI/gachaGetRequest'
import { PostGacha } from 'src/api/GachaAPI/gachaPostRequest'
import { toggleDetails } from 'src/services/fortuneofthenorth.services/GachaList.Function'
import { DeleteGacha } from 'src/api/GachaAPI/gachaDeleteRequest'
import { UpdateGachaEnddate, UpdateGachaStartdate, UpdateRandomValue } from 'src/api/GachaAPI/gachaPutRequest'

function GachaList() {

    const [addItemModal, setAddItemModal] = useState(false)
    const [deleteItemModal, setDeleteItemModal] = useState(false)
    const [gachaList, setGachaList] = useState([])

    const [details, setDetails] = useState([])

    const [startdate, setStartdate] = useState('2019-10-10')
    const [starttime, setStarttime] = useState('00:00')
    const [enddate, setEnddate] = useState('2019-10-10')
    const [endtime, setEndtime] = useState('00:00')
    const [randomvalue, setRandomvalue] = useState()

    const [newstartdate, setNewstartdate] = useState('2019-10-10')
    const [newstarttime, setNewstarttime] = useState('00:00')
    const [newenddate, setNewenddate] = useState('2019-10-10')
    const [newendtime, setNewendtime] = useState('00:00')
    const [newrandomvalue, setNewrandomvalue] = useState()

    GetGachaPeriod({ setGachaList })

    const fields = [
        { key: 'gacha_id', _style: { width: '15%' } },
        { key: 'start_date' },
        { key: 'End_date' },
        { key: 'random_value' },
        {
            key: 'show_details',
            label: '',
            _style: { width: '14%' },
            sorter: false,
            filter: false
        }
    ]

    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader align='right'>
                            <CButton color='primary' onClick={() => setAddItemModal(!addItemModal)}>+ Add Gacha Period</CButton>
                            <CModal
                                show={addItemModal}
                                onClose={() => setAddItemModal(!addItemModal)}
                                color="primary"
                                size="lg"
                            >
                                <CModalHeader>
                                    <CModalTitle>
                                        Add Gacha Period
                                    </CModalTitle>
                                </CModalHeader>
                                <CModalBody>
                                    <CCol>
                                        <CFormGroup row>
                                            <CCol md="3" align='left'>
                                                <CLabel htmlFor="input"><b>Gacha Period Start</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="4">
                                                <CInput type='date' onKeyDown={(e) => e.preventDefault()} onChange={(e) => setStartdate(e.target.value)} />
                                            </CCol>
                                            <CCol xs="12" md="4">
                                                <CInput type='time' onChange={(e) => setStarttime(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3" align='left'>
                                                <CLabel htmlFor="input"><b>Gacha Period End</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="4">
                                                <CInput type='date' onKeyDown={(e) => e.preventDefault()} onChange={(e) => setEnddate(e.target.value)} />
                                            </CCol>
                                            <CCol xs="12" md="4">
                                                <CInput type='time' onChange={(e) => setEndtime(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3" align='left'>
                                                <CLabel htmlFor="input"><b>Random Value</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CInput type='number' placeholder='Set Random Value...' onChange={(e) => setRandomvalue(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                    </CCol>
                                </CModalBody>
                                <CModalFooter>
                                    {PostGacha({ startdate, enddate, starttime, endtime, randomvalue, setAddItemModal, addItemModal, setGachaList })}
                                </CModalFooter>
                            </CModal>
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={gachaList}
                                fields={fields}
                                sorter
                                pagination
                                itemsPerPage={5}
                                scopedSlots={{
                                    'show_details':
                                        (item, index) => {
                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        color="primary"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(index, { setDetails, details }) }}
                                                    >
                                                        {details.includes(index) ? 'Hide' : 'Edit/Update'}
                                                    </CButton>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (item, index) => {
                                            const id = item.gacha_id
                                            const start_date = item.start_date
                                            const end_date = item.End_date
                                            const random_value = item.random_value
                                            return (
                                                <CCollapse show={details.includes(index)}>
                                                    <CCardBody>
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <h4>
                                                                    <i>Edit/Update Item (<small>ID : {id}</small>)</i>
                                                                </h4>
                                                            </CCol>
                                                            <CCol align='left' md='2'>
                                                                <CButton size="sm" color="danger" variant='outline' shape='square' onClick={() => setDeleteItemModal(!deleteItemModal)}>
                                                                    Delete Item</CButton>
                                                                <CModal
                                                                    show={deleteItemModal}
                                                                    onClose={() => setDeleteItemModal(!deleteItemModal)}
                                                                    color="danger"
                                                                    size="md"
                                                                >
                                                                    <CModalHeader closeButton>
                                                                        <CModalTitle>
                                                                            <b>Delete this item?</b>
                                                                        </CModalTitle>
                                                                    </CModalHeader>
                                                                    <CModalBody>

                                                                        <CCol align='center'>
                                                                            <h5>Are you sure?</h5>
                                                                        </CCol>

                                                                    </CModalBody>
                                                                    <CModalFooter>
                                                                        <CRow>
                                                                            <CCol align='center'>
                                                                                {DeleteGacha({ setGachaList, setDeleteItemModal, id })}&nbsp;
                                                                                <CButton type="send" size="sm" color="secondary" onClick={() => setDeleteItemModal(!deleteItemModal)} ><b>Cancel</b></CButton>
                                                                            </CCol>
                                                                        </CRow>
                                                                    </CModalFooter>
                                                                </CModal>
                                                            </CCol>
                                                        </CRow>
                                                        <hr />
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <CFormGroup row>
                                                                    <CCol lg="3" md='3'>
                                                                        <CLabel htmlFor="input"><b>Gacha Period Start</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol lg="2" md="3">
                                                                        <CInput type='date' onKeyDown={(e) => e.preventDefault()} onChange={(e) => setNewstartdate(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol lg="2" md="3">
                                                                        <CInput type='time' onChange={(e) => setNewstarttime(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol lg="2" md="2" >
                                                                        {UpdateGachaStartdate({ setGachaList, newstartdate, newstarttime, end_date, random_value, id })}
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol lg="3" md="3">
                                                                        <CLabel htmlFor="input"><b>Gacha Period End</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol lg="2" md="3">
                                                                        <CInput type='date' onKeyDown={(e) => e.preventDefault()} onChange={(e) => setNewenddate(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol lg="2" md="3">
                                                                        <CInput type='time' onChange={(e) => setNewendtime(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol lg="2" md="2">
                                                                        {UpdateGachaEnddate({ setGachaList, newenddate, newendtime, start_date, random_value, id })}
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol lg="3" md="3">
                                                                        <CLabel htmlFor="input"><b>Random Value</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol lg="4" md="6">
                                                                        <CInput type='number' placeholder='Set New Random Value...' onChange={(e) => setNewrandomvalue(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol lg="2" md="2" >
                                                                        {UpdateRandomValue({ setGachaList, end_date, start_date, newrandomvalue, id })}
                                                                    </CCol>
                                                                </CFormGroup>
                                                            </CCol>
                                                        </CRow>
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        },
                                    "start_date":
                                        (item, index) => {
                                            const date = new Date((item.start_date && item.start_date.split(' ').join('T')) + '.000Z')
                                            return (
                                                <td>
                                                    {date.toString()}
                                                </td>
                                            )
                                        },
                                    "End_date":
                                        (item, index) => {
                                            const date = new Date((item.End_date && item.End_date.split(' ').join('T')) + '.000Z')
                                            return (
                                                <td>
                                                    {date.toString()}
                                                </td>
                                            )
                                        }
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default GachaList