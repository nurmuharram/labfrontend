import React, { useState } from 'react'
import { GetGachaItems, GetGachaLoot, GetGachaPeriod } from 'src/api/GachaAPI/gachaGetRequest'

import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CCollapse,
    CDataTable,
    CFormGroup,
    CInput,
    CLabel,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    CRow,
    CSelect
} from '@coreui/react'
import { PostGachaLoot } from 'src/api/GachaAPI/gachaPostRequest'
import { toggleDetails } from 'src/services/fortuneofthenorth.services/GachaLoot.Function'
import { DeleteGachaLoot } from 'src/api/GachaAPI/gachaDeleteRequest'
import { UpdateGachaLootChance, UpdateGachaLootMaxValue, UpdateGachaLootMinValue } from 'src/api/GachaAPI/gachaPutRequest'

function GachaLoot() {

    const [addItemModal, setAddItemModal] = useState(false)
    const [deleteItemModal, setDeleteItemModal] = useState(false)

    const [gachaLoot, setGachaLoot] = useState([])
    const [gachaItems, setGachaItems] = useState([])
    const [gachaList, setGachaList] = useState([])

    const [details, setDetails] = useState([])

    const [gacha_id, setGacha_id] = useState(1)
    const [gacha_item_id, setGacha_item_id] = useState(1)
    const [chance, setChance] = useState()
    const [min_value, setMin_value] = useState()
    const [max_value, setMax_value] = useState()

    const [newchance, setNewhance] = useState()
    const [newmin_value, setNewmin_value] = useState()
    const [newmax_value, setNewmax_value] = useState()

    GetGachaLoot({ setGachaLoot })
    GetGachaItems({ setGachaItems })
    GetGachaPeriod({ setGachaList })

    const fields = [
        { key: 'gacha_id', _style: { width: '15%' } },
        { key: 'item_type_name' },
        { key: 'item_name' },
        { key: 'amount' },
        { key: 'chance' },
        { key: 'min_value' },
        { key: 'max_value' },
        {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        }
    ]

    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader align='right'>
                            <CButton color='primary' onClick={() => setAddItemModal(!addItemModal)}>+ Add Gacha Loot</CButton>
                            <CModal
                                show={addItemModal}
                                onClose={() => setAddItemModal(!addItemModal)}
                                color="primary"
                                size="lg"
                            >
                                <CModalHeader>
                                    <CModalTitle>
                                        Add Gacha Loot
                                    </CModalTitle>
                                </CModalHeader>
                                <CModalBody>
                                    <CCol>
                                        <CFormGroup row>
                                            <CCol md="3" align='left'>
                                                <CLabel htmlFor="input"><b>Gacha ID</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="7">
                                                <CSelect custom size='md' onChange={(e) => setGacha_id(e.target.value)} >
                                                    {
                                                        gachaList && gachaList.map((item) =>
                                                            <option key={item.gacha_id} value={item.gacha_id}>
                                                                {item.gacha_id} - ({item.start_date} - {item.End_date})
                                                            </option>)
                                                    }
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3" align='left'>
                                                <CLabel htmlFor="input"><b>Gacha Item ID</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="7">
                                                <CSelect custom size='md' onChange={(e) => setGacha_item_id(e.target.value)}>
                                                    {
                                                        gachaItems && gachaItems.map((item) =>
                                                            <option key={item.gacha_item_id} value={item.gacha_item_id}>
                                                                {item.gacha_item_id} - {item.item_type_name} {item.item_name}
                                                            </option>)
                                                    }
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3" align='left'>
                                                <CLabel htmlFor="input"><b>Chance</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="7">
                                                <CInput type='number' placeholder='Set Chance of this loot...' onChange={(e) => setChance(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3" align='left'>
                                                <CLabel htmlFor="input"><b>Min. Value</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="7">
                                                <CInput type='number' placeholder='Set Minimum Value of this loot...' onChange={(e) => setMin_value(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3" align='left'>
                                                <CLabel htmlFor="input"><b>Max. Value</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="7">
                                                <CInput type='number' placeholder='Set Maximum Value of this loot...' onChange={(e) => setMax_value(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                    </CCol>
                                </CModalBody>
                                <CModalFooter>
                                    {PostGachaLoot({ gacha_id, gacha_item_id, chance, min_value, max_value, setAddItemModal, setGachaLoot })}
                                </CModalFooter>
                            </CModal>
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={gachaLoot}
                                fields={fields}
                                sorter
                                pagination
                                itemsPerPage={5}
                                scopedSlots={{
                                    'show_details':
                                        (item, index) => {
                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        color="primary"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(index, { setDetails, details }) }}
                                                    >
                                                        {details.includes(index) ? 'Hide' : 'Edit/Update'}
                                                    </CButton>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (item, index) => {
                                            const id = item.gacha_id
                                            const gachaitemID = item.gacha_item_id
                                            const _chance = item.chance
                                            const minValue = item.min_value
                                            const maxValue = item.max_value
                                            return (
                                                <CCollapse show={details.includes(index)}>
                                                    <CCardBody>
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <h4>
                                                                    <i>Edit/Update Item (<small>ID : {id}</small>)</i>
                                                                </h4>
                                                            </CCol>
                                                            <CCol align='left' md='2'>
                                                                <CButton size="sm" color="danger" variant='outline' shape='square' onClick={() => setDeleteItemModal(!deleteItemModal)}>
                                                                    Delete Item</CButton>
                                                                <CModal
                                                                    show={deleteItemModal}
                                                                    onClose={() => setDeleteItemModal(!deleteItemModal)}
                                                                    color="danger"
                                                                    size="md"
                                                                >
                                                                    <CModalHeader closeButton>
                                                                        <CModalTitle>
                                                                            <b>Delete this item?</b>
                                                                        </CModalTitle>
                                                                    </CModalHeader>
                                                                    <CModalBody>

                                                                        <CCol align='center'>
                                                                            <h5>Are you sure?</h5>
                                                                        </CCol>

                                                                    </CModalBody>
                                                                    <CModalFooter>
                                                                        <CRow>
                                                                            <CCol align='center'>
                                                                                {DeleteGachaLoot({ setGachaLoot, setDeleteItemModal, id, gachaitemID })}&nbsp;
                                                                                <CButton type="send" size="sm" color="secondary" onClick={() => setDeleteItemModal(!deleteItemModal)} ><b>Cancel</b></CButton>
                                                                            </CCol>
                                                                        </CRow>
                                                                    </CModalFooter>
                                                                </CModal>
                                                            </CCol>
                                                        </CRow>
                                                        <hr />
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <CFormGroup row>
                                                                    <CCol md="2" align='left'>
                                                                        <CLabel htmlFor="input"><b>Chance</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CInput type='number' placeholder='Set New Chance of this loot...' onChange={(e) => setNewhance(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol xs="12" md="2" >
                                                                        {UpdateGachaLootChance({ setGachaLoot, newchance, minValue, maxValue, id, gachaitemID })}
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="2" align='left'>
                                                                        <CLabel htmlFor="input"><b>Min. Value</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CInput type='number' placeholder='Set New Minimum Value of this loot...' onChange={(e) => setNewmin_value(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol xs="12" md="2" >
                                                                        {UpdateGachaLootMinValue({ setGachaLoot, _chance, newmin_value, maxValue, id, gachaitemID })}
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="2" align='left'>
                                                                        <CLabel htmlFor="input"><b>Max. Value</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CInput type='number' placeholder='Set New Maximum Value of this loot...' onChange={(e) => setNewmax_value(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol xs="12" md="2" >
                                                                        {UpdateGachaLootMaxValue({ setGachaLoot, _chance, minValue, newmax_value, id, gachaitemID })}
                                                                    </CCol>
                                                                </CFormGroup>
                                                            </CCol>
                                                        </CRow>
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        }
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default GachaLoot