import React, { useState, useEffect } from "react";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { spinner } from "src/components/utils/spinner";
import {
  GetDailyUsers,
  refreshActiveChart,
} from "src/api/dashboardAPI/dashboardGetRequest";

function ActiveUsers() {
  const [dailyUsers, setDailyUsers] = useState([]);

  const date = new Date();
  const day = date.getDate();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;

  const [dateRangePick, setDateRangePick] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect((start, end) => {
    setDateRangePick({ start, end });
  }, []);

  const handleCallback = (start, end) => {
    setDateRangePick({ start, end });
  };

  GetDailyUsers({ setDailyUsers, setIsLoading, year, month, day });

  return (
    <div>
      <div className="text-white card card-accent-info bg-info">
        <div className="card-header">
          <h5>Active Users</h5>
        </div>
        <div className="card-body">
          <i>Select Range:</i>
          <DateRangePicker
            onCallback={handleCallback}
            onApply={refreshActiveChart({
              setDailyUsers,
              setIsLoading,
              dateRangePick,
            })}
          >
            <input
              type="text"
              style={{ height: "1.5rem", backgroundColor: "blanchedalmond" }}
              className="form-control"
            />
          </DateRangePicker>
          <br />
          {!isLoading ? (
            <div className="row">
              <div className="col" align="center">
                <h1
                  style={{
                    color: "white",
                    fontFamily: "sans-serif",
                    fontSize: "60px",
                    textShadow:
                      "1px 1px 2px black, 0 0 1em black, 0 0 0.2em blue",
                  }}
                >
                  {dailyUsers.count
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                  <h6>Active Players</h6>
                </h1>
              </div>
            </div>
          ) : (
            <div style={{ textAlign: "center" }}>
              <i>{spinner()}Loading Data, Please Wait...</i>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
export default ActiveUsers;
