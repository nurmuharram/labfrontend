import React, { useEffect, useState } from "react";
import { CChartBar } from "@coreui/react-chartjs";
import DateRangePicker from "react-bootstrap-daterangepicker";
import CIcon from "@coreui/icons-react";
import { spinner } from "src/components/utils/spinner";
import {
  GetConcurrentUser,
  refreshConcurrentUser,
} from "src/api/dashboardAPI/dashboardGetRequest";
import { _date } from "src/services/dashboard.services/dashboard.function";
import Link from "src/components/link/Link";
import Collapse from "src/components/collapse/Collapse";

function ConcurrentUser() {
  const [collapsed, setCollapsed] = useState(true);
  const [loading, setLoading] = useState(true);

  const [dateRangePick, setDateRangePick] = useState();
  const date = new Date();
  const day = date.getDate();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;

  const [concurrentUsers, setConcurrentUsers] = useState([]);
  GetConcurrentUser({ setConcurrentUsers, setLoading, year, month, day });

  useEffect((start, end) => {
    setDateRangePick({ start, end });
  }, []);

  const handleCallback = (start, end) => {
    setDateRangePick({ start, end });
  };

  const startDate = `${day}/${month}/${year} 00:00`;
  const endDate = `${day}/${month}/${year} 23:59`;

  const concurrentUsersCount =
    concurrentUsers &&
    concurrentUsers.map((users, index) => {
      return users.count;
    });

  return (
    <div>
      <div className="fade show">
        <div className="card card-accent-info">
          <div className="card-header">
            <div className="row">
              &nbsp;&nbsp;{" "}
              <h4>
                Concurrent User{" "}
                <small>
                  <i>
                    ({dateRangePick?.start?.format("DD MMMM YYYY HH:mm")} -{" "}
                    {dateRangePick?.end?.format("DD MMMM YYYY HH:mm")})
                  </i>
                </small>
              </h4>
              <div className="col" align="right">
                <div className="card-header-actions">
                  <Link
                    className="card-header-action"
                    onClick={() => setCollapsed(!collapsed)}
                  >
                    <div
                      data-toggle="tooltip"
                      data-placement="top"
                      title="Show/Hide Card"
                    >
                      <CIcon
                        name={
                          collapsed ? "cil-chevron-bottom" : "cil-chevron-top"
                        }
                      />
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <Collapse show={collapsed}>
            <div className="card-body">
              <i>Select Range:</i>
              <DateRangePicker
                onCallback={handleCallback}
                onApply={refreshConcurrentUser({
                  setConcurrentUsers,
                  setLoading,
                  dateRangePick,
                })}
                initialSettings={{
                  timePicker: true,
                  startDate,
                  endDate,
                  locale: { format: "DD/MM/YYYY HH:mm" },
                }}
              >
                <input type="text" className="form-control" />
              </DateRangePicker>
              <br />
              {!loading ? (
                <CChartBar
                  datasets={[
                    {
                      label: "Users",
                      backgroundColor: "rgba(255, 99, 132, 0.2)",
                      pointBorderColor: "rgb(255, 99, 132)",
                      borderWidth: 1,
                      data: concurrentUsersCount,
                    },

                    //add data below by duplicating previous data
                  ]}
                  options={{
                    tooltips: {
                      enabled: true,
                    },
                  }}
                  labels={_date({ concurrentUsers })}
                />
              ) : (
                <div style={{ textAlign: "center" }}>
                  <i>{spinner()}Loading Data, Please Wait...</i>
                </div>
              )}
            </div>
          </Collapse>
        </div>
      </div>
    </div>
  );
}
export default ConcurrentUser;
