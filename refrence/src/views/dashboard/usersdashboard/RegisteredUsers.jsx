import React, { useState } from "react";
import { CChartPie } from "@coreui/react-chartjs";
import CIcon from "@coreui/icons-react";
import { GetRegisteredUsers } from "src/api/dashboardAPI/dashboardGetRequest";
import { spinner } from "src/components/utils/spinner";
import Link from "src/components/link/Link";
import Collapse from "src/components/collapse/Collapse";

function RegisteredUsers() {
  const [collapsed, setCollapsed] = useState(true);
  const [loading, setLoading] = useState(true);

  const [registerdUsers, setRegisterdUsers] = useState([]);

  GetRegisteredUsers({ setRegisterdUsers, setLoading });

  const count = registerdUsers.map((users, index) => {
    return users.count;
  });

  const accountType = registerdUsers.map((users, index) => {
    return users.account_type;
  });

  return (
    <div>
      <div className="fade show">
        <div className="card card-accent-info">
          <div className="card-header">
            <div className="row">
              &nbsp;&nbsp; <h4>Registered Users</h4>
              <div className="col" align="right">
                <div className="card-header-actions">
                  <Link
                    className="card-header-action"
                    onClick={() => setCollapsed(!collapsed)}
                  >
                    <div
                      data-toggle="tooltip"
                      data-placement="top"
                      title="Show/Hide Card"
                    >
                      <CIcon
                        name={
                          collapsed ? "cil-chevron-bottom" : "cil-chevron-top"
                        }
                      />
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <Collapse show={collapsed}>
            <div className="card-body">
              <br />
              {!loading ? (
                <CChartPie
                  datasets={[
                    {
                      backgroundColor: [
                        "#1b60cf",
                        "#13a829",
                        "#FF6384",
                        "#fac123",
                      ],
                      data: count,
                    },
                  ]}
                  labels={accountType}
                  options={{
                    tooltips: {
                      enabled: true,
                    },
                  }}
                />
              ) : (
                <div style={{ textAlign: "center" }}>
                  <i>{spinner()}Loading Data, Please Wait...</i>
                </div>
              )}
            </div>
          </Collapse>
        </div>
      </div>
    </div>
  );
}
export default RegisteredUsers;
