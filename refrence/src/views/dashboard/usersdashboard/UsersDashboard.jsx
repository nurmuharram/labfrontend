import React from 'react'
import DailyActiveUsers from './DailyActiveUsers'
import ConcurrentUser from './ConcurrentUser'
import ActiveUsers from './ActiveUsers'
import RegisteredUsers from './RegisteredUsers'

function UsersDashboard() {
    return (
        <div>
            <div className="card" style={{ background: '#BEE036' }}>
                <center>
                    <h4><b>User Analytics Dashboard</b></h4>
                    <i>
                        Graphical view of users analytics
                    </i>
                </center>
            </div>
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                    <RegisteredUsers />
                    <DailyActiveUsers />
                </div>
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                    <div className="col-xs-12 col-sm-3 col-md-12">
                        <ActiveUsers />
                        <ConcurrentUser />
                    </div>
                </div>
            </div>
        </div>
    )
}
export default UsersDashboard