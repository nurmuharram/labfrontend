import React, { useState, useEffect } from "react";
import { CChartBar } from "@coreui/react-chartjs";
import DateRangePicker from "react-bootstrap-daterangepicker";
import CIcon from "@coreui/icons-react";
import { spinner } from "src/components/utils/spinner";
import { date } from "src/services/dashboard.services/dashboard.function";
import {
  GetDailyUser,
  refreshDailyUsers,
} from "src/api/dashboardAPI/dashboardGetRequest";
import Collapse from "src/components/collapse/Collapse";
import Link from "src/components/link/Link";

function DailyActiveUsers() {
  const [collapsed, setCollapsed] = useState(true);
  const [loading, setLoading] = useState(true);

  const _date = new Date();

  const day = _date.getDate();
  const year = _date.getFullYear();
  const month = _date.getMonth() + 1;

  const [dateRangePick, setDateRangePick] = useState();

  const [dailyUsers, setDailyUsers] = useState([]);
  
  GetDailyUser({ setDailyUsers, setLoading, year, month, day });

  useEffect((start, end) => {
    setDateRangePick({ start, end });
  }, []);

  const handleCallback = (start, end) => {
    setDateRangePick({ start, end });
  };

  const dailyUserCount =
    dailyUsers &&
    dailyUsers.map((dailyUser, index) => {
      return dailyUser.count;
    });

  return (
    <div>
      <div className="fade show">
        <div className="card card-accent-info">
          <div className="card-header">
            <div className="row">
              &nbsp;&nbsp; <h4>Daily Active Users</h4>
              <div className="col" align="right">
                <div className="card-header-actions">
                  <Link
                    className="card-header-action"
                    onClick={() => setCollapsed(!collapsed)}
                  >
                    <div
                      data-toggle="tooltip"
                      data-placement="top"
                      title="Show/Hide Card"
                    >
                      <CIcon
                        name={
                          collapsed ? "cil-chevron-bottom" : "cil-chevron-top"
                        }
                      />
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <Collapse show={collapsed}>
            <div className="card-body">
              <i>Select Range:</i>
              <DateRangePicker
                onCallback={handleCallback}
                onApply={refreshDailyUsers({
                  setDailyUsers,
                  setLoading,
                  dateRangePick,
                })}
              >
                <input type="text" className="form-control" />
              </DateRangePicker>
              <br />
              {!loading ? (
                <CChartBar
                  datasets={[
                    {
                      label: "Users",
                      backgroundColor: "rgba(54, 162, 235, 0.2)",
                      borderColor: "rgb(54, 162, 235)",
                      borderWidth: 1,
                      data: dailyUserCount,
                    },
                  ]}
                  options={{
                    tooltips: {
                      enabled: true,
                    },
                  }}
                  labels={date({ dailyUsers })}
                />
              ) : (
                <div style={{ textAlign: "center" }}>
                  <i>{spinner()}Loading Data, Please Wait...</i>
                </div>
              )}
            </div>
          </Collapse>
        </div>
      </div>
    </div>
  );
}
export default DailyActiveUsers;
