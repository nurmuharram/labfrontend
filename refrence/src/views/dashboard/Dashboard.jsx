import React, { useState } from 'react'
import { GetCurrentInfo } from "src/api/currentinfo/currentInfoandPermissionGetRequests";

const Dashboard = () => {

const [currentInfo, setCurrentInfo] = useState([])

GetCurrentInfo({setCurrentInfo})

  return (
    <div className='container-fluid'>
      <div className='align-items-center'>
        <div className='col'>
          <figure style={{ position: 'static' }}>
            <img className=' img-responsive img-fluid' src={`${process.env.PUBLIC_URL}/assets/img/dashboard_background.jpg`} width='100%' />
            <figcaption style={{
              position: 'absolute',
              width: '100%',
              'line-height': '5%',
              'text-align': 'left',
              paddingTop: '5%',
              paddingLeft: '3%',
              left: 0,
              top: 0,
              'text-shadow': '0 0 12px grey',
            }}>
              <h1 className='mb-0' style={{'fontSize' : '20px', 'font-family' : 'tahoma'}}><b>WELCOME TO</b></h1>
              <h2 className='mb-0' style={{'fontSize' : '46px', 'font-family' : 'tahoma'}}><b>LOKAPALA</b></h2>
              <h3 className='mb-0' style={{'fontSize' : '30px', 'font-family' : 'tahoma'}}><b>GAME MASTER TOOL!</b></h3>
              <h6 className='mt-2'>You logged in as <b>"{currentInfo.name}" </b></h6>
              <h6 className='mt-2'>Your Role: <b>{currentInfo.role_name} </b></h6>
              
            </figcaption>
          </figure>
        </div>
      </div>
    </div>
  )
}

export default Dashboard; 
