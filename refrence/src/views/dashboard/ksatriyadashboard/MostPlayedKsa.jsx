import React, { useState, useRef } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCollapse,
  CFade,
  CLink,
  CTooltip,
  CCol,
  CRow,
  CDataTable,
  CProgress,
  CSelect,
  CImg,
  CSpinner,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import DataTable from "src/components/table/DataTable";
import { ksaList } from "src/services/dashboard.services/dashboard.function";
import { spinner } from "src/components/utils/spinner";
import Link from "src/components/link/Link";
import Collapse from "src/components/collapse/Collapse";
import { ProgressBar } from "react-bootstrap";

function MostPlayedKsa({
  countsClassic,
  countsRankedBlind,
  countsRankedDraft,
  gm1isLoading,
}) {
  const [collapsed, setCollapsed] = useState(true);
  const fields = [
    { key: "KSA", _style: { width: "30%" } },
    { key: "Name", _style: { width: "40%" } },
    { key: "pick_rate", _style: { width: "50%" } },
    /* {key: 'option', label: '', _style: { width: '10%' }, filter: false} */
  ];

  const [changeList, setChangeList] = useState("0");
  const tableList = useRef();
  tableList.current = changeList;

  const tableChange = () => {
    if (tableList.current === "0") {
      return countsClassic;
    } else if (tableList.current === "1") {
      return countsRankedBlind;
    } else if (tableList.current === "2") {
      return countsRankedDraft;
    }
  };

  const classicFirstData = countsClassic[0];
  const blindFirstData = countsRankedBlind[0];
  const draftFirstData = countsRankedDraft && countsRankedDraft[0];

  const table = () => {
    return (
      <div style={{ overflow: "scroll", height: "23rem" }}>
        <DataTable
          items={tableChange()}
          fields={fields}
          responsive
          loading={gm1isLoading}
          loadingSlot
          noItemsViewSlot={
            <div style={{ textAlign: "center" }}>
              <i>{spinner()}Loading Data, Please Wait...</i>
            </div>
          }
          hover
          sorter
          sorterValue={{ column: "pick_rate", asc: false }}
          scopedSlots={{
            KSA: (item, index) => {
              return (
                <td className="py-2">
                  <CImg
                    className="border-dark shadow-lg"
                    src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${item.ksatriya_id}.png`}
                    fluid
                    shape="rounded"
                    style={{
                      width: "100%",
                      "max-width": "128px",
                      "min-width": "100px",
                      height: "auto",
                    }}
                    align="center"
                  />
                </td>
              );
            },
            Name: (item, index) => {
              return <td className="py-2">{ksaList({ item })}</td>;
            },
            pick_rate: (item, index) => {
              const matchCount = () => {
                return item.match_count
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              };
              const percentage =
                (item.match_count * 100) /
                [
                  tableList.current == "0"
                    ? classicFirstData.match_count
                    : tableList.current == "1"
                    ? blindFirstData.match_count
                    : tableList.current == "2"
                    ? draftFirstData.match_count
                    : classicFirstData.match_count,
                ];
              const result = percentage.toFixed(2);

              return (
                <tr>
                  <td className="py-4">
                    <ProgressBar
                      now={result}
                      variant="info"
                      style={{ width: "80px", height: "7px" }}
                    />
                  </td>
                  <td className="py-3">
                    <div className="row">
                      <div className="col" align="center">
                        <b>{result}%</b>
                        <br />
                        <small>({matchCount()} matches)</small>
                      </div>
                    </div>
                  </td>
                </tr>
              );
            },
          }}
        />
      </div>
    );
  };

  return (
    <div>
      <div className="fade show">
        <div className="card card-accent-primary">
          <div className="card-header">
            <div className="row">
              <div className="col" align="left">
                &nbsp;&nbsp; <h4>Most Played KSA</h4>
              </div>
              <div className="col" align="right">
                <div className="card-header-actions">
                  <Link
                    className="card-header-action"
                    onClick={() => setCollapsed(!collapsed)}
                  >
                    <div
                      data-toggle="tooltip"
                      data-placement="top"
                      title="Show/Hide Card"
                    >
                      <CIcon
                        name={
                          collapsed ? "cil-chevron-bottom" : "cil-chevron-top"
                        }
                      />
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <Collapse show={collapsed}>
            <div className="card-body">
              <div className="row">
                <div className="col" align="right">
                  <div className="col-md-5">
                    <select
                      className="custom-select"
                      onChange={(e) => setChangeList(e.target.value)}
                    >
                      <option value={1}>Classic</option>
                      <option value={2}>Ranked (Blind)</option>
                      <option value={3}>Ranked (Draft)</option>
                    </select>
                  </div>
                  <br />
                </div>
              </div>
              {table()}
            </div>
          </Collapse>
        </div>
      </div>
    </div>
  );
}

export default MostPlayedKsa;
