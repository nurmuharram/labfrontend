import React, { useState, useRef } from "react";
import { ksaList } from "src/services/dashboard.services/dashboard.function";
import CIcon from "@coreui/icons-react";
import { spinner } from "src/components/utils/spinner";
import { ProgressBar } from "react-bootstrap";
import Link from "src/components/link/Link";
import Collapse from "src/components/collapse/Collapse";
import DataTable from "src/components/table/DataTable";

function MostLossKsa({
  countsClassic,
  countsRankedBlind,
  countsRankedDraft,
  gm1isLoading,
}) {
  const [collapsed, setCollapsed] = useState(true);

  const fields = [
    { key: "KSA", _style: { width: "35%" } },
    { key: "Name", _style: { width: "30%" } },
    { key: "Ratio", _style: { width: "50%" } },
    { key: "lose_rate", _style: { width: "10%" } },
    /* {key: 'option', label: '', _style: { width: '10%' }, filter: false} */
  ];

  const [changeList, setChangeList] = useState("0");
  const tableList = useRef();
  tableList.current = changeList;

  const tableChange = () => {
    if (tableList.current === "0") {
      return countsClassic;
    } else if (tableList.current === "1") {
      return countsRankedBlind;
    } else if (tableList.current === "2") {
      return countsRankedDraft;
    }
  };

  const table = () => {
    return (
      <div style={{ overflow: "scroll", height: "23rem" }}>
        <DataTable
          items={tableChange()}
          fields={fields}
          responsive
          loading={gm1isLoading}
          loadingSlot
          noItemsViewSlot={
            <div style={{ textAlign: "center" }}>
              <i>{spinner()}Loading Data, Please Wait...</i>
            </div>
          }
          hover
          sorter
          sorterValue={{ column: "lose_rate", asc: false }}
          scopedSlots={{
            KSA: (item, index) => {
              return (
                <td className="py-2">
                  <img
                    className="border-dark shadow-lg rounded"
                    src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${item.ksatriya_id}.png`}
                    fluid
                    width="128"
                    align="center"
                  />
                </td>
              );
            },
            Name: (item, index) => {
              return <td className="py-2">{ksaList({ item })}</td>;
            },
            Ratio: (item, index) => {
              const loseCount = () => {
                return item.lose_count
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              };

              const totalMatchCount = () => {
                return item.match_count
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              };

              return (
                <tr>
                  <td className="py-4">
                    <ProgressBar
                      now={item.lose_rate}
                      variant="info"
                      style={{ width: "80px", height: "7px" }}
                    />
                  </td>
                  <td className="py-3">
                    <div className="row">
                      <div className="col" align="center">
                        <b>{item.lose_rate}%</b>
                        <br />
                        <small>
                          ({loseCount()} / {totalMatchCount()} matches)
                        </small>
                      </div>
                    </div>
                  </td>
                </tr>
              );
            },
          }}
        />
      </div>
    );
  };

  return (
    <div>
      <div className="fade show">
        <div className="card card-accent-primary">
          <div className="card-header">
            <div className="row">
              <div className="col" align="left">
                &nbsp;&nbsp; <h4>Most Loss KSA</h4>
              </div>
              <div className="col" align="right">
                <div className="card-header-actions">
                  <Link
                    className="card-header-action"
                    onClick={() => setCollapsed(!collapsed)}
                  >
                    <div
                      data-toggle="tooltip"
                      data-placement="top"
                      title="Show/Hide Card"
                    >
                      <CIcon
                        name={
                          collapsed ? "cil-chevron-bottom" : "cil-chevron-top"
                        }
                      />
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <Collapse show={collapsed}>
            <div className="card-body">
              <div className="row">
                <div className="col" align="right">
                  <div className="col-md-5" md="5">
                    <select
                      className="custom-select"
                      onChange={(e) => setChangeList(e.target.value)}
                    >
                      <option value={1}>Classic</option>
                      <option value={2}>Ranked (Blind)</option>
                      <option value={3}>Ranked (Draft)</option>
                    </select>
                  </div>
                  <br />
                </div>
              </div>
              {table()}
            </div>
          </Collapse>
        </div>
      </div>
    </div>
  );
}

export default MostLossKsa;
