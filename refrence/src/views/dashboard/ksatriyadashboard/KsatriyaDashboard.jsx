import React, { useState } from "react";
import {
  GetKsaStatCount1,
  GetKsaStatCount2,
  GetKsaStatCount3,
  GetKsaTotalKda1,
  GetKsaTotalKda2,
  GetKsaTotalKda3,
  GetKsaTotalOwned,
} from "src/api/dashboardAPI/dashboardGetRequest";
import MostKdaKsa from "./MostKdaKsa";
import MostLossKsa from "./MostLossKsa";
import MostPlayedKsa from "./MostPlayedKsa";
import MostPurchaseKsa from "./MostPurchaseKsa";
import MostWinKsa from "./MostWinKsa";

const KsatriyaDashboard = () => {
  const [countsClassic, setCountsClassic] = useState([]);
  const [countsRankedBlind, setCountsRankedBlind] = useState([]);
  const [countsRankedDraft, setCountsRankedDraft] = useState([]);
  const [mostPurchase, setMostPurchase] = useState([]);
  const [kdaClassic, setKdaClassic] = useState([]);
  const [kdaRankedBlind, setKdaRankedBlind] = useState([]);
  const [kdaRankedDraft, setKdaRankedDraft] = useState([]);

  const [gm1isLoading, setGm1isLoading] = useState(true);
  const [mostPurchaseisLoading, setMostPurchaseisLoading] = useState(true);
  const [kdaIsLoading, setKdaIsLoading] = useState(true);

  GetKsaStatCount1({ setCountsClassic, setGm1isLoading });
  GetKsaStatCount2({ setCountsRankedBlind });
  GetKsaStatCount3({ setCountsRankedDraft });
  GetKsaTotalOwned({ setMostPurchase, setMostPurchaseisLoading });
  GetKsaTotalKda1({ setKdaClassic, setKdaIsLoading });
  GetKsaTotalKda2({ setKdaRankedBlind });
  GetKsaTotalKda3({ setKdaRankedDraft });

  return (
    <div>
      <center>
        <button className="btn btn"></button>
        <div className="card" style={{ background: "#2FF795" }}>
          <h4>
            <b>In-Game Statistics</b>
          </h4>
          <i>View in-game data statistics</i>
        </div>
      </center>
      <div className="container-fluid" fluid>
        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-12">
            <MostPurchaseKsa
              mostPurchase={mostPurchase}
              mostPurchaseisLoading={mostPurchaseisLoading}
            />
          </div>
          <div className="col-xs-12 col-sm-6 col-md-12 col-xl-6 col-lg-12">
            <MostPlayedKsa
              countsClassic={countsClassic}
              countsRankedBlind={countsRankedBlind}
              countsRankedDraft={countsRankedDraft}
              gm1isLoading={gm1isLoading}
            />
          </div>
          <div className="col-xs-12 col-sm-6 col-md-12 col-xl-6 col-lg-12">
            <MostKdaKsa
              kdaClassic={kdaClassic}
              kdaRankedBlind={kdaRankedBlind}
              kdaRankedDraft={kdaRankedDraft}
              kdaIsloading={kdaIsLoading}
            />
          </div>

          <div className="col-xs-12 col-sm-6 col-md-12 col-xl-6 col-lg-12">
            <MostWinKsa
              countsClassic={countsClassic}
              countsRankedBlind={countsRankedBlind}
              countsRankedDraft={countsRankedDraft}
              gm1isLoading={gm1isLoading}
            />
          </div>
          <div className="col-xs-12 col-sm-6 col-md-12 col-xl-6 col-lg-12">
            <MostLossKsa
              countsClassic={countsClassic}
              countsRankedBlind={countsRankedBlind}
              countsRankedDraft={countsRankedDraft}
              gm1isLoading={gm1isLoading}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
export default KsatriyaDashboard;
