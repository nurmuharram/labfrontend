import React, { useState, useRef } from "react";
import CIcon from "@coreui/icons-react";
import { spinner } from "src/components/utils/spinner";
import DataTable from "src/components/table/DataTable";
import { ksaList } from "src/services/dashboard.services/dashboard.function";
import Link from "src/components/link/Link";
import Collapse from "src/components/collapse/Collapse";

function MostKdaKsa({
  kdaClassic,
  kdaRankedBlind,
  kdaRankedDraft,
  kdaIsLoading,
}) {
  const [collapsed, setCollapsed] = useState(true);

  const fields = [
    { key: "KSA", _style: { width: "10%" } },
    { key: "Name", _style: { width: "5%" } },
    { key: "KDA", _style: { width: "30%" } },
    { key: "kill_death_rate", _style: { width: "20%" } },
    /* {key: 'option', label: '', _style: { width: '10%' }, filter: false} */
  ];

  const [changeList, setChangeList] = useState("0");
  const tableList = useRef();
  tableList.current = changeList;

  const tableChange = () => {
    if (tableList.current === "0") {
      return kdaClassic;
    } else if (tableList.current === "1") {
      return kdaRankedBlind;
    } else if (tableList.current === "2") {
      return kdaRankedDraft;
    }
  };

  const table = () => {
    return (
      <div style={{ overflow: "scroll", height: "23rem" }}>
        <DataTable
          items={tableChange()}
          fields={fields}
          hover
          sorter
          loading={kdaIsLoading}
          loadingSlot
          sorterValue={{ column: "kill_death_rate", asc: false }}
          noItemsViewSlot={
            <div style={{ textAlign: "center" }}>
              <i>{spinner()}Loading Data, Please Wait...</i>
            </div>
          }
          scopedSlots={{
            KSA: (item, index) => {
              return (
                <td className="py-2">
                  <img
                    className="border-dark shadow-lg rounded"
                    src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${item.ksatriya_id}.png`}
                    fluid
                    width="128"
                    align="center"
                  />
                </td>
              );
            },
            Name: (item, index) => {
              return <td className="py-2">{ksaList({ item })}</td>;
            },
            KDA: (item, index) => {
              return (
                <td className="py-3">
                  <div className="row">
                    <div className="col">
                      <b>Kill : {""}</b>
                      {item.kill_count}
                      <br />
                      <b>Death : {""}</b>
                      {item.death_count}
                      <br />
                      <b>Assist : {""}</b>
                      {item.assist_count}
                    </div>
                  </div>
                </td>
              );
            },
          }}
        />
      </div>
    );
  };

  return (
    <div>
      <div className="fade show">
        <div className="card card-accent-primary">
          <div className="card-header">
            <div className="row">
              <div className="col" align="left">
                &nbsp;&nbsp; <h4>Most KDA KSA</h4>
              </div>
              <div className="col" align="right">
                <div className="card-header-actions">
                  <Link
                    className="card-header-action"
                    onClick={() => setCollapsed(!collapsed)}
                  >
                    <div
                      data-toggle="tooltip"
                      data-placement="top"
                      title="Show/Hide Card"
                    >
                      <CIcon
                        name={
                          collapsed ? "cil-chevron-bottom" : "cil-chevron-top"
                        }
                      />
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <Collapse show={collapsed}>
            <div className="card-body">
              <div className="row">
                <div className="col" align="right">
                  <div className="col-md-5" md="5">
                    <select
                      className="custom-select"
                      onChange={(e) => setChangeList(e.target.value)}
                    >
                      <option value={1}>Classic</option>
                      <option value={2}>Ranked (Blind)</option>
                      <option value={3}>Ranked (Draft)</option>
                    </select>
                  </div>
                  <br />
                </div>
              </div>
              {table()}
            </div>
          </Collapse>
        </div>
      </div>
    </div>
  );
}

export default MostKdaKsa;
