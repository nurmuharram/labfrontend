import React, { useState } from "react";
import CIcon from "@coreui/icons-react";
import { spinner } from "src/components/utils/spinner";
import Link from "src/components/link/Link";
import Collapse from "src/components/collapse/Collapse";
import DataTable from "src/components/table/DataTable";

function MostPurchaseKsa({ mostPurchase, mostPurchaseisLoading }) {
  const [collapsed, setCollapsed] = useState(true);
  const fields = [
    { key: "KSA", _style: { width: "25%" } },
    { key: "Name", _style: { width: "25%" } },
    { key: "Total Purchase", _style: { width: "50%" } },
    /* {key: 'option', label: '', _style: { width: '10%' }, filter: false} */
  ];

  return (
    <div>
      <div className="fade show">
        <div className="card card-accent-primary">
          <div className="card-header">
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <h3>Most Owned Ksatriya</h3>
            </div>
            <div className="row">
              <div className="col" align="right">
                <div className="card-header-actions">
                  <Link
                    className="card-header-action"
                    onClick={() => setCollapsed(!collapsed)}
                  >
                    <div
                      data-toggle="tooltip"
                      data-placement="top"
                      title="Show/Hide Card"
                    >
                      <CIcon
                        name={
                          collapsed ? "cil-chevron-bottom" : "cil-chevron-top"
                        }
                      />
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>

          <Collapse show={collapsed}>
            <div className="card-body">
              <div style={{ overflow: "scroll", height: "20rem" }}>
                <DataTable
                  items={mostPurchase}
                  fields={fields}
                  loading={mostPurchaseisLoading}
                  loadingSlot
                  noItemsViewSlot={
                    <div style={{ textAlign: "center" }}>
                      <i>{spinner()}Loading Data, Please Wait...</i>
                    </div>
                  }
                  hover
                  he
                  responsive
                  sorter
                  sorterValue={{ column: "pick_rate", asc: false }}
                  scopedSlots={{
                    KSA: (item, index) => {
                      return (
                        <td className="py-2">
                          <img
                            className="border-dark shadow-lg rounded"
                            src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${item.ksatriya_id}.png`}
                            fluid
                            width="128"
                            align="center"
                          />
                        </td>
                      );
                    },
                    Name: (item, index) => {
                      return <td className="py-2">{item.ksatriya_name}</td>;
                    },
                    "Total Purchase": (item, index) => {
                      return (
                        <td className="py-3">
                          <div className="row">
                            <div className="col" align="center">
                              {item.player_owned}
                            </div>
                          </div>
                        </td>
                      );
                    },
                  }}
                />
              </div>
            </div>
          </Collapse>
        </div>
      </div>
    </div>
  );
}

export default MostPurchaseKsa;
