import { CCard, CCardBody, CCol, CDataTable, CImg, CRow } from '@coreui/react'
import React from 'react'

function LottoTier({ lottotier }) {

  const fields = [
    { key: 'color_id' },
    { key: 'color_name' },
    { key: 'color_icon' },
    { key: 'weight' },
  ]

  return (
    <div>
      <CRow>
        <CCol>
          <CCard>
            <CCardBody>
              <CDataTable
                items={lottotier}
                fields={fields}
                hover
                itemsPerPageSelect
                itemsPerPage={5}
                pagination
                scopedSlots={{
                  'color_name':
                    (item) => {
                      return (
                        <td>
                          {item.color_name === 'Putih' ? 'White' :
                            item.color_name === 'Hijau' ? 'Green' :
                              item.color_name === 'Biru' ? 'Blue' :
                                item.color_name === 'Emas' ? 'Gold' :
                                  item.color_name === 'Pelangi' ? 'Iridescent' : item.color_name}
                        </td>
                      )
                    },
                  'color_icon':
                    (item) => {
                      return (
                        <td>
                          {item.color_id === 1 ? <><CImg src={`${process.env.PUBLIC_URL}/assets/img/color_icon/orb.white_icon.png`} fluid width='35' align='center' style={{ "pointer-events": "all" }} /></> :
                            item.color_id === 2 ? <><CImg src={`${process.env.PUBLIC_URL}/assets/img/color_icon/orb.green_icon.png`} fluid width='35' align='center' style={{ "pointer-events": "all" }} /></> :
                              item.color_id === 3 ? <><CImg src={`${process.env.PUBLIC_URL}/assets/img/color_icon/orb.blue_icon.png`} fluid width='35' align='center' style={{ "pointer-events": "all" }} /></> :
                                item.color_id === 4 ? <><CImg src={`${process.env.PUBLIC_URL}/assets/img/color_icon/orb.gold_icon.png`} fluid width='35' align='center' style={{ "pointer-events": "all" }} /></> :
                                  item.color_id === 5 ? <><CImg src={`${process.env.PUBLIC_URL}/assets/img/color_icon/orb.iridescent_icon.png`} fluid width='35' align='center' style={{ "pointer-events": "all" }} /></> : item.color_id}
                        </td>
                      )
                    }
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </div>
  )
}

export default LottoTier
