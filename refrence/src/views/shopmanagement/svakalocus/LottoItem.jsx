import React from 'react'
import axios from 'axios'

import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CFormGroup,
    CInput,
    CLabel,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    CRow,
    CSelect,
    CCollapse
} from '@coreui/react'

function LottoItem({
    lottoitem,
    addItemModal,
    setAddItemModal,
    deleteItemModal,
    setDeleteItemModal,
    setItemtype,
    setItemid,
    setAmount,
    setColor_id,
    setDefault_amount,
    setItem_name,
    refreshData,
    itemtype,
    itemid,
    amount,
    color_id,
    default_amount,
    item_name,
    dropdownItemChange,
    dropdownLottoTier,
    addLottoItem,
    toggleDetails,
    details,
}) {

    const fields = [
        { key: 'lotto_item_id' },
        { key: 'item_type_name' },
        { key: 'item_id' },
        { key: 'item_name' },
        { key: 'amount' },
        { key: 'default_amount' },
        { key: 'color_id' },
        {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        }
    ]


    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            <CButton color='primary' onClick={() => setAddItemModal(!addItemModal)}>+ Add Item to Lotto</CButton>
                            <CModal
                                show={addItemModal}
                                onClose={() => setAddItemModal(!addItemModal)}
                                color="primary"
                                size="md"
                            >
                                <CModalHeader closeButton>
                                    <CModalTitle>
                                        Add Lotto Item
                                    </CModalTitle>
                                </CModalHeader>
                                <CModalBody>
                                    <CCol>
                                        <CFormGroup row>
                                            <CCol md="4">
                                                <CLabel htmlFor="input"><b>Item Type</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="6">
                                                <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setItemtype(e.target.value)}>
                                                    <option value={''}>Choose Item Type</option>
                                                    <option value={1}  >Currency</option>
                                                    <option value={6}  >Box</option>
                                                    <option value={5}  >Items</option>
                                                    <option value={2}  >Ksatriya</option>
                                                    <option value={3}  >KSA Skins</option>
                                                    <option value={4}  >Rune</option>
                                                    <option value={11} >Frame</option>
                                                    <option value={12} >Avatar</option>
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="4">
                                                <CLabel htmlFor="input"><b>Item ID</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol md='6'>
                                                <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setItemid(e.target.value)}>
                                                    {dropdownItemChange()}
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="4">
                                                <CLabel htmlFor="input"><b>Amount</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="6">
                                                <CInput type='number'  placeholder='Set Amount...' onChange={(e) => setAmount(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="4">
                                                <CLabel htmlFor="input"><b>Color id</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="6">
                                                <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setColor_id(e.target.value)}>
                                                    {dropdownLottoTier}
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="4">
                                                <CLabel htmlFor="input"><b>Default Amount</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="6">
                                                <CInput type='number' placeholder='Set Default Amount... ' onChange={(e) => setDefault_amount(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="4">
                                                <CLabel htmlFor="input"><b>Item Name</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="6">
                                                <CInput type='text' placeholder='Set Item Name...' onChange={(e) => setItem_name(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                    </CCol>
                                </CModalBody>
                                <CModalFooter>
                                    <CButton color='primary' onClick={addLottoItem}>
                                        Add Lotto
                                    </CButton>
                                </CModalFooter>
                            </CModal>
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={lottoitem}
                                fields={fields}
                                hover
                                itemsPerPage={5}
                                itemsPerPageSelect
                                pagination
                                scopedSlots={{
                                    'show_details':
                                        (item, index) => {

                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        color="primary"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(index) }}
                                                    >
                                                        {details.includes(index) ? 'Close' : 'Edit Item'}
                                                    </CButton>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (item, index) => {

                                            const updateItem = async () => {
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('item_type', parseInt(itemtype));
                                                data.append('item_id', parseInt(itemid))
                                                data.append('amount', amount)
                                                data.append('color_id', parseInt(color_id));
                                                data.append('default_amount', default_amount)
                                                data.append('item_name', item_name)

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/lotto/updateItem?id=${item.lotto_item_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };

                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        alert('Failed to Update!')
                                                    });
                                                setTimeout(refreshData, 100)
                                            };

                                            const deleteItem = () => {

                                                const config = {
                                                    method: 'delete',
                                                    url: `/api/lotto/deleteItem?id=${item.lotto_item_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth')
                                                    }
                                                };

                                                axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Removed!');
                                                        setDeleteItemModal(false)
                                                    })
                                                    .catch((error) => {
                                                        alert('Failed to remove item!');
                                                    });
                                                setTimeout(refreshData, 100);
                                            };

                                            return (
                                                <CCollapse show={details.includes(index)}>
                                                    <CCardBody>
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <h4>
                                                                    <i>Edit/Update (<small>Lotto ID : {item.lotto_item_id}</small>)</i>
                                                                </h4>
                                                            </CCol>
                                                            <CCol align='left' md='2'>
                                                                <CButton size="sm" color="danger" variant='outline' shape='square' onClick={() => setDeleteItemModal(!deleteItemModal)}>
                                                                    Delete Item</CButton>
                                                                <CModal
                                                                    show={deleteItemModal}
                                                                    onClose={() => setDeleteItemModal(!deleteItemModal)}
                                                                    color="danger"
                                                                    size="md"
                                                                >
                                                                    <CModalHeader closeButton>
                                                                        <CModalTitle>
                                                                            <b>Delete this item?</b>
                                                                        </CModalTitle>
                                                                    </CModalHeader>
                                                                    <CModalBody>
                                                                        <CCol align='center'>
                                                                            <h5>Are you sure?</h5>
                                                                        </CCol>
                                                                    </CModalBody>
                                                                    <CModalFooter>
                                                                        <CRow>
                                                                            <CCol align='center'>
                                                                                <CButton type="send" size="sm" color="danger" onClick={deleteItem} ><b>Delete</b></CButton>&nbsp;
                                                                                <CButton type="send" size="sm" color="secondary" onClick={() => setDeleteItemModal(!deleteItemModal)} ><b>Cancel</b></CButton>
                                                                            </CCol>
                                                                        </CRow>
                                                                    </CModalFooter>
                                                                </CModal>
                                                            </CCol>
                                                        </CRow>
                                                        <hr />
                                                        <CRow>
                                                            <CCol>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Item Type</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setItemtype(e.target.value)}>
                                                                            <option value={''}>Choose Item Type</option>
                                                                            <option value={1}  >Currency</option>
                                                                            <option value={6}  >Box</option>
                                                                            <option value={5}  >Items</option>
                                                                            <option value={2}  >Ksatriya</option>
                                                                            <option value={3}  >KSA Skins</option>
                                                                            <option value={4}  >Rune</option>
                                                                            <option value={11} >Frame</option>
                                                                            <option value={12} >Avatar</option>
                                                                        </CSelect>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Item ID</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol md='4'>
                                                                        <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setItemid(e.target.value)}>
                                                                            {dropdownItemChange()}
                                                                        </CSelect>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Amount</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CInput type='text' placeholder='Set Amount...' onChange={(e) => setAmount(e.target.value)} />
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Color id</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setColor_id(e.target.value)}>
                                                                            {dropdownLottoTier}
                                                                        </CSelect>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Default Amount</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CInput type='text' placeholder='Set Default Amount... ' onChange={(e) => setDefault_amount(e.target.value)} />
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Item Name</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CInput type='text' placeholder='Set Item Name...' onChange={(e) => setItem_name(e.target.value)} />
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="6" align='right'>
                                                                        <CButton color='primary' onClick={updateItem}><b>Update</b></CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                            </CCol>
                                                        </CRow>
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        },
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default LottoItem