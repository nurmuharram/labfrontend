import { CCard, CCardBody, CCardHeader, CCol, CContainer, CNav, CNavItem, CNavLink, CTabContent, CTabPane, CTabs } from '@coreui/react'
import React from 'react'

import LottoFeatureservices from 'src/services/svakalocus.services/LottoFeature.services'
import LottoItemservices from 'src/services/svakalocus.services/LottoItem.services'
import LottoListservices from 'src/services/svakalocus.services/LottoList.services'
import LottoLootservices from 'src/services/svakalocus.services/LottoLoot.services'
import LottoTierservices from 'src/services/svakalocus.services/LottoTier.services'

function SvakaLocus() {
    return (
        <div>
            <CCol align='center'>
                <CCard style={{ background: '#9bf54c' }}>
                    <h4><b>Svaka Locus</b></h4>
                    <h6><i>Svaka Locus Settings</i></h6>
                </CCard>
            </CCol>
            <br />
            <CContainer>
                <CCard accentColor='primary'>
                    <CCardHeader>
                        Svaka Locus
                    </CCardHeader>
                    <CCardBody>
                        <CTabs>
                            <CNav variant='tabs'>
                                <CNavItem>
                                    <CNavLink>
                                        Lotto List
                                    </CNavLink>
                                </CNavItem>
                                <CNavItem>
                                    <CNavLink>
                                        Lotto Loot
                                    </CNavLink>
                                </CNavItem>
                                <CNavItem>
                                    <CNavLink>
                                        Lotto Feature
                                    </CNavLink>
                                </CNavItem>
                                <CNavItem>
                                    <CNavLink>
                                        Lotto Item
                                    </CNavLink>
                                </CNavItem>
                                <CNavItem>
                                    <CNavLink>
                                        Lotto Tier
                                    </CNavLink>
                                </CNavItem>
                            </CNav>
                            <CTabContent>
                                <CTabPane>
                                    <LottoListservices/>
                                </CTabPane>
                                <CTabPane>
                                    <LottoLootservices/>
                                </CTabPane>
                                <CTabPane>
                                    <LottoFeatureservices/>
                                </CTabPane>
                                <CTabPane>
                                    <LottoItemservices/>
                                </CTabPane>
                                <CTabPane>
                                   <LottoTierservices/>
                                </CTabPane>
                            </CTabContent>
                        </CTabs>
                    </CCardBody>
                </CCard>
            </CContainer>
        </div>
    )
}

export default SvakaLocus
