import React from 'react'

import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CModal,
    CModalBody,
    CModalHeader,
    CRow,
    CInput,
    CFormGroup,
    CLabel,
    CModalFooter,
    CModalTitle,

} from '@coreui/react'

function LottoList({lottolist, addItemModal, setAddItemModal, setStartdate, setStarttime, setEnddate, setEndtime, addLottoPeriod}) {
    
    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            <CButton color='primary' onClick={() => setAddItemModal(!addItemModal)}>+ Add Lotto Period</CButton>
                            <CModal
                                show={addItemModal}
                                onClose={() => setAddItemModal(!addItemModal)}
                                color="primary"
                                size="md"
                            >
                                <CModalHeader closeButton>
                                    <CModalTitle>
                                        Add Lotto Period
                                    </CModalTitle>
                                </CModalHeader>
                                <CModalBody>
                                    <CCol>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Start date</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="4">
                                                <CInput type='date' onKeyDown={(e) => e.preventDefault()} onChange={(e) => setStartdate(e.target.value)} />
                                            </CCol>
                                            <CCol xs="12" md="4">
                                                <CInput type='time' onChange={(e) => setStarttime(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>End date</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="4">
                                                <CInput type='date' onKeyDown={(e) => e.preventDefault()} onChange={(e) => setEnddate(e.target.value)} />
                                            </CCol>
                                            <CCol xs="12" md="4">
                                                <CInput type='time' onChange={(e) => setEndtime(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                    </CCol>
                                </CModalBody>
                                <CModalFooter>
                                    <CButton color='primary' onClick={addLottoPeriod}>
                                        Add Lotto
                                    </CButton>
                                </CModalFooter>
                            </CModal>
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={lottolist}
                                hover
                                itemsPerPageSelect
                                itemsPerPage={5}
                                pagination
                                scopedSlots={{
                                    'start_date':
                                        (item) => {

                                            const date = item.start_date
                                            const splitdate = date.split(' ')
                                            const joindate = splitdate.join('T')
                                            const joindatez = joindate + '.000Z'

                                            const dateconvert = new Date(joindatez);

                                            return (
                                                <td>
                                                    {dateconvert.toString()}
                                                </td>)
                                        },
                                    'end_date':
                                        (item) => {

                                            const date = item.end_date
                                            const splitdate = date.split(' ')
                                            const joindate = splitdate.join('T')
                                            const joindatez = joindate + '.000Z'

                                            const dateconvert = new Date(joindatez);

                                            return (
                                                <td>
                                                    {dateconvert.toString()}
                                                </td>)
                                        },
                                    }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default LottoList
