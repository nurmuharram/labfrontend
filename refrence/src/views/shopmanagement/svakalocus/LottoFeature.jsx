import React, { useEffect, useState } from 'react'
import axios from 'axios'

import { 
    CButton, 
    CCard, 
    CCardBody, 
    CCardHeader, 
    CCol, 
    CDataTable, 
    CRow,
    CInput,
    CFormGroup,
    CLabel,
    CModal,
    CModalBody,
    CModalHeader,
    CModalTitle,
    CModalFooter,
    CCollapse, 
    CSelect
} from '@coreui/react'

function LottoFeature({
    lottofeature, 
    addItemModal, 
    setAddItemModal, 
    setLotto_id, 
    setLotto_item_id, 
    setPriority, 
    deleteItemModal, 
    setDeleteItemModal, 
    details,
    toggleDetails,
    dropdownLottoId,
    dropdownLottoItemId,
    addLottoFeatured,
    refreshData,
    lotto_id,
    lotto_item_id,
    priority
}) {
    
    const fields = [
        { key: 'lotto_feature_id' },
        { key: 'lotto_id' },
        { key: 'lotto_item_id' },
        { key: 'lotto_item_name' },
        { key: 'priority' },
        {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        }
    ]

    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            <CButton color='primary' onClick={() => setAddItemModal(!addItemModal)}>+ Add Lotto to Featured</CButton>
                            <CModal
                                show={addItemModal}
                                onClose={() => setAddItemModal(!addItemModal)}
                                color="primary"
                                size="md"
                            >
                                <CModalHeader closeButton>
                                    <CModalTitle>
                                    Add Lotto to Featured
                                    </CModalTitle>
                                </CModalHeader>
                                <CModalBody>
                                    <CCol>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Lotto ID</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CSelect type='text' onChange={(e) => setLotto_id(e.target.value)}>
                                                {dropdownLottoId}    
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Lotto Item Id</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CSelect type='text' onChange={(e) => setLotto_item_id(e.target.value)}>
                                                    {dropdownLottoItemId}
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Priority</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CInput type='number' placeholder='Set Lotto priority...' onChange={(e) => setPriority(e.target.value)}/>
                                            </CCol>
                                        </CFormGroup>
                                    </CCol>
                                </CModalBody>
                                <CModalFooter>
                                    <CButton color='primary' onClick={addLottoFeatured}>
                                        Add
                                    </CButton>
                                </CModalFooter>
                            </CModal>
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={lottofeature}
                                fields={fields}
                                hover
                                itemsPerPage={5}
                                itemsPerPageSelect
                                pagination
                                scopedSlots={{
                                    'show_details':
                                        (item, index) => {

                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        color="primary"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(index) }}
                                                    >
                                                        {details.includes(index) ? 'Close' : 'Edit Item'}
                                                    </CButton>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (item, index) => {

                                            const updateItemFeatured = async () => {
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('lotto_id', lotto_id);
                                                data.append('lotto_item_id', lotto_item_id)
                                                data.append('priority', priority)

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/lotto/updateFeature?id=${item.lotto_feature_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };

                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        alert('Failed to Update!')
                                                    });
                                                setTimeout(refreshData, 100)
                                            };

                                            const deleteItemFeatured = () => {
                                                const config = {
                                                    method: 'delete',
                                                    url: `/api/lotto/deleteFeature?id=${item.lotto_feature_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth')
                                                    }
                                                };

                                                axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Removed!');
                                                        setDeleteItemModal(false)
                                                    })
                                                    .catch((error) => {
                                                        alert('Failed to remove item!');
                                                    });
                                                setTimeout(refreshData, 100);
                                            };

                                            return (
                                                <CCollapse show={details.includes(index)}>
                                                    <CCardBody>
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <h4>
                                                                    <i>Edit/Update (<small>Lotto ID : {item.lotto_feature_id}</small>)</i>
                                                                </h4>
                                                            </CCol>
                                                            <CCol align='left' md='2'>
                                                                <CButton size="sm" color="danger" variant='outline' shape='square' onClick={() => setDeleteItemModal(!deleteItemModal)}>
                                                                    Delete Item</CButton>
                                                                <CModal
                                                                    show={deleteItemModal}
                                                                    onClose={() => setDeleteItemModal(!deleteItemModal)}
                                                                    color="danger"
                                                                    size="md"
                                                                >
                                                                    <CModalHeader closeButton>
                                                                        <CModalTitle>
                                                                            <b>Delete this item?</b>
                                                                        </CModalTitle>
                                                                    </CModalHeader>
                                                                    <CModalBody>
                                                                        <CCol align='center'>
                                                                            <h5>Are you sure?</h5>
                                                                        </CCol>
                                                                    </CModalBody>
                                                                    <CModalFooter>
                                                                        <CRow>
                                                                            <CCol align='center'>
                                                                                <CButton type="send" size="sm" color="danger" onClick={deleteItemFeatured} ><b>Delete</b></CButton>&nbsp;
                                                                                <CButton type="send" size="sm" color="secondary" onClick={() => setDeleteItemModal(!deleteItemModal)} ><b>Cancel</b></CButton>
                                                                            </CCol>
                                                                        </CRow>
                                                                    </CModalFooter>
                                                                </CModal>
                                                            </CCol>
                                                        </CRow>
                                                        <hr/>
                                                        <CRow>
                                                            <CCol>
                                                            <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Lotto ID</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CSelect type='text' onChange={(e) => setLotto_id(e.target.value)}>
                                                                            {dropdownLottoId}
                                                                        </CSelect>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Lotto Item Id</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CSelect type='text' onChange={(e) => setLotto_item_id(e.target.value)}>
                                                                            {dropdownLottoItemId}
                                                                        </CSelect>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Priority</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CInput type='text' placeholder='Set Lotto priority...' onChange={(e) => setPriority(e.target.value)} />
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="6" align='right'>
                                                                        <CButton color='primary' onClick={updateItemFeatured}><b>Update</b></CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                            </CCol>
                                                        </CRow>
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        },
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default LottoFeature
