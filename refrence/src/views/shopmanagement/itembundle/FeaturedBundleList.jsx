import React from "react";
import { FeaturedBundleDataTable } from "./components/dataTable/ItemBundleDataTable";

function FeaturedBundleList({ featuredBundleList, setFeaturedBundleList }) {
  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <FeaturedBundleDataTable
                featuredBundleList={featuredBundleList}
                setFeaturedBundleList={setFeaturedBundleList}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FeaturedBundleList;
