import React from "react";
import { DeleteFeaturedBundle } from "src/api/itemBundleApi/ItemBundleDeleteRequest";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from "src/components/modal";

export const DeleteFeaturedBundleModal = ({
  deleteItemModal,
  setDeleteItemModal,
  setFeaturedBundleList,
  item,
}) => {
  return (
    <div>
      <Modal
        show={deleteItemModal}
        onClose={() => setDeleteItemModal(!deleteItemModal)}
        color="danger"
        size="md"
      >
        <ModalHeader closeButton>
          <ModalTitle>
            <b>Delete this item?</b>
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <div className="col" align="center">
            <h5>Are you sure?</h5>
          </div>
        </ModalBody>
        <ModalFooter>
          <div className="row">
            <div className="col" align="center">
              <DeleteFeaturedBundle
                setFeaturedBundleList={setFeaturedBundleList}
                setDeleteItemModal={setDeleteItemModal}
                item={item}
              />
              &nbsp;
              <button
                className="btn btn-secondary btn-sm"
                onClick={() => setDeleteItemModal(!deleteItemModal)}
              >
                <b>Cancel</b>
              </button>
            </div>
          </div>
        </ModalFooter>
      </Modal>
    </div>
  );
};
