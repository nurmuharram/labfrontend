import React, { useState } from "react";
import Collapse from "src/components/collapse/Collapse";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from "src/components/modal";
import DataTable from "src/components/table/DataTable";
import { toggleDetails } from "src/services/utils/toggleDetails";
import { DeleteItemBundle } from "src/api/itemBundleApi/ItemBundleDeleteRequest";
import { FeaturedBundleForms, ItemBundleForms } from "../form/ItemBundleForms";
import { DeleteFeaturedBundleModal } from "../modal/ItemBundleModal";

export const ItemBundleDataTable = ({ bundleList, setBundleList }) => {
  const [details, setDetails] = useState([]);
  const [deleteItemModal, setDeleteItemModal] = useState(false);
  const [newItemId, setNewItemId] = useState("");
  const [itemType, setNewItemType] = useState(1);
  const [newAmount, setNewAmount] = useState("");
  const fields = [
    { key: "shop_id", _style: { width: "8%" } },
    { key: "item_id", _style: { width: "8%" } },
    { key: "item_name", _style: { width: "10%" } },
    { key: "item_type", _style: { width: "11%" } },
    { key: "item_type_name", _style: { width: "11%" } },
    { key: "amount", _style: { width: "11%" } },
    {
      key: "show_details",
      label: "",
      _style: { width: "10%" },
      sorter: false,
      filter: false,
    },
  ];
  return (
    <div>
      <DataTable
        items={bundleList}
        fields={fields}
        tableFilter
        itemsPerPageSelect
        itemsPerPage={10}
        hover
        sorter
        pagination
        scopedSlots={{
          show_details: (item, index) => {
            return (
              <td className="py-2">
                <button
                  className="btn btn-outline-primary btn-sm btn-square"
                  onClick={() => {
                    toggleDetails(index, { setDetails, details });
                  }}
                >
                  {details.includes(index) ? "Close" : "Edit Item"}
                </button>
              </td>
            );
          },
          details: (item, index) => {
            return (
              <Collapse show={details.includes(index)}>
                <div className="card-body">
                  <div className="row">
                    <div className="col" align="left">
                      <h4>
                        <i>
                          Edit/Update Item (
                          <small>Shop ID : {item.shop_id}</small>)
                        </i>
                      </h4>
                    </div>
                    <div className="col-md-2" align="left">
                      <button
                        className="btn btn-sm btn-outline-danger btn-square"
                        onClick={() => setDeleteItemModal(!deleteItemModal)}
                      >
                        Delete Item
                      </button>
                      <Modal
                        show={deleteItemModal}
                        onClose={() => setDeleteItemModal(!deleteItemModal)}
                        color="danger"
                        size="md"
                      >
                        <ModalHeader closeButton>
                          <ModalTitle>
                            <b>Delete this item?</b>
                          </ModalTitle>
                        </ModalHeader>
                        <ModalBody>
                          <div className="col" align="center">
                            <h5>Are you sure?</h5>
                          </div>
                        </ModalBody>
                        <ModalFooter>
                          <div className="row">
                            <div className="col" align="center">
                              <DeleteItemBundle
                                setBundleList={setBundleList}
                                setDeleteItemModal={setDeleteItemModal}
                                item={item}
                              />
                              &nbsp;
                              <button
                                className="btn btn-secondary btn-sm"
                                type="send"
                                onClick={() =>
                                  setDeleteItemModal(!deleteItemModal)
                                }
                              >
                                <b>Cancel</b>
                              </button>
                            </div>
                          </div>
                        </ModalFooter>
                      </Modal>
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col" align="left">
                      <ItemBundleForms
                        setBundleList={setBundleList}
                        setNewAmount={setNewAmount}
                        setNewItemId={setNewItemId}
                        setNewItemType={setNewItemType}
                        itemType={itemType}
                        item={item}
                        newItemId={newItemId}
                        newAmount={newAmount}
                      />
                    </div>
                  </div>
                </div>
              </Collapse>
            );
          },
        }}
      />
    </div>
  );
};

export const FeaturedBundleDataTable = ({
  featuredBundleList, setFeaturedBundleList
}) => {
  const [details, setDetails] = useState([]);
  const [deleteItemModal, setDeleteItemModal] = useState(false);
  const fields = [
    { key: 'shop_id', _style: { width: '5%' } },
    { key: 'description', _style: { width: '15%' } },
    { key: 'start_date', _style: { width: '8%' } },
    { key: 'end_date', _style: { width: '10%' } },
    { key: 'priority', _style: { width: '11%' } },
    {
        key: 'show_details',
        label: '',
        _style: { width: '10%' },
        sorter: false,
        filter: false
    }
]

  return (
    <div>
      <DataTable
        items={featuredBundleList}
        fields={fields}
        hover
        sorter
        pagination
        itemsPerPage={10}
        itemsPerPageSelect
        tableFilter
        scopedSlots={{
          start_date: (item, index) => {
            const date = new Date(
              (item.start_date && item.start_date.split(" ").join("T")) +
                ".000Z"
            );
            return <td>{date.toString()}</td>;
          },
          end_date: (item, index) => {
            const date = new Date(
              (item.end_date && item.end_date.split(" ").join("T")) + ".000Z"
            );
            return (
              <td>
                {item.end_date === null ? "No End Date" : date.toString()}
              </td>
            );
          },
          show_details: (item, index) => {
            return (
              <td className="py-2">
                <button
                  className="btn btn-outline-primary btn-square btn-sm"
                  color="primary"
                  variant="outline"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    toggleDetails(index, { setDetails, details });
                  }}
                >
                  {details.includes(index) ? "Close" : "Edit Item"}
                </button>
              </td>
            );
          },
          details: (item, index) => {
            return (
              <Collapse show={details.includes(index)}>
                <div className="card-body">
                  <div className="row">
                    <div className="col" align="left">
                      <h4>
                        <i>
                          Edit/Update Item (
                          <small>Shop ID : {item.shop_id}</small>)
                        </i>
                      </h4>
                    </div>
                    <div className="col-md-2" align="left">
                      <button
                        className="btn btn-outline-danger btn-sm btn-square"
                        size="sm"
                        color="danger"
                        variant="outline"
                        shape="square"
                        onClick={() => setDeleteItemModal(!deleteItemModal)}
                      >
                        Delete Item
                      </button>
                      <DeleteFeaturedBundleModal
                        deleteItemModal={deleteItemModal}
                        setDeleteItemModal={setDeleteItemModal}
                        setFeaturedBundleList={setFeaturedBundleList}
                        item={item}
                      />
                    </div>
                  </div>
                  <hr />
                  <FeaturedBundleForms
                    setFeaturedBundleList={setFeaturedBundleList}
                    item={item}
                  />
                </div>
              </Collapse>
            );
          },
        }}
      />
    </div>
  );
};
