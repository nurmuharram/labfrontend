import React, { useState } from "react";
import { Form } from "react-bootstrap";
import {
  UpdateFeaturedBundlePriority,
  UpdateFeaturedBundleStartDateEndDate,
  UpdateItemBundle,
} from "src/api/itemBundleApi/ItemBundlePutRequest";
import { showItemSelected } from "src/services/itemBundle.services/itemBundle.Function";
import { SelectItemId } from "src/services/utils/selectItemId";

export const ItemBundleForms = ({
  setBundleList,
  setNewAmount,
  setNewItemId,
  setNewItemType,
  itemType,
  item,
  newItemId,
  newAmount,
}) => {
  return (
    <div>
      <Form>
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label>
              <b>New Item Type</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-3 col-md-3">
            <select
              className="align-items-md-start custom-select"
              onChange={(e) => setNewItemType(e.target.value)}
            >
              <option value={""}>Choose Item Type</option>
              <option value={1}>Currency</option>
              <option value={6}>Box</option>
              <option value={5}>Items</option>
              <option value={2}>Ksatriya</option>
              <option value={3}>KSA Skins</option>
              <option value={4}>Rune</option>
              <option value={11}>Frame</option>
              <option value={12}>Avatar</option>
            </select>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-2" md="2">
            <Form.Label>
              <b>New Item Id</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-3 col-md-3">
            <select
              className="align-items-md-start custom-select"
              onChange={(e) => setNewItemId(e.target.value)}
            >
              <SelectItemId itemType={itemType} />
            </select>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label>
              <b>New Amount</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-3">
            <Form.Control
              type="number"
              placeholder={item.amount}
              onChange={(e) => setNewAmount(e.target.value)}
            />
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-5" align="right">
            <UpdateItemBundle
              item={item}
              itemType={itemType}
              newItemId={newItemId}
              newAmount={newAmount}
              setBundleList={setBundleList}
            />
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};

export const AddItemBundleForms = ({
  itemType,
  setItemType,
  itemId,
  setItemid,
  amount,
  setAmount,
  setPriceCoin,
  setPriceCitrine,
  setPriceLotus,
  setReleasedate,
  setReleasetime,
  setDescription,
  arrayData,
  setArrayData,
}) => {
  return (
    <div>
      <Form>
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label>
              <b>Item Type</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-5">
            <select
              className="align-items-md-start custom-select"
              onChange={(e) => setItemType(e.target.value)}
            >
              <option value={""}>Choose Item Type</option>
              <option value={1}>Currency</option>
              <option value={6}>Box</option>
              <option value={5}>Items</option>
              <option value={2}>Ksatriya</option>
              <option value={3}>KSA Skins</option>
              <option value={4}>Rune</option>
              <option value={11}>Frame</option>
              <option value={12}>Avatar</option>
              <option value={15}>Ksa Fragment</option>
              <option value={16}>Skin parts</option>
            </select>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>Item Name</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-5">
            <select
              className="align-items-md-start custom-select"
              onChange={(e) => setItemid(e.target.value)}
            >
              <SelectItemId itemType={itemType} />
            </select>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>Amount</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-5">
            <Form.Control
              type="number"
              placeholder="Set Amount..."
              onChange={(e) => setAmount(e.target.value)}
            />
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-xs-12 col-md-7" align="right">
            <button
              className="btn btn-md btn-info"
              onClick={(e) => {
                setArrayData([
                  ...arrayData,
                  {
                    item_type: parseInt(itemType),
                    item_id: parseInt(itemId),
                    amount: parseInt(amount),
                  },
                ]);
                e.preventDefault();
              }}
            >
              <b>+ Add Item</b>
            </button>
          </div>
        </Form.Group>
        <hr />
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>Bundle Items</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-5">
            {showItemSelected({ arrayData, setArrayData })}
          </div>
        </Form.Group>
        <hr />
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>Price coin</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-5">
            <Form.Control
              type="number"
              placeholder="Set Price Coin..."
              onChange={(e) => setPriceCoin(e.target.value)}
            />
            <small>
              <i>
                *Leave this form blank if you want to set the items' price
                without this currency
              </i>
            </small>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>Price Citrine</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-5">
            <Form.Control
              type="number"
              placeholder="Set Price Citrine..."
              onChange={(e) => setPriceCitrine(e.target.value)}
            />
            <small>
              <i>
                *Leave this form blank if you want to set the items' price
                without this currency
              </i>
            </small>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>Price Lotus</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-5">
            <Form.Control
              type="number"
              placeholder="Set Price Lotus..."
              onChange={(e) => setPriceLotus(e.target.value)}
            />
            <small>
              <i>
                *Leave this form blank if you want to set the items' price
                without this currency
              </i>
            </small>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>Release date</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-3">
            <Form.Control
              type="date"
              onKeyDown={(e) => e.preventDefault()}
              onChange={(e) => setReleasedate(e.target.value)}
            />
          </div>
          <div className="col-xs-12 col-md-2" xs="12" md="2">
            <Form.Control
              type="time"
              onChange={(e) => setReleasetime(e.target.value)}
            />
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>Description</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-5">
            <Form.Control
              as="textarea"
              placeholder="Set Item Description..."
              onChange={(e) => setDescription(e.target.value)}
            />
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};

export const FeaturedBundleForms = ({ setFeaturedBundleList, item }) => {
  const [startDate, setStartDate] = useState("2019-10-10");
  const [startTime, setStartTime] = useState("00:00");

  const [endDate, setEndDate] = useState("2019-10-10");
  const [endTime, setEndTime] = useState("00:00");

  const [priority, setPriority] = useState();
  return (
    <div>
      <Form>
        <div className="row">
        <div className="col" align="left">
          <Form.Group className="row mb-3">
            <div className="col-md-3">
              <Form.Label htmlFor="input">
                <b>New Start date</b>
              </Form.Label>
            </div>
            :
            <div className="col-xs-12 col-md-4">
              <Form.Control
                type="date"
                onKeyDown={(e) => e.preventDefault()}
                onChange={(e) => setStartDate(e.target.value)}
              />
            </div>
            <div className="col-xs-12 col-md-4">
              <Form.Control
                type="time"
                onChange={(e) => setStartTime(e.target.value)}
              />
            </div>
          </Form.Group>
          <Form.Group className="row mb-4">
            <div className="col-md-3">
              <Form.Label htmlFor="input">
                <b>New End date</b>
              </Form.Label>
            </div>
            :
            <div className="col-xs-12 col-md-4">
              <Form.Control
                type="date"
                onKeyDown={(e) => e.preventDefault()}
                onChange={(e) => setEndDate(e.target.value)}
              />
            </div>
            <div className="col-xs-12 col-md-4">
              <Form.Control
                type="time"
                onChange={(e) => setEndTime(e.target.value)}
              />
            </div>
          </Form.Group>
          <Form.Group className="row mb-4">
            <div className="col-md-11" align="right">
              <UpdateFeaturedBundleStartDateEndDate
                setFeaturedBundleList={setFeaturedBundleList}
                item={item}
                startDate={startDate}
                startTime={startTime}
                endDate={endDate}
                endTime={endTime}
              />
            </div>
          </Form.Group>
        </div>
        <div className="col" align="right">
          <Form.Group className="row mb-3">
            <div className="col-md-3">
              <Form.Label htmlFor="input">
                <b>New Priority</b>
              </Form.Label>
            </div>
            :
            <div className="col-xs-12 col-md-6">
              <Form.Control
                type="number"
                min={50}
                max={100}
                placeholder="Set New Priority value from 50 - 100"
                onChange={(e) => setPriority(e.target.value)}
              />
            </div>
            <UpdateFeaturedBundlePriority
              setFeaturedBundleList={setFeaturedBundleList}
              priority={priority}
              item={item}
            />
          </Form.Group>
        </div>
        </div>
      </Form>
    </div>
  );
};

export const AddFeaturedBundleForms = ({
  setShopId,
  setPriority,
  setStartDate,
  setStartTime,
  setEndDate,
  setEndTime,
  setMaxBuy,
  bundle
}) => {
  return (
    <div>
      <Form className="ml-3">
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>Select Item Bundle</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-5">
            <select
              className="align-items-md-start custom-select"
              onChange={(e) => setShopId(e.target.value)}
            >
              {bundle}
            </select>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>Priority</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-5">
            <Form.Control
              type="number"
              min={50}
              max={100}
              placeholder="Set Priority value from 50 - 100"
              onChange={(e) => setPriority(e.target.value)}
            />
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>Start date</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-3">
            <Form.Control
              type="date"
              onKeyDown={(e) => e.preventDefault()}
              onChange={(e) => setStartDate(e.target.value)}
            />
          </div>
          <div className="col-xs-12 col-md-2">
            <Form.Control
              type="time"
              onChange={(e) => setStartTime(e.target.value)}
            />
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>End date</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-3">
            <Form.Control
              type="date"
              onKeyDown={(e) => e.preventDefault()}
              onChange={(e) => setEndDate(e.target.value)}
            />
          </div>
          <div className="col-xs-12 col-md-2">
            <Form.Control
              type="time"
              onChange={(e) => setEndTime(e.target.value)}
            />
          </div>
        </Form.Group>

        <Form.Group className="row mb-3">
          <div className="col-md-2">
            <Form.Label htmlFor="input">
              <b>Max Buy</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-5">
            <Form.Control
              type="number"
              placeholder="Set Items` Max Buy"
              onChange={(e) => setMaxBuy(e.target.value)}
            />
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};
