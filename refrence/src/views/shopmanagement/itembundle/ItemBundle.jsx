import React, { useState } from 'react'
import axios from 'axios'
import { GetFeaturedBundle, GetItemBundle } from 'src/api/itemBundleApi/ItemBundleGetRequest'
import FeaturedBundleList from './FeaturedBundleList'
import CreateFeaturedBundle from './CreateFeaturedBundle'
import { Tab, Tabs } from "react-bootstrap";
import { ItemBundleDataTable } from './components/dataTable/ItemBundleDataTable'
import { AddItemBundle } from 'src/api/itemBundleApi/ItemBundlePostRequest'
import { AddItemBundleForms } from './components/form/ItemBundleForms'

function ItemBundle() {
    const [key, setKey] = useState('FeaturedBundleList')

    const [bundleList, setBundleList] = useState([])
    const [featuredBundleList, setFeaturedBundleList] = useState([])

    const [arrayData, setArrayData] = useState([])
    
    const [itemType, setItemType] = useState(1)
    const [itemId, setItemid] = useState(1)
    const [amount, setAmount] = useState()
    const [priceCoin, setPriceCoin] = useState('')
    const [priceCitrine, setPriceCitrine] = useState('')
    const [priceLotus, setPriceLotus] = useState('')
    const [releaseDate, setReleasedate] = useState('2021-10-10')
    const [releaseTime, setReleasetime] = useState('00:00')
    const [description, setDescription] = useState('')

    GetItemBundle({setBundleList}) 
    GetFeaturedBundle({setFeaturedBundleList})

    return (
      <div>
        <div className="col" align="center">
          <div className="card" style={{ background: "#9bf54c" }}>
            <h4>
              <b>Bundle</b>
            </h4>
            <h6>
              <i>Featured Bundle / Item Bundle Settings</i>
            </h6>
          </div>
        </div>
        <br />
        <div className="container">
          <div className="card">
            <div className="card-header">Item Bundle</div>
            <div className="card-body">
              <Tabs
                id="controlled-tab"
                activeKey={key}
                onSelect={(k) => setKey(k)}
                className="mb-3"
                variant="pills"
              >
                <Tab eventKey="FeaturedBundleList" title="Featured Bundle List">
                  <FeaturedBundleList
                    featuredBundleList={featuredBundleList}
                    setFeaturedBundleList={setFeaturedBundleList}
                  />
                </Tab>
                <Tab
                  eventKey="CreateFeaturedBundle"
                  title="Create Featured Bundle"
                >
                  <CreateFeaturedBundle
                    setFeaturedBundleList={setFeaturedBundleList}
                  />
                </Tab>
                <Tab eventKey="ItemBundleList" title="Item Bundle List">
                  <div className="row">
                    <div className="col">
                      <div className="card">
                        <div className="card-body">
                          <ItemBundleDataTable
                            bundleList={bundleList}
                            setBundleList={setBundleList}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </Tab>
                <Tab eventKey="CreateItemBundle" title="Create Item Bundle">
                  <div className="row">
                    <div className="col">
                      <div className="card">
                        <div className="card-header">Create A new Shop Item Bundle</div>
                        <div className="card-body">
                          <br />
                          <AddItemBundleForms
                           itemType={itemType}
                           setItemType={setItemType}
                           itemId={itemId}
                           setItemid={setItemid}
                           amount={amount}
                           setAmount={setAmount}
                           setPriceCoin={setPriceCoin}
                           setPriceCitrine={setPriceCitrine}
                           setPriceLotus={setPriceLotus}
                           setReleasedate={setReleasedate}
                           setReleasetime={setReleasetime}
                           setDescription={setDescription}
                           arrayData={arrayData}
                           setArrayData={setArrayData}
                          />
                        </div>
                        <div className="card-footer">
                          <div className="row">
                            <div className="col" align="center">
                            <AddItemBundle
                              setBundleList={setBundleList}
                              arrayData={arrayData}
                              priceCitrine={priceCitrine}
                              priceCoin={priceCoin}
                              priceLotus={priceLotus}
                              description={description}
                              releaseDate={releaseDate}
                              releaseTime={releaseTime}
                            />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Tab>
              </Tabs>
            </div>
          </div>
        </div>
      </div>
    );
}

export default ItemBundle
