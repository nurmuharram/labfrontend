import React, { useState } from "react";
import { GetBundles } from "src/api/itemBundleApi/ItemBundleGetRequest";
import { AddFeaturedBundleForms } from "./components/form/ItemBundleForms";
import { AddFeaturedBundle } from "src/api/itemBundleApi/ItemBundlePostRequest";

function CreateFeaturedBundle({ setFeaturedBundleList }) {
  const [bundles, setBundles] = useState([]);
  const [shopId, setShopId] = useState();
  const [startDate, setStartDate] = useState("2019-10-10");
  const [startTime, setStartTime] = useState("00:00");

  const [endDate, setEndDate] = useState("2019-10-10");
  const [endTime, setEndTime] = useState("00:00");

  const [priority, setPriority] = useState();
  const [maxBuy, setMaxBuy] = useState();

  GetBundles({ setBundles });

  const bundleFilter = bundles.filter((item) => item.item_type === null);

  const bundle = bundleFilter.map((item) => {
    return (
      <option key={item.shop_id} value={item.shop_id}>
        {item.shop_id} - {item.description}
      </option>
    );
  });

  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="car-body">
              <br />
              <AddFeaturedBundleForms
                setShopId={setShopId}
                setStartDate={setStartDate}
                setStartTime={setStartTime}
                setEndDate={setEndDate}
                setEndTime={setEndTime}
                setMaxBuy={setMaxBuy}
                setPriority={setPriority}
                bundle={bundle}
              />
            </div>
            <div className="card-footer">
              <div className="row">
                <div className="col" align="center">
                  <AddFeaturedBundle
                    shopId={shopId}
                    startDate={startDate}
                    startTime={startTime}
                    endDate={endDate}
                    endTime={endTime}
                    priority={priority}
                    maxBuy={maxBuy}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateFeaturedBundle;
