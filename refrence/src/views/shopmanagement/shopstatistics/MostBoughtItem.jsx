import React, {useState } from 'react'
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CImg,
    CRow,
    CSpinner,
} from '@coreui/react'
import { GetMostBoughtItem } from 'src/api/ShopStatisticAPI/ShopStatsGetRequest'

function MostBoughtItem() {

    const [mostpurchaseisLoading, setMostpurchaseisLoading] = useState(true)

    const [mostpurchase, setMostpurchase] = useState([])

    GetMostBoughtItem({setMostpurchase})

    const fields = [
        { key: 'shop_id', _style: { width: '10%' } },
        { key: 'Item', _style: { width: '32%' } },
        { key: 'times_bought', _style: { width: '32%' } },
        /* {key: 'option', label: '', _style: { width: '10%' }, filter: false} */
    ];

    const spinner = () => {
        return (<div>
            <CSpinner variant="pulse" size="xxl" color="info" />
        </div>

        )
    }

    return (
        <div>
            <CCardHeader>
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <h3>Most Bought Items</h3>
                </div>
            </CCardHeader>
            <CCardBody>
                <div style={{ overflow: "scroll", height: "40rem" }}>
                    <CDataTable
                        items={mostpurchase}
                        fields={fields}
                        loading={mostpurchaseisLoading}
                        loadingSlot
                        noItemsViewSlot={<div style={{ textAlign: "center" }}><i>{spinner()}Loading Data, Please Wait...</i></div>}
                        hover
                        responsive
                        scopedSlots={{
                            'Item':
                                (item, index) => {
                                    return (
                                        <td className="py-2">
                                            <CImg
                                                className="border-dark shadow-lg"
                                                src={item.item_type === 2 ? `${process.env.PUBLIC_URL}/assets/img/ksa_icon/${item.item_id}.png` :
                                                    item.item_type === 3 ? `${process.env.PUBLIC_URL}/assets/img/ksa_skin/${item.item_id}.png` :
                                                        `${process.env.PUBLIC_URL}/assets/img/noimage.png`}
                                                fluid
                                                shape="rounded"
                                                width='110'
                                                height='60'
                                                align='center' />
                                            &nbsp;&nbsp;&nbsp;
                                            {item.item_type_name} - <b>{item.item_name}</b>
                                        </td>
                                    )
                                },
                            'times_bought':
                                (item, index) => {
                                    return (
                                        <td className="py-3">
                                            Bought <b>{item.times_bought}</b> times
                                        </td>
                                    )
                                },
                        }}
                    />
                </div>
            </CCardBody>
        </div>
    )
}

export default MostBoughtItem
