import React from 'react'
import {
    CCard,
    CCardBody,
    CCol,
    CContainer,
    CNav,
    CNavItem,
    CNavLink,
    CRow,
    CTabContent,
    CTabPane,
    CTabs
} from '@coreui/react'
import MostBoughtItem from './MostBoughtItem'
import TopUpHistory from './TopUpHistory'

function ShopStatistic() {
    return (
        <div>
            <CCol align='center'>
                <CCard style={{ background: '#9bf54c' }}>
                    <h4><b>Shop Statistics</b></h4>
                    <h6><i>General Shop Statistics</i></h6>
                </CCard>
            </CCol>
            <br />
            <CContainer fluid>
                <CRow>
                    <CCol>
                        <CCard accentColor='primary'>
                            <CCardBody>
                            <CTabs>
                                <CNav variant='tabs'>
                                    <CNavItem>
                                        <CNavLink>
                                            Most Bought Items
                                        </CNavLink>
                                    </CNavItem>
                                    <CNavItem>
                                        <CNavLink>
                                            Top-Up History
                                        </CNavLink>
                                    </CNavItem>
                                </CNav>
                                <CTabContent>
                                    <CTabPane>
                                        <MostBoughtItem />
                                    </CTabPane>
                                    <CTabPane>
                                        <TopUpHistory/>
                                    </CTabPane>
                                </CTabContent>
                            </CTabs>
                            </CCardBody>
                        </CCard>
                    </CCol>
                </CRow>
            </CContainer>
        </div>
    )
}

export default ShopStatistic
