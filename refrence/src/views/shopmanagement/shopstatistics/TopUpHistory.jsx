import React, { useState } from 'react'
import {
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow
} from '@coreui/react'
import { GetTopUpHistory } from 'src/api/ShopStatisticAPI/ShopStatsGetRequest';
import { hidenextbttn, hideprevbttn, skipUndefined } from 'src/services/ShopStatistic.services/TopUpHistory.Functions';

function TopUpHistory() {

    const [topuplist, setTopuplist] = useState([])

    const [changelist, setChangelist] = useState(0);

    const fields = [
        { key: 'user_id' },
        { key: 'user_name' },
        { key: 'item' },
        { key: 'item_bought' },
        { key: 'amount' },
        { key: 'price' },
        { key: 'partner_id' },
        { key: 'request_date' },
        { key: 'trx_id' },
        { key: 'publisher_ref_id' },
        /* {key: 'option', label: '', _style: { width: '10%' }, filter: false} */
    ];

    GetTopUpHistory({ setTopuplist, changelist })

    return (
        <div>
            <CCardHeader>
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <h3>Top Up History</h3>
                </div>
            </CCardHeader>
            <CCardBody>
                <CDataTable
                    items={topuplist}
                    fields={fields}
                    border
                    scopedSlots={{
                        'item_bought':
                            (item, index) => {
                                return (
                                    <td>
                                        {item.item_type_name} - {item.item_name}
                                    </td>
                                )
                            }
                    }}
                />
                <CRow>
                {topuplist === null ? <></> : <><CCol align='left'>
                    {hideprevbttn({setChangelist, changelist, topuplist})}
                </CCol>
                <CCol align='right'>
                    {hidenextbttn({setChangelist, changelist, topuplist})}
                </CCol></>}
                </CRow>
            </CCardBody>
        </div>
    )
}

export default TopUpHistory
