import React from "react";
import { Form } from "react-bootstrap";
import {
  UpdateAmount,
  UpdateDescription,
  UpdatePriceCitrine,
  UpdatePriceCoin,
  UpdatePriceLotus,
  UpdateReleaseDate,
} from "src/api/ShopManagerApi/shopManagerPutRequest";
import { SelectItemId } from "src/services/utils/selectItemId";

export const ShopManagerForms = ({
  setItems,
  item,
  setNewAmount,
  setNewDescription,
  setNewPriceCitrine,
  setNewPriceCoin,
  setNewPriceLotus,
  newPriceCoin,
  newPriceLotus,
  newPriceCitrine,
  newAmount,
  newDescription,
  setNewReleaseDate,
  setNewReleaseTime,
  newReleaseDate,
  newReleaseTime
}) => {
  const skipPriceCoinIfNull = () => {
    if (item.price_coin === null) {
      return "";
    } else {
      return item.price_coin;
    }
  };

  const skipPriceCitrineIfNull = () => {
    if (item.price_citrine === null) {
      return "";
    } else {
      return item.price_citrine;
    }
  };

  const skipPriceLotusIfNull = () => {
    if (item.price_lotus === null) {
      return "";
    } else {
      return item.price_lotus;
    }
  };
  return (
    <div>
      <div className="col" align="left">
        <Form>
          <Form.Group className="row mb-3">
            <div className="col-md-5">
              <Form.Label>
                <b>New Price Coin</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <Form.Control
                type="number"
                onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                placeholder={
                  item.price_coin === null
                    ? "--No Price Set--"
                    : item.price_coin
                }
                onChange={(e) => setNewPriceCoin(e.target.value)}
              />
            </div>
            <div className="col">
              <UpdatePriceCoin
                item={item}
                setItems={setItems}
                skipPriceCitrineIfNull={skipPriceCitrineIfNull}
                skipPriceLotusIfNull={skipPriceLotusIfNull}
                newPriceCoin={newPriceCoin}
              />
            </div>
          </Form.Group>
          <Form.Group className="row mb-3">
            <div className="col-md-5">
              <Form.Label>
                <b>New Price Citrine</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <Form.Control
                type="number"
                onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                placeholder={
                  item.price_citrine === null
                    ? "--No Price Set--"
                    : item.price_citrine
                }
                onChange={(e) => setNewPriceCitrine(e.target.value)}
              />
            </div>
            <div className="col">
              <UpdatePriceCitrine
                item={item}
                setItems={setItems}
                skipPriceCoinIfNull={skipPriceCoinIfNull}
                skipPriceLotusIfNull={skipPriceLotusIfNull}
                newPriceCitrine={newPriceCitrine}
              />
            </div>
          </Form.Group>
          <Form.Group className="row mb-3">
            <div className="col-md-5">
              <Form.Label>
                <b>New Price Lotus</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <Form.Control
                size="sm"
                type="number"
                onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                placeholder={
                  item.price_lotus === null
                    ? "--No Price Set--"
                    : item.price_lotus
                }
                onChange={(e) => setNewPriceLotus(e.target.value)}
              />
            </div>
            <div className="col">
              <UpdatePriceLotus
                item={item}
                setItems={setItems}
                skipPriceCoinIfNull={skipPriceCoinIfNull}
                skipPriceCitrineIfNull={skipPriceCitrineIfNull}
                newPriceLotus={newPriceLotus}
              />
            </div>
          </Form.Group>
        </Form>
      </div>
      <div className="col" align="left">
        <Form>
          <Form.Group className="row mb-3">
            <div className="col-md-5">
              <Form.Label>
                <b>New Amount</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <Form.Control
                size="sm"
                type="number"
                onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                placeholder={item.amount}
                onChange={(e) => setNewAmount(e.target.value)}
              />
            </div>
            <div className="col">
              <UpdateAmount
                item={item}
                setItems={setItems}
                skipPriceCoinIfNull={skipPriceCoinIfNull}
                skipPriceCitrineIfNull={skipPriceCitrineIfNull}
                newAmount={newAmount}
                skipPriceLotusIfNull={newAmount}
              />
            </div>
          </Form.Group>

          <Form.Group className="row mb-3">
            <div className="col-md-5">
              <Form.Label>
                <b>New Description</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <Form.Control
                size="sm"
                placeholder={
                  item.description === null
                    ? "--No Description Set--"
                    : item.description
                }
                onChange={(e) => setNewDescription(e.target.value)}
              />
            </div>
            <div className="col">
              <UpdateDescription
                item={item}
                setItems={setItems}
                newDescription={newDescription}
              />
            </div>
          </Form.Group>
          <Form.Group className="row mb-3">
          <div className="col-md-5" align="left">
            <Form.Label>
              <b>New Release Date</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-3">
            <Form.Control
              type="date"
              onKeyDown={(e) => e.preventDefault()}
              onChange={(e) => setNewReleaseDate(e.target.value)}
            />
          </div>
          <div className="col-xs-12 col-md-3">
            <Form.Control
              type="time"
              onChange={(e) => setNewReleaseTime(e.target.value)}
            />
          </div>
          <div className="col">
          <UpdateReleaseDate
                item={item}
                newReleaseDate={newReleaseDate}
                newReleaseTime={newReleaseTime}
              />
          </div>
        </Form.Group>
        </Form>
      </div>
    </div>
  );
};

export const AddItemForm = ({
  setItemType,
  setItemId,
  setAmount,
  setPriceCoin,
  setPriceCitrine,
  setPriceLotus,
  setReleaseDate,
  setReleaseTime,
  setDescription,
  itemType
}) => {
  return (
    <div>
      <Form>
        <Form.Group className="row mb-3">
          <div className="col-md-3" align="left">
            <Form.Label>
              <b>Item Type</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-8" align="left">
            <select
              className="custom-select align-items-md-start"
              onChange={(e) => setItemType(e.target.value)}
            >
              <option>Choose Item Type</option>
              <option value={6}>Box</option>
              <option value={5}>Items</option>
              <option value={2}>Ksatriya</option>
              <option value={3}>KSA Skins</option>
              <option value={4}>Rune</option>
              <option value={11}>Frame</option>
              <option value={12}>Avatar</option>
            </select>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-3" align="left">
            <Form.Label>
              <b>Item</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-8" align="left">
            <select
              className="custom-select align-items-md-start"
              onChange={(e) => setItemId(e.target.value)}
            >
              {SelectItemId({ itemType })}
            </select>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-3" align="left">
            <Form.Label>
              <b>Amount</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-8" align="left">
            <Form.Control
              type="number"
              placeholder="Set Amount..."
              onKeyPress={(event) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              onChange={(e) => setAmount(e.target.value)}
            />
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-3" align="left">
            <Form.Label>
              <b>Price Coin</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-8" align="left">
            <Form.Control
              type="number"
              onKeyPress={(event) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              placeholder="Set Price Coin*"
              onChange={(e) => setPriceCoin(e.target.value)}
            />
            <small>
              <i>
                *Leave this form blank if you want to set the items' price
                without this currency
              </i>
            </small>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-3" align="left">
            <Form.Label>
              <b>Price Citrine</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-8" align="left">
            <Form.Control
              type="number"
              placeholder="Set Price Ctirine*"
              onKeyPress={(event) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              onChange={(e) => setPriceCitrine(e.target.value)}
            />
            <small>
              <i>
                *Leave this form blank if you want to set the items' price
                without this currency
              </i>
            </small>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-3" align="left">
            <Form.Label>
              <b>Price Lotus</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-8" align="left">
            <Form.Control
              type="number"
              onKeyPress={(event) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              placeholder="Set Price Lotus*"
              onChange={(e) => setPriceLotus(e.target.value)}
            />
            <small>
              <i>
                *Leave this form blank if you want to set the items' price
                without this currency
              </i>
            </small>
          </div>
        </Form.Group>
        <Form.Group className="row mb-3">
          <div className="col-md-3" align="left">
            <Form.Label>
              <b>Release Date</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-4">
            <Form.Control
              type="date"
              onKeyDown={(e) => e.preventDefault()}
              onChange={(e) => setReleaseDate(e.target.value)}
            />
          </div>
          <div className="col-xs-12 col-md-4">
            <Form.Control
              type="time"
              onChange={(e) => setReleaseTime(e.target.value)}
            />
          </div>
        </Form.Group>
        <Form.Group className="row mb-4">
          <div className="col-md-3" align="left">
            <Form.Label>
              <b>Description</b>
            </Form.Label>
          </div>
          :
          <div className="col-xs-12 col-md-8" align="left">
            <Form.Control
              as="textarea"
              placeholder="Set Items' Description..."
              onChange={(e) => setDescription(e.target.value)}
            />
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};
