import React, { useState } from 'react'
import { AddShopItem } from 'src/api/ShopManagerApi/shopManagerPostRequest';
import {
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    ModalTitle,
  } from "src/components/modal";
import { AddItemForm } from '../form/ShopManagerForms';

export const ShopManagerModal = ({addItemModal, setAddItemModal, setItems}) => {
    const [itemType, setItemType] = useState("");
    const [itemId, setItemId] = useState("");
    const [amount, setAmount] = useState("");
    const [priceCoin, setPriceCoin] = useState("");
    const [priceCitrine, setPriceCitrine] = useState("");
    const [priceLotus, setPriceLotus] = useState("");
    const [releaseDate, setReleaseDate] = useState("2019-10-10");
    const [releaseTime, setReleaseTime] = useState("00:00");
    const [description, setDescription] = useState("");
  
  return (
    <div><Modal
    show={addItemModal}
    onClose={() => setAddItemModal(!addItemModal)}
    color="info"
    size="lg"
  >
    <ModalHeader closeButton>
      <ModalTitle>
        <b>Add Shop Item</b>
      </ModalTitle>
    </ModalHeader>
    <ModalBody>
      <br />
      <AddItemForm
        setItemType={setItemType}
        setItemId={setItemId}
        setAmount={setAmount}
        setPriceCoin={setPriceCoin}
        setPriceCitrine={setPriceCitrine}
        setPriceLotus={setPriceLotus}
        setReleaseDate={setReleaseDate}
        setReleaseTime={setReleaseTime}
        setDescription={setDescription}
        itemType={itemType}
      />
    </ModalBody>
    <ModalFooter>
      <div className="row">
        <div className="col" align="center">
          <AddShopItem
            itemId={itemId}
            itemType={itemType}
            amount={amount}
            priceCoin={priceCoin}
            priceLotus={priceLotus}
            priceCitrine={priceCitrine}
            releaseDate={releaseDate}
            releaseTime={releaseTime}
            setAddItemModal={setAddItemModal}
            setItems={setItems}
            description={description}
          />
          &nbsp;&nbsp;
          <button
            className="btn btn-secondary"
            onClick={() => setAddItemModal(!addItemModal)}
          >
            Cancel
          </button>
        </div>
      </div>
    </ModalFooter>
  </Modal></div>
  )
}
