import React, { useState } from "react";
import DataTable from "src/components/table/DataTable";
import Collapse from "src/components/collapse/Collapse";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from "src/components/modal";
import { toggleDetails } from "src/services/utils/toggleDetails";
import { ShopManagerForms } from "../form/ShopManagerForms";
import { DeleteItemShop } from "src/api/ShopManagerApi/shopManagerDeleteRequest";

export const ShopManagerDataTable = ({ items, setItems }) => {
  const [details, setDetails] = useState([]);
  const [deleteItemModal, setDeleteItemModal] = useState(false);
  const [newPriceCoin, setNewPriceCoin] = useState("");
  const [newPriceCitrine, setNewPriceCitrine] = useState("");
  const [newPriceLotus, setNewPriceLotus] = useState("");
  const [newDescription, setNewDescription] = useState("");
  const [newReleaseDate, setNewReleaseDate] = useState("2019-10-10");
  const [newReleaseTime, setNewReleaseTime] = useState("00:00");
  const [newAmount, setNewAmount] = useState("");
  const fields = [
    { key: "shop_id", _style: { width: "8%" }, filter: false },
    { key: "description", _style: { width: "15%" } },
    { key: "Item Type", _style: { width: "10%" } },
    { key: "price_coin", _style: { width: "11%" }, filter: false },
    { key: "price_citrine", _style: { width: "11%" }, filter: false },
    { key: "price_lotus", _style: { width: "11%" }, filter: false },
    { key: "release_date", _style: { width: "15%" }, filter: false },
    { key: "amount", _style: { width: "9%" }, filter: false },
    {
      key: "show_details",
      label: "",
      _style: { width: "10%" },
      sorter: false,
      filter: false,
    },
  ];

  return (
    <div>
      {" "}
      <DataTable
        items={items}
        fields={fields}
        tableFilter
        itemsPerPageSelect
        itemsPerPage={5}
        hover
        sorterValue={{ column: "shop_id", asc: false }}
        sorter
        pagination
        scopedSlots={{
          release_date: (item, index) => {
            const date = new Date(
              (item.release_date && item.release_date.split(" ").join("T")) +
                ".000Z"
            );
            return <td>{date.toString()}</td>;
          },
          "Item Type": (item, index) => {
            const itemType = () => {
              if (item.item_type === null) {
                return "Bundle";
              } else if (item.item_type === 2) {
                return "Ksatriya";
              } else if (item.item_type === 6) {
                return "Box";
              } else if (item.item_type === 5) {
                return "Items";
              } else if (item.item_type === 3) {
                return "KSA Skin";
              } else if (item.item_type === 11) {
                return "Frame";
              } else if (item.item_type === 12) {
                return "Avatar";
              } else if (item.item_type === 14) {
                return "Vahana";
              } else if (item.item_type === 4) {
                return "Rune";
              } else if (item.item_type === 9) {
                return "Skin Part";
              }
            };

            return <td className="py-2">{itemType()}</td>;
          },
          show_details: (item, index) => {
            return (
              <td className="py-2">
                <button
                  className="btn btn-outline-primary btn-sm"
                  onClick={() => {
                    toggleDetails(index, {
                      details,
                      setDetails,
                    });
                  }}
                >
                  {details.includes(index) ? "Close" : "Edit Item"}
                </button>
              </td>
            );
          },
          details: (item, index) => {           
            return (
              <Collapse show={details.includes(index)}>
                <div className="card-body">
                  <div className="row">
                    <div className="col-md-6" align="left">
                      <h4>
                        <i>
                          Edit/Update Item (
                          <small>Shop ID : {item.shop_id}</small>)
                        </i>
                      </h4>
                    </div>
                    <div className="col-md-6" align="left">
                      <button
                        className="btn btn-outline-danger btn-sm"
                        onClick={() => setDeleteItemModal(!deleteItemModal)}
                      >
                        Delete Item
                      </button>
                      <Modal
                        show={deleteItemModal}
                        onClose={() => setDeleteItemModal(!deleteItemModal)}
                        color="danger"
                        size="md"
                      >
                        <ModalHeader closeButton>
                          <ModalTitle>
                            <b>Delete this item?</b>
                          </ModalTitle>
                        </ModalHeader>
                        <ModalBody>
                          <div className="col" align="center">
                            <h5>Are you sure?</h5>
                          </div>
                        </ModalBody>
                        <ModalFooter>
                          <div className="row">
                            <div className="col" align="center">
                              <DeleteItemShop setDeleteItemModal={setDeleteItemModal} item={setDeleteItemModal} setItems={setItems}/>
                              &nbsp;
                              <button
                                className="btn btn-secondary btn-sm"
                                onClick={() =>
                                  setDeleteItemModal(!deleteItemModal)
                                }
                              >
                                <b>Cancel</b>
                              </button>
                            </div>
                          </div>
                        </ModalFooter>
                      </Modal>
                    </div>
                  </div>

                  <hr />
                  <div className="row">
                    <ShopManagerForms
                      item={item}
                      setNewAmount={setNewAmount}
                      setNewDescription={setNewDescription}
                      setNewPriceCitrine={setNewPriceCitrine}
                      setNewPriceCoin={setNewPriceCoin}
                      setNewPriceLotus={setNewPriceLotus}
                      newPriceCoin={newPriceCoin}
                      newPriceLotus={newPriceLotus}
                      newPriceCitrine={newPriceCitrine}
                      newAmount={newAmount}
                      newDescription={newDescription}
                      setItems={setItems}
                      newReleaseDate={newReleaseDate}
                      newReleaseTime={newReleaseTime}
                      setNewReleaseDate={setNewReleaseDate}
                      setNewReleaseTime={setNewReleaseTime}
                    />
                  </div>
                </div>
              </Collapse>
            );
          },
        }}
      />
    </div>
  );
};
