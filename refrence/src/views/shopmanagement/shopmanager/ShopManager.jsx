import React, { useState } from "react";
import { GetAllShopItems } from "src/api/ShopManagerApi/shopManagerGetRequest";
import { ShopManagerDataTable } from "./components/dataTable/ShopManagerDataTable";
import { ShopManagerModal } from "./components/modal/ShopManagerModal";

function ShopManager() {
  const [items, setItems] = useState([]);
  const [addItemModal, setAddItemModal] = useState(false);

  const [changeItemTypeList, setChangeItemTypeList] = useState("0");

  GetAllShopItems({ changeItemTypeList, setItems });

  return (
    <div>
      <div className="col" align="center">
        <div className="card" style={{ background: "#9bf54c" }}>
          <h4>
            <b>Item Manager</b>
          </h4>
          <h6>
            <i>General Shop Items Manager</i>
          </h6>
        </div>
      </div>
      <br />

      <div className="container-fluid">
        <div className="card">
          <div className="card-header">Item Manager</div>
          <div className="card-body">
            <div className="row">
              <div className="col">
                <div className="card">
                  <div className="card-body">
                    <div className="col-md-3" align="left">
                      <small>Select List View :</small>
                      <select
                        className="custom-select"
                        onChange={(e) => setChangeItemTypeList(e.target.value)}
                      >
                        <option value={0}>All Items</option>
                        <option value={2}>Ksatriya</option>
                        <option value={3}>KSA Skins</option>
                        <option value={4}>Rune</option>
                        <option value={5}>Misc Items</option>
                        <option value={6}>Box</option>
                        <option value={9}>Skin Part</option>
                        <option value={14}>Vahana Skins</option>
                      </select>
                    </div>
                    <div className="col" align="center">
                      <button
                        className="btn btn-md btn-info"
                        onClick={() => setAddItemModal(!addItemModal)}
                      >
                        + Add Item
                      </button>
                      <ShopManagerModal
                        addItemModal={addItemModal}
                        setAddItemModal={setAddItemModal}
                        setItems={setItems}
                      />
                    </div>
                    <ShopManagerDataTable items={items} setItems={setItems} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ShopManager;
