import React, { useState, useEffect } from 'react'
import {
    CCol,
    CForm,
    CFormGroup,
    CModalBody,
    CModalHeader,
    CSelect,
    CLabel,
    CInput,

} from '@coreui/react'

import { useSelector, useDispatch } from 'react-redux'

import { getVoucherTemplates } from 'src/redux/action/generatevoucherAction/getVoucherTemplatesAction'
import { ModalBody } from 'src/components/modal';

function GenerateVoucher({ setCount, setExpiredate, setExpiretime, setVoucherid }) {


    const dispatch = useDispatch();

    const vtemplates = useSelector((state) => state.vtemplates.vtemplates);
    useEffect(() => {
        dispatch(getVoucherTemplates());
    }, []);

    const dropdownVouchers = vtemplates.map((voucher, index) => {
        return (<option key={voucher.voucher_id} value={voucher.voucher_id}>{voucher.voucher_id} - {voucher.detail}</option>)
    });


    return (
        <div>
            <ModalBody>
                <br />
                <CForm className="form-horizontal" encType='multipart/form-data' >
                    <CFormGroup row>
                        <CCol md="4">
                            <CLabel htmlFor="select"><b>Voucher Template</b></CLabel>
                        </CCol>
                        <CCol xs="12" md="8">
                            <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setVoucherid(e.target.value)}>
                            {dropdownVouchers}
                            </CSelect>
                        </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                        <CCol md="4">
                            <CLabel htmlFor="select"><b>Count</b></CLabel>
                        </CCol>
                        <CCol xs="12" md="8">
                            <CInput onChange={(e) => setCount(e.target.value)}/>
                        </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                        <CCol md="4">
                            <CLabel htmlFor="input"><b>Expire Date</b></CLabel>
                        </CCol>
                        <CCol md="4">
                            <CInput type="date" onKeyDown={(e) => e.preventDefault()} onChange={(e) => setExpiredate(e.target.value)} />
                        </CCol>
                        <CCol md="4">
                            <CInput type="time" onChange={(e) => setExpiretime(e.target.value)} />
                        </CCol>
                    </CFormGroup>
                </CForm>
            </ModalBody>
        </div>
    )
}

export default GenerateVoucher
