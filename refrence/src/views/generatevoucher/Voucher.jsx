import React from 'react'
import {
    CCol,
    CContainer,
    CCard,
    CTabs,
    CNav,
    CNavItem,
    CNavLink,
    CTabContent,
    CTabPane,
    CCardBody
} from '@coreui/react'
import CreateVoucherTemplate from './CreateVoucherTemplate'
import GenerateVoucherList from './GenerateVoucherList'

function Voucher() {
    return (
        <div>
            <CCol align='center'>
            <CCard style={{background: '#9bf54c'}}>
            <h4>
                <b>Voucher</b>
            </h4>
            <h6>
                <i>Generate Voucher Settings</i>
            </h6>
            </CCard>
            </CCol>
            <br />
            <CContainer>
                <CCard>
                    <CCardBody>
                        <CTabs>
                            <CNav variant="tabs">
                                <CNavItem>
                                    <CNavLink>
                                        Create Voucher Template
                                    </CNavLink>
                                </CNavItem>
                                <CNavItem>
                                    <CNavLink>
                                        Generate Voucher
                                    </CNavLink>
                                </CNavItem>
                            </CNav>
                            <CTabContent>
                                <CTabPane>
                                    <CreateVoucherTemplate/>
                                </CTabPane>
                                <CTabPane>
                                    <GenerateVoucherList/>
                                </CTabPane>
                            </CTabContent>
                        </CTabs>
                    </CCardBody>
                </CCard>
            </CContainer>
        </div>
    )
}

export default Voucher
