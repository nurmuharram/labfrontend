import React, { useState, useEffect } from 'react'
import {
    CRow,
    CCol,
    CCard,
    CCardBody,
    CCardFooter,
    CButton,
    CCardHeader,
    CFormGroup,
    CForm,
    CLabel,
    CSelect,
    CInput,
    CTextarea,
    CModal,
    CModalBody,
    CModalTitle,
    CModalHeader,
    CCollapse,
    CDataTable,
    CModalFooter
} from '@coreui/react'

import { useSelector, useDispatch } from 'react-redux'

import { getKsatriyas } from 'src/redux/action/mailAction/getKsatriyasAction';
import { getBoxes } from 'src/redux/action/mailAction/getBoxesAction';
import { getSkins } from 'src/redux/action/mailAction/getSkinsAction';
import { getRunes } from 'src/redux/action/mailAction/getRunesAction'
import { getFrames } from 'src/redux/action/mailAction/getFramesAction';
import { getAvatars } from 'src/redux/action/mailAction/getAvatarsAction';
import { getItems } from 'src/redux/action/mailAction/getItemsAction';

import axios from 'axios';
import { getVoucherTemplates } from 'src/redux/action/generatevoucherAction/getVoucherTemplatesAction';
import CIcon from '@coreui/icons-react';
import { Modal, ModalBody, ModalHeader } from 'src/components/modal';
import Collapse from 'src/components/collapse/Collapse';

function CreateVoucherTemplate() {



    const [details, setDetails] = useState([])

    const [itemtype, setItemtype] = useState('');
    const [itemid, setItemid] = useState(1);
    const [amount, setAmount] = useState('');
    const [detail, setDetail] = useState('');

    const [deleteVoucher, setDeleteVoucher] = useState(false)

    const [listvoucher, setListvoucher] = useState(false)

    const dispatch = useDispatch();

    const vtemplates = useSelector((state) => state.vtemplates.vtemplates);
    useEffect(() => {
        dispatch(getVoucherTemplates());
    }, []);

    const refreshTemplatelist = () => {
        dispatch(getVoucherTemplates());
    };

    /* GET Boxes - Get Request */
    const boxes = useSelector((state) => state.boxes.boxes);

    useEffect(() => {
        dispatch(getBoxes());
    }, []);

    /* GET Ksatriyas - Get Request */
    const ksatriyas = useSelector((state) => state.ksatriyas.ksatriyas);
    useEffect(() => {
        dispatch(getKsatriyas());
    }, []);

    /* GET Items - Get Request */
    const items = useSelector((state) => state.items.items);
    useEffect(() => {
        dispatch(getItems());
    }, []);

    /* GET skins - Get Request */
    const skins = useSelector((state) => state.skins.skins);
    useEffect(() => {
        dispatch(getSkins());
    }, []);

    /* GET rune - Get Request */
    const runes = useSelector((state) => state.runes.runes);
    useEffect(() => {
        dispatch(getRunes());
    }, []);

    /* GET frames - Get Request */
    const frames = useSelector((state) => state.frames.frames);
    useEffect(() => {
        dispatch(getFrames());
    }, []);

    /* GET Avatar - Get Request */
    const avatars = useSelector((state) => state.avatars.avatars);
    useEffect(() => {
        dispatch(getAvatars());
    }, []);

    /* skip undefined */
    if (typeof (vtemplates[(vtemplates.length - 1)]) == 'undefined') {
        return <></>
    }

    const lastdata = vtemplates[vtemplates.length - 1].voucher_id

    /* Box Mapping */
    const dropdownBoxes = boxes.map((box, index) => {
        return (<option key={box.box_id} value={box.box_id}>[Box] - {box.box_name}</option>)
    });

    /* KSA Mapping */
    const dropdownKsatriyas = ksatriyas.map((ksatriya, index) => {
        return (<option key={ksatriya.ksatriya_id} value={ksatriya.ksatriya_id}>[KSA] - {ksatriya.ksatriya_name}</option>)
    });

    /* items Mapping */
    const dropdownItems = items.map((item, index) => {
        return (<option key={item.misc_id} value={item.misc_id}>[Item] - {item.misc_name}</option>)
    });

    /* skins Mapping */
    const dropdownSkins = skins.map((item, index) => {
        const skinname = () => {
            if (item.ksatriya_skin_id === 100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 101) {
                return "Yanaka"
            } else if (item.ksatriya_skin_id === 300) {
                return "Default"
            } else if (item.ksatriya_skin_id === 301) {
                return "Arbalest Armor"
            } else if (item.ksatriya_skin_id === 302) {
                return "Homeroom Teacher"
            } else if (item.ksatriya_skin_id === 400) {
                return "Default"
            } else if (item.ksatriya_skin_id === 401) {
                return "Cybuff Funk"
            } else if (item.ksatriya_skin_id === 402) {
                return "Eternal Anarchy"
            } else if (item.ksatriya_skin_id === 500) {
                return "Default"
            } else if (item.ksatriya_skin_id === 600) {
                return "Default"
            } else if (item.ksatriya_skin_id === 601) {
                return "Thunder Phantom"
            } else if (item.ksatriya_skin_id === 602) {
                return "Badhayan Nyandi"
            } else if (item.ksatriya_skin_id === 700) {
                return "Default"
            } else if (item.ksatriya_skin_id === 701) {
                return "Patih Mada"
            } else if (item.ksatriya_skin_id === 800) {
                return "Default"
            } else if (item.ksatriya_skin_id === 801) {
                return "Guardian of Manthana"
            } else if (item.ksatriya_skin_id === 900) {
                return "Default"
            } else if (item.ksatriya_skin_id === 901) {
                return "Carimata Marauder"
            } else if (item.ksatriya_skin_id === 902) {
                return "Lady Jock"
            } else if (item.ksatriya_skin_id === 1000) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1001) {
                return "Cerberus"
            } else if (item.ksatriya_skin_id === 1100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1101) {
                return "Smokestack Outburst"
            } else if (item.ksatriya_skin_id === 1102) {
                return "Endless Angst"
            } else if (item.ksatriya_skin_id === 1200) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1201) {
                return "Divine Warlord"
            } else if (item.ksatriya_skin_id === 1202) {
                return "Undefined"
            } else if (item.ksatriya_skin_id === 1300) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1301) {
                return "Volcanic Lightning"
            } else if (item.ksatriya_skin_id === 1302) {
                return "Party Wrecker"
            } else if (item.ksatriya_skin_id === 1400) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1401) {
                return "Serene Melody"
            } else if (item.ksatriya_skin_id === 1500) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1501) {
                return "Brave Princess"
            } else if (item.ksatriya_skin_id === 1600) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1601) {
                return "Alighted Asura"
            } else if (item.ksatriya_skin_id === 1700) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1701) {
                return "Battle of Kudadu"
            } else if (item.ksatriya_skin_id === 1702) {
                return "Krtarajasa"
            } else if (item.ksatriya_skin_id === 1800) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1900) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1901) {
                return "Undefined"
            } else if (item.ksatriya_skin_id === 1903) {
                return "Knightly Trip"
            } else if (item.ksatriya_skin_id === 2000) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2001) {
                return "Singharajni"
            } else if (item.ksatriya_skin_id === 2100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2200) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2300) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2400) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2500) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2600) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2601) {
                return "Unswerving Fellowship"
            } else if (item.ksatriya_skin_id === 2700) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2702) {
                return "Lestari Mudan"
            } else if (item.ksatriya_skin_id === 2800) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2802) {
                return "Jinan"
            } else if (item.ksatriya_skin_id === 2900) {
                return "Default"
            } else if (item.ksatriya_skin_id === 3000) {
                return "Default"
            } else if (item.ksatriya_skin_id === 3002) {
                return "Champion of Diver"
            } else if (item.ksatriya_skin_id === 90100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 90101) {
                return "Black Suit"
            } else {
                return item.ksatriya_skin_id
            }
        }
        return (<option key={item.ksatriya_skin_id} value={item.ksatriya_skin_id}>[Skin] {item.ksatriya_name} - {skinname()}</option>)
    });

    /* rune Mapping */
    const dropdownRunes = runes.map((rune, index) => {
        return (<option key={rune.rune_id} value={rune.rune_id}>[Rune] - {rune.name} ({rune.description})</option>)
    });

    /* frames Mapping */
    const dropdownFrames = frames.map((frame, index) => {
        return (<option key={frame.frame_id} value={frame.frame_id}>[Frame] - {frame.description}</option>)
    });

    /* Avatar Mapping */
    const dropdownAvatars = avatars.map((avatar, index) => {
        return (<option key={avatar.avatar_id} value={avatar.avatar_id}>[Avatar] - {avatar.description}</option>)
    });

    const currency = () => {
        return (<><option key={'ori'} value={1}>[Currency] - Ori</option>
            <option key={'citrine'} value={2}>[Currency] - Citrine</option>
            <option key={'lotus'} value={3}>[Currency] - Lotus</option></>)
    }

    const dropdownItemChange = () => {
        if (itemtype === '1') {
            return (currency());
        } else if (itemtype === '6') {
            return (dropdownBoxes)
        } else if (itemtype === '5') {
            return (dropdownItems)
        } else if (itemtype === '2') {
            return (dropdownKsatriyas)
        } else if (itemtype === '3') {
            return (dropdownSkins)
        } else if (itemtype === '4') {
            return (dropdownRunes)
        } else if (itemtype === '11') {
            return (dropdownFrames)
        } else if (itemtype === '12') {
            return (dropdownAvatars)
        }
    };


    const addVoucherTemplate = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('item_type', itemtype);
        data.append('item_id', itemid);
        data.append('amount', amount);
        data.append('detail', detail);
        data.append('voucher_id', lastdata + 1);

        const config = {
            method: 'POST',
            url: `/api/voucher/addVoucher`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Voucher Template Created!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshTemplatelist, 100)
    }

    const toggleDetails = (index) => {
        const position = details.indexOf(index)
        let newDetails = details.slice()
        if (position !== -1) {
            newDetails.splice(position, 1)
        } else {
            newDetails = [...details, index]
        }
        setDetails(newDetails)
    }


    const fields = [
        { key: 'voucher_id', _style: { width: '10%' } },
        { key: 'voucher_name', _style: { width: '30%' } },
        { key: 'item_type_name', _style: { width: '20%' } },
        { key: 'item_name', _style: { width: '20%' } },
        { key: 'amount', _style: { width: '10%' } },
        {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        }
    ]


    return (
        <div>
            <div className="row">
                <div className="col">
                    <div className="card">
                        <div className="card-header">
                            <div className="row">
                                <div className="col-md-12" align='left'>
                                <small><i><b>Create Voucher Template</b></i></small>
                                </div>

                            </div>
                        </div>
                        <div className="card-body">
                        <div className="col" align='left'>
                            <button className='btn btn-primary btn-sm' onClick={() => setListvoucher(!listvoucher)} >Voucher Template lists</button>
                                    <Modal
                                        show={listvoucher}
                                        onClose={() => setListvoucher(!listvoucher)}
                                        color="info"
                                        size="xl"
                                    >
                                        <ModalHeader closeButton>
                                            <CModalTitle>
                                                <b>Voucher Templates</b>
                                            </CModalTitle>
                                        </ModalHeader>
                                        <ModalBody>
                                            <CDataTable
                                                items={vtemplates}
                                                fields={fields}
                                                columnFilter
                                                tableFilter
                                                sorterValue={{ column: "voucher_id", asc: false }}
                                                itemsPerPageSelect
                                                itemsPerPage={5}
                                                hover
                                                sorter
                                                pagination
                                                scopedSlots={{
                                                    'voucher_name':
                                                        (item, index) => {
                                                            return (
                                                                <td >
                                                                    {item.detail}
                                                                </td>
                                                            )
                                                        },
                                                    'show_details':
                                                        (item, index) => {
                                                            return (
                                                                <td className="py-2">
                                                                    <button className='btn btn-info btn-sm' onClick={() => { toggleDetails(index) }} >{details.includes(index) ? 'Hide' : 'Options'}</button>
                                                                </td>
                                                            )
                                                        },
                                                    'details':
                                                        (item, index) => {

                                                            const updateVoucher = (e) => {
                                                                e.preventDefault()
                                                                const FormData = require('form-data');
                                                                const data = new FormData();
                                                                data.append('item_type', itemtype);
                                                                data.append('item_id', itemid);
                                                                data.append('amount', amount);

                                                                const config = {
                                                                    method: 'PUT',
                                                                    url: `/api/voucher/updateVoucherDetail?voucher_id=${item.voucher_id}`,
                                                                    headers: {
                                                                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                                    },
                                                                    data: data
                                                                };
                                                        
                                                                axios(config)
                                                                    .then((response) => {
                                                                        console.log(JSON.stringify(response.data));
                                                                        alert('Voucher Updated!')
                                                                    })
                                                                    .catch((error) => {
                                                                        console.log(error);
                                                                        alert('Failed!')
                                                                    });
                                                                setTimeout(refreshTemplatelist, 100)
                                                            };
                
                                                            const deleteItem = () => {
                
                                                                const config = {
                                                                    method: 'delete',
                                                                    url: `/api/voucher/deleteVoucherDetail?voucher_id=${item.voucher_id}`,
                                                                    headers: {
                                                                        Authorization: 'Bearer ' + localStorage.getItem('auth')
                                                                    }
                                                                };
                                                    
                                                                axios(config)
                                                                    .then((response) => {
                                                                        console.log(JSON.stringify(response.data));
                                                                        alert('Item Removed!');
                                                                        setDeleteVoucher(false)
                                                                    })
                                                                    .catch((error) => {
                                                                        alert('Failed!');
                                                                    });
                                                                setTimeout(refreshTemplatelist, 100);
                                                            }; 

                                                            return (
                                                                <Collapse show={details.includes(index)}>
                                                                    <div className="card-body">
                                                                        <div className="row">
                                                                            <div className="col" align='left'>
                                                                            <h6>
                                                                                    <i>Edit Voucher :  {item.detail}</i>
                                                                                </h6>
                                                                            </div>
                                                                        </div>
                                                                        <br />
                                                                        <div className="form-group row">
                                                                            {/* <div className="col col-md-2">
                                                                            <CLabel htmlFor="select"><b>New Item Type</b></CLabel>
                                                                            </div> */}
                                                                            <CCol md="2">
                                                                                <CLabel htmlFor="select"><b>New Item Type</b></CLabel>
                                                                            </CCol>
                                                                            :
                                                                            <CCol xs="12" md="5">
                                                                                <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setItemtype(e.target.value)}>
                                                                                    <option >Choose Item Type</option>
                                                                                    <option value={1}  >Currency</option>
                                                                                    <option value={6}  >Box</option>
                                                                                    <option value={5}  >Items</option>
                                                                                    <option value={2}  >Ksatriya</option>
                                                                                    <option value={3}  >KSA Skins</option>
                                                                                    <option value={4}  >Rune</option>
                                                                                    <option value={11} >Frame</option>
                                                                                    <option value={12} >Avatar</option>
                                                                                </CSelect>
                                                                            </CCol>          
                                                                        </div>
                                                                        <CFormGroup row>

                                                                        </CFormGroup>
                                                                        <CFormGroup row>
                                                                            <CCol md="2">
                                                                                <CLabel htmlFor="input"><b>New Item</b></CLabel>
                                                                            </CCol>
                                                                            :
                                                                            <CCol xs="12" md="5">
                                                                                <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setItemid(e.target.value)}>
                                                                                    {dropdownItemChange()}
                                                                                </CSelect>
                                                                            </CCol>
                                                                        </CFormGroup>
                                                                        <CFormGroup row>
                                                                            <CCol md="2">
                                                                                <CLabel htmlFor="input"><b>Amount</b></CLabel>
                                                                            </CCol>
                                                                            :
                                                                            <CCol xs="12" md="5">
                                                                                <CInput type='number' placeholder='Amount' defaultValue={item.amount} onChange={(e) => setAmount(e.target.value)} />
                                                                            </CCol>
                                                                        </CFormGroup>
                                                                        <CFormGroup row>
                                                                            <CCol md='6' align='right'>
                                                                                <CButton size="sm" color="info"  onClick={updateVoucher} >
                                                                                    Update Voucher
                                                                                </CButton>
                                                                                {' '} or {' '}
                                                                                <CButton size="sm" color="danger" className="ml-1" onClick={() => setDeleteVoucher(!deleteVoucher)}>
                                                                                    <CIcon name='cil-trash' size='sm' /> Delete Voucher</CButton>
                                                                                <CModal
                                                                                    show={deleteVoucher}
                                                                                    onClose={() => setDeleteVoucher(!deleteVoucher)}
                                                                                    color="danger"
                                                                                    size="md"
                                                                                >
                                                                                    <CModalHeader closeButton>
                                                                                        <CModalTitle>
                                                                                            <b>Delete this voucher?</b>
                                                                                        </CModalTitle>
                                                                                    </CModalHeader>
                                                                                    <CModalBody>
                                                                                        <CCol align='center'>
                                                                                            <h5>Are you sure?</h5>
                                                                                        </CCol>
                                                                                    </CModalBody>
                                                                                    <CModalFooter>
                                                                                        <CRow>
                                                                                            <CCol align='center'>
                                                                                                <CButton type="send" size="sm" color="danger"  onClick={deleteItem} ><b>Delete</b></CButton>&nbsp;
                                                                                                <CButton type="send" size="sm" color="secondary" onClick={() => setDeleteVoucher(!deleteVoucher)} ><b>Cancel</b></CButton>
                                                                                            </CCol>
                                                                                        </CRow>
                                                                                    </CModalFooter>
                                                                                </CModal>
                                                                            </CCol>
                                                                        </CFormGroup>
                                                                    </div>

                                                                </Collapse>
                                                            )
                                                        }
                                                }}
                                            />


                                        </ModalBody>
                                        <CModalFooter>
                                            <CRow>
                                                <CCol align='center'>
                                                    <CButton type="send" color="secondary" onClick={() => setListvoucher(!listvoucher)} ><b>Close</b></CButton>
                                                </CCol>
                                            </CRow>
                                        </CModalFooter>
                                    </Modal>
                        </div>

                            <br />
                            <CForm className="form-horizontal" encType='multipart/form-data' >
                                <CFormGroup row>
                                    <CCol md="2">
                                        <CLabel htmlFor="input"><b>Voucher Name</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="5">
                                        <CInput onChange={(e) => setDetail(e.target.value)} />
                                    </CCol>
                                </CFormGroup>
                                <CFormGroup row>
                                    <CCol md="2">
                                        <CLabel htmlFor="select"><b>Item Type</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="5">
                                        <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setItemtype(e.target.value)}>
                                            <option >Choose Item Type</option>
                                            <option value={1}  >Currency</option>
                                            <option value={6}  >Box</option>
                                            <option value={5}  >Items</option>
                                            <option value={2}  >Ksatriya</option>
                                            <option value={3}  >KSA Skins</option>
                                            <option value={4}  >Rune</option>
                                            <option value={11} >Frame</option>
                                            <option value={12} >Avatar</option>
                                        </CSelect>
                                    </CCol>
                                </CFormGroup>
                                <CFormGroup row>
                                    <CCol md="2">
                                        <CLabel htmlFor="input"><b>Item Name</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="5">
                                        <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setItemid(e.target.value)}>
                                            {dropdownItemChange()}
                                        </CSelect>
                                    </CCol>
                                </CFormGroup>
                                <CFormGroup row>
                                    <CCol md="2">
                                        <CLabel htmlFor="input"><b>Amount</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="5">
                                        <CInput placeholder='Set Amount' type='number' onChange={(e) => setAmount(e.target.value)} />
                                    </CCol>
                                </CFormGroup>
                            </CForm>

                        </div>



                        <CCardFooter>
                            <div className="row">
                                <div className="col" align='center'>
                                <CButton className="btn-pill" type="send" size="sm" color="primary" onClick={addVoucherTemplate} ><b>Create Voucher Template</b></CButton>
                                </div>
                            </div>
                        </CCardFooter>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CreateVoucherTemplate
