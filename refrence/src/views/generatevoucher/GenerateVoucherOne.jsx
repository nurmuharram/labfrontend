import React, { useState, useEffect } from 'react'
import {
    CCol,
    CForm,
    CFormGroup,
    CModalBody,
    CModalHeader,
    CSelect,
    CLabel,
    CInput,

} from '@coreui/react'

import { useSelector, useDispatch } from 'react-redux'

import { getVoucherTemplates } from 'src/redux/action/generatevoucherAction/getVoucherTemplatesAction'

function GenerateVoucherOne({ setSecret_key, setExpireddateone, setMax_claim, setVoucheridone }) {
    const dispatch = useDispatch();

    const vtemplates = useSelector((state) => state.vtemplates.vtemplates);
    useEffect(() => {
        dispatch(getVoucherTemplates());
    }, []);

    const dropdownVouchers = vtemplates.map((voucher, index) => {
        return (<option key={voucher.voucher_id} value={voucher.voucher_id}>{voucher.voucher_id} - {voucher.detail}</option>)
    });

    const [expiredate, setExpiredate] = useState('2019-10-10')
    const [expiretime, setExpiretime] = useState('00:00')

    const convertExpiredDate = new Date(expiredate + ' ' + expiretime + ':' + '00')

    const expired_date = convertExpiredDate.toISOString().split('T').join(' ').replace('.000Z', '')

    setExpireddateone(expired_date)

    return (
        <div>
                <CForm className="form-horizontal" encType='multipart/form-data' >
                    <CFormGroup row>
                        <CCol md="4">
                            <CLabel htmlFor="select"><b>Voucher Template</b></CLabel>
                        </CCol>
                        <CCol xs="12" md="8">
                            <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setVoucheridone(e.target.value)}>
                            {dropdownVouchers}
                            </CSelect>
                        </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                        <CCol md="4">
                            <CLabel htmlFor="select"><b>Secret Key</b></CLabel>
                        </CCol>
                        <CCol xs="12" md="8">
                            <CInput maxLength='16' onChange={(e) => setSecret_key(e.target.value)}/>
                        </CCol>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><i>*Secret key must be no more than 16 (sixteen) characters length</i></small>
                    </CFormGroup>
                    <CFormGroup row>
                        <CCol md="4">
                            <CLabel htmlFor="input"><b>Expire Date</b></CLabel>
                        </CCol>
                        <CCol md="4">
                            <CInput type="date" onKeyDown={(e) => e.preventDefault()} onChange={(e) => setExpiredate(e.target.value)} />
                        </CCol>
                        <CCol md="4">
                            <CInput type="time" onChange={(e) => setExpiretime(e.target.value)} />
                        </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                        <CCol md="4">
                            <CLabel htmlFor="input"><b>Max Claim</b></CLabel>
                        </CCol>
                        <CCol md="8">
                            <CInput placeholder='Set Max Claim' type='number' onChange={(e) => setMax_claim(e.target.value)} />
                        </CCol>

                    </CFormGroup>
                </CForm>
        </div>
    )
}

export default GenerateVoucherOne
