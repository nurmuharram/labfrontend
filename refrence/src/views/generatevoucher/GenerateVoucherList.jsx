import React, { useState, useEffect } from 'react'

import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CButton,
    CCollapse,
    CModalHeader,
    CModalTitle,
    CModalBody,
    CModal,
    CContainer,
    CModalFooter,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CLink
} from '@coreui/react'

import axios from 'axios';

import { useSelector, useDispatch } from 'react-redux'
import { getVouchers } from 'src/redux/action/generatevoucherAction/getVouchersAction';
import GenerateVoucher from './GenerateVoucher';
import { getVoucherTemplates } from 'src/redux/action/generatevoucherAction/getVoucherTemplatesAction';
import CIcon from '@coreui/icons-react';
import { getVoucherOne } from 'src/redux/action/generatevoucherAction/getVoucherOneAction';
import GenerateVoucherOne from './GenerateVoucherOne';
import VoucherOneHistoryList from './VoucherOneHistoryList';
import { GetVoucher1000List, GetVoucherList, GetVoucherOne, GetVouchers } from 'src/api/generatevoucherAPI/getVouchersGetRequest';
import { Modal, ModalBody, ModalFooter, ModalHeader, ModalTitle } from 'src/components/modal';
import { Form, Spinner } from 'react-bootstrap';

function GenerateVoucherList() {

    const [details, setDetails] = useState([])
    const [onedetails, setOnedetails] = useState([])
    const [vouchers, setVouchers] = useState([])
    const [voucherOne, setVoucherOne] = useState([])
    const [listVouchers, setListVouchers] = useState([])
    const [oneThousandListVoucher, setOneThousandListVoucher] = useState([])
    const [genvouchermodal, setGenvouchermodal] = useState(false)
    const [genvoucheronemodal, setGenvoucheronemodal] = useState(false)
    const [deleteVoucher, setDeleteVoucher] = useState(false)
    const [deleteVoucherOne, setdeleteVoucherOne] = useState(false)
    const [viewdata, setViewdata] = useState(false)
    const [exportCustomVoucherModal, setExportCustomVoucherModal] = useState(false)

    const [expiredate, setExpiredate] = useState('2019-10-10')
    const [expiretime, setExpiretime] = useState('00:00')
    const [voucherid, setVoucherid] = useState(1)
    const [count, setCount] = useState()

    const [countVoucher, setCountVoucher] = useState(100)
    const [offsetVoucher, setOffsetVoucher] = useState(0)

    const [secret_key, setSecret_key] = useState('')
    const [expireddateone, setExpireddateone] = useState('')
    const [max_claim, setMax_claim] = useState()
    const [voucheridone, setVoucheridone] = useState(1)

    const [generateVoucherIsLoading, setGenerateVoucherIsLoading] = useState(false)
    const [generateVoucherOneIsLoading, setGenerateVoucherOneIsLoading] = useState(false)

    const [changeTable, setChangeTable] = useState('1')

    const convertExpiredDate = new Date(expiredate + ' ' + expiretime + ':' + '00')

    const expired_date = convertExpiredDate.toISOString().split('T').join(' ').replace('.000Z', '')

    GetVoucherList({ setVouchers })
    GetVoucher1000List({ setOneThousandListVoucher })
    GetVoucherOne({ setVoucherOne })

    const dispatch = useDispatch();

    const vtemplates = useSelector((state) => state.vtemplates.vtemplates);
    useEffect(() => {
        dispatch(getVoucherTemplates());
    }, []);

    const dropdownVouchers = vtemplates.map((voucher, index) => {
        return (<option key={voucher.voucher_id} value={voucher.voucher_id}>{voucher.voucher_id} - {voucher.detail}</option>)
    });

    const refreshVoucherlist = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/voucher/queryVoucherExport?count=5000&offset=0`, config).then(res => {
            const items = res.data;
            setVouchers(items);
        });
    };

    const refreshVoucherOnelist = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/voucher/getAllVoucherOne`, config).then(res => {
            const items = res.data;
            setVoucherOne(items);
        });
    };

    const toggleDetails = (index) => {
        const position = details.indexOf(index)
        let newDetails = details.slice()
        if (position !== -1) {
            newDetails.splice(position, 1)
        } else {
            newDetails = [...details, index]
        }
        setDetails(newDetails)
    }

    const toggleoneDetails = (index) => {
        const position = onedetails.indexOf(index)
        let newOnedetails = onedetails.slice()
        if (position !== -1) {
            newOnedetails.splice(position, 1)
        } else {
            newOnedetails = [...onedetails, index]
        }
        setDetails(newOnedetails)
    }


    const fields = [
        { key: 'id', _style: { width: '5%' } },
        { key: 'voucher_name', _style: { width: '20%' } },
        { key: 'key', _style: { width: '15%' } },
        { key: 'created_date', _style: { width: '15%' } },
        { key: 'expired_date', _style: { width: '15%' } },
        { key: 'redeemed', _style: { width: '15%' } },
        { key: 'claimed_date', _style: { width: '15%' } },
        {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        }
    ];



    const fieldsvone = [
        { key: 'id', _style: { width: '8%' } },
        { key: 'voucher_name', _style: { width: '18%' } },
        { key: 'secret_key', _style: { width: '16%' } },
        { key: 'created_date', _style: { width: '15%' } },
        { key: 'expired_date', _style: { width: '15%' } },
        { key: 'max_claim', _style: { width: '11%' } },
        { key: 'remaining_claim', _style: { width: '11%' } },

        {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        }
    ];

    //Export to Csv Function
    function exportCsv() {
        var csvRow = [];
        var A = [['id', 'detail', 'key', 'created_date', 'expired_date', 'claimed_by', 'claimed_date']];
        var re = listVouchers;
        for (var item = 0; item < re.length; item++) {
            A.push([re[item].id, re[item].detail, re[item].key, re[item].created_date, re[item].expired_date, re[item].user_name, re[item].claimed_date]);
        }
        for (var i = 0; i < A.length; i++) {
            csvRow.push(A[i].join(","))
        }
        var csvString = csvRow.join("%0A");
        var a = document.createElement("a");
        a.href = 'data:attachment/csv.,' + csvString;
        a.target = '_Blank';
        a.download = 'Custom_Voucher_List.csv';
        document.body.appendChild(a);
        a.click();
    }

    //Export 1000 voucher list to Csv Function
    function export1000ListToCsv() {
        var csvRow = [];
        var A = [['id', 'detail', 'key', 'created_date', 'expired_date', 'claimed_by', 'claimed_date']];
        var re = oneThousandListVoucher;
        for (var item = 0; item < re.length; item++) {
            A.push([re[item].id, re[item].detail, re[item].key, re[item].created_date, re[item].expired_date, re[item].user_name, re[item].claimed_date]);
        }
        for (var i = 0; i < A.length; i++) {
            csvRow.push(A[i].join(","))
        }
        var csvString = csvRow.join("%0A");
        var a = document.createElement("a");
        a.href = 'data:attachment/csv.,' + csvString;
        a.target = '_Blank';
        a.download = 'Last_1000_Voucher_List.csv';
        document.body.appendChild(a);
        a.click();
    }

    const generateVoucher = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('voucher_id', voucherid);
        data.append('count', count);
        data.append('expire_date', expired_date);

        const config = {
            method: 'POST',
            url: `/api/voucher/GenerateVoucher`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        setGenerateVoucherIsLoading(true)

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Voucher Generated!')
                setGenvouchermodal(false);
                setGenerateVoucherIsLoading(false);
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshVoucherlist, 100)
    };

    const generateVoucherOne = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('secret_key', secret_key);
        data.append('expired_date', expireddateone);
        data.append('max_claim', max_claim);
        data.append('voucher_id', voucheridone);

        const config = {
            method: 'POST',
            url: `/api/voucher/addVoucherOne`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        setGenerateVoucherOneIsLoading(true)

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('One Key Voucher Generated!')
                setGenerateVoucherOneIsLoading(false)
                setGenvoucheronemodal(false);
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshVoucherOnelist, 100)
    };

    useEffect(() => {
        GetVouchers({ setListVouchers, countVoucher, offsetVoucher });
    }, [countVoucher,offsetVoucher])


    return (
        <div>
            <div className="row">
                <div className="col">
                    <div className="card">
                        <div className="card-header">
                        <small><i>Generate Voucher</i></small>
                        </div>
                        <div className="card-body">
                            <CFormGroup row>
                                <CCol md='3'>
                                    <CLabel htmlFor="select"><i>Select Voucher Type :</i></CLabel>
                                    <CSelect custom size='sm' name="select" id="select" onChange={(e) => setChangeTable(e.target.value)}>
                                        <option value={1}>Voucher Key Per User</option>
                                        <option value={2}>One Voucher Key to Multiple Users</option>
                                    </CSelect>
                                </CCol>
                            </CFormGroup>
                            {changeTable === '1' ? <> <CContainer>
                                <CCol align='center'>
                                    <CButton color="info" size="md" xs='3' onClick={() => setGenvouchermodal(!genvouchermodal)}><i><b>Generate Voucher</b></i></CButton>
                                    <CModal
                                        show={genvouchermodal}
                                        onClose={() => setGenvouchermodal(!genvouchermodal)}
                                        color="info"
                                        size="md"
                                    >
                                        <CModalHeader closeButton>
                                            <CModalTitle>
                                                <b>Generate Voucher</b>
                                            </CModalTitle>
                                        </CModalHeader>
                                        <CModalBody>

                                            <GenerateVoucher setExpiredate={setExpiredate} setExpiretime={setExpiretime} setCount={setCount} setVoucherid={setVoucherid} />

                                        </CModalBody>
                                        <CModalFooter>
                                            <CRow>
                                                <CCol align='center'>
                                                    {generateVoucherIsLoading === false ?
                                                        <CButton className="btn-pill" type="send" size="sm" color="primary" onClick={generateVoucher} ><b>Generate!</b></CButton> :
                                                        <CButton className="btn-pill" type="send" size="sm" color="primary" disabled ><Spinner as="span"
                                                            animation="border"
                                                            size="sm"
                                                            role="status"
                                                            aria-hidden="true" /><b>Creating...</b></CButton>}
                                                </CCol>
                                            </CRow>
                                        </CModalFooter>
                                    </CModal>
                                </CCol>
                            </CContainer>
                                <br />
                                <CDataTable
                                    items={vouchers}
                                    fields={fields}
                                    tableFilter
                                    sorterValue={{ column: "id", asc: false }}
                                    itemsPerPageSelect
                                    itemsPerPage={5}
                                    hover
                                    sorter
                                    pagination
                                    scopedSlots={{
                                        'show_details':
                                            (item, index) => {
                                                return (
                                                    <td className="py-2">
                                                        <CButton
                                                            color="primary"
                                                            variant="outline"
                                                            shape="square"
                                                            size="sm"
                                                            onClick={() => { toggleDetails(index) }}
                                                        >
                                                            {details.includes(index) ? 'Hide' : 'Options'}
                                                        </CButton>
                                                    </td>
                                                )
                                            },
                                        'details':
                                            (item, index) => {

                                                const updateVoucher = (e) => {
                                                    e.preventDefault()
                                                    const FormData = require('form-data');
                                                    const data = new FormData();
                                                    data.append('voucher_id', voucherid);

                                                    const config = {
                                                        method: 'PUT',
                                                        url: `/api/voucher/updateVoucher?id=${item.id}`,
                                                        headers: {
                                                            Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                        },
                                                        data: data
                                                    };

                                                    axios(config)
                                                        .then((response) => {
                                                            console.log(JSON.stringify(response.data));
                                                            alert('Voucher Updated!')
                                                            setGenvouchermodal(false);
                                                        })
                                                        .catch((error) => {
                                                            console.log(error);
                                                            alert('Failed!')
                                                        });
                                                    setTimeout(refreshVoucherlist, 100)
                                                };

                                                const deleteItem = () => {

                                                    const config = {
                                                        method: 'delete',
                                                        url: `api/voucher/deleteVoucher?id=${item.id}`,
                                                        headers: {
                                                            Authorization: 'Bearer ' + localStorage.getItem('auth')
                                                        }
                                                    };

                                                    axios(config)
                                                        .then((response) => {
                                                            console.log(JSON.stringify(response.data));
                                                            alert('Item Removed!');
                                                            setDeleteVoucher(false)
                                                        })
                                                        .catch((error) => {
                                                            alert('Failed to attach item!');
                                                        });
                                                    setTimeout(refreshVoucherlist, 100);
                                                };


                                                return (
                                                    <CCollapse show={details.includes(index)}>
                                                        <CCardBody>
                                                            <CFormGroup row>
                                                                <CCol md="2">
                                                                    <CLabel htmlFor="select"><b>New Voucher Template :</b></CLabel>
                                                                </CCol>
                                                                <CCol xs="12" md="4">
                                                                    <CSelect size='sm' custom name="select" id="select" className="align-items-md-start" onChange={(e) => setVoucherid(e.target.value)}>
                                                                        {dropdownVouchers}
                                                                    </CSelect>
                                                                </CCol>
                                                            </CFormGroup>
                                                            <CFormGroup row>
                                                                <CCol md='6' align='right'>
                                                                    <CButton size="sm" color="info" onClick={updateVoucher}>
                                                                        Update Voucher
                                                                    </CButton>
                                                                    {' '} or {' '}
                                                                    <CButton size="sm" color="danger" className="ml-1" onClick={() => setDeleteVoucher(!deleteVoucher)}>
                                                                        X &nbsp; Delete Voucher</CButton>
                                                                    <CModal
                                                                        show={deleteVoucher}
                                                                        onClose={() => setDeleteVoucher(!deleteVoucher)}
                                                                        color="danger"
                                                                        size="md"
                                                                    >
                                                                        <CModalHeader closeButton>
                                                                            <CModalTitle>
                                                                                <b>Delete this voucher?</b>
                                                                            </CModalTitle>
                                                                        </CModalHeader>
                                                                        <CModalBody>

                                                                            <CCol align='center'>
                                                                                <h5>Are you sure?</h5>
                                                                            </CCol>

                                                                        </CModalBody>
                                                                        <CModalFooter>
                                                                            <CRow>
                                                                                <CCol align='center'>
                                                                                    <CButton type="send" size="sm" color="danger" onClick={deleteItem} ><b>Delete</b></CButton>&nbsp;
                                                                                    <CButton type="send" size="sm" color="secondary" onClick={() => setDeleteVoucher(!deleteVoucher)} ><b>Cancel</b></CButton>
                                                                                </CCol>
                                                                            </CRow>
                                                                        </CModalFooter>
                                                                    </CModal>
                                                                </CCol>
                                                            </CFormGroup>

                                                        </CCardBody>
                                                    </CCollapse>
                                                )
                                            },
                                        'redeemed':
                                            (item, index) => {
                                                const skipnull = () => {
                                                    if (item.user_id === null) {
                                                        return 'unclaimed'
                                                    } else {
                                                        return <>{item.user_id} - {item.user_name}</>
                                                    }
                                                }
                                                return (
                                                    <td>
                                                        {skipnull()}
                                                    </td>
                                                )
                                            },
                                        'claimed_date':
                                            (item, index) => {

                                                const date = new Date((item.claimed_date && item.claimed_date.split(' ').join('T')) + '.000Z')

                                                const skipnull = () => {
                                                    if (item.claimed_date === null) {
                                                        return 'unclaimed'
                                                    } else {
                                                        return date.toString()
                                                    }
                                                }
                                                return (
                                                    <td>
                                                        {skipnull()}
                                                    </td>
                                                )
                                            },
                                        'created_date':
                                            (item, index) => {

                                                const date = new Date((item.created_date && item.created_date.split(' ').join('T')) + '.000Z')

                                                return (
                                                    <td>
                                                        {date.toString()}
                                                    </td>
                                                )
                                            },
                                        'expired_date':
                                            (item, index) => {

                                                const date = new Date((item.expired_date && item.expired_date.split(' ').join('T')) + '.000Z')

                                                return (
                                                    <td>
                                                        {date.toString()}
                                                    </td>
                                                )
                                            },
                                        'voucher_name':
                                            (item, index) => {
                                                return (
                                                    <td>
                                                        {item.detail}
                                                    </td>
                                                )
                                            },
                                    }}
                                />
                                <div align='right'>
                                    <button className='btn btn-primary' onClick={() => setExportCustomVoucherModal(!exportCustomVoucherModal)}>
                                        Export Voucher List to CSV (Custom)
                                    </button>&nbsp;
                                    <button className='btn btn-primary' onClick={export1000ListToCsv}>
                                        Export Last 1000 List to CSV
                                    </button>
                                    <Modal
                                        show={exportCustomVoucherModal}
                                        onClose={() => setExportCustomVoucherModal(!exportCustomVoucherModal)}
                                        color="primary"
                                        size="md"
                                    >
                                        <ModalHeader>
                                            <ModalTitle>
                                                Custom Export to CSV
                                            </ModalTitle>
                                        </ModalHeader>
                                        <ModalBody>
                                            <Form>
                                                <Form.Group className="mb-3" controlId="description">
                                                    <div className="row">
                                                        <div className="col-md-3">
                                                            <Form.Label>
                                                                <b>Start From</b>
                                                            </Form.Label>
                                                        </div>
                                                        :
                                                        <div className="col-md-8">
                                                            <Form.Control
                                                                type="number"
                                                                name="VersionString"
                                                                onKeyPress={(event) => {
                                                                    if (!/[0-9]/.test(event.key)) {
                                                                        event.preventDefault();
                                                                    }
                                                                }}
                                                                placeholder="Set Start List From..."
                                                                onChange={(e) => setOffsetVoucher(e.target.value)}
                                                            />
                                                        </div>
                                                    </div>
                                                </Form.Group>
                                                <Form.Group className="mb-3" controlId="description">
                                                    <div className="row">
                                                        <div className="col-md-3">
                                                            <Form.Label>
                                                                <b>To</b>
                                                            </Form.Label>
                                                        </div>
                                                        :
                                                        <div className="col-md-8">
                                                            <Form.Control
                                                                type="number"
                                                                name="VersionString"
                                                                onKeyPress={(event) => {
                                                                    if (!/[0-9]/.test(event.key)) {
                                                                        event.preventDefault();
                                                                    }
                                                                }}
                                                                placeholder="Set End List..."
                                                                onChange={(e) => setCountVoucher(e.target.value)}
                                                            />
                                                        </div>
                                                    </div>
                                                </Form.Group>

                                            </Form>
                                        </ModalBody>
                                        <ModalFooter>
                                            {listVouchers && listVouchers.length > 0 ? <button className='btn btn-primary' onClick={exportCsv}>
                                                Export to CSV
                                            </button> : <button className='btn btn-primary' disabled>
                                                Export to CSV
                                            </button>}
                                            <button className='btn btn-secondary' onClick={() => setExportCustomVoucherModal(!exportCustomVoucherModal)}>
                                                Cancel
                                            </button>
                                        </ModalFooter>
                                    </Modal>
                                </div> </> : <><CContainer>
                                    <CCol align='center'>
                                        <CButton color="info" size="md" xs='3' onClick={() => setGenvoucheronemodal(!genvoucheronemodal)}><i><b>Generate One Voucher Key</b></i></CButton>
                                        <CModal
                                            show={genvoucheronemodal}
                                            onClose={() => setGenvoucheronemodal(!genvoucheronemodal)}
                                            color="info"
                                            size="md"
                                        >
                                            <CModalHeader closeButton>
                                                <CModalTitle>
                                                    <b>Generate One Key Voucher</b>
                                                </CModalTitle>
                                            </CModalHeader>
                                            <CModalBody>

                                                <GenerateVoucherOne setSecret_key={setSecret_key} setExpireddateone={setExpireddateone} setMax_claim={setMax_claim} setVoucheridone={setVoucheridone} />

                                            </CModalBody>
                                            <CModalFooter>
                                                <CRow>
                                                    <CCol align='center'>
                                                    {generateVoucherOneIsLoading === false ?
                                                        <CButton className="btn-pill" type="send" size="sm" color="primary" onClick={generateVoucherOne} ><b>Generate!</b></CButton> :
                                                        <CButton className="btn-pill" type="send" size="sm" color="primary" disabled ><Spinner as="span"
                                                            animation="border"
                                                            size="sm"
                                                            role="status"
                                                            aria-hidden="true" /><b>Creating...</b></CButton>}
                                                    </CCol>
                                                </CRow>
                                            </CModalFooter>
                                        </CModal>
                                    </CCol>
                                </CContainer>
                                <br />
                                <CDataTable
                                    items={voucherOne}
                                    fields={fieldsvone}
                                    tableFilter
                                    sorterValue={{ column: "id", asc: false }}
                                    itemsPerPageSelect
                                    itemsPerPage={5}
                                    hover
                                    sorter
                                    pagination
                                    scopedSlots={{
                                        'created_date':
                                            (item, index) => {

                                                const date = new Date((item.created_date && item.created_date.split(' ').join('T')) + '.000Z')

                                                return (
                                                    <td>
                                                        {date.toString()}
                                                    </td>
                                                )
                                            },
                                        'expired_date':
                                            (item, index) => {

                                                const date = new Date((item.expired_date && item.expired_date.split(' ').join('T')) + '.000Z')

                                                return (
                                                    <td>
                                                        {date.toString()}
                                                    </td>
                                                )
                                            },
                                        'voucher_name':
                                            (item, index) => {

                                                return (
                                                    <td  >
                                                        {item.details}
                                                    </td>
                                                )
                                            },
                                        'show_details':
                                            (item, index) => {
                                                return (
                                                    <td className="py-2">
                                                        <CButton
                                                            color="primary"
                                                            variant="outline"
                                                            shape="square"
                                                            size="sm"
                                                            onClick={() => { toggleDetails(index) }}
                                                        >
                                                            {details.includes(index) ? 'Hide' : 'Options'}
                                                        </CButton>
                                                    </td>
                                                )
                                            },
                                        'details':
                                            (item, index) => {

                                                const updateVoucher = (e) => {
                                                    e.preventDefault()
                                                    const FormData = require('form-data');
                                                    const data = new FormData();
                                                    data.append('id', item.id);

                                                    const config = {
                                                        method: 'PUT',
                                                        url: `/api/voucher/updateVoucher?id=${item.id}`,
                                                        headers: {
                                                            Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                        },
                                                        data: data
                                                    };

                                                    axios(config)
                                                        .then((response) => {
                                                            console.log(JSON.stringify(response.data));
                                                            alert('Voucher Updated!')
                                                            setGenvouchermodal(false);
                                                        })
                                                        .catch((error) => {
                                                            console.log(error);
                                                            alert('Failed!')
                                                        });
                                                    setTimeout(refreshVoucherlist, 100)
                                                };

                                                const deleteItem = () => {

                                                    const config = {
                                                        method: 'delete',
                                                        url: `api/voucher/deleteVoucherOne?id=${item.id}`,
                                                        headers: {
                                                            Authorization: 'Bearer ' + localStorage.getItem('auth')
                                                        }
                                                    };

                                                    axios(config)
                                                        .then((response) => {
                                                            console.log(JSON.stringify(response.data));
                                                            alert('Item Removed!');
                                                            setdeleteVoucherOne(false)
                                                        })
                                                        .catch((error) => {
                                                            alert('Failed to delete item!');
                                                        });
                                                    setTimeout(refreshVoucherOnelist, 100);
                                                };

                                                const updateVoucherId = (e) => {
                                                    e.preventDefault()
                                                    const FormData = require('form-data');
                                                    const data = new FormData();
                                                    data.append('voucher_id', voucherid);

                                                    const config = {
                                                        method: 'PUT',
                                                        url: `/api/voucher/updateVoucherItems?id=${item.id}`,
                                                        headers: {
                                                            Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                        },
                                                        data: data
                                                    };

                                                    axios(config)
                                                        .then((response) => {
                                                            console.log(JSON.stringify(response.data));
                                                            alert('Voucher Updated!')
                                                        })
                                                        .catch((error) => {
                                                            console.log(error);
                                                            alert('Failed!')
                                                        });
                                                    setTimeout(refreshVoucherOnelist, 100)
                                                };

                                                const updateVoucherSecretkey = (e) => {
                                                    e.preventDefault()
                                                    const FormData = require('form-data');
                                                    const data = new FormData();
                                                    data.append('secret_key', secret_key);

                                                    const config = {
                                                        method: 'PUT',
                                                        url: `/api/voucher/updateVoucherOneSecretKey?id=${item.id}`,
                                                        headers: {
                                                            Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                        },
                                                        data: data
                                                    };

                                                    axios(config)
                                                        .then((response) => {
                                                            console.log(JSON.stringify(response.data));
                                                            alert('Voucher Updated!')
                                                        })
                                                        .catch((error) => {
                                                            console.log(error);
                                                            alert('Failed!')
                                                        });
                                                    setTimeout(refreshVoucherOnelist, 100)
                                                };

                                                const updateVoucherExpiredDate = (e) => {
                                                    e.preventDefault()
                                                    const FormData = require('form-data');
                                                    const data = new FormData();
                                                    data.append('expired_date', expired_date);

                                                    const config = {
                                                        method: 'PUT',
                                                        url: `/api/voucher/updateVoucherOneExpiredDate?id=${item.id}`,
                                                        headers: {
                                                            Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                        },
                                                        data: data
                                                    };

                                                    axios(config)
                                                        .then((response) => {
                                                            console.log(JSON.stringify(response.data));
                                                            alert('Voucher Updated!')
                                                        })
                                                        .catch((error) => {
                                                            console.log(error);
                                                            alert('Failed!')
                                                        });
                                                    setTimeout(refreshVoucherOnelist, 100)
                                                };

                                                const date = new Date((item.created_date && item.created_date.split(' ').join('T')) + '.000Z')

                                                return (
                                                    <CCollapse show={details.includes(index)}>
                                                        <CCardBody>
                                                            <CFormGroup row>
                                                                <CCol align='left'>
                                                                    <CLink onClick={() => setViewdata(!viewdata)}><u>View Voucher Redeem History</u></CLink>
                                                                    <CModal
                                                                        show={viewdata}
                                                                        onClose={() => setViewdata(!viewdata)}
                                                                        color="info"
                                                                        size="md"
                                                                    >
                                                                        <CModalHeader closeButton>
                                                                            <CModalTitle>
                                                                                <b>One Key Voucher Claimed History</b>
                                                                            </CModalTitle>
                                                                        </CModalHeader>

                                                                        <CModalBody>
                                                                            <br />
                                                                            <CFormGroup row>
                                                                                <CCol md="3" align='left'>
                                                                                    <CLabel htmlFor="select"><b>Voucher ID</b></CLabel>
                                                                                </CCol>
                                                                                :
                                                                                <CCol xs="12" md="6" align='left' >
                                                                                    {item.voucher_id} - ({item.details})
                                                                                </CCol>
                                                                            </CFormGroup>
                                                                            <CFormGroup row>
                                                                                <CCol md="3" align='left'>
                                                                                    <CLabel htmlFor="select"><b>Voucher Key</b></CLabel>
                                                                                </CCol>
                                                                                :
                                                                                <CCol xs="12" md="4" align='left'>
                                                                                    {item.secret_key}
                                                                                </CCol>
                                                                            </CFormGroup>
                                                                            <CFormGroup row>
                                                                                <CCol md="3" align='left'>
                                                                                    <CLabel htmlFor="select"><b>Created Date</b></CLabel>
                                                                                </CCol>
                                                                                :
                                                                                <CCol xs="12" md="7" align='left'>
                                                                                    {date.toString()}
                                                                                </CCol>
                                                                            </CFormGroup>
                                                                            <VoucherOneHistoryList itemid={item.id} />
                                                                        </CModalBody>
                                                                        <CModalFooter>
                                                                            <CRow>
                                                                                <CCol align='center'>
                                                                                    <CButton type="send" size="sm" color="secondary" onClick={() => setViewdata(!viewdata)} ><b>Close</b></CButton>
                                                                                </CCol>
                                                                            </CRow>
                                                                        </CModalFooter>
                                                                    </CModal>
                                                                </CCol>
                                                                <CCol align='right'>
                                                                    <CButton size="sm" color="danger" className="ml-1" onClick={() => setdeleteVoucherOne(!deleteVoucherOne)}>
                                                                        X &nbsp; Delete Voucher</CButton>
                                                                    <CModal
                                                                        show={deleteVoucherOne}
                                                                        onClose={() => setdeleteVoucherOne(!deleteVoucherOne)}
                                                                        color="danger"
                                                                        size="md"
                                                                    >
                                                                        <CModalHeader closeButton>
                                                                            <CModalTitle>
                                                                                <b>Delete this voucher?</b>
                                                                            </CModalTitle>
                                                                        </CModalHeader>
                                                                        <CModalBody>

                                                                            <CCol align='center'>
                                                                                <h5>Are you sure?</h5>
                                                                            </CCol>

                                                                        </CModalBody>
                                                                        <CModalFooter>
                                                                            <CRow>
                                                                                <CCol align='center'>
                                                                                    <CButton type="send" size="sm" color="danger" onClick={deleteItem} ><b>Delete</b></CButton>&nbsp;
                                                                                    <CButton type="send" size="sm" color="secondary" onClick={() => setdeleteVoucherOne(!deleteVoucherOne)} ><b>Cancel</b></CButton>
                                                                                </CCol>
                                                                            </CRow>
                                                                        </CModalFooter>
                                                                    </CModal>
                                                                </CCol>
                                                            </CFormGroup>
                                                            <hr />
                                                            <CFormGroup row>
                                                                <CCol md="2">
                                                                    <CLabel htmlFor="select"><b>New Voucher Template</b></CLabel>
                                                                </CCol>
                                                                :
                                                                <CCol xs="12" md="4">
                                                                    <CSelect size='sm' custom name="select" id="select" className="align-items-md-start" onChange={(e) => setVoucherid(e.target.value)}>
                                                                        {dropdownVouchers}
                                                                    </CSelect>
                                                                </CCol>
                                                                <CCol md='3' align='left'>
                                                                    <CButton size="sm" color="info" onClick={updateVoucherId}>
                                                                        Update Voucher Template
                                                                    </CButton>
                                                                </CCol>
                                                            </CFormGroup>
                                                            <hr />
                                                            <CFormGroup row>
                                                                <CCol md="2">
                                                                    <CLabel htmlFor="select"><b>New Expired Date</b></CLabel>
                                                                </CCol>
                                                                :
                                                                <CCol md="2" align='left'>
                                                                    <CInput type="date" onKeyDown={(e) => e.preventDefault()} onChange={(e) => setExpiredate(e.target.value)} />
                                                                </CCol>
                                                                <CCol md="2" align='left'>
                                                                    <CInput type="time" onChange={(e) => setExpiretime(e.target.value)} />
                                                                </CCol>
                                                                <CCol md='3' align='left'>
                                                                    <CButton size="sm" color="info" onClick={updateVoucherExpiredDate}>
                                                                        Update Expired Date
                                                                    </CButton>
                                                                </CCol>
                                                            </CFormGroup>
                                                            <hr />
                                                            <CFormGroup row>
                                                                <CCol md="2">
                                                                    <CLabel htmlFor="select"><b>New Secret Key</b></CLabel>
                                                                </CCol>
                                                                :
                                                                <CCol xs="12" md="4">
                                                                    <CInput maxLength='16' onChange={(e) => setSecret_key(e.target.value)} />
                                                                </CCol>
                                                                <CCol md='3' align='left'>
                                                                    <CButton size="sm" color="info" onClick={updateVoucherSecretkey}>
                                                                        Update Secret Key
                                                                    </CButton>
                                                                </CCol>
                                                            </CFormGroup>

                                                        </CCardBody>
                                                    </CCollapse>
                                                )
                                            },

                                    }}
                                /></>}


                        </div>
                        
                    </div>
                </div>
            </div>
            <CRow>
                <CCol>

                </CCol>
            </CRow>
        </div>
    )
}

export default GenerateVoucherList
