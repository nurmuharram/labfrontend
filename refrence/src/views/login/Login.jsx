import React, {useState, useEffect} from 'react'
import { useHistory } from 'react-router'
import {
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CAlert,

} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { useDispatch, useSelector } from 'react-redux'
import { loginTypes } from 'src/redux/action/types/loginTypes'


const Login = (props) => {
  const [showModal, setShowModal] = useState(false)
  const [email,setEmail]=useState("");
  const [password,setPassword]=useState("");
  const loginData = useSelector(state => state.loginReducer.data);
  const dispatch = useDispatch();
  const loginRequest = data=>dispatch ({type:loginTypes.LOGIN_REQUEST,payload:data});
  const login =()=>{loginRequest(inputData)};
  const inputData = {email,password,}
  const history = useHistory ();
  const error = useSelector (state => state.loginReducer.error)
  //note BE nya ditmbhkan permission untuk url localhost3000 atau semuanya, 


  useEffect(() => {
    if (loginData.length>0){
      localStorage.setItem('auth',loginData)
      history.push('/dashboard')
      window.location.reload("/dashboard")
    }
  }, [loginData]);

  useEffect((state) => {
    if (error?.response?.status===502) {
   setShowModal(true)
    }
    },[error])
    console.log('ini utk error',error?.response)

  // useEffect(() => {
  //   if (props.error?.response?.status===502) {
  //     window.location.reload("/dashboard")
  //     return false;
  //   }
  //   },[props.error])    
        

    useEffect((state) => {
      if (error?.response?.status===500) {
     alert('Server Down')
      }
      },[error])
      console.log('ini utk error down',error?.response)  

    // useEffect(() => {
    //   if (props.error?.response?.status===401) {
    //   localStorage.removeItem('auth')
    //   history.push("login")
    //   }
    //   },[props.error])
    //   console.log('ini utk error',props.error?.response)
    
    // useEffect(()=>{
    //   if(localStorage.getItem()){
    //     history.push("/dashboard")
    //   }
    // }
    
    // ,[])

    return (
      <div className="c-app c-default-layout flex-row align-items-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-5">
              <div className="card-group">
                <div className="card p-4 bg-gray-100">



                  <center>
                    <img className="img-fluid" 
                      src={`${process.env.PUBLIC_URL}/avatars/Lokapala_logo_login.png`} 
                        alt="logo"   
                        width="90%" />
                  </center>
                  <CCardBody>
                    <CForm>
                      <CInputGroup className="mb-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-user" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput type="text" placeholder="email" autoComplete="email" 
                        onChange={(e)=>setEmail(e.target.value)} />
                      </CInputGroup>
                      <CInputGroup className="mb-4">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-lock-locked" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput type="password" placeholder="Password" autoComplete="current-password" 
                        onChange={(e)=>setPassword(e.target.value)}/>
                      </CInputGroup>
                      <CRow>
                        <CCol>
                          <center>
                          <button type="button" class="btn btn-success" 
                          onClick={login}
                          >Login</button>
                          </center>
                        </CCol>
                        {/* <CCol xs="6" className="text-right">
                          <CButton color="link" className="px-0">Forgot password?</CButton>
                        </CCol> */}
                      </CRow>
                    </CForm>
                  </CCardBody>

                    <CAlert color="danger"show={showModal} onClick={() => setShowModal(false)}>
                         Please check your password and email
                    </CAlert> 
                    </div>
              </div>              
            </div>
          </div>
        </div>
      </div>
    )
}

export default Login
