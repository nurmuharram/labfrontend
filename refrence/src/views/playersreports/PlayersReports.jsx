import React, { useState, useEffect, useRef } from 'react'
import toast, { Toaster } from 'react-hot-toast';

import {
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CContainer,
  CDataTable,
  CBadge,
  CLink,
  CButton,
  CInput,
  CRow,
  CSpinner

} from '@coreui/react'

import axios from 'axios'
import { useHistory } from 'react-router'
import CIcon from '@coreui/icons-react'



function PlayersReports() {

  const history = useHistory();

  const [reports, setReports] = useState([])


  /* Table Data */
  const fields = [
    { key: 'report_id', _style: { width: '5%' } },
    { key: 'report_description', _style: { width: '10%' } },
    { key: 'room_id', _style: { width: '10%' } },
    { key: 'reporters', _style: { width: '10%' } },
    { key: 'reported', _style: { width: '10%' } },
    { key: 'message', _style: { width: '10%' } },
    { key: 'report_date', _style: { width: '10%' } },
    { key: 'checked', _style: { width: '4%' } }
    /* { key: 'Approve Report', _style: { width: '10%' } } */
  ]

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/report/getReports?count=10&offset=0`, config).then(res => {
      const items = res.data;
      setReports(items);
    });
  }, [])

  const refreshReportslist = () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/report/getReports?count=10&offset=${listchange.current}`, config).then(res => {
      const items = res.data;
      setReports(items);
    });
  }

  const [changelist, setChangelist] = useState(0);

  const listchange = useRef();
  listchange.current = changelist;

  const decrementforprev = () => { setChangelist(changelist - 10); setTimeout(refreshReportslist, 100) }
  const incrementfornext = () => { setChangelist(changelist + 10); setTimeout(refreshReportslist, 100) }

  const prevbttn = () => {
    return <CButton className="info" size="md" color="primary" onClick={decrementforprev}>Prev Lists</CButton>;
  }

  const nextbttn = () => {
    return <CButton className="info" size="md" color="primary" onClick={incrementfornext} >Next Lists</CButton>;
  }

  const hideprevbttn = () => {

    if (changelist === 0) {
      return <></>
    } else {
      return prevbttn();
    }
  };

  const hidenextbttn = () => {

    if (Object.keys(reports).length < 10) {
      return <></>
    } else if (Object.keys(reports).length >= 10) {
      return nextbttn();
    }
  };

  const spinner = () => {
    return ( <div>
        <CSpinner variant="pulse" size="xxl" color="info" />
    </div>
        
    )
}

  return (
    <div>
      <CCol align='center'>
        <CCard style={{ background: '#9bf54c' }}>
          <h4><b>Players' Reports</b></h4>
          <h6>
            <i>
              List of Players' Reports
            </i>
          </h6>
        </CCard>
      </CCol>

      <br />
      <CContainer fluid>
        <CCol className="mb-4">
          <CCard accentColor="primary">
            <CCardHeader>
              Players' Reports List
            </CCardHeader>
            <CCardBody>
              <CDataTable
                items={reports}
                fields={fields}
                hover
                striped
                noItemsViewSlot={<div style={{textAlign: "center"}}><i>{spinner()}Loading Data, Please Wait...</i></div>}
                //loading
                //sorterValue
                scopedSlots={{
                  'report_description':
                    (item) => {
                      const getBadge = report_type => {
                        switch (report_type) {
                          case 'AFK': return 'warning'
                          case ' AFK': return 'warning'
                          case 'Low Level Skill': return 'info'
                          case ' Low Level Skill': return 'info'
                          case ' Cheating': return 'danger'
                          case 'Cheating': return 'danger'
                          case ' Throwing': return 'secondary'
                          case 'Throwing': return 'secondary'
                          case 'Feeding': return 'light'
                          case ' Feeding': return 'light'
                          case 'Inappropriate Name': return 'warning'
                          default: return 'primary'
                        }
                      };

                      return (
                        <td>
                          {item.description.split(",").map(desc =>
                            <CBadge color={getBadge(desc)}>
                              {desc}
                            </CBadge>
                          )}
                        </td>
                      )
                    },
                  'reported':
                    (item) => (
                      <td>
                        <CLink onClick={() => history.push(`/playerdetail/${item.reported_user_id}`)}>{item.reported_user_name}</CLink>
                      </td>
                    ),
                  'reporters':
                    (item) => {
                      const reporters = item.reporter_user_name.split(', ')
                      const reporterOne = reporters[0]
                      const reporterTwo = reporters[1]
                      const reporterThree = reporters[2]

                      const reportersid = item.reporter_user_id.split(', ')
                      const reporteridOne = reportersid[0]
                      const reporteridTwo = reportersid[1]
                      const reporteridThree = reportersid[2]
                      
                      return (
                        <td>
                          <CLink onClick={() => history.push(`/playerdetail/${reporteridOne}`)}>{reporterOne}</CLink>,&nbsp;
                          <CLink onClick={() => history.push(`/playerdetail/${reporteridTwo}`)}>{reporterTwo}</CLink>,&nbsp;
                          <CLink onClick={() => history.push(`/playerdetail/${reporteridThree}`)}>{reporterThree}</CLink>
                        </td>
                      )
                    },
                  'room_id':
                    (item) => (
                      <td>
                        <CLink onClick={() => history.push(`/matchdetails/${item.room_id}`)}>{item.room_id}</CLink>
                      </td>
                    ),
                    'checked':
                    (item) => (
                      <td>
                        {item.checked === 1 ? <div>&#10004;</div> : <div>&#x2716;</div>}
                      </td>
                    ),
                  'message':
                    (item) => {
                      const message = () => {
                        if (item.message === "") {
                          return "-"
                        } else {
                          return item.message
                        }
                      }
                      return (
                        <td>
                          {message()}
                        </td>
                      )
                    },
                  'Approve Report':
                    (item, index) => {
                      const approve = () => {
                        const config = {
                          method: 'PUT',
                          url: `/api/report/approveProfileReport?report_id=${item.report_id}`,
                          headers: {
                            Authorization: 'Bearer ' + localStorage.getItem('auth'),
                          },

                        };

                        axios(config)
                          .then((response) => {
                            toast.success("Report Approved!")
                          })
                          .catch((error) => {
                            console.log(error);
                            toast.error('Approve Failed!')
                          });
                      }

                      const approvebutton = () => {
                        return (
                          <div key={item.report_id}>
                            <CButton color="info" size="sm" onClick={() => { approve(); { history.push(`/playerdetail/${item.reported_user_id}`) } }}>Approval</CButton>
                          </div>
                        )
                      }

                      const rejectbttn = () => {
                        return (
                          <div key={item.report_id}>
                            <CButton color="danger" size="sm" onClick={() => history.push(`/playerdetail/${item.reported_user_id}`)}><CIcon name="cil-x" size="sm" />{' '}Reject</CButton>
                          </div>
                        )
                      }

                      const approvechnge = () => {
                        if (item.checked === 0) {
                          return approvebutton();
                        } else if (item.checked === 1) {
                          return rejectbttn();
                        }
                      }
                      return (
                        <td>
                          {approvechnge()}
                        </td>
                      )
                    },
                  "report_date":
                    (item, index) => {
                      const date = new Date((item.report_date && item.report_date.split(' ').join('T')) + '.000Z')
                      return (
                        <td>
                          {date.toString()}
                        </td>
                      )
                    }

                }}
              />
              <CRow>
                {reports === null ? <></> : <><CCol align='left'>
                  {hideprevbttn()}
                </CCol>
                  <CCol align='right'>
                    {hidenextbttn()}
                  </CCol></>}
              </CRow>
            </CCardBody>

            <CCardFooter>

            </CCardFooter>
          </CCard>
        </CCol>
      </CContainer>
    </div>
  )
}

export default PlayersReports
