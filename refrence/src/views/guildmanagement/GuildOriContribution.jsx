import React from 'react'
import DataTable from 'src/components/table/DataTable';


function GuildOriContribution({ oricontribution, hideNextButton, hidePreviousButton }) {

    const fields = [
        { key: 'id', filter: false },
        { key: 'user_id', filter: false },
        { key: 'user_name', filter: false },
        { key: 'year_week', filter: false },
        { key: 'amount', filter: false },
        { key: 'contribution_date', filter: false },
        { key: 'room_id', filter: false },

        /* {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        } */
    ]

    return (
        <div>
            <div className="row">
                
            </div>

                <div className="row">
                    <div className="col">
                        <div className="card">
                            <div className="card-body">
                            <DataTable
                                items={oricontribution}
                                fields={fields}
                                hover
                                pagination
                                border
                                noItemsViewSlot={<>
                                    <div className="row" align='center'>
                                    <div className="col">
                                        <h4>No Data</h4>
                                    </div>
                                </div></>}                            
                                responsive
                                itemsPerPage={10}
                                scopedSlots={{
                                    'id':
                                        (item) => (
                                            <td>
                                                {item.guild_ori_cont_id}
                                            </td>)
                                }}

                            />
                            <div className="row">
                                {oricontribution === null ? <></> : <>
                                <div className="col" align='left'>
                                {hidePreviousButton()}
                                </div>
                                <div className="col" align='right'>
                                {hideNextButton()}
                                </div></>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default GuildOriContribution
