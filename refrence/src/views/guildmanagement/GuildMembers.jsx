import { CCard, CCardBody, CCol, CDataTable, CRow } from '@coreui/react'
import React, { useState, useEffect } from 'react'
import DataTable from 'src/components/table/DataTable'

function GuildMembers({ guildmembers, history }) {

    const fields = [
        { key: 'User_id', filter: false },
        { key: 'user_name', filter: false },
        { key: 'join_date', filter: false },
        { key: 'member_rank', filter: false },
        { key: 'last_check_in', filter: false },
        /* {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        } */
    ]

    return (
        <div>
            <div className="row">
                <div className="col">
                    <div className="card">
                        <div className="card-body">
                            <DataTable
                                    items={guildmembers}
                                    fields={fields}
                                    sorterValue={{ column: "join_date", asc: true }}
                                    hover
                                    pagination
                                    border
                                    responsive
                                    itemsPerPage={10}
                                    clickableRows
                                    onRowClick={(item) => history.push(`/playerdetail/${item.User_id}`)}

                                />
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default GuildMembers
