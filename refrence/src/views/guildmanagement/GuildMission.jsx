import React from 'react';
import DataTable from 'src/components/table/DataTable';


function GuildMission({ guildmissions, hideNextButton, hidePreviousButton }) {

    const fieldsmission = [
        { key: 'mission_id', filter: false },
        { key: 'description', filter: false },
        { key: 'level_req', filter: false },
        { key: 'target', filter: false },
        { key: 'difficulty', filter: false },
        { key: 'completion', filter: false },
        { key: 'start_date', filter: false },
        { key: 'end_date', filter: false },
        { key: 'done_date', filter: false },
        { key: 'reward', filter: false },
        /* {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        } */
    ]

    return (
        <div>
            <div className="row">
                <div className="col">
                    <div className="card">
                        <div className="card-body">
                            <DataTable
                                items={guildmissions}
                                fields={fieldsmission}
                                hover
                                pagination
                                border
                                noItemsViewSlot={<>

                                <div className="row" align='center'>
                                    <div className="col">
                                        <h4>No Data</h4>
                                    </div>
                                </div></>}
                                responsive
                                itemsPerPage={10}
                                scopedSlots={{
                                    'mission_id':
                                        (item) => (
                                            <td>
                                                {item.gulld_mission_id}
                                            </td>),
                                    'reward':
                                        (item) => (
                                            <td>
                                                {item.item_name}
                                            </td>),
                                    'done_date':
                                        (item) => (
                                            <td>
                                                {item.done_date === null ? 'Unfinished' : item.done_date}
                                            </td>),
                                    'completion':
                                        (item) => (
                                            <td>
                                                {item.completion_type}
                                            </td>),
                                }}

                            />
                            <div className="row">
                                {guildmissions === null ? <></> : <>
                                <div className="col" align='left'>
                                {hidePreviousButton()}
                                </div>
                                <div className="col" align='right'>
                                {hideNextButton()}
                                </div></>}
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default GuildMission
