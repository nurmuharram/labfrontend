import React from 'react';
import DataTable from 'src/components/table/DataTable';


function GuildMemberCheckinLog({ guildmembercheckin, hideNextButton, hidePreviousButton}) {

    const fields = [
        { key: 'check_in_id', filter: false },
        { key: 'user_id', filter: false },
        { key: 'user_name', filter: false },
        { key: 'check_in_date', filter: false },
        /* {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        } */
    ]

    return (
        <div>
            <div className="row">
                <div className="col">
                    <div className="card-body">
                    <DataTable
                                items={guildmembercheckin}
                                fields={fields}
                                hover
                                pagination
                                border
                                noItemsViewSlot={<>
                                <div className="row" align='center'>
                                    <div className="col">
                                        <h4>No Data</h4>
                                    </div>
                                </div></>}
                                responsive
                                itemsPerPage={10}
                            />
                            <div className="row">
                                {guildmembercheckin === null ? <></> : <>
                                <div className="col" align='left'>
                                {hidePreviousButton()}
                                </div>
                                <div className="col" align='right'>
                                {hideNextButton()}
                                </div></>}
                            </div>

                    </div>
                </div>
            </div>

        </div>
    )
}

export default GuildMemberCheckinLog
