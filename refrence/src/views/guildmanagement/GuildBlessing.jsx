import React from 'react';
import DataTable from 'src/components/table/DataTable';


function GuildBlessing({guildblessings, hideNextButton, hidePreviousButton}) {

    const fieldsBlessings = [
        { key: 'user_name', filter: false },
        { key: 'play_date', filter: false },
        { key: 'start_date', filter: false },
        { key: 'end_date', filter: false },
        { key: 'item_description', filter: false },
        { key: 'claim_date', filter: false },
        { key: 'obtained_time', filter: false },
        /* {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        } */
    ]

    return (
        <div>
            <div className="row">
                <div className="col">
                    <div className="card">
                        <div className="card-body">

                            <DataTable
                                items={guildblessings}
                                fields={fieldsBlessings}
                                hover
                                border
                                responsive
                                noItemsViewSlot={<>
                                <div className="row" align='center'>
                                    <div className="col">
                                        <h4>No Data</h4>
                                    </div>
                                </div></>}
                                itemsPerPage={10}
                                scopedSlots={{
                                    'mission_id':
                                        (item) => (
                                            <td>
                                                {item.gulld_mission_id}
                                            </td>),
                                    'reward':
                                        (item) => (
                                            <td>
                                                {item.item_name}
                                            </td>),
                                    'obtained_time':
                                        (item) => (
                                            <td>
                                                {item.obtained_time === null ? 'Not obtained yet' : item.obtained_time}
                                            </td>)
                                    
                                }}

                            />
                            <div className="row">
                                {guildblessings === null ? <></> : <>
                                <div className="col" align='left'>
                                {hidePreviousButton()}
                                </div>
                                <div className="col" align='right'>
                                {hideNextButton()}
                                </div></>}
                            </div>
                           

                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default GuildBlessing
