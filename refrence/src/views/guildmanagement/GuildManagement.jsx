import React, {useState } from 'react'
import { useHistory } from 'react-router-dom'
import { GetGuildList } from 'src/api/guildManagementApi/GuildGetApi'
import DataTable from 'src/components/table/DataTable'

function GuildManagement() {
    
    const history = useHistory()

    const [guildData, setGuildData] = useState([])
    
    GetGuildList({setGuildData})

    const fields = [
        { key: 'guild_id', filter: false },
        { key: 'guild_name', filter: false },
        { key: 'guild_initial', filter: false },
        { key: 'country', filter: false },
        { key: 'member_count', filter: false },
        /* {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        } */
    ]

    return (
        <div>
            <div className="col" align="center">
            <div className="card" style={{ background: "#9bf54c" }}>
            <h4><b>Guild Management</b></h4>
            <h6><i>View Guilds</i></h6>
            </div>
            </div>
            <br />
            <div className="container-fluid">
                <div className="col">
                    <div className='card card-accent-primary'>
                        <div className="card-header" align="center">
                            <h5><b>Guild List</b></h5>
                        </div>
                            <div className="card-body">

                                <DataTable
                                    items={guildData}
                                    fields={fields}
                                    pagination
                                    itemsPerPage={10}
                                    itemsPerPageSelect
                                    sorterValue={{column: "guild_id", asc: false}}
                                    clickableRows
                                    hover
                                    tableFilter
                                    onRowClick={(item) => history.push(`/guilddashboard/${item.guild_id}`)}
                                    scopedSlots={{
                                        'guild_initial':
                                            (item) => (
                                                <td>
                                                    {item.guild_initital}
                                                </td>
                                            )
                                    }}
                                    />
                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}

export default GuildManagement
