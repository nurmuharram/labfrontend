import React from 'react';
import DataTable from 'src/components/table/DataTable';


function GuildMemberRankLogs({ guildRankLogs, hideNextButton, hidePreviousButton }) {

    
    const fields = [
        { key: 'rank_log_id', filter: false },
        { key: 'user_name', filter: false },
        { key: 'Member Rank (Before > After)', filter: false },
        { key: 'changelog_date', filter: false },
        { key: 'user_incharge', filter: false },
        /* {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        } */
    ]

    return (
        <div>
            <div className="row">
                <div className="col">
                    <div className="card">
                        <div className="card-body">
                        <DataTable
                                items={guildRankLogs}
                                fields={fields}
                                hover
                                pagination
                                border
                                noItemsViewSlot={<>
                                <div className="row" align='center'>
                                    <div className="col">
                                        <h4>No Data</h4>
                                    </div>
                                </div></>}
                                responsive
                                itemsPerPage={10}
                                scopedSlots={{
                                    'rank_log_id':
                                        (item) => (
                                            <td>
                                                {item.guild_member_rank_log_id}
                                            </td>),
                                    'user_incharge':
                                        (item) => (
                                            <td>
                                                {item.user_name_incharge}
                                            </td>),
                                    'Member Rank (Before > After)':
                                        (item) => (
                                            <td>
                                                {item.before_rank_description} &#8594; {item.after_rank_description}
                                            </td>),
                                }}

                            />
                            <div className="row">
                                {guildRankLogs === null ? <></> : <>
                                <div className="col" align='left'>
                                {hidePreviousButton()}
                                </div>
                                <div className="col" align='right'>
                                {hideNextButton()}
                                </div></>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default GuildMemberRankLogs
