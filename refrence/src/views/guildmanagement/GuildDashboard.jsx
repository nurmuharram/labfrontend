import React, { useEffect, useState } from 'react'

import {
    
    CCol,CNav,
    CNavItem,
    CNavLink,

    CTabContent,
    CTabPane,
    CTabs,

    CModalTitle,

} from '@coreui/react'
import GuildMembersservices from 'src/services/guild.services/GuildMembers.services';
import GuildMissionservices from 'src/services/guild.services/GuildMission.services';
import GuildCitrineContributionservices from 'src/services/guild.services/GuildCitrineContribution.services';
import GuildOriContributionservices from 'src/services/guild.services/GuildOriContribution.services';
import GuildMemberCheckinLogsservices from 'src/services/guild.services/GuildMemberCheckinLogs.services';
import GuildMemberRankLogsservices from 'src/services/guild.services/GuildMemberRankLogs.services';
import GuildMemberLogsservices from 'src/services/guild.services/GuildMemberLogs.services';
import GuildBlessingservices from 'src/services/guild.services/GuildBlessing.services';
import { ChangeGuildInitial, ChangeGuildMotto, ChangeGuildName } from 'src/api/guildManagementApi/GuildPostApi';
import { Modal, ModalBody, ModalFooter, ModalHeader, ModalTitle } from 'src/components/modal';

function GuildDashboard({ match, guilddata, history }) {
    const [guildName, setGuildName] = useState(false);
    const [guildInitial, setGuildInitial] = useState(false);
    const [guildMotto, setGuildMotto] = useState(false);

    return (
        <div>
            <div className="col" align="center">

                <div className="card" style={{ background: "#9bf54c" }}>
                    <h4><b>Guild Dashboard</b></h4>
                    <h6>
                        <i>
                            View Guilds' Data
                        </i>
                    </h6>

                 </div>    

            </div>

            <br />
            <div className="container-fluid">
                <div className="row">
                    <div className="col">
                        <button className='btn btn-info btn-md btn-ghost-info' onClick={() => history.goBack()} >&#8592; Back to Previous Page </button>
                        <hr />
                        <div className="card card-accent-primary">
                            <div className="card-header">
                                <div className="row">
                                    <div className="col">
                                        <i>Guild ID: {match.params.guild_id}</i>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-lg-4 col-md-4" align='center'>
                                        <div className="col" align='center'>
                                            <label ><h4><b> {guilddata.guild_name} </b></h4></label>
                                        </div>
                                        <div className="col" align='center'>
                                            <label>({guilddata.guild_initital})</label>
                                        </div>
                                        
                                        <div className="col" align='center'>
                                        <img
                                        className="border-dark shadow-lg rounded"
                                        src={`${process.env.PUBLIC_URL}/assets/img/avataricon/1.png`}
                                        fluid
                                        width="130"
                                        align="center"/> 
                                        </div>

                                        <div className="col" align='center'>
                                            <label>
                                                <i>
                                                Guild id: {guilddata.guild_id}
                                                </i>
                                            </label>
                                        </div>
                                        <div className="col" align='center'>
                                        <button className='btn btn-info btn-sm ' onClick={() => setGuildName(!guildName)} >Change Guild Name </button> 
                                                                               
                                            <Modal
                                                show={guildName}
                                                onClose={() => setGuildName(!guildName)}
                                                color="info"
                                            >
                                                <ModalHeader closeButton>
                                                    <CModalTitle>Set Guild Name</CModalTitle>
                                                </ModalHeader>
                                                <ModalBody align="center">
                                                    <div className="col">
                                                        <u align="center">New Guild Name</u>
                                                    </div>
                                                    <br />
                                                    <div className="col">
                                                        <input class="form-control" placeholder='Max 30 Character' type="text" maxLength={30} onChange={(e) => (setGuildName(e.target.value))}/>
                                                    </div>
                                                   
                                                </ModalBody>
                                                <ModalFooter>
                                                    <ChangeGuildName match={match} guildName={guildName} setGuildName={setGuildName} />
                                                    <button className='btn btn-secondary btn-sm ' onClick={() => setGuildName(!guildName)} >Cancel </button> 
                                                </ModalFooter>
                                            </Modal>
                                            <br />
                                        </div>

                                        <div className="col" align='center'>
                                        <button className='btn btn-primary btn-sm ' onClick={() => setGuildInitial(!guildInitial)} >Change Guild Initial </button> 
                                                                               
                                            <Modal
                                                show={guildInitial}
                                                onClose={() => setGuildInitial(!guildInitial)}
                                                color="info"
                                            >
                                                <ModalHeader closeButton>
                                                    <CModalTitle>Set Guild Initial</CModalTitle>
                                                </ModalHeader>
                                                <ModalBody align="center">
                                                    <div className="col">
                                                        <u align="center">New Guild Initial</u>
                                                    </div>
                                                    <br />
                                                    <div className="col">
                                                        <input class="form-control" placeholder='Max 4 Character' type="text" maxLength={4} onChange={(e) => (setGuildInitial(e.target.value))}/>
                                                    </div>
                                                    {/* <CButton color='light'>{karma}</CButton> */}
                                                </ModalBody>
                                                <ModalFooter>
                                                    <ChangeGuildInitial match={match} guildInitial={guildInitial} setGuildInitial={setGuildInitial}/>
                                                    <button className='btn btn-secondary btn-sm ' onClick={() => setGuildInitial(!guildInitial)} >Cancel </button> 
                                                </ModalFooter>
                                            </Modal>
                                            <br />
                                        </div>

{/* 
                                    </CCol> */}
                                    </div>
                                    <div className="col-lg-6 col-md-8" align="left">
                                            <br />
                                             <button className='btn btn-success btn-sm ' onClick={() => setGuildMotto(!guildMotto)} >Change Guild Motto </button> 

                                                <Modal
                                                        show={guildMotto}
                                                        onClose={() => setGuildMotto(!guildMotto)}
                                                        color="info"
                                                >
                                                    <ModalHeader closeButton>
                                                        
                                                        <ModalTitle>Set Guild Motto</ModalTitle>
                                                    </ModalHeader>
                                                    <ModalBody align="center">
                                                        <div className="col">
                                                            <u align="center">New Guild Motto</u>
                                                        </div>
                                                        <br />
                                                        <div className="col">
                                                            <input class="form-control" placeholder='Max 30 Character' type="text" maxLength={512} onChange={(e) => (setGuildMotto(e.target.value))}/>
                                                        </div>
                                                        {/* <CButton color='light'>{karma}</CButton> */}
                                                    </ModalBody>
                                                    <ModalFooter>
                                                        <ChangeGuildMotto match={match} guildMotto={guildMotto} setGuildMotto={setGuildMotto}/>
                                                        <button className='btn btn-success btn-sm ' onClick={() => setGuildMotto(!guildMotto)} >Cancel </button> 
                                                    </ModalFooter>
                                                </Modal>
                                            <br />
                                        <div className="bd-example">
                                            <dl className="row">
                                                <dt className="col-lg-4 col-md-4">Guild Owner</dt>
                                                <dd className="col-lg-6 col-md-6">:&nbsp;&nbsp;&nbsp;&nbsp;{guilddata.guild_owner_name}</dd>
                                                <dt className="col-sm-4">Country</dt>
                                                <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;{guilddata.country}&nbsp;</dd>
                                                <dt className="col-sm-4">Rank Requirement</dt>
                                                <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;<b><i>{
                                                    guilddata.rank_requirement === 0 ? 'No Rank' :
                                                        guilddata.rank_requirement === 1 ? 'Ardharathi' :
                                                            guilddata.rank_requirement === 2 ? 'Rathi' :
                                                                guilddata.rank_requirement === 3 ? 'Ekarathi' :
                                                                    guilddata.rank_requirement === 4 ? 'Atirathi' :
                                                                        guilddata.rank_requirement === 5 ? 'Maharathi' :
                                                                            guilddata.rank_requirement === 6 ? 'Atimaharathi' :
                                                                                guilddata.rank_requirement === 7 ? 'Mahamaharathi' : ''
                                                }</i></b></dd>
                                                <dt className="col-sm-4">Member Count</dt>
                                                <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;{guilddata.member_count}</dd>
                                                <dt className="col-sm-4">Max Member</dt>
                                                <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;<b><i>{guilddata.max_member}</i></b></dd>
                                                <dt className="col-sm-4">Guild Level</dt>
                                                <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;<b><i>{guilddata.guild_level}</i></b></dd>
                                                <dt className="col-sm-4">Guild Motto</dt>
                                                <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;<b><i>{guilddata.motto}</i></b></dd>
                                                <dt className="col-sm-4">Blessing Level</dt>
                                                <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;<b><i>{guilddata.guild_blessing_level}</i></b></dd>
                                            </dl>
                                        </div>                                        
                                    </div>
                                    <CCol md='8' lg="6" align="left">

                                    </CCol>
                                    {/* <CCol md='3'>
                                    <CCol md='12' align='left'>
                                    <CLabel><h4><b>{guilddata.guild_name}</b></h4></CLabel>
                                    </CCol>
                                    <CCol md='12' align='left'>
                                    <CLabel>({guilddata.guild_initital})</CLabel>
                                    </CCol>
                                    </CCol> */}
                                </div>
                            </div>

                        </div>
  
                        <hr />
                        <div className="card">
                            <div className="card-body">
                            <CTabs>
                                    <CNav variant='tabs'>
                                        <CNavItem>
                                            <CNavLink>
                                                Guild Member
                                            </CNavLink>
                                        </CNavItem>
                                        <CNavItem>
                                            <CNavLink>
                                                Guild Mission
                                            </CNavLink>
                                        </CNavItem>
                                        <CNavItem>
                                            <CNavLink>
                                                Guild Blessing
                                            </CNavLink>
                                        </CNavItem>
                                        <CNavItem>
                                            <CNavLink>
                                                Logs
                                            </CNavLink>
                                        </CNavItem>
                                        <CNavItem>
                                            <CNavLink>
                                                Contribution
                                            </CNavLink>
                                        </CNavItem>
                                    </CNav>
                                    <CTabContent>
                                        <CTabPane>
                                            <GuildMembersservices match={match} />
                                        </CTabPane>
                                        <CTabPane>
                                            <GuildMissionservices match={match} />
                                        </CTabPane>
                                        <CTabPane>
                                            <GuildBlessingservices match={match} />
                                        </CTabPane>
                                        <CTabPane>
                                            <CTabs>
                                                <br />
                                                <CNav variant="tabs">
                                                    <CNavItem>
                                                        <CNavLink>
                                                            Member Logs
                                                        </CNavLink>
                                                    </CNavItem>
                                                    <CNavItem>
                                                        <CNavLink>
                                                            Member Rank Logs
                                                        </CNavLink>
                                                    </CNavItem>
                                                    <CNavItem>
                                                        <CNavLink>
                                                            Member Check-in Logs
                                                        </CNavLink>
                                                    </CNavItem>
                                                </CNav>
                                                <CTabContent>
                                                    <CTabPane>
                                                        <GuildMemberLogsservices match={match} />
                                                    </CTabPane>
                                                    <CTabPane>
                                                        <GuildMemberRankLogsservices match={match} />
                                                    </CTabPane>
                                                    <CTabPane>
                                                        <GuildMemberCheckinLogsservices match={match} />
                                                    </CTabPane>
                                                </CTabContent>
                                            </CTabs>
                                        </CTabPane>
                                        <CTabPane>
                                            <CTabs>
                                                <br />
                                                <CNav variant='tabs'>
                                                    <CNavItem>
                                                        <CNavLink>
                                                            Ori Contribution
                                                        </CNavLink>
                                                    </CNavItem>
                                                    <CNavItem>
                                                        <CNavLink>
                                                            Citrine Contribution
                                                        </CNavLink>
                                                    </CNavItem>
                                                </CNav>
                                                <CTabContent>
                                                    <CTabPane>
                                                        <GuildOriContributionservices match={match} />
                                                    </CTabPane>
                                                    <CTabPane>
                                                        <GuildCitrineContributionservices match={match} />
                                                    </CTabPane>
                                                </CTabContent>
                                            </CTabs>
                                        </CTabPane>
                                    </CTabContent>
                                </CTabs>
                            </div>
                        </div>

                    </div>
                    
                </div>

            </div>
        </div>
    )
}

export default GuildDashboard
