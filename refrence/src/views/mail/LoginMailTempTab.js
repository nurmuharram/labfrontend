import React, { useEffect, useState } from 'react'
import toast, { Toaster } from 'react-hot-toast';
import {
    
    CButton,
    CLabel,
    CCol,
    CSelect,
    CFormGroup,
    CRow,
    CInput,
    CForm,
    CCardFooter,
    CCard,
    CCardBody
} from '@coreui/react'
import { useSelector, useDispatch } from 'react-redux'
import { getTemplates } from 'src/redux/action/mailAction/getmailtemplAction'
import axios from 'axios'

function LoginMailTempTab({ setLoginmailModal, refreshLoginMail }) {

    const [_attachments, set_attachments] = useState([0])

    const [startdate, setStartdate] = useState('2019-10-10')
    const [starttime, setStarttime] = useState('00:00')
    const start_date = startdate + ' ' + starttime + ':' + '00'
    const [enddate, setEnddate] = useState('2019-10-10')
    const [endtime, setEndtime] = useState('00:00')
    const end_date = enddate + " " + endtime + ':' + '00'

    const [template_id, setTemplate_id] = useState('')
    const [parameterlogin, setParameterlogin] = useState('')
    //const [secondary, setSecondary] = useState(false)

    /* GET TEMPLATES - GET Request with Redux */
    const dispatch = useDispatch();
    const templates = useSelector((state) => state.templates.templates);


    const adatestart = new Date(start_date)

    const toisostart = adatestart.toISOString();

    const splitdatestart = toisostart.split('T')

    const fixdatestart = splitdatestart.join(' ')


    const adateend = new Date(end_date)

    const toisoend = adateend.toISOString();

    const splitdateend = toisoend.split('T')

    const fixdateend = splitdateend.join(' ')



    /* useEffect(() => {
        const config = {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
          }
        }
        axios.get(`api/mail/getAttachmentByTemplateIdOnly?template_id=${template_id}`, config).then(res => {
          const _attachments = res.data;
          set_attachments(_attachments);
        });
      }, [template_id]) */

    useEffect(() => {
        dispatch(getTemplates());
    }, []);

    const listDropdown = templates.map((template, index) => {
        return (<option key={template.toString()} value={template.template_id} >T#{template.template_id} - {template.subject}</option>)
    })

    /* const parameter = _attachments && _attachments.map((item)=>{
        return JSON.stringify(`{'${item.item_name}' ${":"} ${item.amount}}`)
     });

     const param = `template_id: ${template_id}` + '; ' + parameter */
 

    /* CREATE LOGIN MAIL - POST Request */
    const addLoginMail = async (e) => {
        e.preventDefault();

        const data = new FormData();
        data.append('template_id', template_id);

        data.append('start_date', fixdatestart.replace('.000Z', ''));
        data.append('end_date', fixdateend.replace('.000Z', ''))

        const config = {
            method: 'POST',
            url: `/api/mail/addLoginMail`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),

            },
            data: data
        };

        await axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                toast.success("Login Mail Sent!")
                setLoginmailModal(false)
            })
            .catch((error) => {
                console.log(error);
                toast.error("Failed to Send!")

            });
            setTimeout(refreshLoginMail, 100)
    }

    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardBody>
                            <CFormGroup row>

                                <CCol md="3">
                                    <b>Mail Template</b>
                                </CCol>
                                <CCol>
                                    <CRow>
                                        <b>:</b>
                                        <CCol xs="12" md="10">
                                            <CSelect custom name="select" id="select" className="align-self-sm-start" onChange={(e) => setTemplate_id(e.target.value)} >
                                                <option value="1">Choose a template...</option>
                                                {listDropdown}
                                            </CSelect>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CFormGroup>
                            <CFormGroup row>
                                <CCol md="3">
                                    <b>Set Schedule</b>
                                </CCol>
                                <CCol xs="12" md="8">
                                    {/* Data will be inserted in here */}
                                    <CRow>
                                        <b>:</b>
                                        <CCol md="12" xl="8">
                                            <CCol align="left">
                                                <CLabel> <i>Start Date</i></CLabel> </CCol>
                                            <CForm action="" method="post" className="form-horizontal">
                                                <CFormGroup row>
                                                    <CCol xs="6">
                                                        <CInput type="date" onChange={(e) => setStartdate(e.target.value)} />
                                                    </CCol>
                                                    <CCol xs="6">
                                                        <CInput type="time" onChange={(e) => setStarttime(e.target.value)} />
                                                    </CCol>
                                                </CFormGroup>
                                            </CForm>

                                        </CCol>

                                        <CCol md="12" xl="8">
                                            <CCol align="left">
                                                <CLabel> <i>End Date</i></CLabel> </CCol>
                                            <CForm action="" method="post" className="form-horizontal">
                                                <CFormGroup row>
                                                    <CCol xs="6">
                                                        <CInput type="date" onChange={(e) => setEnddate(e.target.value)} />
                                                    </CCol>
                                                    <CCol xs="6">
                                                        <CInput type="time" onChange={(e) => setEndtime(e.target.value)} />
                                                    </CCol>
                                                </CFormGroup>
                                            </CForm>

                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CFormGroup>
                        </CCardBody>
                        <CCardFooter>
                            <CCol align='right'>
                            <CButton color="primary" onClick={addLoginMail} >
                                Send Login Mail
                            </CButton>
                            </CCol>
                        </CCardFooter>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default LoginMailTempTab
