import React, { useState, useEffect, useRef } from 'react'
import {
    CRow,
    CCol,
    CCard,
    CCardHeader,
    CCardBody,
    CDataTable,
    CBadge,
    CButton,
    CCollapse,
    CInput,
    CContainer,
    CModal,
    CModalHeader,
    CModalBody,
    CModalTitle,
    CSpinner
} from '@coreui/react'

import { useSelector, useDispatch } from 'react-redux'
import { getMails } from 'src/redux/action/mailAction/getMailsAction'
import axios from 'axios'
import SendMailsTempTab from './SendMailsTempTab'
import SendCustomMails from './SendCustomMail'




function ScheduledMail({ items, customDropdown, listDropdown, template_id, custom_message_id, setCustom_message_id, _attachments, setTemplate_id, handleChange, handleChangeCustom }) {
    const [details, setDetails] = useState([])

    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    const send_date = date + " " + time;

    const [sendmailmodal, setSendmailmodal] = useState(false)
    const [sendcustommailmodal, setSendcustommailmodal] = useState(false)

    const [mails, setMails] = useState([])

    const mailschange = useRef()
    mailschange.current = mails;


    //const [items, setItems] = useState(MailTemporaryData)
    


    /* Show/Hide Toggle to update Send Date */
    const toggleDetails = (index) => {
        const position = details.indexOf(index)
        let newDetails = details.slice()
        if (position !== -1) {
            newDetails.splice(position, 1)
        } else {
            newDetails = [...details, index]
        }
        setDetails(newDetails)
    }

    /* Table Data */
    const fields = [
        { key: 'mail_id', _style: { width: '10%' }, filter: false  },
        { key: 'reciever_id', _style: { width: '20%' }, filter: false  },
        { key: 'send_date', _style: { width: '20%' }, filter: false  },
        { key: 'parameter', _style: { width: '30%'}, filter: false },

        { key: 'status', _style: { width: '30%' }, filter: false },
        /* {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        } */
    ]

    /* GET MAILS - GET Request */
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`api/mail/get?count=10&offset=${listchange.current}`, config).then(res => {
            const mails = res.data;
            setMails(mails);
        });
    }, []);



    /* mails data refresh */
    const refreshMail = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`api/mail/get?count=10&offset=${listchange.current}`, config).then(res => {
            const mails = res.data;
            setMails(mails);
        });
    }

    const [changelist, setChangelist] = useState(0);

    const listchange = useRef();
    listchange.current = changelist;

    const decrementforprev = () => { setChangelist(changelist - 10); setTimeout(refreshMail, 100) }
    const incrementfornext = () => { setChangelist(changelist + 10); setTimeout(refreshMail, 100) }

    const prevbttn = () => {
        return <CButton className="info" size="md" color="primary" onClick={decrementforprev}>Prev Lists</CButton>;
    }

    const hideprevbttn = () => {

        if (changelist === 0) {
            return <></>
        } else {
            return prevbttn();
        }
    };

    /* UPDATE SEND DATE - PUT Request */
    const updateSenddate = async (mail_id) => {
        /* const id = mails.match.params.mail_id */
        const FormData = require('form-data');
        const data = new FormData();
        data.append('send_date', send_date);

        const config = {
            method: 'PUT',
            url: `/api/mail/setSendDate?=${mail_id}`,
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };


        await axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Send Date Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Update!')
            });
    }

    const spinner = () => {
        return ( <div>
            <CSpinner variant="pulse" size="xxl" color="info" />
        </div>
            
        )
    }


    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            <b>Scheduled Mail(s)</b>
                        </CCardHeader>
                        <CCardBody>
                            <CContainer>
                                <CCol align='right'>
                                    <CButton color="info" size="md" xs='3' onClick={() => setSendmailmodal(!sendmailmodal)}>Send Mail to Player(s)</CButton>
                                    <CModal
                                        show={sendmailmodal}
                                        onClose={() => setSendmailmodal(!sendmailmodal)}
                                        color="info"
                                        size="xl"
                                    >
                                        <CModalHeader closeButton>
                                            <CModalTitle>
                                                <b>Send Mail to a Player</b>
                                            </CModalTitle>
                                        </CModalHeader>
                                        <CModalBody>
                                            <CContainer>
                                                <SendMailsTempTab
                                                    customDropdown={customDropdown}
                                                    listDropdown={listDropdown}
                                                    items={items}
                                                    template_id={template_id}
                                                    custom_message_id={custom_message_id}
                                                    setCustom_message_id={setCustom_message_id}
                                                    _attachments={_attachments}
                                                    setSendmailmodal={setSendmailmodal}
                                                    refreshMail={refreshMail}
                                                    setTemplate_id={setTemplate_id}
                                                    handleChange={handleChange}
                                                />
                                            </CContainer>
                                        </CModalBody>
                                    </CModal>
                                </CCol>
                                <br />
                                <CCol align='right'>
                                    <CButton color="info" size="md" xs='3' onClick={() => setSendcustommailmodal(!sendcustommailmodal)}>Send Custom Mail to Player(s)</CButton>
                                    <CModal
                                        show={sendcustommailmodal}
                                        onClose={() => setSendcustommailmodal(!sendcustommailmodal)}
                                        color="info"
                                        size="xl"
                                    >
                                        <CModalHeader closeButton>
                                            <CModalTitle>
                                                <b>Send Custom Mail to a Player</b>
                                            </CModalTitle>
                                        </CModalHeader>
                                        <CModalBody>
                                            <CContainer>
                                                <SendCustomMails
                                                    customDropdown={customDropdown}
                                                    items={items}
                                                    template_id={template_id}
                                                    custom_message_id={custom_message_id}
                                                    setCustom_message_id={setCustom_message_id}
                                                    _attachments={_attachments}
                                                    setSendcustommailmodal={setSendcustommailmodal}
                                                    refreshMail={refreshMail}
                                                    setTemplate_id={setTemplate_id}
                                                    handleChangeCustom={handleChangeCustom}
                                                />
                                            </CContainer>
                                        </CModalBody>
                                    </CModal>
                                </CCol>
                            </CContainer>
                            <br />
                            <CDataTable
                                items={mails}
                                fields={fields}
                                hover
                                loadingSlot={true}
                                noItemsViewSlot={<div style={{textAlign: "center"}}><i>{spinner()}Loading Data, Please Wait...</i></div>}
                                scopedSlots={{
                                    'status':
                                        (mail) => {
                                            return(
                                            <td>
                                                {mail.confirm_read === 0 ? <><CBadge color='success'>
                                                    Sent
                                                </CBadge></>:<><CBadge color='info'>
                                                    Read
                                                </CBadge></>}
                                            </td>)
                                        },
                                        'send_date':
                                        (mail) => {

                                            const date = mail.send_date
                                            const splitdate = date.split(' ')
                                            const joindate = splitdate.join('T')
                                            const joindatez = joindate + '.000Z'

                                            const dateconvert = new Date(joindatez);

                                            return(
                                            <td>
                                                {dateconvert.toString()}
                                            </td>)
                                        },
                                        
                                   /*  'show_details':
                                        (mail, index) => {
                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        className="btn-pill"
                                                        color="info"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(mail.mail_id) }}
                                                    >
                                                        {details.includes(mail.mail_id) ? 'Hide' : 'Update Mail'}
                                                    </CButton>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (mail, index) => {
                                            return (
                                                <CCollapse show={details.includes(mail.mail_id)}>
                                                    <CCardBody>
                                                        <h4>
                                                            {mail.mail_id}
                                                        </h4>
                                                        <CCol md="2">

                                                            <p className="text-muted">Update Send Date: <CInput type="date" size="sm" md="2" onChange={(e) => setDate(e.target.value)}></CInput><CInput type="time" size="sm" md="2" onChange={(e) => setTime(e.target.value)}></CInput></p>

                                                            <CButton size="sm" color="info" onClick={updateSenddate}>
                                                                Update
                                                            </CButton>

                                                        </CCol>
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        } */
                                }}
                            />
                            <CRow>
                        <CCol align='left'>
                          {hideprevbttn()}
                        </CCol>
                        <CCol align='right'>
                          <CButton className="info" size="md" color="primary" onClick={incrementfornext} >Next Lists</CButton>
                        </CCol>
                      </CRow>
                        </CCardBody>
                        
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}
export default ScheduledMail