import React, { useState, useEffect } from 'react';
import toast, { Toaster } from 'react-hot-toast';
import {
    CButton,

    CModalBody,
    CModalFooter,

    CNav,
    CNavItem,
    CNavLink,
    CTabContent,
    CTabPane,
    CFormGroup,
    CCol,
    CRow,
    CInput,
    CInputGroup,
    CCard,
    CTabs,
    CLabel,
    CCardBody,
    CButtonGroup,
    CImg
} from '@coreui/react';

import CIcon from '@coreui/icons-react';

import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux'
import { getKsatriyas } from 'src/redux/action/mailAction/getKsatriyasAction';
import { getBoxes } from 'src/redux/action/mailAction/getBoxesAction';
import ReactPaginate from 'react-paginate';
import { getSkins } from 'src/redux/action/mailAction/getSkinsAction';
import { getRunes } from 'src/redux/action/mailAction/getRunesAction'
import { getFrames } from 'src/redux/action/mailAction/getFramesAction';
import { getAvatars } from 'src/redux/action/mailAction/getAvatarsAction';
import { getItems } from 'src/redux/action/mailAction/getItemsAction';


function AttachmentsCustoms({ setModalprimary, custom_message_id }) {

    //const [primary, setPrimary] = useState(false)

    /* Pagination */
    const [boxpageNumber, setBoxpageNumber] = useState(0)
    const boxPerPage = 12;
    const boxpagesVisited = boxpageNumber * boxPerPage;

    const [itemspageNumber, setItemspageNumber] = useState(0)
    const itemsPerPage = 12;
    const itemspagesVisited = itemspageNumber * itemsPerPage;

    const [ksapageNumber, setKsapageNumber] = useState(0)
    const ksaPerPage = 12;
    const ksapagesVisited = ksapageNumber * ksaPerPage;

    const [skinpageNumber, setSkinpageNumber] = useState(0)
    const skinPerPage = 12;
    const skinpagesVisited = skinpageNumber * skinPerPage;

    const [runepageNumber, setRunepageNumber] = useState(0)
    const runePerPage = 12;
    const runepagesVisited = runepageNumber * runePerPage;

    const [framepageNumber, setFramepageNumber] = useState(0)
    const framePerPage = 12;
    const framepagesVisited = framepageNumber * framePerPage;

    const [avatarpageNumber, setAvatarpageNumber] = useState(0)
    const avatarPerPage = 12;
    const avatarpagesVisited = avatarpageNumber * avatarPerPage;

    const [getattach, setGetattach] = useState({
        item_id: '',
        item_type: '',
        amount: ''
    })

    /* Search States */
    const [ksatriyaSearch, setKsatriyaSearch] = useState('')
    const [boxSearch, setBoxSearch] = useState('')
    const [skinSearch, setSkinSearch] = useState('')
    const [runeSearch, setRuneSearch] = useState('')
    const [frameSearch, setFrameSearch] = useState('')
    const [avatarSearch, setAvatarSearch] = useState('')
    const [itemSearch, setItemSearch] = useState('')

    /* SetState for Currency */
    const [oriitem_id,
        //setOritem_id
    ] = useState(1)
    const [currencyitem_type,
        //setCurrencyitem_type
    ] = useState(1)
    const [oriamount, setOriamount] = useState('')

    const [citrineitem_id,
        //setCitrineitem_id
    ] = useState(2)
    const [citrineamount, setCitrineamount] = useState('')

    const [lotusitem_id,
        //setLotusitem_id
    ] = useState(3)
    const [lotusamount, setLotusamount] = useState('')

    /* Another Currency States */
    /* const [ori, setOri] = useState();
    const [citrine, setCitrine] = useState();
    const [lotus, setLotus] = useState(); */


    /* KSA States */
    const [ksa_id, setKsa_id] = useState({
        ksatriya_id: '',
        ksatriya_name: ''
    })
    const [ksaitem_type,
        //setKsaitem_type
    ] = useState(2)
    const [ksaamount,
        //setKsaamount
    ] = useState(1)

    /* Box States */
    const [box_id, setBox_id] = useState({
        box_id: '',
        box_name: ''
    })
    const [boxitem_type, //
    ] = useState(6)
    const [boxamount, setBoxamount] = useState('')

    /* Skin States */
    const [ksatriya_skin_id, setKsatriya_skin_id] = useState({
        ksatriya_skin_id: '',
        ksatriya_name: ''
    })
    const [skinitem_type,
        //setSkinitem_type
    ] = useState(3)
    const [skinamount,
        //setSkinamount
    ] = useState(1)

    /* Runes States */
    const [rune_id, setRune_id] = useState({
        rune_id: '',
        name: '',
    })
    const [runeitem_type,
        //setRuneitem_type
    ] = useState(4)
    const [runeamount,
        setRuneamount
    ] = useState('')

    /* Frames States */
    const [frame_id, setFrame_id] = useState({
        frame_id: '',
        description: ''
    })
    const [frameitem_type,
        //setFrameitem_type
    ] = useState(11)
    const [frameamount,
        //setFrameamount
    ] = useState(1)

    /* Avatars States */
    const [avatar_id, setAvatar_id] = useState({
        avatar_id: '',
        description: ''
    })
    const [avataritem_type,
        //setAvataritem_type
    ] = useState(12)
    const [avataramount,
        //setAvataramount
    ] = useState(1)

    /* Avatars States */
    const [misc_id, setMisc_id] = useState({
        misc_id: '',
        misc_name: ''
    })
    const [miscitem_type,
        //setAvataritem_type
    ] = useState(5)
    const [miscamount,
        setMiscamount
    ] = useState('')

    const onClickgetitemnestedbox = (e) => {
        setBox_id({
            box_id: e.target.value,
            box_name: e.target.id
        })
    }

    const onClickgetitemnestedmisc = (e) => {
        setMisc_id({
            misc_id: e.target.value,
            misc_name: e.target.id
        })
    }

    const onClickgetitemnestedksatriya = (e) => {
        setKsa_id({
            ksatriya_id: e.target.value,
            ksatriya_name: e.target.id
        })
    }

    const onClickgetitemnestedksatriyaskin = (e) => {
        setKsatriya_skin_id({
            ksatriya_skin_id: e.target.value,
            ksatriya_name: e.target.id
        })
    }

    const onClickgetitemnestedrune = (e) => {
        setRune_id({
            rune_id: e.target.value,
            name: e.target.id,
        })
    }

    const onClickgetitemnestedframe = (e) => {
        setFrame_id({
            frame_id: e.target.value,
            description: e.target.id,
        })
    }

    const onClickgetitemnestedavatar = (e) => {
        setAvatar_id({
            avatar_id: e.target.value,
            description: e.target.id,
        })
    }




    /* ADD Ori */
    const addOri = (e) => {
        e.preventDefault()
        const data = new FormData();
        data.append('custom_message_id', custom_message_id);
        data.append('item_id', oriitem_id);
        data.append('item_type', currencyitem_type);
        data.append('amount', oriamount);

        const config = {
            method: 'post',
            url: 'api/mail/attachItem',
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                toast.success('Ori Added!')

            })
            .catch((error) => {
                console.log(error);
                toast.error('Failed to Add Attachment!')
            });
        setTimeout(refreshAttachments, 100)
    }


    /* ADD Citrine */
    const addCitrine = async (e) => {
        e.preventDefault()
        const data = new FormData();
        data.append('custom_message_id', custom_message_id);
        data.append('item_id', citrineitem_id);
        data.append('item_type', currencyitem_type);
        data.append('amount', citrineamount);


        const config = {
            method: 'post',
            url: 'api/mail/attachItem',
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        await axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                toast.success('Citrine Added!');
            })
            .catch((error) => {
                console.log(error);
                toast.error('Failed to Add Attachment: You need to type amount of the item')
            });
        setTimeout(refreshAttachments, 100)
    };


    /* ADD Lotus */
    const addLotus = async (e) => {
        e.preventDefault()
        const data = new FormData();
        data.append('custom_message_id', custom_message_id)
        data.append('item_id', lotusitem_id);
        data.append('item_type', currencyitem_type);
        data.append('amount', lotusamount);


        const config = {
            method: 'post',
            url: 'api/mail/attachItem',
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),

            },
            data: data
        };

        await axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                toast.success('Lotus Added!')
            })
            .catch((error) => {
                console.log(error);
                toast.error('Failed to Add Attachment!')
            });
        setTimeout(refreshAttachments, 100)
    }


    const refreshAttachments = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`api/mail/getAttachmentByCustomMessageIdOnly?custom_message_id=${custom_message_id}`, config).then(res => {
            const _attachments = res.data;
            set_attachments(_attachments);
        });
    }

    const [_attachments, set_attachments] = useState([])



    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`api/mail/getAttachmentByCustomMessageIdOnly?custom_message_id=${custom_message_id}`, config).then(res => {
            const _attachments = res.data;
            set_attachments(_attachments);
        });
    }, [custom_message_id])


    const attachmentsSect = _attachments && _attachments.map(item => {
        const skinname = () => {
            if (item.item_id === 100 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 101 && item.item_type_id === 3) {
                return "Yanaka"
            } else if (item.item_id === 300 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 301 && item.item_type_id === 3) {
                return "Arbalest Armor"
            } else if (item.item_id === 302 && item.item_type_id === 3) {
                return "Homeroom Teacher"
            } else if (item.item_id === 400 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 401 && item.item_type_id === 3) {
                return "Cybuff Funk"
            } else if (item.item_id === 402 && item.item_type_id === 3) {
                return "Eternal Anarchy"
            } else if (item.item_id === 500 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 600 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 601 && item.item_type_id === 3) {
                return "Thunder Phantom"
            } else if (item.item_id === 602 && item.item_type_id === 3) {
                return "Badhayan Nyandi"
            } else if (item.item_id === 700 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 701 && item.item_type_id === 3) {
                return "Patih Mada"
            } else if (item.item_id === 800 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 801 && item.item_type_id === 3) {
                return "Guardian of Manthana"
            } else if (item.item_id === 900 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 901 && item.item_type_id === 3) {
                return "Carimata Marauder"
            } else if (item.item_id === 902 && item.item_type_id === 3) {
                return "Lady Jock"
            } else if (item.item_id === 1000 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 1001 && item.item_type_id === 3) {
                return "Cerberus"
            } else if (item.item_id === 1100 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 1101 && item.item_type_id === 3) {
                return "Smokestack Outburst"
            } else if (item.item_id === 1102 && item.item_type_id === 3) {
                return "Endless Angst"
            } else if (item.item_id === 1200 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 1201 && item.item_type_id === 3) {
                return "Divine Warlord"
            } else if (item.item_id === 1202 && item.item_type_id === 3) {
                return "Undefined"
            } else if (item.item_id === 1300 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 1301 && item.item_type_id === 3) {
                return "Volcanic Lightning"
            } else if (item.item_id === 1302 && item.item_type_id === 3) {
                return "Party Wrecker"
            } else if (item.item_id === 1400 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 1401 && item.item_type_id === 3) {
                return "Serene Melody"
            } else if (item.item_id === 1500 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 1501 && item.item_type_id === 3) {
                return "Brave Princess"
            } else if (item.item_id === 1600 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 1601 && item.item_type_id === 3) {
                return "Alighted Asura"
            } else if (item.item_id === 1700 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 1701 && item.item_type_id === 3) {
                return "Battle of Kudadu"
            } else if (item.item_id === 1702 && item.item_type_id === 3) {
                return "Krtarajasa"
            } else if (item.item_id === 1800 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 1900 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 1901 && item.item_type_id === 3) {
                return "Undefined"
            } else if (item.item_id === 1903 && item.item_type_id === 3) {
                return "Knightly Trip"
            } else if (item.item_id === 2000 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 2001 && item.item_type_id === 3) {
                return "Singharajni"
            } else if (item.item_id === 2100 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 2200 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 2300 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 2400 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 2500 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 2600 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 2601 && item.item_type_id === 3) {
                return "Unswerving Fellowship"
            } else if (item.item_id === 2700 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 2702 && item.item_type_id === 3) {
                return "Lestari Mudan"
            } else if (item.item_id === 2800 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 2802 && item.item_type_id === 3) {
                return "Jinan"
            } else if (item.item_id === 2900 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 3000 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 3002 && item.item_type_id === 3) {
                return "Champion of Diver"
            } else if (item.item_id === 90100 && item.item_type_id === 3) {
                return "Default"
            } else if (item.item_id === 90101 && item.item_type_id === 3) {
                return "Black Suit"
            } else if (item.item_id >= 0 && item.item_type_id === 12) {
                return "Avatar"
            } else if (item.item_id >= 0 && item.item_type_id === 11) {
                return "Frame"
            } else if (item.item_id >= 0 && item.item_type_id === 4) {
                return "Rune"
            } else if (item.item_id >= 0 && item.item_type_id === 2) {
                return "KSA"
            } else if (item.item_id >= 0 && item.item_type_id === 5) {
                return "Items"
            } else if (item.item_id >= 0 && item.item_type_id === 6) {
                return "Box"
            } else if (item.item_id >= 0 && item.item_type_id === 1) {
                return "Currency"
            } else {
                return item.item_id
            }
        }

        const deleteAttachments = () => {

            const config = {
                method: 'delete',
                url: `api/mail/deleteAttachmentByCustomMessageId?custom_message_id=${custom_message_id}&item_id=${item.item_id}&item_type=${item.item_type_id}`,
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('auth')
                }
            };

            axios(config)
                .then((response) => {
                    console.log(JSON.stringify(response.data));
                    toast.success('Item Removed!');
                })
                .catch((error) => {
                    toast.error('Failed to attach item!');
                });
            setTimeout(refreshAttachments, 100);
        };

        return (

            <div>
                <CCol md="12" style={{ padding: '0.5rem' }}>
                    <CButtonGroup>
                        <CButton className="btn-pill" size="sm" color='danger' >{skinname()} - <b>{item.item_name} </b>({item.amount})</CButton>
                        <CButton onClick={deleteAttachments} className="btn-pill" size="sm" color='danger'><CIcon name="cil-x"></CIcon></CButton>
                    </CButtonGroup>
                </CCol>
            </div>
        )

    })


    const dispatch = useDispatch();


    /* GET Ksatriyas - Get Request */
    const ksatriyas = useSelector((state) => state.ksatriyas.ksatriyas);
    useEffect(() => {
        dispatch(getKsatriyas());
    }, []);

    /* KSA Mapping */
    const displayKsatriyas = ksatriyas
        .slice(ksapagesVisited, ksapagesVisited + ksaPerPage)
        .filter((ksatriya) => {
            if (ksatriyaSearch === "") {
                return ksatriya
            } else if (ksatriya.ksatriya_name.toLowerCase().includes(ksatriyaSearch.toLowerCase())) {
                return ksatriya
            }
        })
        .map((ksatriya, index) => {

            /* ADD Ksatriya */
            const addKsatriya = async (e) => {
                e.preventDefault()
                const data = new FormData();
                data.append('custom_message_id', custom_message_id)
                data.append('item_id', ksatriya.ksatriya_id);
                data.append('item_type', ksaitem_type);
                data.append('amount', ksaamount);


                const config = {
                    method: 'post',
                    url: 'api/mail/attachItem',
                    headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),

                    },
                    data: data
                };

                await axios(config)
                    .then((response) => {
                        console.log(JSON.stringify(response.data));
                        toast.success(<b>Item Attached!</b>)
                    })
                    .catch((error) => {
                        console.log(error);
                        toast.error('Failed to Add Attachment!')
                    });
                setTimeout(refreshAttachments, 100)
            }

            return (<>
                <CCol md="4" lg="4" xl="2">
                    <CCol align="center">
                        <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${ksatriya.ksatriya_id}.png`} fluid shape="rounded" width='150' align='center' onClick={onClickgetitemnestedksatriya} style={{ "pointer-events": "all" }} />
                    </CCol>
                    <CCol align="center">
                        <b>{ksatriya.ksatriya_name}</b>
                    </CCol>

                    <CCol align="center">
                        <CButton key={ksatriya.ksatriya_id} value={ksatriya.ksatriya_id} id={ksatriya.ksatriya_name} color="info" size='sm' onClick={addKsatriya}>Add Item</CButton>
                    </CCol>

                    <br />
                </CCol> </>)
        })

    /* Ksatriya Pagination */
    const pageCount = Math.ceil(ksatriyas.length / ksaPerPage);
    const changePage = ({ selected }) => {
        setKsapageNumber(selected);
    }

    /* GET Items - Get Request */
    const items = useSelector((state) => state.items.items);
    useEffect(() => {
        dispatch(getItems());
    }, []);


    /* Items Mapping */
    const displayItems = items
        .slice(itemspagesVisited, itemspagesVisited + itemsPerPage)
        .filter((item) => {
            if (itemSearch === "") {
                return item
            } else if (item.misc_name.toLowerCase().includes(itemSearch.toLowerCase())) {
                return item
            }
        })
        .map((item, index) => {

            /* ADD Items */
            const addItems = async (e) => {
                e.preventDefault()
                const data = new FormData();
                data.append('custom_message_id', custom_message_id);
                data.append('item_id', item.misc_id);
                data.append('item_type', miscitem_type);
                data.append('amount', miscamount);

                const config = {
                    method: 'post',
                    url: 'api/mail/attachItem',
                    headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),

                    },
                    data: data
                };

                await axios(config)
                    .then((response) => {
                        console.log(JSON.stringify(response.data));
                        toast.success('Item Attached!')
                    })
                    .catch((error) => {
                        console.log(error);
                        toast.error('Failed to Add Attachment! Seems like you forgot to type the amount.')
                    });
                setTimeout(refreshAttachments, 100)
            }

            return (<><CCol md="4" lg="4" xl="2">
                <CCard align="center"   >
                    <CCardBody>
                        <b>{item.misc_id} - {item.misc_name}</b>
                    </CCardBody>
                    <CInput type="number" placeholder="Amount to give" onChange={(e) => setMiscamount(e.target.value)} />
                    <CCol>
                        <CButton key={item.misc_id} value={item.misc_id} id={item.misc_name} color="info" size='sm' onClick={addItems} >Add Item</CButton>
                    </CCol>
                </CCard>
                <br />
            </CCol></>)
        })


    /* Items Pagination */
    const itemspageCount = Math.ceil(items.length / itemsPerPage);
    const itemschangePage = ({ selected }) => {
        setItemspageNumber(selected);
    }

    /* GET Boxes - Get Request */
    const boxes = useSelector((state) => state.boxes.boxes);
    useEffect(() => {
        dispatch(getBoxes());
    }, []);


    /* Boxes Mapping */
    const displayBoxes = boxes
        .slice(boxpagesVisited, boxpagesVisited + boxPerPage)
        .filter((box) => {
            if (boxSearch === "") {
                return box
            } else if (box.box_name.toLowerCase().includes(boxSearch.toLowerCase())) {
                return box
            }
        })
        .map((box, index) => {

            /* ADD Box */
            const addBox = async (e) => {
                e.preventDefault()
                const data = new FormData();
                data.append('custom_message_id', custom_message_id)
                data.append('item_id', box.box_id);
                data.append('item_type', boxitem_type);
                data.append('amount', boxamount);


                const config = {
                    method: 'post',
                    url: 'api/mail/attachItem',
                    headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),

                    },
                    data: data
                };

                await axios(config)
                    .then((response) => {
                        console.log(JSON.stringify(response.data));
                        toast.success('Item Attached!')
                    })
                    .catch((error) => {
                        console.log(error);
                        toast.error('Failed to Add Attachment! Seems like you forgot to type the amount.')
                    });
                setTimeout(refreshAttachments, 100)
            }

            return (<><CCol md="4" lg="4" xl="2">
                <CCard align="center"   >
                    <CCardBody>
                        <b>{box.box_id} - {box.box_name}</b>
                    </CCardBody>
                    <CInput type="number" placeholder="Amount to give" onChange={(e) => setBoxamount(e.target.value)} />
                    <CCol>
                        <CButton key={box.box_id} value={box.box_id} id={box.box_name} color="info" size='sm' onClick={addBox} >Add Item</CButton>
                    </CCol>
                </CCard>
                <br />
            </CCol></>)
        })


    /* Boxes Pagination */
    const boxespageCount = Math.ceil(boxes.length / boxPerPage);
    const boxeschangePage = ({ selected }) => {
        setBoxpageNumber(selected);
    }

    /* GET Skins - Get Request */
    const skins = useSelector((state) => state.skins.skins);
    useEffect(() => {
        dispatch(getSkins());
    }, []);


    /* Skins Mapping */
    const displaySkins = skins
        .slice(skinpagesVisited, skinpagesVisited + skinPerPage)
        .filter((skin) => {
            if (skinSearch === "") {
                return skin
            } else if (skin.ksatriya_skin_id && skin.ksatriya_skin_id.toString().includes(skinSearch.toString())) {
                return skin
            } else if (skin.ksatriya_name && skin.ksatriya_name.toLowerCase().includes(skinSearch.toLowerCase())) {
                return skin
            }
        })
        .map((skin, index) => {
            const skinname = () => {
                if (skin.ksatriya_skin_id === 100) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 101) {
                    return "Yanaka"
                } else if (skin.ksatriya_skin_id === 300) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 301) {
                    return "Arbalest Armor"
                } else if (skin.ksatriya_skin_id === 302) {
                    return "Homeroom Teacher"
                } else if (skin.ksatriya_skin_id === 400) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 401) {
                    return "Cybuff Funk"
                } else if (skin.ksatriya_skin_id === 402) {
                    return "Eternal Anarchy"
                } else if (skin.ksatriya_skin_id === 500) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 600) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 601) {
                    return "Thunder Phantom"
                } else if (skin.ksatriya_skin_id === 602) {
                    return "Badhayan Nyandi"
                } else if (skin.ksatriya_skin_id === 700) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 701) {
                    return "Patih Mada"
                } else if (skin.ksatriya_skin_id === 800) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 801) {
                    return "Guardian of Manthana"
                } else if (skin.ksatriya_skin_id === 900) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 901) {
                    return "Carimata Marauder"
                } else if (skin.ksatriya_skin_id === 902) {
                    return "Lady Jock"
                } else if (skin.ksatriya_skin_id === 1000) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 1001) {
                    return "Cerberus"
                } else if (skin.ksatriya_skin_id === 1100) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 1101) {
                    return "Smokestack Outburst"
                } else if (skin.ksatriya_skin_id === 1102) {
                    return "Endless Angst"
                } else if (skin.ksatriya_skin_id === 1200) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 1201) {
                    return "Divine Warlord"
                } else if (skin.ksatriya_skin_id === 1202) {
                    return "Undefined"
                } else if (skin.ksatriya_skin_id === 1300) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 1301) {
                    return "Volcanic Lightning"
                } else if (skin.ksatriya_skin_id === 1302) {
                    return "Party Wrecker"
                } else if (skin.ksatriya_skin_id === 1400) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 1401) {
                    return "Serene Melody"
                } else if (skin.ksatriya_skin_id === 1500) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 1501) {
                    return "Brave Princess"
                } else if (skin.ksatriya_skin_id === 1600) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 1601) {
                    return "Alighted Asura"
                } else if (skin.ksatriya_skin_id === 1700) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 1701) {
                    return "Battle of Kudadu"
                } else if (skin.ksatriya_skin_id === 1702) {
                    return "Krtarajasa"
                } else if (skin.ksatriya_skin_id === 1800) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 1900) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 1901) {
                    return "Undefined"
                } else if (skin.ksatriya_skin_id === 1903) {
                    return "Knightly Trip"
                } else if (skin.ksatriya_skin_id === 2000) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 2001) {
                    return "Singharajni"
                } else if (skin.ksatriya_skin_id === 2100) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 2200) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 2300) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 2400) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 2500) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 2600) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 2601) {
                    return "Unswerving Fellowship"
                } else if (skin.ksatriya_skin_id === 2700) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 2702) {
                    return "Lestari Mudan"
                } else if (skin.ksatriya_skin_id === 2800) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 2802) {
                    return "Jinan"
                } else if (skin.ksatriya_skin_id === 2900) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 3000) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 3002) {
                    return "Champion of Diver"
                } else if (skin.ksatriya_skin_id === 90100) {
                    return "Default"
                } else if (skin.ksatriya_skin_id === 90101) {
                    return "Black Suit"
                } else {
                    return skin.ksatriya_skin_id
                }
            };

            /* ADD Skin */
            const addSkin = async (e) => {
                e.preventDefault()
                const data = new FormData();
                data.append('custom_message_id', custom_message_id);
                data.append('item_id', skin.ksatriya_skin_id);
                data.append('item_type', skinitem_type);
                data.append('amount', skinamount);


                const config = {
                    method: 'post',
                    url: 'api/mail/attachItem',
                    headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),

                    },
                    data: data
                };

                await axios(config)
                    .then((response) => {
                        console.log(JSON.stringify(response.data));
                        toast.success('Item Attached!')
                    })
                    .catch((error) => {
                        console.log(error);
                        toast.error('Failed to Add Attachment!')
                    });
                setTimeout(refreshAttachments, 100)
            };

            return (<>
                <CCol md="4" lg="4" xl="2">
                    <CCol align="center">
                        <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_skin/${skin.ksatriya_skin_id}.png`} fluid shape="rounded" width='150' align='center' onClick={onClickgetitemnestedksatriya} style={{ "pointer-events": "all" }} />
                    </CCol>
                    <CCol align="center">
                        <b>{skin.ksatriya_name} - {skinname()}</b>
                    </CCol>
                    <CCol align="center">
                        <CButton key={skin.ksatriya_skin_id} value={skin.ksatriya_skin_id} id={skin.ksatriya_name} color="info" size='sm' onClick={addSkin}>Add Item</CButton>
                    </CCol>
                    <br />
                </CCol></>)
        })

    /* Skins Pagination */
    const skinspageCount = Math.ceil(skins.length / skinPerPage);
    const skinschangePage = ({ selected }) => {
        setSkinpageNumber(selected);
    }


    /* GET Runes - Get Request */
    const runes = useSelector((state) => state.runes.runes);
    useEffect(() => {
        dispatch(getRunes());
    }, []);


    /* Runes Mapping */
    const displayRunes = runes
        .slice(runepagesVisited, runepagesVisited + runePerPage)
        .filter((rune) => {
            if (runeSearch === "") {
                return rune
            } else if (rune.name.toLowerCase().includes(runeSearch.toLowerCase())) {
                return rune
            }
            else if (rune.description.toLowerCase().includes(runeSearch.toLowerCase())) {
                return rune
            }

        })
        .map((rune, index) => {

            /* ADD Rune */
            const addRune = async (e) => {
                e.preventDefault()
                const data = new FormData();
                data.append('custom_message_id', custom_message_id);
                data.append('item_id', rune.rune_id);
                data.append('item_type', runeitem_type);
                data.append('amount', runeamount);


                const config = {
                    method: 'post',
                    url: 'api/mail/attachItem',
                    headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),

                    },
                    data: data
                };

                await axios(config)
                    .then((response) => {
                        console.log(JSON.stringify(response.data));
                        toast.success('Item Attached!')
                    })
                    .catch((error) => {
                        console.log(error);
                        toast.error('Failed to Add Attachment! Seems like you forgot to type the amount.')
                    });
                setTimeout(refreshAttachments, 100)
            }

            return (<><CCol md="4" lg="4" xl="2">
                <CCard align="center"   >
                    <CCardBody>
                        <b>{rune.name} - {rune.description} </b>
                        <br />
                        <i>{rune.rune_color}</i>
                    </CCardBody>
                    <CInput type="number" placeholder="Amount to give" onChange={(e) => setRuneamount(e.target.value)} />
                    <CCol>
                        <CButton key={rune.rune_id} value={rune.rune_id} id={rune.name} color="info" size='sm' onClick={addRune}>Add Item</CButton>

                    </CCol>
                </CCard>
                <br />
            </CCol></>)
        })


    /* Runes Pagination */
    const runespageCount = Math.ceil(runes.length / runePerPage);
    const runeschangePage = ({ selected }) => {
        setRunepageNumber(selected);
    }


    /* GET Frames - Get Request */
    const frames = useSelector((state) => state.frames.frames);
    useEffect(() => {
        dispatch(getFrames());
    }, []);



    /* Frames Mapping */
    const displayFrames = frames
        .slice(framepagesVisited, framepagesVisited + framePerPage)
        .filter((frame) => {
            if (frameSearch === "") {
                return frame
            } else if (frame.description.toLowerCase().includes(frameSearch.toLowerCase())) {
                return frame
            }
        })
        .map((frame, index) => {

            /* ADD Frame */
            const addFrame = async (e) => {
                e.preventDefault()
                const data = new FormData();
                data.append('custom_message_id', custom_message_id);
                data.append('item_id', frame.frame_id);
                data.append('item_type', frameitem_type);
                data.append('amount', frameamount);


                const config = {
                    method: 'post',
                    url: 'api/mail/attachItem',
                    headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),

                    },
                    data: data
                };

                await axios(config)
                    .then((response) => {
                        console.log(JSON.stringify(response.data));
                        toast.success('Item Attached!')
                    })
                    .catch((error) => {
                        console.log(error);
                        toast.error('Failed to Add Attachment!')
                    });
                setTimeout(refreshAttachments, 100)
            }

            return (<><CCol md="4" lg="4" xl="2">
                <CCard align="center"  >
                    <CCardBody>
                        <b>{frame.frame_id} </b>
                        <br />
                        <i>{frame.description}</i>
                    </CCardBody>
                    <CCol>
                        <CButton key={frame.frame_id} value={frame.frame_id} id={frame.description} color="info" size='sm' onClick={addFrame}>Add Item</CButton>
                    </CCol>
                </CCard>
                <br />
            </CCol>
            </>)
        })

    /* Frames Pagination */
    const framespageCount = Math.ceil(frames.length / framePerPage);
    const frameschangePage = ({ selected }) => {
        setFramepageNumber(selected);
    }

    /* GET Avatars - Get Request */
    const avatars = useSelector((state) => state.avatars.avatars);
    useEffect(() => {
        dispatch(getAvatars());
    }, []);


    /* Avatars Mapping */
    const displayAvatars = avatars
        .slice(avatarpagesVisited, avatarpagesVisited + avatarPerPage)
        .filter((avatar) => {
            if (avatarSearch === "") {
                return avatar
            } else if (avatar.description.toLowerCase().includes(avatarSearch.toLowerCase())) {
                return avatar
            }
        })
        .map((avatar, index) => {

            /* ADD Avatar */
            const addAvatar = async (e) => {
                e.preventDefault()
                const data = new FormData();
                data.append('custom_message_id', custom_message_id);
                data.append('item_id', avatar.avatar_id);
                data.append('item_type', avataritem_type);
                data.append('amount', avataramount);


                const config = {
                    method: 'post',
                    url: 'api/mail/attachItem',
                    headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),

                    },
                    data: data
                };

                await axios(config)
                    .then((response) => {
                        console.log(JSON.stringify(response.data));
                        toast.success('Item Attached!')
                    })
                    .catch((error) => {
                        console.log(error);
                        toast.error('Failed to Add Attachment!')
                    });
                setTimeout(refreshAttachments, 100)
            }

            return (<><CCol md="4" lg="4" xl="2">
                <CCol align="center">
                    <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/avataricon/${avatar.avatar_id}.png`} fluid shape="rounded" width='110' align='center' onClick={onClickgetitemnestedksatriya} style={{ "pointer-events": "all" }} />
                </CCol>
                <CCol align="center">
                    <b>{avatar.description}</b>
                </CCol>
                <CCol align="center">
                    <CButton key={avatar.avatar_id} value={avatar.avatar_id} id={avatar.description} color="info" size='sm' onClick={addAvatar}>Add Item</CButton>
                </CCol>

                <br />
            </CCol></>)
        })

    /* Frames Pagination */
    const avatarspageCount = Math.ceil(avatars.length / avatarPerPage);
    const avatarschangePage = ({ selected }) => {
        setAvatarpageNumber(selected);
    }

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`api/mail/getAttachmentByCustomMessageIdOnly?custom_message_id=${custom_message_id}`, config).then(res => {
            const getattach = res.data;
            setGetattach(getattach);
        });
    }, [custom_message_id])

    return (
        <div key="attachmentsModal">
            <CModalBody>
                <CTabs>
                    <CNav variant="tabs">
                        <CNavItem>
                            <CNavLink>
                                Currency
                            </CNavLink>
                        </CNavItem>
                        <CNavItem>
                            <CNavLink>
                                Box
                            </CNavLink>
                        </CNavItem>
                        <CNavItem>
                            <CNavLink>
                                Items
                            </CNavLink>
                        </CNavItem>
                        <CNavItem>
                            <CNavLink>
                                Ksatriya
                            </CNavLink>
                        </CNavItem>
                        <CNavItem>
                            <CNavLink>
                                Skin KSA
                            </CNavLink>
                        </CNavItem>
                        <CNavItem>
                            <CNavLink>
                                Rune
                            </CNavLink>
                        </CNavItem>
                        <CNavItem>
                            <CNavLink>
                                Frame
                            </CNavLink>
                        </CNavItem>
                        <CNavItem>
                            <CNavLink>
                                Avatar
                            </CNavLink>
                        </CNavItem>
                    </CNav>
                    <CTabContent>
                        <CTabPane>
                            <hr />
                            <CFormGroup>
                                <CCol md="9">
                                    <CLabel htmlFor="textarea-input"><b>Ori/Coin :</b></CLabel>
                                </CCol>
                                <CCol md="7" lg="6" xl="3">
                                    {/* Data will be inserted in here */}
                                    <CRow>
                                        <CCol>
                                            <CInputGroup>
                                                <CInput type='number' id="1" name="ori/currency" placeholder='Set Amount of  Ori...' min={0} onChange={(e) => setOriamount(e.target.value)} />
                                                <CButton onClick={addOri} size="sm" color="info" >ADD</CButton>
                                            </CInputGroup>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CFormGroup>
                            <CFormGroup>
                                <CCol md="3">
                                    <CLabel htmlFor="textarea-input"><b>Citrine :</b></CLabel>
                                </CCol>
                                <CCol md="7" lg="6" xl="3">
                                    {/* Data will be inserted in here */}
                                    <CRow>

                                        <CCol>
                                            <CInputGroup>
                                                <CInput type='number' id="2" name="citrine" placeholder='Set Amount of  Citrine...' min={0} onChange={(e) => setCitrineamount(e.target.value)} />
                                                <CButton onClick={addCitrine} size="sm" color="info" >ADD</CButton>
                                            </CInputGroup>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CFormGroup>
                            <CFormGroup>
                                <CCol md="3">
                                    <CLabel htmlFor="textarea-input"><b>Lotus :</b></CLabel>
                                </CCol>
                                <CCol md="7" lg="6" xl="3">
                                    {/* Data will be inserted in here */}
                                    <CRow>
                                        <CCol>
                                            <CInputGroup>
                                                <CInput type='number' id="3" name="input2-group2" placeholder='Set Amount of  Lotus...' min={0} onChange={(e) => setLotusamount(e.target.value)} />
                                                <CButton onClick={addLotus} size="sm" color="info" >ADD</CButton>
                                            </CInputGroup>
                                        </CCol>
                                    </CRow>
                                </CCol>
                            </CFormGroup>
                        </CTabPane>
                        <CTabPane>
                            <hr />
                            <CRow>
                                <CCol xs="4" align="right">

                                    <CInput type="text" placeholder="Search Boxes here..." size="md" onChange={(e) => { setBoxSearch(e.target.value) }} />

                                </CCol>
                            </CRow>

                            <br />

                            <CRow>

                                {displayBoxes}
                            </CRow>
                            <CCol align="left" md="12">
                                <ReactPaginate
                                    previousLabel={"Prev"}
                                    nextLabel={"Next"}
                                    pageCount={boxespageCount}
                                    onPageChange={boxeschangePage}
                                    containerClassName={"paginationBttns"}
                                    previousLinkClassName={"PreviousBttn"}
                                    nextLinkClassName={"nextBttn"}
                                    disabledClassName={"paginationDisabled"}
                                    activeClassName={"paginationActive"}

                                />
                            </CCol>


                        </CTabPane>
                        <CTabPane>
                            <hr />

                            <CRow>
                                <CCol xs="4" align="right">

                                    <CInput type="text" placeholder="Search Items here..." size="" onChange={(e) => { setItemSearch(e.target.value) }} />

                                </CCol>
                            </CRow>
                            <br />

                            <CRow>

                                {displayItems}
                            </CRow>
                        
                            <CCol align="left" md="12">
                                <ReactPaginate
                                    previousLabel={"Prev"}
                                    nextLabel={"Next"}
                                    pageCount={itemspageCount}
                                    onPageChange={itemschangePage}
                                    containerClassName={"paginationBttns"}
                                    previousLinkClassName={"PreviousBttn"}
                                    nextLinkClassName={"nextBttn"}
                                    disabledClassName={"paginationDisabled"}
                                    activeClassName={"paginationActive"}
                                />
                            </CCol>


                        </CTabPane>
                        <CTabPane>

                            <hr />

                            <CRow>
                                <CCol xs="4" align="right">

                                    <CInput type="text" placeholder="Search Ksatriya here..." size="md" onChange={(e) => { setKsatriyaSearch(e.target.value) }} />

                                </CCol>
                            </CRow>
                            <br />

                            <CRow>

                                {displayKsatriyas}

                            </CRow>
                            
                            <br />

                            <CCol align="center">
                                <ReactPaginate
                                    previousLabel={"Prev"}
                                    nextLabel={"Next"}
                                    pageCount={pageCount}
                                    onPageChange={changePage}
                                    containerClassName={"paginationBttns"}
                                    previousLinkClassName={"PreviousBttn"}
                                    nextLinkClassName={"nextBttn"}
                                    disabledClassName={"paginationDisabled"}
                                    activeClassName={"paginationActive"}
                                />
                            </CCol>
                        </CTabPane>
                        <CTabPane>
                            <hr />

                            <CRow>
                                <CCol xs="4" align="right">

                                    <CInput type="text" placeholder="Search Skins here..." size="" onChange={(e) => { setSkinSearch(e.target.value) }} />

                                </CCol>
                            </CRow>
                            <br />

                            <CRow>

                                {displaySkins}

                            </CRow>
                            <br/>
                            <CCol align="left" md="12">
                                <ReactPaginate
                                    previousLabel={"Prev"}
                                    nextLabel={"Next"}
                                    pageCount={skinspageCount}
                                    onPageChange={skinschangePage}
                                    containerClassName={"paginationBttns"}
                                    previousLinkClassName={"PreviousBttn"}
                                    nextLinkClassName={"nextBttn"}
                                    disabledClassName={"paginationDisabled"}
                                    activeClassName={"paginationActive"}
                                />
                            </CCol>
                        </CTabPane>
                        <CTabPane>
                            <hr />

                            <CRow>
                                <CCol xs="4" align="right">

                                    <CInput type="text" placeholder="Search Runes here..." size="" onChange={(e) => { setRuneSearch(e.target.value) }} />

                                </CCol>
                            </CRow>
                            <br />
                            <CRow>

                                {displayRunes}


                            </CRow>
                            <br/>
                            <CCol align="left" md="12">
                                <ReactPaginate
                                    previousLabel={"Prev"}
                                    nextLabel={"Next"}
                                    pageCount={runespageCount}
                                    onPageChange={runeschangePage}
                                    containerClassName={"paginationBttns"}
                                    previousLinkClassName={"PreviousBttn"}
                                    nextLinkClassName={"nextBttn"}
                                    disabledClassName={"paginationDisabled"}
                                    activeClassName={"paginationActive"}
                                />
                            </CCol>
                        </CTabPane>
                        <CTabPane>
                            <hr />
                            <CRow>
                                <CCol xs="4" align="right">
                                    <CInput type="text" placeholder="Search Frames here..." size="" onChange={(e) => { setFrameSearch(e.target.value) }} />

                                </CCol>
                            </CRow>
                            <br />

                            <CRow>

                                {displayFrames}

                            </CRow>
                            <br/>
                            <CCol align="left" md="12">
                                <ReactPaginate
                                    previousLabel={"Prev"}
                                    nextLabel={"Next"}
                                    pageCount={framespageCount}
                                    onPageChange={frameschangePage}
                                    containerClassName={"paginationBttns"}
                                    previousLinkClassName={"PreviousBttn"}
                                    nextLinkClassName={"nextBttn"}
                                    disabledClassName={"paginationDisabled"}
                                    activeClassName={"paginationActive"}
                                />
                            </CCol>
                        </CTabPane>
                        <CTabPane>
                            <hr />
                            <CRow>
                                <CCol xs="4" align="right">
                                    <CInput type="text" placeholder="Search Avatars here..." size="" onChange={(e) => { setAvatarSearch(e.target.value) }} />
                                </CCol>
                            </CRow>
                            <br />

                            <CRow>

                                {displayAvatars}

                            </CRow>
                            
                            <CCol align="left" md="12">
                                <ReactPaginate
                                    previousLabel={"Prev"}
                                    nextLabel={"Next"}
                                    pageCount={avatarspageCount}
                                    onPageChange={avatarschangePage}
                                    containerClassName={"paginationBttns"}
                                    previousLinkClassName={"PreviousBttn"}
                                    nextLinkClassName={"nextBttn"}
                                    disabledClassName={"paginationDisabled"}
                                    activeClassName={"paginationActive"}
                                />
                            </CCol>
                        </CTabPane>
                    </CTabContent>
                </CTabs>
            </CModalBody>
            <hr />
            <CModalBody>
                <CRow>
                    <CCol md="2"><b>Attachments </b></CCol>

                    <CCol xs="12" md="9">
                        {/* Data will be inserted in here */}


                        <CCol md="12">

                            <CRow>
                                <b>:</b>
                                {attachmentsSect}

                            </CRow>

                        </CCol>

                    </CCol>

                </CRow>
            </CModalBody>
        </div>
    )
}

export default AttachmentsCustoms
