import React, { useState, useEffect, useRef } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CContainer,

} from '@coreui/react'

import ScheduledMail from './ScheduledMail'
import ScheduledLoginMail from './ScheduledLoginMail'
import { useHistory } from 'react-router'
import TemplateLists from './TemplateLists'

import { getTemplates } from 'src/redux/action/mailAction/getmailtemplAction'
import { getCustoms } from 'src/redux/action/mailAction/getCustomsAction'
import { useSelector, useDispatch } from 'react-redux'
import toast, { Toaster } from 'react-hot-toast';
import axios from 'axios'



function Mail(props) {

  const [_attachments, set_attachments] = useState([0])

  const [isLoading, setIsLoading] = useState(false)


  /* Modals */
  const [modalprimary, setModalprimary] = useState(false)

  const [template_id, setTemplate_id] = useState('')

  const [custom_message_id, setCustom_message_id] = useState('')
  const history = useHistory();

  useEffect(() => {
    if (props.error?.response?.status===401) {
    history.push("/login")
    }
    },[props.error])
    console.log('ini utk error',props.error?.response)


  const [items, setItems] = useState([{
    template_id: '',
    subject: '',
    message: '',
    message_id: ''
  }])

  /* GET TEMPLATES - GET Request with Redux */
  const dispatch = useDispatch();
  const templates = useSelector((state) => state.templates.templates);
  useEffect(() => {
    dispatch(getTemplates());
  }, []);

  /* templates data refresh */
  const refreshTemplates = () => {
    dispatch(getTemplates());
  }

  /* GET CUSTOMS - GET Request with Redux */
  const customs = useSelector((state) => state.customs.customs);
  useEffect(() => {
    dispatch(getCustoms());
  }, []);

  /* customs data refresh */
  const refreshCustoms = () => {
    dispatch(getCustoms());
  }

  const loadTemplateData = () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/mail/getTemplate?id=${template_id}`, config).then(res => {
      const items = res.data;
      setItems(items);
    });
  }

  const listchange = useRef();
  listchange.current = template_id;

  const loadtemdata = () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/mail/getTemplate?id=${listchange.current}`, config).then(res => {
      const items = res.data;
      setItems(items);
    });
  }


  const handleChange = e => {
    setTemplate_id(e.target.value);
  }

  const handleChangeCustom = e => {
    setCustom_message_id(e.target.value);
  }

  //console.log(template_id)
  /* List Dropdown with template_id as a Value */
  const listDropdown = templates.map((template, index) => {
    return (<option key={template.template_id.toString()} value={template.template_id} >T#{template.template_id} : {template.subject}</option>)
  });

  /* List Dropdown with message_id as a Value */
  const customDropdown = customs.map((custom, index) => {
    return (<option key={custom.message_id.toString()} value={custom.message_id} onClick={(e) => setCustom_message_id(e.target.value)} >C#{custom.message_id} : {custom.subject}</option>)
  });




  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/mail/getTemplate?id=${template_id}`, config).then(res => {
      const items = res.data;
      setItems(items);
    });
  }, [template_id])


  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/mail/getCustom?id=${custom_message_id}`, config).then(res => {
      const items = res.data;
      setItems(items);
    });
  }, [custom_message_id])

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/mail/getAttachmentByTemplateIdOnly?template_id=${template_id}`, config).then(res => {
      const _attachments = res.data;
      set_attachments(_attachments);
    });
  }, [template_id])

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/mail/getAttachmentByCustomMessageIdOnly?custom_message_id=${custom_message_id}`, config).then(res => {
      const _attachments = res.data;
      set_attachments(_attachments);
    });
  }, [custom_message_id])

  const refreshAttachments = () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/mail/getAttachmentByTemplateIdOnly?template_id=${template_id}`, config).then(res => {
      const _attachments = res.data;
      set_attachments(_attachments);
    });
  }



  return (
    <div className="mail">
      <Toaster 
      containerStyle={{
        top: 100,
        left: 300,
        bottom: 20,
        right: 20,
      }}
      />
      <CCol align='center'>
        <CCard style={{ background: '#9bf54c' }}>
          <h4>
            <b>Mail & Gift Form</b>
          </h4>
          <h6>
            <i>Send mails and gifts to player(s)</i>
          </h6>
        </CCard>
      </CCol>
      <br />
      <CContainer>
        <CCol className="mb-4">
          <CCard accentColor="primary">
            <CCardHeader>
              Mail & Gift Form
            </CCardHeader>
            <CCardBody>
              <CTabs>
                <CNav variant="tabs">
                  <CNavItem>
                    <CNavLink>
                      Create/Edit Mail Template(s)
                    </CNavLink>
                  </CNavItem>

                  <CNavItem>
                    <CNavLink>
                      Send Mail(s)
                    </CNavLink>
                  </CNavItem>
                  <CNavItem>
                    <CNavLink>
                      Send Login Mail(s)
                    </CNavLink>
                  </CNavItem>
                </CNav>
                <CTabContent>
                  <CTabPane>
                    <TemplateLists
                      setTemplate_id={setTemplate_id}
                      listDropdown={listDropdown}
                      setCustom_message_id={setCustom_message_id}
                      customDropdown={customDropdown}
                      items={items}
                      custom_message_id={custom_message_id}
                      template_id={template_id}
                      refreshTemplates={refreshTemplates}
                      refreshCustoms={refreshCustoms}
                      setModalprimary={setModalprimary}
                      modalprimary={modalprimary}
                      _attachments={_attachments}
                      isLoading={isLoading}
                      setIsLoading={setIsLoading}
                      toast={toast}
                    />
                  </CTabPane>
                  <CTabPane>
                    <ScheduledMail
                      customDropdown={customDropdown}
                      listDropdown={listDropdown}
                      items={items}
                      template_id={template_id}
                      custom_message_id={custom_message_id}
                      setCustom_message_id={setCustom_message_id}
                      _attachments={_attachments}
                      setTemplate_id={setTemplate_id}
                      handleChange={handleChange}
                      handleChangeCustom={handleChangeCustom}
                    />
                  </CTabPane>
                  <CTabPane>
                    <ScheduledLoginMail />
                  </CTabPane>
                </CTabContent>
              </CTabs>
            </CCardBody>
          </CCard>
        </CCol>
      </CContainer>
    </div>
  )
}

export default Mail
