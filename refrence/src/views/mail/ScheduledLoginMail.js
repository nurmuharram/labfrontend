import React, { useState, useEffect } from 'react'
import {
    CRow,
    CCol,
    CCard,
    CCardHeader,
    CCardBody,
    CDataTable,
    CBadge,
    CButton,
    CCollapse,
    CInput,
    CFormGroup,
    CForm,
    CContainer,
    CModal,
    CModalBody,
    CModalHeader,
    CModalTitle,
    CModalFooter
} from '@coreui/react'

import { useSelector, useDispatch } from 'react-redux'
import { getLoginMail } from 'src/redux/action/mailAction/getLoginMailAction'
import axios from 'axios'
import LoginMailTempTab from './LoginMailTempTab'
import toast from 'react-hot-toast'



function ScheduledLoginMail() {



    const [details, setDetails] = useState([])
    const [send_date, setSend_date] = useState('')

    const [startdate, setStartdate] = useState('2019-10-10')
    const [starttime, setStarttime] = useState('00:00')
    const start_date = startdate + ' ' + starttime + ':' + '00'
    const [enddate, setEnddate] = useState('2019-10-10')
    const [endtime, setEndtime] = useState('00:00')
    const end_date = enddate + " " + endtime + ':' + '00'

    const _date = new Date()

    const yyyy = _date.getFullYear()
    const mm = _date.getMonth() + 1
    const dd = _date.getDate()

    const hh = _date.getHours()
    const min = _date.getMinutes()
    const ss = _date.getSeconds()

    const date = yyyy + '-' + mm + '-' + dd;
    const time = hh + ':' + min + ':' + ss;
    const today_date = date + " " + time;

    const adatestart = new Date(start_date)

    const toisostart = adatestart.toISOString();

    const splitdatestart = toisostart.split('T')

    const fixdatestart = splitdatestart.join(' ')


    const adateend = new Date(end_date)

    const toisoend = adateend.toISOString();

    const splitdateend = toisoend.split('T')

    const fixdateend = splitdateend.join(' ')

    const [loginmailModal, setLoginmailModal] = useState('')


    /* Show/Hide Toggle to update Send Date */
    const toggleDetails = (index) => {
        const position = details.indexOf(index)
        let newDetails = details.slice()
        if (position !== -1) {
            newDetails.splice(position, 1)
        } else {
            newDetails = [...details, index]
        }
        setDetails(newDetails)
    }

    /* Table Data */
    const fields = [
        { key: 'template_id', _style: { width: '10%' } },
        { key: 'start_date', _style: { width: '25%' } },
        'end_date',

        { key: 'status', _style: { width: '20%' }, filter: false },
        {
            key: 'show_details',
            label: '',
            _style: { width: '15%' },
            sorter: false,
            filter: false
        },
        /* {key: 'option', label: '', _style: { width: '10%' }, filter: false} */
    ]

    /* GET MAILS - GET Request with Redux */
    const dispatch = useDispatch();
    const loginmails = useSelector((state) => state.loginmails.loginmails);
    useEffect(() => {
        dispatch(getLoginMail());
    }, []);

    /* customs data refresh */
    const refreshLoginMail = () => {
        dispatch(getLoginMail());
    }



    const statusData = () => {
        if (loginmails.start_date === today_date) {
            return 'Pending'
        } else if (loginmails.start_date >= today_date && loginmails.end_date <= today_date) {
            return 'on Going'
        } else if (loginmails.end_date > today_date) {
            return 'Finished!'
        }
    }


    const getBadge = (status) => {
        switch (status) {
            case 'Pending': return 'warning'
            case 'on Going': return 'info'
            case 'Finished': return 'danger'
            default: return 'primary'
        }
    }


    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            <b>Scheduled Login Mail(s)</b>
                        </CCardHeader>
                        <CCardBody>
                            <CContainer>
                                <CCol align='right'>
                                    <CButton color="info" size="md" xs='3' onClick={() => setLoginmailModal(!loginmailModal)}>Send Mail to all Player</CButton>
                                    <CModal
                                        show={loginmailModal}
                                        onClose={() => setLoginmailModal(!loginmailModal)}
                                        color="info"
                                        size="xl"
                                    >
                                        <CModalHeader closeButton>
                                            <CModalTitle>
                                                <b>Send Login Mail to all Players</b>
                                            </CModalTitle>
                                        </CModalHeader>
                                        <CModalBody>
                                            <CContainer>
                                                <LoginMailTempTab
                                                    setLoginmailModal={setLoginmailModal}
                                                    refreshLoginMail={refreshLoginMail}
                                                />
                                            </CContainer>
                                        </CModalBody>
                                    </CModal>
                                </CCol>
                            </CContainer>
                            <br />
                            <CDataTable
                                items={loginmails}
                                fields={fields}
                                columnFilter
                                itemsPerPageSelect
                                itemsPerPage={5}
                                hover
                                sorter
                                pagination
                                scopedSlots={{
                                    'start_date':
                                        (mail) => {

                                            const date = mail.start_date
                                            const splitdate = date.split(' ')
                                            const joindate = splitdate.join('T')
                                            const joindatez = joindate + '.000Z'

                                            const dateconvert = new Date(joindatez);

                                            return (
                                                <td>
                                                    {dateconvert.toString()}
                                                </td>)
                                        },
                                    'end_date':
                                        (mail) => {

                                            const date = mail.end_date
                                            const splitdate = date.split(' ')
                                            const joindate = splitdate.join('T')
                                            const joindatez = joindate + '.000Z'

                                            const dateconvert = new Date(joindatez);

                                            return (
                                                <td>
                                                    {dateconvert.toString()}
                                                </td>)
                                        },
                                    'status':
                                        (item) => {
                                            const date = item.start_date
                                            const splitdate = date.split(' ')
                                            const joindate = splitdate.join('T')
                                            const joindatez = joindate + '.000Z'

                                            const dateconvert = new Date(joindatez);

                                            const dateend = item.end_date
                                            const splitdateend = dateend.split(' ')
                                            const joindateend = splitdateend.join('T')
                                            const joindatezend = joindateend + '.000Z'

                                            const dateconvertend = new Date(joindatezend);

                                            var d1 = new Date()
                                            var today_date = d1.toISOString().slice(0, -5)


                                            return (
                                                <td>
                                                    {today_date > dateconvertend.toISOString().slice(0, -5) ? <><CBadge color='success'>
                                                        Finished
                                                    </CBadge></> : today_date >= dateconvert.toISOString().slice(0, -5) && today_date <= dateconvertend.toISOString().slice(0, -5) ? <><CBadge color='warning'>
                                                        On Going
                                                    </CBadge></> : today_date < dateconvert.toISOString().slice(0, -5) ? <><CBadge color='info'>
                                                        Pending
                                                    </CBadge></> : <><CBadge color='info'>
                                                        Pending
                                                    </CBadge></>}
                                                </td>)
                                        },
                                    'show_details':
                                        (item, index) => {
                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        className="btn-pill"
                                                        color="info"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(index) }}
                                                    >
                                                        {details.includes(index) ? 'Hide' : 'Update Login Mail'}
                                                    </CButton>

                                                </td>
                                            )
                                        },
                                    'details':
                                        (item, index) => {
                                            /* UPDATE SEND DATE - PUT Request */
                                            const updateSenddate = async () => {
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('start_date', fixdatestart.replace('.000Z', ''));
                                                data.append('end_date', fixdateend.replace('.000Z', ''))

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/mail/updateLoginMail?id=${item.template_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };

                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        toast.success('Send Date Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        toast.error('Failed to Update!')
                                                    });
                                                setTimeout(refreshLoginMail, 100)
                                            }
                                            return (
                                                <CCollapse show={details.includes(index)}>
                                                    <CCardBody>

                                                        <CForm>
                                                            <CFormGroup row>
                                                                <CCol xs="2">
                                                                    <p className="text-muted"><b><i>Start Date:</i></b> <CInput type="date" size="sm" md="2" onChange={(e) => setStartdate(e.target.value)} /><b> <i>Start Time:</i></b><CInput type="time" size="sm" onChange={(e) => setStarttime(e.target.value)} /></p>
                                                                </CCol>
                                                                <CCol xs="2" align='right'>
                                                                    <p className="text-muted"><b><i>End Date:</i></b> <CInput type="date" size="sm" md="2" onChange={(e) => setEnddate(e.target.value)} /><b><i>End Time:</i></b><CInput type="time" size="sm" onChange={(e) => setEndtime(e.target.value)} /></p>
                                                                    <CButton size="sm" color="info" onClick={updateSenddate}>
                                                                        Update
                                                                    </CButton>
                                                                </CCol>

                                                            </CFormGroup>

                                                        </CForm>

                                                    </CCardBody>

                                                </CCollapse>
                                            )
                                        },
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}
export default ScheduledLoginMail