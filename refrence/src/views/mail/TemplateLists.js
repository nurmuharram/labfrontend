import React, { useState, useEffect } from 'react'
import {
    CRow,
    CCol,
    CCard,
    CCardHeader,
    CCardBody,
    CModalHeader,
    CModalTitle,
    CModalBody,
    CContainer,
    CModal,
    CButton,
    CDataTable, 
    CCollapse,
    CInput,
    CSpinner,
    CSelect,
    CLabel,
    CFormGroup,
    CTextarea,
    CForm,
    CModalFooter

} from '@coreui/react'
import EditMailsTempTab from './EditMailsTempTab'
import AttachmentsModal from './AttachmentsModal'

import { getTemplates } from 'src/redux/action/mailAction/getmailtemplAction'
import { getCustoms } from 'src/redux/action/mailAction/getCustomsAction'
import { useSelector, useDispatch } from 'react-redux'
import AttachmentsCustoms from './AttachmentsCustoms'

import axios from 'axios'


function TemplateLists({ setTemplate_id, listDropdown, setCustom_message_id, customDropdown, items, custom_message_id, template_id, refreshTemplates, refreshCustoms, isLoading, setIsLoading, toast}) {

    const [createtemplatemodal, setCreatetemplatemodal] = useState(false)
    const [details, setDetails] = useState([])
    const [modals, setModals] = useState([])
    const [custommodals, setCustommodals] = useState([])

    const [subject, setSubject] = useState("")
    const [message, setMessage] = useState("")

    const [customSubject, setCustomSubject] = useState("")
    const [customMessage, setCustomMessage] = useState("")


    /* GET TEMPLATES - GET Request with Redux */
    const dispatch = useDispatch();
    const templates = useSelector((state) => state.templates.templates);
    const temploading = useSelector((state) => state.templates.temploading)
    useEffect(() => {
        dispatch(getTemplates());
    }, []);

    /* GET CUSTOMS - GET Request with Redux */
    const customs = useSelector((state) => state.customs.customs);
    const customsLoading = useSelector((state) => state.customs.loading);
    useEffect(() => {
        dispatch(getCustoms());
    }, []);

    /* Show/Hide Toggle to add attachments*/
    const toggleDetails = (index) => {
        const position = details.indexOf(index)
        let newDetails = details.slice()
        if (position !== -1) {
            newDetails.splice(position, 1)
        } else {
            newDetails = [...details, index]
        }
        setDetails(newDetails)
    }

    const toggleModal = (index) => {
        const position = modals.indexOf(index)
        let newModals = modals.slice()
        if (position !== -1) {
            newModals.splice(position, 1)
        } else {
            newModals = [...modals, index]
        }
        setModals(newModals)
    }

    const toggleCustomModal = (index) => {
        const position = custommodals.indexOf(index)
        let newModals = custommodals.slice()
        if (position !== -1) {
            newModals.splice(position, 1)
        } else {
            newModals = [...custommodals, index]
        }
        setCustommodals(newModals)
    }



    /* Table Data */
    const fields = [
        { key: 'template_id', _style: { width: '3%' } },
        { key: 'subject', _style: { width: '15%' } },
        { key: 'message', _style: { width: '20%' }, sorter: false, filter: false },
        {
            key: 'show_modals',
            label: '',
            _style: { width: '5%' },
            sorter: false,
            filter: false
        },
        {
            key: 'show_details',
            label: '',
            _style: { width: '5%' },
            sorter: false,
            filter: false
        },
        /* {key: 'option', label: '', _style: { width: '10%' }, filter: false} */
    ]

    /* Table Data */
    const customfields = [
        { key: 'message_id', _style: { width: '3%' } },
        { key: 'subject', _style: { width: '15%' } },
        { key: 'message', _style: { width: '20%' }, sorter: false, filter: false },
        {
            key: 'show_modals',
            label: '',
            _style: { width: '5%' },
            sorter: false,
            filter: false
        },
        {
            key: 'show_details',
            label: '',
            _style: { width: '5%' },
            sorter: false,
            filter: false
        },
        /* {key: 'option', label: '', _style: { width: '10%' }, filter: false} */
    ]

    const [itemget, setItemget] = useState('')

    const spinner = () => {
        return ( <div>
            <CSpinner variant="pulse" size="xxl" color="info" />
        </div>
            
        )
    }


    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            <b>Mail Template(s) list</b>
                        </CCardHeader>
                        <CCardBody>
                            <CContainer>
                                <CCol align='center'>
                                    <CButton color="info" size="md" xs='3' onClick={() => setCreatetemplatemodal(!createtemplatemodal)}><i><b>Add Mail Template / Custom Mail</b></i></CButton>
                                    <CModal
                                        show={createtemplatemodal}
                                        onClose={() => setCreatetemplatemodal(!createtemplatemodal)}
                                        color="info"
                                        size="xl"
                                    >
                                        <CModalHeader closeButton>
                                            <CModalTitle>
                                                <b>Create a Mail Template / Custom Message</b>
                                            </CModalTitle>
                                        </CModalHeader>
                                        <CModalBody>
                                            <CContainer>
                                                <EditMailsTempTab
                                                    setTemplate_id={setTemplate_id}
                                                    listDropdown={listDropdown}
                                                    setCustom_message_id={setCustom_message_id}
                                                    customDropdown={customDropdown}
                                                    items={items}
                                                    custom_message_id={custom_message_id}
                                                    template_id={template_id}
                                                    refreshTemplates={refreshTemplates}
                                                    refreshCustoms={refreshCustoms}
                                                    setCreatetemplatemodal={setCreatetemplatemodal}
                                                />
                                            </CContainer>
                                        </CModalBody>
                                    </CModal>
                                </CCol>
                            </CContainer>
                            <br />
                            <CDataTable
                                items={templates}
                                fields={fields}
                                columnFilter
                                itemsPerPageSelect
                                itemsPerPage={5}
                                hover
                                loading={temploading}
                                loadingSlot={true}
                                noItemsViewSlot={<div style={{textAlign: "center"}}><i>{spinner()}Loading Data, Please Wait...</i></div>}
                                sorter
                                sorterValue={{ column: "template_id", asc: false }}
                                pagination
                                scopedSlots={{
                                    'show_modals':
                                        (mail, index) => {

                                            /* UPDATE - PUT Request */
                                            const updateFormMessage = async () => {
                                                /* const id = mails.match.params.mail_id */
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('subject', mail.subject);
                                                data.append('message', message);

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/mail/updateTemplate?template_id=${mail.template_id}`,
                                                    headers: {
                                                        'Authorization': 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };


                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        toast.success('Template Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        toast.error('Failed to Update!')
                                                    });
                                                    setTimeout(refreshTemplates, 100)
                                                    toggleModal(index)
                                            }
                                            /* UPDATE - PUT Request */
                                            const updateFormSubject = async () => {
                                                /* const id = mails.match.params.mail_id */
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('subject', subject);
                                                data.append('message', mail.message);

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/mail/updateTemplate?template_id=${mail.template_id}`,
                                                    headers: {
                                                        'Authorization': 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };


                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        toast.success('Template Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        toast.error('Failed to Update!')
                                                    });
                                                    setTimeout(refreshTemplates, 100)
                                                    toggleModal(index)
                                            }

                                            return (
                                                <td className="py-2 col-md-1 col-lg-1">
                                                    <CButton
                                                        className="btn-pill"
                                                        color="info"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleModal(index) }}
                                                    >
                                                        {modals.includes(index) ? 'Editing...' : 'Edit Template'}
                                                    </CButton>
                                                    <CModal
                                                        show={modals.includes(index)}
                                                        onClose={() => { toggleModal(index) }}
                                                        size='lg'
                                                        color='primary'
                                                    >
                                                        <CModalHeader closeButton>
                                                            <h4>
                                                                Template_ID: {mail.template_id}
                                                            </h4>
                                                        </CModalHeader>
                                                        <CModalBody>
                                                            <CForm action="" method="post" encType="multipart/form-data" className="form-horizontal">
                                                                <CFormGroup row>
                                                                    <CCol md="3">
                                                                        <CLabel htmlFor="select"><b>Subject</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        {mail.subject}
                                                                    </CCol>

                                                                    {/* <CCol xs="12" md="2">
                                                                        <CButton color='primary' size='sm' onClick={() => {navigator.clipboard.writeText(mail.subject)}}>Copy Text</CButton>
                                                                    </CCol> */}
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="3">
                                                                        <CLabel htmlFor="select"><b>New Subject</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="7">
                                                                        <CInput placeholder='Input new Subject...' onChange={(e) => setSubject(e.target.value)} />
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="10" align='right'>
                                                                        <CButton color='primary' size='sm' onClick={updateFormSubject}>Update Subject</CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <hr />
                                                                <CFormGroup row>
                                                                    <CCol md="3">
                                                                        <CLabel htmlFor="textarea-input"><b>Message</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="7">
                                                                        {mail.message}
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="3">
                                                                        <CLabel htmlFor="textarea-input"><b>New Message</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="7">
                                                                        <CTextarea
                                                                            name="textarea-input"
                                                                            id="textarea-input"
                                                                            rows="7"
                                                                            placeholder='Input new Message...'
                                                                            onChange={(e) => setMessage(e.target.value)}
                                                                        />
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="10" align='right'>
                                                                        <CButton color='primary' size='sm' onClick={updateFormMessage}>Update Message</CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                            </CForm>
                                                        </CModalBody>
                                                        <CModalFooter>
                                                            <CButton color='secondary' size='md' onClick={() => { toggleModal(index) }}>Close</CButton>
                                                        </CModalFooter>
                                                    </CModal>
                                                </td>
                                            )
                                        },
                                    'show_details':
                                        (mail, index) => {
                                            return (
                                                <td className="py-2 col-md-1 col-lg-1">
                                                    <CButton
                                                        className="btn-pill"
                                                        color="info"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(mail.template_id) }}
                                                    >
                                                        {details.includes(mail.template_id) ? 'Close Window' : 'Add Attachments'}
                                                    </CButton>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (mail, index) => {
                                            return (
                                                <CCollapse show={details.includes(mail.template_id)}>
                                                    <CCardBody>
                                                        <h4>
                                                            Template_ID: {mail.template_id}
                                                        </h4>
                                                        <AttachmentsModal template_id={mail.template_id} />
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        }

                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <hr/>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            <b>Custom Mail(s) list</b>
                        </CCardHeader>
                        <CCardBody>
                            <br />
                            <CDataTable
                                items={customs}
                                fields={customfields}
                                columnFilter
                                itemsPerPageSelect
                                itemsPerPage={5}
                                hover
                                loading={customsLoading}
                                loadingSlot={true}
                                noItemsViewSlot={<div style={{textAlign: "center"}}><i>{spinner()}Loading Data, Please Wait...</i></div>}
                                sorter
                                sorterValue={{column: "message_id", asc: false}}
                                pagination
                                scopedSlots={{
                                    'show_modals':
                                        (mail, index) => {

                                            /* UPDATE - PUT Request */
                                            const updateFormMessage = async () => {
                                                /* const id = mails.match.params.mail_id */
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('subject', mail.subject);
                                                data.append('message', customMessage);

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/mail/updateCustom?id=${mail.message_id}`,
                                                    headers: {
                                                        'Authorization': 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };


                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        toast.success('Custom Mail Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        toast.error('Failed to Update!')
                                                    });
                                                    setTimeout(refreshCustoms, 100)
                                                    toggleCustomModal(index)
                                            }
                                            /* UPDATE - PUT Request */
                                            const updateFormSubject = async () => {
                                                /* const id = mails.match.params.mail_id */
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('subject', customSubject);
                                                data.append('message', mail.message);

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/mail/updateCustom?id=${mail.message_id}`,
                                                    headers: {
                                                        'Authorization': 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };


                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        toast.success('Custom Mail Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        toast.error('Failed to Update!')
                                                    });
                                                    setTimeout(refreshCustoms, 100)
                                                    toggleCustomModal(index)
                                            }

                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        className="btn-pill"
                                                        color="info"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleCustomModal(index) }}
                                                    >
                                                        {custommodals.includes(index) ? 'Editing...' : 'Edit Template'}
                                                    </CButton>
                                                    <CModal
                                                        show={custommodals.includes(index)}
                                                        onClose={() => { toggleCustomModal(index) }}
                                                        size='lg'
                                                        color='primary'
                                                    >
                                                        <CModalHeader closeButton>
                                                            <h4>
                                                                Message_ID: {mail.message_id}
                                                            </h4>
                                                        </CModalHeader>
                                                        <CModalBody>
                                                            <CForm action="" method="post" encType="multipart/form-data" className="form-horizontal">
                                                                <CFormGroup row>
                                                                    <CCol md="3">
                                                                        <CLabel htmlFor="select"><b>Subject</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        {mail.subject}
                                                                    </CCol>

                                                                    {/* <CCol xs="12" md="2">
                                                                        <CButton color='primary' size='sm' onClick={() => {navigator.clipboard.writeText(mail.subject)}}>Copy Text</CButton>
                                                                    </CCol> */}
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="3">
                                                                        <CLabel htmlFor="select"><b>New Subject</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="7">
                                                                        <CInput placeholder='Input new Subject...' onChange={(e) => setCustomSubject(e.target.value)} />
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="10" align='right'>
                                                                        <CButton color='primary' size='sm' onClick={updateFormSubject}>Update Subject</CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <hr />
                                                                <CFormGroup row>
                                                                    <CCol md="3">
                                                                        <CLabel htmlFor="textarea-input"><b>Message</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="7">
                                                                        {mail.message}
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="3">
                                                                        <CLabel htmlFor="textarea-input"><b>New Message</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="7">
                                                                        <CTextarea
                                                                            name="textarea-input"
                                                                            id="textarea-input"
                                                                            rows="7"
                                                                            placeholder='Input new Message...'
                                                                            onChange={(e) => setCustomMessage(e.target.value)}
                                                                        />
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="10" align='right'>
                                                                        <CButton color='primary' size='sm' onClick={updateFormMessage}>Update Message</CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                            </CForm>
                                                        </CModalBody>
                                                        <CModalFooter>
                                                            <CButton color='secondary' size='md' onClick={() => { toggleCustomModal(index) }}>Close</CButton>
                                                        </CModalFooter>
                                                    </CModal>
                                                </td>
                                            )
                                        },
                                    'show_details':
                                    (mail, index) => {
                                        return (
                                            <td className="py-2">
                                                <CButton
                                                    className="btn-pill"
                                                    color="info"
                                                    variant="outline"
                                                    shape="square"
                                                    size="sm"
                                                    onClick={() => { toggleDetails(mail.message_id) }}
                                                >
                                                    {details.includes(mail.message_id) ? 'Close Window' : 'Add Attachments'}
                                                </CButton>
                                            </td>
                                        )
                                    },
                                'details':
                                    (mail, index) => {
                                        return (
                                            <CCollapse show={details.includes(mail.message_id)}>
                                                <CCardBody>
                                                    <h4>
                                                        Custom_Message_ID: {mail.message_id}
                                                    </h4>
                                                    <AttachmentsCustoms custom_message_id={mail.message_id}/>
                                                </CCardBody>
                                            </CCollapse>
                                        )
                                    }

                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default TemplateLists
