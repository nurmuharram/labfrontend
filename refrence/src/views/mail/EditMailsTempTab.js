import React, { useState } from 'react';
import toast, { Toaster } from 'react-hot-toast';
import {
    CButton,
    CCard,
    CCardBody,
    CCardFooter,
    CCol,
    CRow,
    CForm,
    CFormGroup,
    CLabel,
    CTextarea,
    CSelect,
    CInput,
    

} from '@coreui/react'


import axios from 'axios';


function EditMailsTempTab({ setTemplate_id, listDropdown, setCustom_message_id, customDropdown, items, custom_message_id, template_id, refreshTemplates, refreshCustoms, setCreatetemplatemodal  }) {

    const [subject, setSubject] = useState('');
    const [message, setMessage] = useState('');
    const [item_type_id, 
        //setItem_type_id
    ] = useState('');
    const [item_id, 
        //setItem_id
    ] = useState('')
    const [amount, 
        //setAmount
    ] = useState('')
    

    /* Formdata for POST Request */
    const FormData = require('form-data');


    /* CREATE MAIL TEMPLATE - POST Request */
    const saveTemplate = async (e) => {

        const data = new FormData();
        data.append('subject', subject);
        data.append('message', message);


        const config = {
            method: 'POST',
            url: 'api/mail/createTemplate',
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),

            },
            data: data
        };

        await axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                toast.success("Template Created!");
                setCreatetemplatemodal(false);
            })
            .catch((error) => {
                console.log(error);
                toast.error("Failed to Create Template!")

            });
            setTimeout(refreshTemplates, 100)
    }
    /* const updateTemplate = () => {
        const FormData = require('form-data');
        const data = new FormData();
        data.append('subject', subject);
        data.append('message', message);

        const config = {
            method: 'PUT',
            url: `api/mail/updateTemplate?template_id=${template_id}`,
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Template Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Update!')
            });
    } */

    const saveCustomMessage = (e) => {

        const data = new FormData();
        data.append('subject', subject);
        data.append('message', message);

        const config = {
            method: 'POST',
            url: 'api/mail/createCustom',
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),

            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                toast.success("Custom Message Created!");
                setCreatetemplatemodal(false);
            })
            .catch((error) => {
                console.log(error);
                toast.error("Failed to Create!")

            });
            setTimeout(refreshCustoms, 100)
    }
   

    return (
        <div key="edittemplate">
            <CRow>
                <CCol>
                    <CCard>
                        <CCardBody>
                            <CForm action="" method="post" encType="multipart/form-data" className="form-horizontal" >
                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="text-input"><b>Subject</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CInput id="text-input" name="subject" defaultValue={items.subject} onChange={(e) => setSubject(e.target.value)} />
                                    </CCol>
                                </CFormGroup>
                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="textarea-input" name="message"><b>Message</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CTextarea
                                            name="message"
                                            id="textarea-input"
                                            rows="9"
                                            defaultValue={items.message}
                                            onChange={(e) => setMessage(e.target.value)}
                                        /* value={template.message} */
                                        />
                                    </CCol>
                                </CFormGroup>
                                
                            </CForm>
                        </CCardBody>
                        <CCardFooter>
                            <CRow>
                            <CCol align='left'>
                                <CButton className="btn-pill" type="submit" size="sm" color="primary" onClick={saveCustomMessage}><b>Create Custom Message</b></CButton>
                                
                                 {/*    {RenderInputField} */}

                                </CCol>
                                <CCol align='Right'>
                                <CButton className="btn-pill" type="submit" size="sm" color="primary" onClick={saveTemplate}><b>Create a new Mail Template</b></CButton>
                                
                                 {/*    {RenderInputField} */}

                                </CCol>
                                
                                
                            </CRow>
                        </CCardFooter>
                    </CCard>
                </CCol>
            </CRow >
        </div >
    )
}

export default EditMailsTempTab
