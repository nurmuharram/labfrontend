import React from 'react'

import {
    CCol,
    CCard,
    CRow,
    CCardBody,
    CFormGroup,
    CForm,
    CSelect,
    CLabel,
    CModal,
    CModalHeader,
    CModalTitle,
    CButton,
    CCardFooter
} from '@coreui/react'

import AttachmentsModal from './AttachmentsModal'
import Attachments from './Attachments'


function AddAttachmentsTab({ setTemplate_id, listDropdown, setCustom_message_id, customDropdown, setModalprimary, modalprimary, template_id, custom_message_id, _attachments, refreshAttachments, items }) {




    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardBody>
                            <CForm>
                                <CFormGroup row >
                                    <CCol md="3">
                                        <CLabel htmlFor="select"><b> Mail Template</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CSelect custom name="select" id="select" className="align-items-lg-start">
                                            <option value="0" onClick={() => setTemplate_id(0)}>Choose a template or a Custom Message...</option>
                                            {listDropdown}
                                            <option value="0" onClick={() => setCustom_message_id(0)}>######CUSTOM MESSAGES######</option>
                                            {customDropdown}
                                        </CSelect>
                                    </CCol>
                                </CFormGroup>
                                <CFormGroup row>
                                    <CCol xs="12" md="3">
                                        <b>Attachment(s)</b>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        {/* Data will be inserted in here */}
                                        <CCol md="9">

                                            <CRow>
                                                <b>:</b>
                                                <Attachments _attachments={_attachments} />
                                            </CRow>
                                        </CCol>
                                    </CCol>
                                </CFormGroup>
                            </CForm>
                            <CCol align='right'>
                                <CButton size='sm' color="primary" onClick={() => setModalprimary(!modalprimary)} className="mr-1">
                                    <b>Add / Edit Attachment(s)</b>
                                </CButton>
                            </CCol>

                            <CModal
                                show={modalprimary}
                                onClose={() => setModalprimary(!modalprimary)}
                                color="primary"
                                size="xl"
                            >

                                <CModalHeader>
                                    <CModalTitle>Add/Edit Mail Attachment(s) to <i>"{items.subject}"</i>  </CModalTitle>
                                </CModalHeader>

                                <AttachmentsModal setModalprimary={setModalprimary} template_id={template_id} custom_message_id={custom_message_id} _attachments={_attachments} refreshAttachments={refreshAttachments} />

                            </CModal>
                        </CCardBody>
                        <CCardFooter>


                            <CCol align="left">
                                <i>Template Chosen : <b>(T#{items.template_id}) (C#{items.message_id}) - {items.subject}</b></i>
                            </CCol>
                        </CCardFooter>
                    </CCard>
                </CCol>

            </CRow>
        </div>
    )
}

export default AddAttachmentsTab
