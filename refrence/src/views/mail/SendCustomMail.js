import React, { useState, useEffect } from 'react'
import axios from 'axios'
import toast, { Toaster } from 'react-hot-toast';


import {
    CButton,
    CCard,
    CCardBody,
    CCardFooter,
    CCol,
    CRow,
    CForm,
    CModal,
    CModalHeader,
    CModalTitle,
    CFormGroup,
    CLabel,
    CSelect,
    CTextarea,

 



} from '@coreui/react'
import AddRecipientModal from './AddRecipientModal'
import Attachments from './Attachments'

function SendCustomMails({ listDropdown, customDropdown, items, template_id, setTemplate_id, custom_message_id, setCustom_message_id, _attachments, setSendcustommailmodal, refreshMail, handleChangeCustom }) {

    const _date = new Date()

    const yyyy = _date.getFullYear()
    const mm = _date.getMonth() + 1
    const dd = _date.getDate()

    const hh = _date.getHours()
    const min = _date.getMinutes()
    const ss = _date.getSeconds()

    const date = yyyy + '-' + mm + '-' + dd;
    const time = hh + ':' + min + ':' + ss;
    const send_date = date + " " + time;

    const adate = new Date(send_date)

    const toiso = adate.toISOString();

    const splitdate = toiso.split('T')

    const fixdate = splitdate.join(' ')

    const [playername, setPlayername] = useState([{
        user_id: '',
        user_name: '',
    }])

    const [arraydata, setArraydata] = useState([])

    const adata = {
      recipient_users: arraydata
    }
 
    const [user_id, setUser_id] = useState('')
   

    const [primary, setPrimary] = useState(false)

    const initialstate = 0;
   
    const [mail_type, 
        //setMail_type
    ] = useState('System')
    const [sender_id, 
        //setSender_id
    ] = useState('')
   // const [receiver_id, setReceiver_id] = useState('')
    
    
    //const [status, setStatus] = useState('')


    const parameter = _attachments && _attachments.map((item)=>{
       return JSON.stringify(`{'${item.item_name}' ${":"} ${item.amount}}`)
    });

    
    const sendCustomMail =  (e, sendMail) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('mail_template', '')
        data.append('mail_type', mail_type)
        data.append('sender_id', sender_id)
        data.append('receiver_id', JSON.stringify(adata))
        data.append('send_date', fixdate.replace('.000Z', ''))
        data.append('parameter', parameter && parameter)
        data.append('custom_message_id', custom_message_id)

        const config = {
            method: 'POST',
            url: `api/mail/send`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                toast.success('Custom Mail Send!')
                setSendcustommailmodal(false)
            })
            .catch((error) => {
                console.log(error);
                toast.error('Failed to Send Custom Mail!')
            });
            setTimeout(refreshMail, 100)
    }

    const arrdata = arraydata && arraydata.map((data, index) => {
        return <><CButton color='info' size='sm' style={{padding: '0.2rem'}}>{JSON.stringify(data.recipient_user_id)} - {JSON.stringify(data.user_name)}</CButton> &nbsp;&nbsp;</>
      }) 
    
      const addeditreceipt = () => {
        if (arraydata.length <= 0) {
            return 'Add Recipient(s)'
        } else if (arraydata.length > 0){
            return 'Edit Recipient(s)'
        }
    }

    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardBody>
                            <CForm action="" method="post" encType="multipart/form-data" className="form-horizontal">
                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="select"><b>Mail Template</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CSelect custom name="select" id="select" className="align-items-md-start" onChange={handleChangeCustom}>
                                            <option value="0" disabled>Choose Custom message...</option>
                                            {customDropdown}
                                        </CSelect>
                                    </CCol>
                                </CFormGroup>
                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="textarea-input"><b>Message</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CTextarea
                                            name="textarea-input"
                                            id="textarea-input"
                                            rows="9"
                                            defaultValue={items.message}
                                            disabled
                                        />
                                    </CCol>
                                </CFormGroup>
                                <CFormGroup row>
                                    <CCol xs="12" md="3">
                                        <b>Attachment(s)</b>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        {/* Data will be inserted in here */}


                                        <CCol md="16">

                                            <CRow>
                                                <b>:</b>
                                                <Attachments _attachments={_attachments} />
                                            </CRow>
                                        </CCol>

                                    </CCol>
                                </CFormGroup>
                                <CFormGroup row>
                                    <CCol xs="12" md="3">
                                        <b>Recipient</b>
                                    </CCol>
                                    <CCol  xs="12" md="9">
                                        {/* Data will be inserted in here */}
                                        <CRow>
                                            <b>:</b>&nbsp;&nbsp;
                                           <CCol md="2rem" ><i><b>{arrdata}</b></i></CCol>
                                        </CRow>
                                    </CCol>
                                </CFormGroup>
                                <CCol align="center">
                                <CButton color="primary" size="sm" onClick={() => setPrimary(!primary)} className="mr-1">{addeditreceipt()}</CButton>
                                </CCol>
                                <CModal
                                    show={primary}
                                    onClose={() => setPrimary(!primary)}
                                    color="primary"
                                    size="xl"
                                >
                                    <CModalHeader> 
                                        <CModalTitle>Recipient (Players) List </CModalTitle>
                                    </CModalHeader>
                                    <AddRecipientModal setPrimary={setPrimary} primary={primary} setUser_id={setUser_id} user_id={user_id} setPlayername={setPlayername} playername={playername} setArraydata={setArraydata} arraydata={arraydata} />
                                </CModal>
                            </CForm>
                        </CCardBody>
                        <CCardFooter>
                            <CRow>
                                <CCol align='right'>
                                    <CButton className="btn-pill" type="send" size="sm" color="primary" onClick={sendCustomMail} ><b>Send Custom Mail to Player(s)</b></CButton>
                                </CCol>
                            </CRow>
                        </CCardFooter>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default SendCustomMails
