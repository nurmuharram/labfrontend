import React from 'react';
import { CButton, CCol } from '@coreui/react';



function Attachments({ _attachments, itemget, setItemget}) {

    

    return (
       _attachments && _attachments.map(item => (
            <div key="attachments">
                <CCol md="12" style={{padding: '0.5rem'}}>
                <CButton key={item.toString()} value={item.item_id} size="sm" color='secondary' >{item.item_type} - <b>{item.item_name}</b>({item.amount})</CButton>
               </CCol>
            </div>
        ))
    )
}


export default Attachments
