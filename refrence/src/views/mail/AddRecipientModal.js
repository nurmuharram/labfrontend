import React, {
    useState,
    useEffect,
    useRef
} from 'react'
import {
    CModalBody,
    CButton,
    CModalFooter,
    CCardBody,
    CCol,
    CRow,
    CFormGroup,
    CSelect,
    CInput,
    CDataTable,
    CInputGroup,
    CDropdown,
    CDropdownToggle,
    CDropdownMenu,
    CDropdownItem,


} from '@coreui/react'
import { useSelector, useDispatch } from 'react-redux'
import { getPlayers } from 'src/redux/action/mailAction/getPlayersAction';
import axios from 'axios';
import toast, { Toaster } from 'react-hot-toast';
import CIcon from '@coreui/icons-react';


function AddRecipientModal({ setPrimary, primary, setPidvalue, pidvalue, user_id, setUser_id, playername, setPlayername, setArraydata, arraydata }) {

    /* GET TEMPLATES - GET Request with Redux */
    /* const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(getPlayers());
    }, []); */
    const [_players, set_players] = useState([])

    const [playerSearch, setPlayerSearch] = useState('')
    /*  const [details, setDetails] = useState('')
     const [selected, setSelected] = useState() */

    const [players, setPlayers] = useState([{
        user_id: ''
    }])

    const [userid, setUserid] = useState()


    const [changelist, setChangelist] = useState(0);

    const listchange = useRef();
    listchange.current = changelist;

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`api/players/getplayers?count=5&offset=${listchange.current}`, config).then(res => {
            const players = res.data;
            setPlayers(players);
        });
    }, [])

    const [searchby, setSearchby] = useState('')

    const changesearchby = () => {

        if (searchby === 'Player ID') {
          return idinput();
        } else if (searchby === 'Username') {
          return usernameinput();
        } else if (searchby === 'Referral ID') {
          return referralidinput();
      }};

      const namesearchby = () => {
        if (searchby === 'Player ID') {
          return 'PID';
        } else if (searchby === 'Username') {
          return 'Username';
        } else if (searchby === 'Referral ID') {
          return 'Referral ID';
      }};

    const [uninput, setUninput] = useState('')
  const uninputchange = useRef();
  uninputchange.current = uninput;

  const [inputid, setInputid] = useState('')
  const inputidchange = useRef();
  inputidchange.current = inputid;

  const [inputrefid, setInputrefid] = useState('')
  const inputrefidchange = useRef();
  inputrefidchange.current = inputrefid;

  const submitusername = () => {

    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/players/getplayerbyname?user_name=${uninputchange.current}`, config).then(res => {
      const players = res.data;
      setPlayers(players);
    });
  }

  

  const submitid = () => {

    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/players/getplayer?user_id=${inputidchange.current}`, config).then(res => {
      const players = res.data;
      setPlayers(Object([players]));
    });
  }

  const submitrefid = () => {

    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/players/getplayerbyrefid?ref_id=${inputrefidchange.current}`, config).then(res => {
      const players = res.data;
      setPlayers(players);
    });
  }


  const idinput = () => {
    return <><CInput type="text" placeholder="Search by Player ID here..." value={inputidchange.current} onChange={(e) => setInputid(e.target.value.replace(/\D/,''))} /><CButton color="primary" size="sm" onClick={!inputidchange.current.length<1 ? submitid : refreshPlayerslist}>Search</CButton></>;
  }

  const usernameinput = () => {
    return <><CInput placeholder="Search by Username here..." value={uninputchange.current} onChange={(e) => setUninput(e.target.value)} /><CButton color="primary" size="sm" onClick={!uninputchange.current.length<1 ? submitusername : refreshPlayersdefault}>Search</CButton></>;
  }

  const referralidinput = () => {
    return <><CInput placeholder="Search by Referral ID here..." value={inputrefidchange.current} onChange={(e) => setInputrefid(e.target.value)} /><CButton color="primary" size="sm" onClick={!inputrefidchange.current.length<1 ? submitrefid : refreshPlayerslist}>Search</CButton></>;
  }


    /* GET TEMPLATE to manual mapping - Get Request */
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`api/players/getplayer?user_id=${user_id}`, config).then(res => {
            const playername = res.data;
            setPlayername(playername);
        });
    }, [user_id])

    const refreshPlayerslist = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`api/players/getplayers?count=5&offset=${listchange.current}`, config).then(res => {
            const items = res.data;
            setPlayers(items);
        });
    }

    const refreshPlayersdefault = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`api/players/getplayers?count=5&offset=0`, config).then(res => {
            const items = res.data;
            setPlayers(items);
        });
    }


    const items = _players

    const searchTable = () => {
        return <><CDataTable
          items={items}
          fields={[{
            key: 'user_id', _classes: 'font-weight-bold', _style: { width: '9%' }, filter: false
          },
          {
            key: 'user_name', filter: false
          }          
          ]}
          clickableRows
          pagination={false}
          hover
          striped
          onRowClick={(player) => setUser_id(player.user_id)}   
          
        /></>;
      }

    const prevbttn = () => {
        return <CButton className="info" size="sm" color="primary" onClick={decrementforprev}>Previous Lists</CButton>;
    }

    const nextbttn = () => {
        return <CButton className="info" size="sm" color="primary" onClick={incrementfornext} >Next Lists</CButton>;
    }

    const hideprevbttn = () => {

        if (changelist === 0) {
            return <></>
        } else if (Object.keys(players).length < 5){
          return <></>
        } else {
            return prevbttn();
        }
    };

    const hidenextbttn = () => {

        if (Object.keys(players).length < 5) {
            return <></>
        } else if (Object.keys(players).length >= 5) {
            return nextbttn();
        }
    };
    

    const decrementforprev = () => { setChangelist(changelist - 5); setTimeout(refreshPlayerslist, 100) }
    const incrementfornext = () => { setChangelist(changelist + 5); setTimeout(refreshPlayerslist, 100) }


  const arrdata = arraydata && arraydata.map((data, index) => {
    return <><CButton color='info' className='btn-behance' size='sm' style={{ padding: '0.2rem' }} >
      {JSON.stringify(data.recipient_user_id)} - {JSON.stringify(data.user_name)} | <CIcon size='sm' name='cil-x' onClick={() => {
      arraydata.splice(index, 1);
      setArraydata([...arraydata]);
      toast.success(`${JSON.stringify(data.user_name)} removed from the list!`)
    }}></CIcon>
    </CButton> &nbsp;&nbsp;</>
  }) 


    return (
        <div>

            <CModalBody>

                <CCardBody>
                    <CCardBody>
                      <CRow>
                      <CCol md='5'>
                        <CInputGroup>
                            <CDropdown className="input-group-prepend">
                                <CDropdownToggle caret color="primary" size="sm">
                                    Search by {namesearchby()}
                                </CDropdownToggle>
                                <CDropdownMenu>
                                    <CDropdownItem id="Username" onClick={(e) => setSearchby(e.target.id)}>Player Username</CDropdownItem>
                                    <CDropdownItem id="Player ID" onClick={(e) => setSearchby(e.target.id)}>Player ID</CDropdownItem>
                                    <CDropdownItem id="Referral ID" onClick={(e) => setSearchby(e.target.id)}>Referral ID</CDropdownItem>

                                </CDropdownMenu>
                            </CDropdown>
                            {changesearchby()}
                        </CInputGroup>
                        </CCol>
                      </CRow>
                      <CRow>
                      <CCol align='left'>
                      <small><i>PROTIP: *To get accurate and precise search results, please search using Player ID or Referral ID.</i></small>
                      </CCol>
                      </CRow>
                        <br/>
                      <CRow>
                        <CCol align="center">
                          <b><i>Recipient lists</i></b>
                        </CCol>
                      </CRow>
                      <br />
                        {players === null ? <><CRow>
                        <CCol align='center'>
                          <h4>Ooopss... No Players Found! Check Your "Search By" or spelling</h4>
                        </CCol>
                        </CRow></>:<><CDataTable
                            items={players}
                            fields={[
                                { key: 'user_id', _style: { width: '4%' } },
                                { key: 'user_name', _style: { width: '10%' } },
                            ]}
                            hover
                            clickableRows
                            onRowClick={(player) => setArraydata([...arraydata, {recipient_user_id: player.user_id, user_name: player.user_name}])}
                        />
                        <CRow>
                            <CCol align='left'>
                                {hideprevbttn()}
                            </CCol>
                            <CCol align='right'>
                                {hidenextbttn()}
                            </CCol>
                        </CRow></>}
                    </CCardBody>

                    <CFormGroup row>

                        <CCol xs="12" md="4">
                            <b>Recipient(s)</b>
                        </CCol>
                        <CCol xs="12" md="8">
                            {/* Data will be inserted in here */}
                            <CRow>
                                <b>:</b>
                                &nbsp;&nbsp;&nbsp;
                                {arrdata}
                            </CRow>
                        </CCol>
                    </CFormGroup>
                </CCardBody>
                
            </CModalBody>

            <CModalFooter>                
                <CButton color="primary" onClick={() => setPrimary(!primary)}>
                    Confirm
                </CButton>{' '}
                <CButton color="secondary" onClick={() => setPrimary(!primary)}>
                    Cancel
                </CButton>
            </CModalFooter>

        </div>
    )
}


export default AddRecipientModal
