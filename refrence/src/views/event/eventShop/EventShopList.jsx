import React, { useState } from 'react'
import { GetAnniversaryShopList } from 'src/api/EventEnergyAPI/eventEnergyGetRequest';
import DataTable from 'src/components/table/DataTable';
import { ShopListModal } from '../components/modals/ShopListModal';

function EventShopList() {
    const [shopList, setShopList] = useState([])
    const [addItemModal, setAddItemModal] = useState(false);
    GetAnniversaryShopList({ setShopList });
    return (
      <div>
        <div className="pb-3">
          <button
            className="btn btn-primary btn-md"
            onClick={() => setAddItemModal(!addItemModal)}
          >
            + Add Event Shop Period
          </button>
          <ShopListModal setAddItemModal={setAddItemModal} addItemModal={addItemModal} setShopList={setShopList} />
        </div>
        <DataTable items={shopList} itemsPerPage={5} pagination />
      </div>
    );
}

export default EventShopList