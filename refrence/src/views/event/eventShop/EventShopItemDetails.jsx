import React, { useState } from 'react'
import { GetAnniversaryShopItemDetails } from 'src/api/EventEnergyAPI/eventEnergyGetRequest';
import DataTable from 'src/components/table/DataTable';
import { ShopitemDetailsModal } from '../components/modals/ShopitemDetailsModal';

function EventShopItemDetails() {
  const [itemDetails, setItemDetails] = useState([])
  const [addItemModal, setAddItemModal] = useState(false);
  GetAnniversaryShopItemDetails({ setItemDetails });
  return (
    <div>
      <div className="pb-3">
        <button
          className="btn btn-primary btn-md"
          onClick={() => setAddItemModal(!addItemModal)}
        >
          + Add Item Details
        </button>
        <ShopitemDetailsModal setAddItemModal={setAddItemModal} addItemModal={addItemModal} setShopList={setItemDetails} />
      </div>
      <DataTable items={itemDetails} itemsPerPage={5} pagination />
    </div>
  );
}

export default EventShopItemDetails