import React, { useState } from 'react'
import { GetAnniversaryShopItemList } from 'src/api/EventEnergyAPI/eventEnergyGetRequest';
import DataTable from 'src/components/table/DataTable';
import { ShopItemListModal } from '../components/modals/ShopItemListModal';

function EventShopItemList() {
  const [itemList, setItemList] = useState([])
  const [addItemModal, setAddItemModal] = useState(false);
  GetAnniversaryShopItemList({ setItemList });
  return (
    <div>
      <div className="pb-3">
        <button
          className="btn btn-primary btn-md"
          onClick={() => setAddItemModal(!addItemModal)}
        >
          + Add Shop Items
        </button>
        <ShopItemListModal setAddItemModal={setAddItemModal} addItemModal={addItemModal} setShopList={setItemList} />
      </div>
      <DataTable items={itemList} itemsPerPage={5} pagination />
    </div>
  );
}

export default EventShopItemList