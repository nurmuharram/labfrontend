import React, { useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import EventMissionDetail from "./eventMission/EventMissionDetail";
import MissionEvent from "./eventMission/MissionEvent";
import MissionList from "./eventMission/MissionList";
import MissionReward from "./eventMission/MissionReward";
import MissionType from "./eventMission/MissionType";

function AnniversaryEventMission() {
  const [key, setKey] = useState('MissionType');

  return (
    <div>
      <Tabs
        id="controlled-tab"
        activeKey={key}
        onSelect={(k) => setKey(k)}
        className="mb-3 space-right2"
        variant="tabs"
        color="red"
      >
        <Tab eventKey="MissionType" title="Mission Type">
          <MissionType />
        </Tab>
        <Tab eventKey="MissionList" title="Mission List">
          <MissionList />
        </Tab>
        <Tab eventKey="MissionEvent" title="Mission Event">
          <MissionEvent />
        </Tab>
        <Tab eventKey="EventMissionDetail" title="Event Mission Detail">
          <EventMissionDetail />
        </Tab>
        <Tab eventKey="MissionReward" title="Mission Reward">
          <MissionReward />
        </Tab>
      </Tabs>
    </div>
  );
}

export default AnniversaryEventMission;
