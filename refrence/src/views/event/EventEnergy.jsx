import React, { useState } from 'react'
import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CCollapse,
    CDataTable,
    CFormGroup,
    CInput,
    CLabel,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    CRow,
    CSelect
} from '@coreui/react'
import { GetEventEnergy, GetEventList } from 'src/api/EventEnergyAPI/eventEnergyGetRequest'
import { EventEnergyItemFunction, EventEnergyItemUpdate, ItemReward, TargetEnergy_, toggleDetails } from 'src/services/evenEnergy.services/EventEnergy.Function'
import { AddEventEnergy } from 'src/api/EventEnergyAPI/eventEnergyPostRequest'
import { UpdateTargetenergy } from 'src/api/EventEnergyAPI/eventEnergyPutRequest'

function EventEnergy() {

    const [addItemModal, setAddItemModal] = useState(false)
    const [deleteItemModal, setDeleteItemModal] = useState(false)
    const [details, setDetails] = useState([])
    const [eventEnergy, setEventEnergy] = useState([])
    const [eventList, setEventList] = useState([])
    const [arrayData, setArrayData] = useState([])
    const [target_energy, setTarget_energy] = useState([])

    const [itemtype, setItemtype] = useState()
    const [itemid, setItemid] = useState(1)
    const [amount, setAmount] = useState()
    const [eventId, setEventId] = useState(1)
    const [startdate, setStartdate] = useState('2019-10-10')
    const [starttime, setStarttime] = useState('00:00')
    const [enddate, setEnddate] = useState('2019-10-10')
    const [endtime, setEndtime] = useState('00:00')
    const [maxEnergy, setMaxEnergy] = useState()
    const [reward, setReward] = useState()
    const [targetEnergy, setTargetEnergy] = useState()

    const [newitemtype, setNewitemtype] = useState()
    const [newitemid, setNewitemid] = useState(1)
    const [newamount, setNewamount] = useState()
    const [newtargetEnergy, setNewtargetEnergy] = useState()

    GetEventEnergy({ setEventEnergy })
    GetEventList({ setEventList })

    const fields = [
        { key: 'event_energy', _style: { width: '10%' } },
        { key: 'start_time' },
        { key: 'end_time' },
        { key: 'max_energy' },
        { key: 'reward' },
        {
            key: 'show_details',
            label: '',
            _style: { width: '14%' },
            sorter: false,
            filter: false
        }
    ]

    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            <CButton color='primary' onClick={() => setAddItemModal(!addItemModal)}>+ Add Event Energy Items</CButton>
                            <CModal
                                show={addItemModal}
                                onClose={() => setAddItemModal(!addItemModal)}
                                color="primary"
                                size="lg"
                            >
                                <CModalHeader closeButton>
                                    <CModalTitle>
                                        Add Event Energy Items
                                    </CModalTitle>
                                </CModalHeader>
                                <CModalBody>
                                    <CCol>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Event ID</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CSelect custom onChange={(e) => setEventId(e.target.value)}>
                                                    {eventList.map((item) =>
                                                        <option key={item.event_id} value={item.event_id}>{item.event_id} - {item.event_name}</option>
                                                    )}
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Start date</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="4">
                                                <CInput type='date' onKeyDown={(e) => e.preventDefault()} onChange={(e) => setStartdate(e.target.value)} />
                                            </CCol>
                                            <CCol xs="12" md="4">
                                                <CInput type='time' onChange={(e) => setStarttime(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>End date</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="4">
                                                <CInput type='date' onKeyDown={(e) => e.preventDefault()} onChange={(e) => setEnddate(e.target.value)} />
                                            </CCol>
                                            <CCol xs="12" md="4">
                                                <CInput type='time' onChange={(e) => setEndtime(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Max Energy</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CInput type='text' placeholder='Set Max Energy...' onChange={(e) => setMaxEnergy(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Reward</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CInput type='text' placeholder='Set Reward...' onChange={(e) => setReward(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <hr />
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Target Energy</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="6">
                                                <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setTargetEnergy(e.target.value)}>
                                                    <option value={''}>Choose Target Energy</option>
                                                    <option value={1000}  >1000</option>
                                                    <option value={2000}  >2000</option>
                                                    <option value={4000}  >4000</option>
                                                    <option value={8000}  >8000</option>
                                                    <option value={16000}  >16000</option>
                                                    <option value={32000}  >32000</option>
                                                    <option value={64000} >64000</option>
                                                    <option value={128000} >128000</option>
                                                    <option value={256000} >256000</option>
                                                    <option value={512000} >512000</option>
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol xs="12" md="9" align='right'>
                                                <CButton size="sm" color="info" onClick={() => setTarget_energy([...target_energy, { target_energy: targetEnergy }])} ><b>+ Add Target Energy</b></CButton>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Target Energy</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                {TargetEnergy_({ target_energy, setTarget_energy })}
                                            </CCol>
                                        </CFormGroup>
                                        <hr />
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Item Type</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="6">
                                                <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setItemtype(e.target.value)}>
                                                    <option value={''}>Choose Item Type</option>
                                                    <option value={1}  >Currency</option>
                                                    <option value={6}  >Box</option>
                                                    <option value={5}  >Items</option>
                                                    <option value={2}  >Ksatriya</option>
                                                    <option value={3}  >KSA Skins</option>
                                                    <option value={4}  >Rune</option>
                                                    <option value={11} >Frame</option>
                                                    <option value={12} >Avatar</option>
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Item ID</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol md='6'>
                                                <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setItemid(e.target.value)}>
                                                    {EventEnergyItemFunction({ itemtype, itemid })}
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Amount</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="6">
                                                <CInput type='text' placeholder='Set Amount...' onChange={(e) => setAmount(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol xs="12" md="9" align='right'>
                                                <CButton size="sm" color="info" onClick={() => setArrayData([...arrayData, { item_type: parseInt(itemtype), item_id: parseInt(itemid), amount: parseInt(amount) }])} ><b>+ Add Item</b></CButton>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Item Reward</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                {ItemReward({ arrayData, setArrayData })}
                                            </CCol>
                                        </CFormGroup>
                                    </CCol>
                                </CModalBody>
                                <CModalFooter>
                                    {AddEventEnergy({ startdate, enddate, starttime, endtime, setAddItemModal, setEventEnergy, eventId, maxEnergy, arrayData, target_energy, reward })}
                                </CModalFooter>
                            </CModal>
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={eventEnergy}
                                fields={fields}
                                itemsPerPage={5}
                                pagination
                                scopedSlots={{
                                    'show_details':
                                        (item, index) => {
                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        color="primary"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(index, { setDetails, details }) }}
                                                    >
                                                        {details.includes(index) ? 'Hide' : 'Edit/Update'}
                                                    </CButton>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (item, index) => {
                                            const id = item.event_energy
                                            return (
                                                <CCollapse show={details.includes(index)}>
                                                    <CCardBody>
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <h4>
                                                                    <i>Edit/Update Item (<small>ID : {id}</small>)</i>
                                                                </h4>
                                                            </CCol>
                                                        </CRow>
                                                        <hr />
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <CFormGroup row>
                                                                    <CCol md="3">
                                                                        <CLabel htmlFor="input"><b>Target Energy</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="6">
                                                                        <CSelect custom name="select" id="select" className="align-items-md-start" onChange={(e) => setNewtargetEnergy(e.target.value)}>
                                                                            <option value={''}>Choose Target Energy</option>
                                                                            <option value={1000}  >1000</option>
                                                                            <option value={2000}  >2000</option>
                                                                            <option value={4000}  >4000</option>
                                                                            <option value={8000}  >8000</option>
                                                                            <option value={16000}  >16000</option>
                                                                            <option value={32000}  >32000</option>
                                                                            <option value={64000}  >64000</option>
                                                                            <option value={128000} >128000</option>
                                                                            <option value={256000} >256000</option>
                                                                            <option value={512000} >512000</option>
                                                                        </CSelect>
                                                                    </CCol>
                                                                    <CCol xs="12" md="2" >
                                                                        {UpdateTargetenergy({ setEventEnergy, newtargetEnergy, id })}
                                                                    </CCol>
                                                                </CFormGroup>
                                                            </CCol>
                                                        </CRow>
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        },
                                    "start_time":
                                        (item, index) => {
                                            const date = new Date((item.start_time && item.start_time.split(' ').join('T')) + '.000Z')
                                            return (
                                                <td>
                                                    {date.toString()}
                                                </td>
                                            )
                                        },
                                    "end_time":
                                        (item, index) => {
                                            const date = new Date((item.end_time && item.end_time.split(' ').join('T')) + '.000Z')
                                            return (
                                                <td>
                                                    {date.toString()}
                                                </td>
                                            )
                                        },
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default EventEnergy
