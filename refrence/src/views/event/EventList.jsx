import React, { useState } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CFormGroup,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
  CSelect,
  CCollapse,
} from "@coreui/react";
import { GetEventList } from "src/api/EventEnergyAPI/eventEnergyGetRequest";
import {
  DropdownKsatriya,
  toggleDetails,
} from "src/services/evenEnergy.services/EventList.Function";
import {
  AddEvent,
  AddEventAnniversary,
} from "src/api/EventEnergyAPI/eventEnergyPostRequest";
import { DeleteEvent } from "src/api/EventEnergyAPI/eventEnergyDeleteRequest";
import {
  UpdateEventEnddate,
  UpdateEventExpireddate,
  UpdateEventName,
  UpdateEventStartdate,
} from "src/api/EventEnergyAPI/eventEnergyPutRequest";

function EventList() {
  const [addItemModal, setAddItemModal] = useState(false);
  const [addAnniversaryModal, setAddAnniversaryModal] = useState(false);
  const [deleteItemModal, setDeleteItemModal] = useState(false);
  const [details, setDetails] = useState([]);
  const [eventList, setEventList] = useState([]);

  const [event_name, setEvent_name] = useState("");
  const [startdate, setStartdate] = useState("2019-10-10");
  const [starttime, setStarttime] = useState("00:00");
  const [enddate, setEnddate] = useState("2019-10-10");
  const [endtime, setEndtime] = useState("00:00");
  const [expired_date, setExpired_date] = useState("2019-10-10");
  const [expired_time, setExpired_time] = useState("00:00");
  const [energy_base, setEnergy_base] = useState();
  const [ksatriyaid, setKsatriyaid] = useState(1);
  const [boostrank, setBoostrank] = useState();
  const [trial, setTrial] = useState();
  const [boostpermanent, setBoostpermanent] = useState();

  const [newstartdate, setNewstartdate] = useState("2019-10-10");
  const [newstarttime, setNewstarttime] = useState("00:00");
  const [newenddate, setNewenddate] = useState("2019-10-10");
  const [newendtime, setNewendtime] = useState("00:00");
  const [newexpired_date, setNewexpired_date] = useState("2019-10-10");
  const [newexpired_time, setNewexpired_time] = useState("00:00");
  const [neweventName, setNeweventName] = useState();
  const todayDate = new Date().toISOString().split("T")[0];
  const [anniversaryForm, setAnniversaryForm] = useState({
    event_name: "",
    start_date: todayDate,
    start_time: "00:00",
    end_date: todayDate,
    end_time: "00:00",
    expired_date: todayDate,
    expired_time: "00:00",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setAnniversaryForm({ ...anniversaryForm, [name]: value });
  };

  GetEventList({ setEventList });

  const fields = [
    { key: "event_id", _style: { width: "7%" } },
    { key: "event_name" },
    { key: "start_time" },
    { key: "end_time" },
    { key: "expired_date" },
    {
      key: "show_details",
      label: "",
      _style: { width: "14%" },
      sorter: false,
      filter: false,
    },
  ];

  return (
    <div>
      <CRow>
        <CCol>
          <CCard>
            <CCardHeader>
              <div align="right">
                <button
                  className="btn btn-primary"
                  onClick={() => setAddAnniversaryModal(!addAnniversaryModal)}
                >
                  + Add Event Anniversary Period
                </button>
                &nbsp;&nbsp;
                <button
                  className="btn btn-primary"
                  onClick={() => setAddItemModal(!addItemModal)}
                >
                  + Add Event Energy Period
                </button>
              </div>
              <div>
                <CModal
                  show={addAnniversaryModal}
                  onClose={() => setAddAnniversaryModal(!addAnniversaryModal)}
                  color="primary"
                  size="md"
                >
                  <CModalHeader closeButton>
                    <CModalTitle>New Event Anniversary Period</CModalTitle>
                  </CModalHeader>
                  <CModalBody>
                    <CCol>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>Event Name</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="8">
                          <CInput
                            type="text"
                            name="event_name"
                            value={anniversaryForm.event_name}
                            placeholder="Set Event Name..."
                            onChange={handleChange}
                          />
                        </CCol>
                      </CFormGroup>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>Start date</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="4">
                          <CInput
                            type="date"
                            name="start_date"
                            value={anniversaryForm.start_date}
                            onKeyDown={(e) => e.preventDefault()}
                            onChange={handleChange}
                          />
                        </CCol>
                        <CCol xs="12" md="4">
                          <CInput
                            type="time"
                            name="start_time"
                            value={anniversaryForm.start_time}
                            onChange={handleChange}
                          />
                        </CCol>
                      </CFormGroup>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>End date</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="4">
                          <CInput
                            type="date"
                            name="end_date"
                            value={anniversaryForm.end_date}
                            onKeyDown={(e) => e.preventDefault()}
                            onChange={handleChange}
                          />
                        </CCol>
                        <CCol xs="12" md="4">
                          <CInput
                            type="time"
                            name="end_time"
                            value={anniversaryForm.end_time}
                            onChange={handleChange}
                          />
                        </CCol>
                      </CFormGroup>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>Expired Date</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="4">
                          <CInput
                            type="date"
                            name="expired_date"
                            value={anniversaryForm.expired_date}
                            onKeyDown={(e) => e.preventDefault()}
                            onChange={handleChange}
                          />
                        </CCol>
                        <CCol xs="12" md="4">
                          <CInput
                            type="time"
                            name="expired_time"
                            value={anniversaryForm.expired_time}
                            onChange={handleChange}
                          />
                        </CCol>
                      </CFormGroup>
                    </CCol>
                  </CModalBody>
                  <CModalFooter>
                    {AddEventAnniversary({
                      anniversaryForm,
                      setEventList,
                      setAddAnniversaryModal,
                    })}
                  </CModalFooter>
                </CModal>
              </div>
              <div>
                <CModal
                  show={addItemModal}
                  onClose={() => setAddItemModal(!addItemModal)}
                  color="primary"
                  size="lg"
                >
                  <CModalHeader closeButton>
                    <CModalTitle>New Event Energy Period</CModalTitle>
                  </CModalHeader>
                  <CModalBody>
                    <CCol>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>Event Name</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="8">
                          <CInput
                            type="text"
                            placeholder="Set Event Name..."
                            onChange={(e) => setEvent_name(e.target.value)}
                          />
                        </CCol>
                      </CFormGroup>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>Start date</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="4">
                          <CInput
                            type="date"
                            onKeyDown={(e) => e.preventDefault()}
                            onChange={(e) => setStartdate(e.target.value)}
                          />
                        </CCol>
                        <CCol xs="12" md="4">
                          <CInput
                            type="time"
                            onChange={(e) => setStarttime(e.target.value)}
                          />
                        </CCol>
                      </CFormGroup>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>End date</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="4">
                          <CInput
                            type="date"
                            onKeyDown={(e) => e.preventDefault()}
                            onChange={(e) => setEnddate(e.target.value)}
                          />
                        </CCol>
                        <CCol xs="12" md="4">
                          <CInput
                            type="time"
                            onChange={(e) => setEndtime(e.target.value)}
                          />
                        </CCol>
                      </CFormGroup>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>Expired Date</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="4">
                          <CInput
                            type="date"
                            onKeyDown={(e) => e.preventDefault()}
                            onChange={(e) => setExpired_date(e.target.value)}
                          />
                        </CCol>
                        <CCol xs="12" md="4">
                          <CInput
                            type="time"
                            onChange={(e) => setExpired_time(e.target.value)}
                          />
                        </CCol>
                      </CFormGroup>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>Energy Base</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="8">
                          <CInput
                            type="number"
                            placeholder="Set Energy Base..."
                            onChange={(e) => setEnergy_base(e.target.value)}
                          />
                        </CCol>
                      </CFormGroup>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>Boosting Rank</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="8">
                          <CInputGroup>
                            <CInput
                              type="number"
                              placeholder="Set Boosting Rank..."
                              onChange={(e) => setBoostrank(e.target.value)}
                            />
                            <CInputGroupPrepend>
                              <CInputGroupText>00%</CInputGroupText>
                            </CInputGroupPrepend>
                          </CInputGroup>
                        </CCol>
                      </CFormGroup>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>Boosting Permanent</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="8">
                          <CInputGroup>
                            <CInput
                              type="number"
                              placeholder="Set Boosting Permanent..."
                              onChange={(e) =>
                                setBoostpermanent(e.target.value)
                              }
                            />
                            <CInputGroupPrepend>
                              <CInputGroupText>00%</CInputGroupText>
                            </CInputGroupPrepend>
                          </CInputGroup>
                        </CCol>
                      </CFormGroup>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>Trial</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="8">
                          <CInputGroup>
                            <CInput
                              type="number"
                              placeholder="Set Trial..."
                              onChange={(e) => setTrial(e.target.value)}
                            />
                            <CInputGroupPrepend>
                              <CInputGroupText>00%</CInputGroupText>
                            </CInputGroupPrepend>
                          </CInputGroup>
                        </CCol>
                      </CFormGroup>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel htmlFor="input">
                            <b>Ksatriya ID</b>
                          </CLabel>
                        </CCol>
                        :
                        <CCol xs="12" md="8">
                          <CSelect
                            custom
                            onChange={(e) => setKsatriyaid(e.target.value)}
                          >
                            {DropdownKsatriya()}
                          </CSelect>
                        </CCol>
                      </CFormGroup>
                    </CCol>
                  </CModalBody>
                  <CModalFooter>
                    {AddEvent({
                      startdate,
                      enddate,
                      starttime,
                      endtime,
                      expired_date,
                      expired_time,
                      energy_base,
                      setAddItemModal,
                      setEventList,
                      ksatriyaid,
                      boostrank,
                      trial,
                      boostpermanent,
                      event_name,
                    })}
                  </CModalFooter>
                </CModal>
              </div>
            </CCardHeader>
            <CCardBody>
              <CDataTable
                items={eventList}
                fields={fields}
                itemsPerPage={5}
                pagination
                scopedSlots={{
                  show_details: (item, index) => {
                    return (
                      <td className="py-2">
                        <CButton
                          color="primary"
                          variant="outline"
                          shape="square"
                          size="sm"
                          onClick={() => {
                            toggleDetails(index, { setDetails, details });
                          }}
                        >
                          {details.includes(index) ? "Hide" : "Edit/Update"}
                        </CButton>
                      </td>
                    );
                  },
                  details: (item, index) => {
                    const id = item.event_id;
                    const starttime = item.start_time;
                    const endtime = item.end_time;
                    const _expireddate = item.expired_date;
                    return (
                      <CCollapse show={details.includes(index)}>
                        <CCardBody>
                          <CRow>
                            <CCol align="left">
                              <h4>
                                <i>
                                  Edit/Update Item (<small>ID : {id}</small>)
                                </i>
                              </h4>
                            </CCol>
                            <CCol align="left" md="2">
                              <CButton
                                size="sm"
                                color="danger"
                                variant="outline"
                                shape="square"
                                onClick={() =>
                                  setDeleteItemModal(!deleteItemModal)
                                }
                              >
                                Delete Item
                              </CButton>
                              <CModal
                                show={deleteItemModal}
                                onClose={() =>
                                  setDeleteItemModal(!deleteItemModal)
                                }
                                color="danger"
                                size="md"
                              >
                                <CModalHeader closeButton>
                                  <CModalTitle>
                                    <b>Delete this item?</b>
                                  </CModalTitle>
                                </CModalHeader>
                                <CModalBody>
                                  <CCol align="center">
                                    <h5>Are you sure???</h5>
                                  </CCol>
                                </CModalBody>
                                <CModalFooter>
                                  <CRow>
                                    <CCol align="center">
                                      {DeleteEvent({
                                        setEventList,
                                        setDeleteItemModal,
                                        id,
                                      })}
                                      &nbsp;
                                      <CButton
                                        type="send"
                                        size="sm"
                                        color="secondary"
                                        onClick={() =>
                                          setDeleteItemModal(!deleteItemModal)
                                        }
                                      >
                                        <b>Cancel</b>
                                      </CButton>
                                    </CCol>
                                  </CRow>
                                </CModalFooter>
                              </CModal>
                            </CCol>
                          </CRow>
                          <hr />
                          <CRow>
                            <CCol align="left">
                              <CFormGroup row>
                                <CCol md="3">
                                  <CLabel htmlFor="input">
                                    <b>New Event Name</b>
                                  </CLabel>
                                </CCol>
                                :
                                <CCol xs="12" md="4">
                                  <CInput
                                    type="text"
                                    placeholder="Set New Event Name..."
                                    onChange={(e) =>
                                      setNeweventName(e.target.value)
                                    }
                                  />
                                </CCol>
                                <CCol xs="12" md="2">
                                  {UpdateEventName({
                                    setEventList,
                                    neweventName,
                                    id,
                                  })}
                                </CCol>
                              </CFormGroup>
                              <CFormGroup row>
                                <CCol md="3">
                                  <CLabel htmlFor="input">
                                    <b>New Start Date</b>
                                  </CLabel>
                                </CCol>
                                :
                                <CCol xs="12" md="2">
                                  <CInput
                                    type="date"
                                    onKeyDown={(e) => e.preventDefault()}
                                    onChange={(e) =>
                                      setNewstartdate(e.target.value)
                                    }
                                  />
                                </CCol>
                                <CCol xs="12" md="2">
                                  <CInput
                                    type="time"
                                    onChange={(e) =>
                                      setNewstarttime(e.target.value)
                                    }
                                  />
                                </CCol>
                                <CCol xs="12" md="2">
                                  {UpdateEventStartdate({
                                    setEventList,
                                    newstartdate,
                                    newstarttime,
                                    endtime,
                                    _expireddate,
                                    id,
                                  })}
                                </CCol>
                              </CFormGroup>
                              <CFormGroup row>
                                <CCol md="3">
                                  <CLabel htmlFor="input">
                                    <b>New End Date</b>
                                  </CLabel>
                                </CCol>
                                :
                                <CCol xs="12" md="2">
                                  <CInput
                                    type="date"
                                    onKeyDown={(e) => e.preventDefault()}
                                    onChange={(e) =>
                                      setNewenddate(e.target.value)
                                    }
                                  />
                                </CCol>
                                <CCol xs="12" md="2">
                                  <CInput
                                    type="time"
                                    onChange={(e) =>
                                      setNewendtime(e.target.value)
                                    }
                                  />
                                </CCol>
                                <CCol xs="12" md="2">
                                  {UpdateEventEnddate({
                                    setEventList,
                                    newenddate,
                                    newendtime,
                                    starttime,
                                    _expireddate,
                                    id,
                                  })}
                                </CCol>
                              </CFormGroup>
                              <CFormGroup row>
                                <CCol md="3">
                                  <CLabel htmlFor="input">
                                    <b>New Expired Date</b>
                                  </CLabel>
                                </CCol>
                                :
                                <CCol xs="12" md="2">
                                  <CInput
                                    type="date"
                                    onKeyDown={(e) => e.preventDefault()}
                                    onChange={(e) =>
                                      setNewexpired_date(e.target.value)
                                    }
                                  />
                                </CCol>
                                <CCol xs="12" md="2">
                                  <CInput
                                    type="time"
                                    onChange={(e) =>
                                      setNewexpired_time(e.target.value)
                                    }
                                  />
                                </CCol>
                                <CCol xs="12" md="2">
                                  {UpdateEventExpireddate({
                                    setEventList,
                                    newexpired_date,
                                    newexpired_time,
                                    endtime,
                                    starttime,
                                    id,
                                  })}
                                </CCol>
                              </CFormGroup>
                            </CCol>
                          </CRow>
                        </CCardBody>
                      </CCollapse>
                    );
                  },
                  start_time: (item, index) => {
                    const date = new Date(
                      (item.start_time &&
                        item.start_time.split(" ").join("T")) + ".000Z"
                    );
                    return <td>{date.toString()}</td>;
                  },
                  end_time: (item, index) => {
                    const date = new Date(
                      (item.end_time && item.end_time.split(" ").join("T")) +
                        ".000Z"
                    );
                    return <td>{date.toString()}</td>;
                  },
                  expired_date: (item, index) => {
                    const date = new Date(
                      (item.expired_date &&
                        item.expired_date.split(" ").join("T")) + ".000Z"
                    );
                    return <td>{date.toString()}</td>;
                  },
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </div>
  );
}

export default EventList;
