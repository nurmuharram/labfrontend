import React, { useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import AnniversaryEventMission from "./AnniversaryEventMission";
import AnniversaryEventShop from "./AnniversaryEventShop";

function EventAnniversary() {
  const [key, setKey] = useState("AnniversaryEventMission");
  return (
    <div>
        <br/>
      <Tabs
        id="controlled-tab"
        activeKey={key}
        onSelect={(k) => setKey(k)}
        className="mb-3"
        variant="pills"
        color="red"
      >
        <Tab eventKey="AnniversaryEventMission" title="Event Mission">
          <AnniversaryEventMission />
        </Tab>
        <Tab eventKey="AnniversaryEventShop" title="Event Shop">
          <AnniversaryEventShop />
        </Tab>
      </Tabs>
    </div>
  );
}

export default EventAnniversary;
