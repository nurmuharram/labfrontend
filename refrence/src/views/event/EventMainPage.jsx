import React from 'react'
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CContainer,
    CNav,
    CNavItem,
    CNavLink,
    CTabContent,
    CTabPane,
    CTabs,
} from '@coreui/react'
import EventList from './EventList'
import EventEnergy from './EventEnergy'
import EventAnniversary from './EventAnniversary'

function EventEnergyMain() {
    return (
      <div>
        <div className="col" align="center">
          <div className="card" style={{ background: "#9bf54c" }}>
            <h4>
              <b>Lokapala Events</b>
            </h4>
            <h6>
              <i>Add/Customize Events</i>
            </h6>
          </div>
        </div>
        <CContainer fluid>
          <CCard accentColor="primary">
            <CCardHeader>
              <div className="col" align="center">
                <h5>
                  <b>Event Manager</b>
                </h5>
              </div>
            </CCardHeader>
            <CCardBody>
              <CTabs>
                <CNav variant="tabs">
                  <CNavItem>
                    <CNavLink>All Event List</CNavLink>
                  </CNavItem>
                  <CNavItem>
                    <CNavLink>Event Energy Settings</CNavLink>
                  </CNavItem>
                  <CNavItem>
                    <CNavLink>Event Anniversary Settings</CNavLink>
                  </CNavItem>
                </CNav>
                <CTabContent>
                  <CTabPane>
                    <EventList />
                  </CTabPane>
                  <CTabPane>
                    <EventEnergy />
                  </CTabPane>
                  <CTabPane>
                    <EventAnniversary />
                  </CTabPane>
                </CTabContent>
              </CTabs>
            </CCardBody>
          </CCard>
        </CContainer>
      </div>
    );
}

export default EventEnergyMain
