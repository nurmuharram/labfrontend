import React, { useState } from "react";
import { GetAnniversaryMissionEventDetail } from "src/api/EventEnergyAPI/eventEnergyGetRequest";
import Link from "src/components/link/Link";
import DataTable from "src/components/table/DataTable";
import {
  EventDetailModal,
  MissionEventDetailModal,
} from "../components/modals/MissionEventDetailModal";

function EventMissionDetail() {
  const [missionEventDetail, setMissionEventDetail] = useState([]);
  const [addItemModal, setAddItemModal] = useState(false);
  const [eventDetailsModal, setEventDetailsModal] = useState(false);
  GetAnniversaryMissionEventDetail({ setMissionEventDetail });
  return (
    <div>
      <div className="pb-3">
        <button
          className="btn btn-primary btn-md"
          onClick={() => setAddItemModal(!addItemModal)}
        >
          +Add Mission Event Detail
        </button>
        <MissionEventDetailModal
          addItemModal={addItemModal}
          setAddItemModal={setAddItemModal}
          setMissionEventDetail={setMissionEventDetail}
        />
      </div>
      <DataTable
        items={missionEventDetail}
        itemsPerPage={5}
        itemsPerPageSelect
        pagination
        columnFilter
        /* scopedSlots={{
          event_name: (item, index) => {
            const eventId = item.event_id
            return (
              <td>
                <Link>
                <a onClick={() => setEventDetailsModal(!eventDetailsModal)}>
                  {item.event_name}
                </a>
                <EventDetailModal
                  setEventDetailsModal={setEventDetailsModal}
                  eventDetailsModal={eventDetailsModal}
                  eventId={eventId}
                />
                </Link>
              </td>
            );
          },
        }} */
      />
    </div>
  );
}

export default EventMissionDetail;
