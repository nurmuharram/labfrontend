import React, { useState } from "react";
import { GetAnniversaryMissionEvent } from "src/api/EventEnergyAPI/eventEnergyGetRequest";
import DataTable from "src/components/table/DataTable";
import MissionEventModal from "../components/modals/MissionEventModal";

function MissionEvent() {
  const [missionEvent, setMissionEvent] = useState([]);
  const [addItemModal, setAddItemModal] = useState(false);
  GetAnniversaryMissionEvent({ setMissionEvent });
  return (
    <div>
      <div className="pb-3">
        <button
          className="btn btn-primary btn-md"
          onClick={() => setAddItemModal(!addItemModal)}
        >
          +Add Mission Event
        </button>
        <MissionEventModal addItemModal={addItemModal} setAddItemModal={setAddItemModal} setMissionEvent={setMissionEvent}/>
      </div>
      <DataTable items={missionEvent} itemsPerPage={5} pagination />
    </div>
  );
}

export default MissionEvent;
