import React, { useState } from "react";
import { GetAnniversaryMissionEventReward } from "src/api/EventEnergyAPI/eventEnergyGetRequest";
import DataTable from "src/components/table/DataTable";
import MissionEventRewardModal from "../components/modals/MissionEventRewardModal";

function MissionReward() {
  const [missionReward, setMissionReward] = useState([]);
  const [addItemModal, setAddItemModal] = useState(false);
  GetAnniversaryMissionEventReward({ setMissionReward });
  const fields = [
    { key: "mission_reward_id", _style: { width: "15%" } },
    { key: "event_id" },
    { key: "event_name" },
    /* {
      key: "show_details",
      label: "",
      _style: { width: "14%" },
      sorter: false,
      filter: false,
    }, */
  ];
  return (
    <div>
      <div className="pb-3">
        <button
          className="btn btn-primary btn-md"
          onClick={() => setAddItemModal(!addItemModal)}
        >
          + Add Mission Reward
        </button>
        <MissionEventRewardModal
          addItemModal={addItemModal}
          setAddItemModal={setAddItemModal}
          setMissionReward={setMissionReward}
        />
      </div>
      <DataTable
        items={missionReward}
        itemsPerPage={5}
        pagination
        fields={fields}
      />
    </div>
  );
}

export default MissionReward;
