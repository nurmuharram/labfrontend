import React, { useState } from "react";
import { GetEventAnniversaryMission } from "src/api/EventEnergyAPI/eventEnergyGetRequest";
import DataTable from "src/components/table/DataTable";
import EventMissionListModal from "../components/modals/MissionListModal";

function MissionList() {
  const [missionList, setMissionList] = useState([]);
  const [addItemModal, setAddItemModal] = useState(false);
  GetEventAnniversaryMission({ setMissionList });
  return (
    <div>
      <div className="pb-3">
        <button
          className="btn btn-primary btn-md"
          onClick={() => setAddItemModal(!addItemModal)}
        >
          + Add Mission
        </button>
        <EventMissionListModal setAddItemModal={setAddItemModal} addItemModal={addItemModal} />
      </div>
      <DataTable items={missionList} itemsPerPage={5} pagination />
    </div>
  );
}

export default MissionList;
