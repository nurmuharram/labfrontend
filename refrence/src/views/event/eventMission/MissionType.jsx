import React, { useState } from "react";
import { GetAnniversaryMissionType } from "src/api/EventEnergyAPI/eventEnergyGetRequest";
import DataTable from "src/components/table/DataTable";
import EventMissionTypeModal from "../components/modals/MissionTypeModal";

function MissionType() {
  const [anniversaryMissionType, setAnniversaryMissionType] = useState([]);
  const [addItemModal, setAddItemModal] = useState(false);
  GetAnniversaryMissionType({ setAnniversaryMissionType });
  return (
    <div>
      <div className="pb-3">
        <button
          className="btn btn-primary btn-md"
          onClick={() => setAddItemModal(!addItemModal)}
        >
          + Add Mission Type
        </button>
        <EventMissionTypeModal
          setAddItemModal={setAddItemModal}
          addItemModal={addItemModal}
          setAnniversaryMissionType={setAnniversaryMissionType}
        />
      </div>
      <DataTable items={anniversaryMissionType} itemsPerPage={5} pagination />
    </div>
  );
}

export default MissionType;
