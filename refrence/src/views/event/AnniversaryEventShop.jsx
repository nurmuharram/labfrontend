import React, { useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import EventShopItemDetails from "./eventShop/EventShopItemDetails";
import EventShopItemList from "./eventShop/EventShopItemList";
import EventShopList from "./eventShop/EventShopList";

function AnniversaryEventShop() {
  const [key, setKey] = useState('EventShopList');
  return (
    <div>
      <br/>
      <Tabs
        id="controlled-tab"
        activeKey={key}
        onSelect={(k) => setKey(k)}
        className="mb-3 space-right2"
        variant="tabs"
        color="red"
      >
        <Tab eventKey="EventShopList" title="Event Shop List">
          <EventShopList />
        </Tab>
        <Tab eventKey="EventShopItemList" title="Shop Item List">
          <EventShopItemList />
        </Tab>
        <Tab eventKey="EventShopItemDetails" title="Event Shop Item Details">
          <EventShopItemDetails />
        </Tab>
      </Tabs>
    </div>
  );
}

export default AnniversaryEventShop;
