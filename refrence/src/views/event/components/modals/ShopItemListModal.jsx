import React, { useState } from "react";
import { AddAnniversaryShopItems } from "src/api/EventEnergyAPI/eventEnergyPostRequest";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from "src/components/modal";
import { ShopItemListForm } from "../foms/ShopItemListForm";

export const ShopItemListModal = ({ addItemModal, setAddItemModal, setItemList }) => {
  const [itemsForm, setItemsForm] = useState({
    itemType: 1,
    itemID: 1,
    amount: "",
    maxBuy: "",
  });
  const [arrayData, setArrayData] = useState([]);
  return (
    <div>
      <Modal
        show={addItemModal}
        onClose={() => setAddItemModal(!addItemModal)}
        color="primary"
        size="lg"
      >
        <ModalHeader closeButton>
          <ModalTitle>Add Shop Items</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <ShopItemListForm
            itemsForm={itemsForm}
            setItemsForm={setItemsForm}
            setArrayData={setArrayData}
            arrayData={arrayData}
          />
        </ModalBody>
        <ModalFooter>
          {" "}
          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => setAddItemModal(!addItemModal)}
          >
            Cancel
          </button>
          {arrayData.length > 0 ? (
            <AddAnniversaryShopItems
              setItemList={setItemList}
              setAddItemModal={setAddItemModal}
              arrayData={arrayData}
            />
          ) : (
            <button className="btn btn-primary disabled">+ Add</button>
          )}
        </ModalFooter>
      </Modal>
    </div>
  );
};
