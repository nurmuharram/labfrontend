import React, { useState } from "react";
import { ModalFooter } from "react-bootstrap";
import { AddEventAnniversaryMissionType } from "src/api/EventEnergyAPI/eventEnergyPostRequest";
import {
  Modal,
  ModalBody,
  ModalHeader,
  ModalTitle,
} from "src/components/modal";
import { AddMissionTypeForm } from "../foms/MissionTypeForm";

export default function EventMissionTypeModal({
  addItemModal,
  setAddItemModal,
  setAnniversaryMissionType,
}) {
  const [missionType, setMissionType] = useState("");
  return (
    <div>
      <Modal
        show={addItemModal}
        onClose={() => setAddItemModal(!addItemModal)}
        color="primary"
        size="md"
      >
        <ModalHeader closeButton>
          <ModalTitle>Add Mission Type</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <AddMissionTypeForm setMissionType={setMissionType} />
        </ModalBody>
        <ModalFooter>
          {" "}
          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => setAddItemModal(!addItemModal)}
          >
            Cancel
          </button>
          {missionType.length > 5 ? (
            <AddEventAnniversaryMissionType
              setAnniversaryMissionType={setAnniversaryMissionType}
              setAddItemModal={setAddItemModal}
              missionType={missionType}
            />
          ) : (
            <button className="btn btn-primary disabled">+ Add</button>
          )}
        </ModalFooter>
      </Modal>
    </div>
  );
}
