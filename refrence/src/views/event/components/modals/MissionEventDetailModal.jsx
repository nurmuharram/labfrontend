import React, { useState } from "react";
import { GetAnniversaryMissionEventDetailbyeventID } from "src/api/EventEnergyAPI/eventEnergyGetRequest";
import { AddEventAnniversaryMissionDetail } from "src/api/EventEnergyAPI/eventEnergyPostRequest";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from "src/components/modal";
import DataTable from "src/components/table/DataTable";
import { MissionDetailForm } from "../foms/MissionDetailForm";

export function MissionEventDetailModal({
  addItemModal,
  setAddItemModal,
  setMissionEventDetail,
}) {
  const [eventDetailForm, setEventDetailForm] = useState({
    eventID: 1,
    eventMissionId: 49,
  });
  return (
    <div>
      <Modal
        show={addItemModal}
        onClose={() => setAddItemModal(!addItemModal)}
        color="primary"
        size="md"
      >
        <ModalHeader closeButton>
          <ModalTitle>Add Mission</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <MissionDetailForm
            eventDetailForm={eventDetailForm}
            setEventDetailForm={setEventDetailForm}
          />
        </ModalBody>
        <ModalFooter>
          {" "}
          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => setAddItemModal(!addItemModal)}
          >
            Cancel
          </button>
          <AddEventAnniversaryMissionDetail
            setMissionEventDetail={setMissionEventDetail}
            setAddItemModal={setAddItemModal}
            eventDetailForm={eventDetailForm}
          />
        </ModalFooter>
      </Modal>
    </div>
  );
}

export function EventDetailModal({
  eventDetailsModal,
  setEventDetailsModal,
  eventId,
}) {
  const [eventDetails, setEventDetails] = useState([]);
  GetAnniversaryMissionEventDetailbyeventID({ setEventDetails, eventId });
  return (
    <div>
      <Modal
        show={eventDetailsModal}
        onClose={() => setEventDetailsModal(!eventDetailsModal)}
        color="primary"
        size="xl"
      >
        <ModalHeader closeButton>
          <ModalTitle>Event Details</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <DataTable items={eventDetails} />
        </ModalBody>
        <ModalFooter>
          {" "}
          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => setEventDetailsModal(!eventDetailsModal)}
          >
            Close
          </button>
        </ModalFooter>
      </Modal>
    </div>
  );
}
