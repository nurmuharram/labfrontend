import React, { useState } from "react";
import { AddEventAnniversaryShop } from "src/api/EventEnergyAPI/eventEnergyPostRequest";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from "src/components/modal";
import { ShopListForm } from "../foms/ShopListForm";

export const ShopListModal = ({ addItemModal, setAddItemModal, setShopList }) => {
  const todayDate = new Date().toISOString().split("T")[0];
  const [addEventShopForm, setAddEventShopForm] = useState({
    eventID: 1,
    miscID: 1,
    start_date: todayDate,
    start_time: "00:00",
    end_date: todayDate,
    end_time: "00:00",
  });
  return (
    <div>
      <Modal
        show={addItemModal}
        onClose={() => setAddItemModal(!addItemModal)}
        color="primary"
        size="md"
      >
        <ModalHeader closeButton>
          <ModalTitle>Add Event Shop Period</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <ShopListForm
            addEventShopForm={addEventShopForm}
            setAddEventShopForm={setAddEventShopForm}
          />
        </ModalBody>
        <ModalFooter>
          {" "}
          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => setAddItemModal(!addItemModal)}
          >
            Cancel
          </button>
          {addEventShopForm.eventID.length > 0 || 
          addEventShopForm.miscID.length > 0 ? (
            <AddEventAnniversaryShop
              setShopList={setShopList}
              setAddItemModal={setAddItemModal}
              addEventShopForm={addEventShopForm}
            />
          ) : (
            <button className="btn btn-primary disabled">+ Add</button>
          )}
        </ModalFooter>
      </Modal>
    </div>
  );
};
