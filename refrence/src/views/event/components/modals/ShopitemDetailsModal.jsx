import React, { useState } from 'react'
import { AddAnniversaryShopItemDetails } from 'src/api/EventEnergyAPI/eventEnergyPostRequest';
import {
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    ModalTitle,
} from "src/components/modal";
import { ShopItemDetailsForm } from '../foms/ShopItemDetailsForm';

export const ShopitemDetailsModal = ({addItemModal, setAddItemModal, setItemDetails}) => {
  const [itemDetailsForm, setItemDetailsForm] = useState({
    eventShopID: 1,
    shopItemId: 1,
    price: ''
  })

  const [arrayData, setArrayData] = useState([]);
  return (
    <div>
      <Modal
        show={addItemModal}
        onClose={() => setAddItemModal(!addItemModal)}
        color="primary"
        size="lg"
      >
        <ModalHeader closeButton>
          <ModalTitle>Add Item Details</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <ShopItemDetailsForm
            itemDetailsForm={itemDetailsForm}
            setItemDetailsForm={setItemDetailsForm}
            arrayData={arrayData}
            setArrayData={setArrayData}
          />
        </ModalBody>
        <ModalFooter>
          {" "}
          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => setAddItemModal(!addItemModal)}
          >
            Cancel
          </button>
          {arrayData.length > 0 ? (
            <AddAnniversaryShopItemDetails
              setItemDetails={setItemDetails}
              setAddItemModal={setAddItemModal}
              itemDetailsForm={itemDetailsForm}
              arrayData={arrayData}
            />
          ) : (
            <button className="btn btn-primary disabled">+ Add</button>
          )}
        </ModalFooter>
      </Modal>
    </div>
  );
}
