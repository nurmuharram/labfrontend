import React, { useState } from "react";
import { ModalFooter } from "react-bootstrap";
import { AddEventAnniversaryMission } from "src/api/EventEnergyAPI/eventEnergyPostRequest";
import {
  Modal,
  ModalBody,
  ModalHeader,
  ModalTitle,
} from "src/components/modal";
import { AddMissionForm } from "../foms/MissionListForm";

export default function EventMissionListModal({
  addItemModal,
  setAddItemModal,
  setMissionList,
}) {
  const [addMissionForm, setAddMissionForm] = useState({
    description: '',
    missionType: 1,
    target: '',
  });
  return (
    <div>
      <Modal
        show={addItemModal}
        onClose={() => setAddItemModal(!addItemModal)}
        color="primary"
        size="md"
      >
        <ModalHeader closeButton>
          <ModalTitle>Add Mission</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <AddMissionForm
            addMissionForm={addMissionForm}
            setAddMissionForm={setAddMissionForm}
          />
        </ModalBody>
        <ModalFooter>
          {" "}
          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => setAddItemModal(!addItemModal)}
          >
            Cancel
          </button>
          {addMissionForm.description.length > 4 &&
          addMissionForm.missionType.length > 0 &&
          addMissionForm.target > 0 ? (
            <AddEventAnniversaryMission
              setMissionList={setMissionList}
              setAddItemModal={setAddItemModal}
              addMissionForm={addMissionForm}
            />
          ) : (
            <button className="btn btn-primary disabled">+ Add</button>
          )}
        </ModalFooter>
      </Modal>
    </div>
  );
}
