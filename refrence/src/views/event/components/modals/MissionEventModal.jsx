import React, { useState } from "react";
import { AddEventAnniversaryMissionEvent } from "src/api/EventEnergyAPI/eventEnergyPostRequest";
import {
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    ModalTitle,
  } from "src/components/modal";
import { MissionEventForm } from "../foms/MissionEventForm";

function MissionEventModal({addItemModal, setAddItemModal, setMissionEvent}) {
    const todayDate = new Date().toISOString().split('T')[0];
    const [missionEventForm, setMissionEventForm] = useState({
        eventID: '',
        start_date: todayDate,
        start_time: '00:00',
        end_date: todayDate,
        end_time: '00:00',
    })

  return (
    <div>
      <Modal
        show={addItemModal}
        onClose={() => setAddItemModal(!addItemModal)}
        color="primary"
        size="md"
      >
        <ModalHeader closeButton>
          <ModalTitle>Add Mission</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <MissionEventForm
            missionEventForm={missionEventForm}
            setMissionEventForm={setMissionEventForm}
          />
        </ModalBody>
        <ModalFooter>
          {" "}
          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => setAddItemModal(!addItemModal)}
          >
            Cancel
          </button>
          {missionEventForm.eventID.length > 0 ? (
            <AddEventAnniversaryMissionEvent
              setMissionEvent={setMissionEvent}
              setAddItemModal={setAddItemModal}
              missionEventForm={missionEventForm}
            />
          ) : (
            <button className="btn btn-primary disabled">+ Add</button>
          )}
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default MissionEventModal;
