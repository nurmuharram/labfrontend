import React, { useState } from "react";
import { AddEventMissionRewards } from "src/api/EventEnergyAPI/eventEnergyPostRequest";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from "src/components/modal";
import { AddMissionRewardForm } from "../foms/MissionRewardForm";

function MissionEventRewardModal({addItemModal, setAddItemModal, setMissionReward}) {
    const [rewardForm, setRewardForm] = useState({
        missionRewardID: 0,
        missionID: 0,
        itemType: 1,
        itemID: 1,
        amount: ''
    })
    const [arrayData, setArrayData] = useState([])
  return (
    <div>
      {" "}
      <Modal
        show={addItemModal}
        onClose={() => setAddItemModal(!addItemModal)}
        color="primary"
        size="lg"
      >
        <ModalHeader closeButton>
          <ModalTitle>Add Mission Reward</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <AddMissionRewardForm
            rewardForm={rewardForm}
            setRewardForm={setRewardForm}
            arrayData={arrayData}
            setArrayData={setArrayData}
          />
        </ModalBody>
        <ModalFooter>
          {" "}
          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => setAddItemModal(!addItemModal)}
          >
            Cancel
          </button>
          {arrayData.length > 0 ? (
            <AddEventMissionRewards
              setMissionReward={setMissionReward}
              setAddItemModal={setAddItemModal}
              arrayData={arrayData}
              rewardForm={rewardForm}
            />
          ) : (
            <button className="btn btn-primary disabled">+ Add</button>
          )}
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default MissionEventRewardModal;
