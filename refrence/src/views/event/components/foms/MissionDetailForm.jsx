import { useState } from "react";
import { Form } from "react-bootstrap";
import { GetAnniversaryEventMission, GetEventList } from "src/api/EventEnergyAPI/eventEnergyGetRequest";

export const MissionDetailForm = ({setEventDetailForm, eventDetailForm}) => {
    const [eventList, setEventList] = useState([])
    const [missionEvent, setMissionEvent] = useState([])
    GetEventList({setEventList})
    GetAnniversaryEventMission({setMissionEvent})
    const handleChange = (e) => {
        const { name, value } = e.target;
        setEventDetailForm({ ...eventDetailForm, [name]: value });
      };
  return (
    <Form>
      <Form.Group className="mb-3" controlId="missiontype">
        <div className="row">
          <div className="col-md-4">
            <Form.Label>
              <b>Event ID</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-6">
            <select name="eventID" onChange={handleChange}>
              {eventList.map((type) => (
                <option value={type.event_id}>
                  {type.event_id} - {type.event_name}
                </option>
              ))}
            </select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="missiontype">
        <div className="row">
          <div className="col-md-4">
            <Form.Label>
              <b>Event Mission ID</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-6">
            <select name="eventMissionId" onChange={handleChange}>
              {missionEvent.map((type) => (
                <option value={type.event_mission_id}>
                  {type.event_mission_id} - {type.event_name}
                </option>
              ))}
            </select>
          </div>
        </div>
      </Form.Group>
    </Form>
  );
};
