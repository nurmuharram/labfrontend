import React, { useState } from "react";
import { Form } from "react-bootstrap";
import { GetAnniversaryShopItemList, GetAnniversaryShopList } from "src/api/EventEnergyAPI/eventEnergyGetRequest";
import { AnniversaryShopItemsDetails } from "src/services/evenEnergy.services/EventAnniversary.function";

export const ShopItemDetailsForm = ({itemDetailsForm, setItemDetailsForm, arrayData,
  setArrayData,}) => {
  const [shopList, setShopList] = useState([])
  const [itemList, setItemList] = useState([])
  GetAnniversaryShopList({ setShopList });
  GetAnniversaryShopItemList({ setItemList });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setItemDetailsForm({ ...itemDetailsForm, [name]: value });
  };
  return (
    <div>
      <Form>
        <Form.Group className="mb-3" controlId="eventshopid">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Event Shop ID</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <select name="eventShopID" onChange={handleChange}>
                {shopList.map((type) => (
                  <option value={type.event_shop_id}>
                    {type.event_shop_id} - {type.event_name} ({type.misc_name})
                  </option>
                ))}
              </select>
            </div>
          </div>
        </Form.Group>
        <hr/>
        <Form.Group className="mb-3" controlId="shopItemId">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Shop Item</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <select name="shopItemId" onChange={handleChange}>
                {itemList.map((type) => (
                  <option value={type.shop_item_id}>
                   {type.shop_item_id}. {type.item_type_name} - {type.item_name}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="description">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Price</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <Form.Control
                type="text"
                name="price"
                onKeyPress={(event) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
                value={itemDetailsForm.price}
                placeholder="Set Item Price..."
                onChange={handleChange}
              />
            </div>
          </div>
        </Form.Group>
        <div className="col-md-9 pb-3" align="right">
          {itemDetailsForm.shopItemId.length > 0 && itemDetailsForm.price.length > 0 ? (
            <button
              className="btn btn-info btn-sm"
              onClick={(e) => {
                setArrayData([
                  ...arrayData,
                  {
                    shop_item_id: parseInt(itemDetailsForm.shopItemId),
                    price: parseInt(itemDetailsForm.price),
                  },
                ]);
                e.preventDefault();
              }}
            >
              <b>+ Add Item</b>
            </button>
          ) : (
            <button className="btn btn-info btn-sm" disabled>
              <b>+ Add Item</b>
            </button>
          )}
        </div>
        <Form.Group className="mb-3" controlId="missiontype">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Items & Price</b>
              </Form.Label>
            </div>
            :&nbsp;&nbsp;&nbsp;
            <div className="row col-md-7">
              {AnniversaryShopItemsDetails({ arrayData, setArrayData })}
            </div>
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};
