import { useState } from "react";
import { Form } from "react-bootstrap";
import { GetAnniversaryMissionType, GetEventList } from "src/api/EventEnergyAPI/eventEnergyGetRequest";
import { MissionEventReward } from "src/services/evenEnergy.services/EventAnniversary.function";
import { EventEnergyItemFunction } from "src/services/evenEnergy.services/EventEnergy.Function";

export const AddMissionRewardForm = ({
  rewardForm,
  setRewardForm,
  arrayData,
  setArrayData,
}) => {
  const [eventList, setEventList] = useState([]);
  const [anniversaryMissionType, setAnniversaryMissionType] = useState([]);
  GetAnniversaryMissionType({ setAnniversaryMissionType });
  GetEventList({ setEventList });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setRewardForm({ ...rewardForm, [name]: value });
  };
  const itemtype = rewardForm.itemType;

  return (
    <Form>
      <Form.Group className="mb-3" controlId="missiontype">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Mission Reward Id</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-6">
            <select name="missionRewardID" onChange={handleChange}>
              {eventList.map((type) => (
                <option value={type.event_id}>
                  {type.event_id} - {type.event_name}
                </option>
              ))}
            </select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="missiontype">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Mission Id</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-6">
            <select name="missionID" onChange={handleChange}>
              {anniversaryMissionType.map((type) => (
                <option value={type.mission_type_id}>
                  {type.mission_type_id} - {type.description}
                </option>
              ))}
            </select>
          </div>
        </div>
      </Form.Group>
      <hr />
      <Form.Group className="mb-3" controlId="missiontype">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Item Type</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-6">
            <select name="itemType" onChange={handleChange}>
              <option value={""}>Choose Item Type</option>
              <option value={1}>Currency</option>
              <option value={6}>Box</option>
              <option value={5}>Items</option>
              <option value={2}>Ksatriya</option>
              <option value={3}>KSA Skins</option>
              <option value={4}>Rune</option>
              <option value={11}>Frame</option>
              <option value={12}>Avatar</option>
            </select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="missiontype">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Item Name</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-6">
            <select name="itemID" onChange={handleChange}>
              {EventEnergyItemFunction({ itemtype })}
            </select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="missiontype">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Amount</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-4">
            <Form.Control
              type="number"
              name="amount"
              onKeyPress={(event) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              value={rewardForm.amount}
              placeholder="Set Amount..."
              onChange={handleChange}
            />
          </div>
        </div>
      </Form.Group>
      <div className="col-md-7 pb-3" align="right">
        <button
          className="btn btn-info btn-sm"
          onClick={(e) => {
            setArrayData([
              ...arrayData,
              {
                mission_id: parseInt(rewardForm.missionRewardID),
                item_type: parseInt(rewardForm.itemType),
                item_id: parseInt(rewardForm.itemID),
                amount: parseInt(rewardForm.amount),
              },
            ]);
            e.preventDefault();
          }}
        >
          <b>+ Add Item</b>
        </button>
      </div>
      <Form.Group className="mb-3" controlId="missiontype">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Item Chosen</b>
            </Form.Label>
          </div>
          :&nbsp;&nbsp;&nbsp;
          <div className="row col-md-7">
            {MissionEventReward({ arrayData, setArrayData })}
          </div>
        </div>
      </Form.Group>
    </Form>
  );
};
