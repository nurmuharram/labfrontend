import { useState } from "react";
import { Form } from "react-bootstrap";
import { GetAnniversaryMissionType } from "src/api/EventEnergyAPI/eventEnergyGetRequest";

export const AddMissionForm = ({ setAddMissionForm, addMissionForm }) => {
  const [missionType, setAnniversaryMissionType] = useState([]);
  const handleChange = (e) => {
    const { name, value } = e.target;
    setAddMissionForm({ ...addMissionForm, [name]: value });
  };
  GetAnniversaryMissionType({ setAnniversaryMissionType });
  return (
    <div>
      <Form>
        <Form.Group className="mb-3" controlId="description">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Description</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <Form.Control
                type="text"
                name="description"
                value={addMissionForm.description}
                placeholder="Set Mission Name..."
                onChange={handleChange}
              />
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="missiontype">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Mission Type</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <select name="missionType" onChange={handleChange}>
                {missionType.map((type) => (
                  <option value={type.mission_type_id}>
                    {type.mission_type_id} - {type.description}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="target">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Target</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <Form.Control
                type="number"
                name="target"
                onKeyPress={(event) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
                value={addMissionForm.target}
                placeholder="Set Mission Target..."
                onChange={handleChange}
              />
            </div>
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};
