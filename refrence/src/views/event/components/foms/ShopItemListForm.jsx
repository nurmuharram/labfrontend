import React from "react";
import { Form } from "react-bootstrap";
import { AnniversaryShopItems } from "src/services/evenEnergy.services/EventAnniversary.function";
import { EventEnergyItemFunction } from "src/services/evenEnergy.services/EventEnergy.Function";

export const ShopItemListForm = ({
  setItemsForm,
  itemsForm,
  arrayData,
  setArrayData,
}) => {
  const handleChange = (e) => {
    const { name, value } = e.target;
    setItemsForm({ ...itemsForm, [name]: value });
  };
  const itemtype = itemsForm.itemType;
  return (
    <div>
      <Form>
        <Form.Group className="mb-3" controlId="itemtype">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Item Type</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <select name="itemType" onChange={handleChange}>
                <option value={""}>Choose Item Type</option>
                <option value={1}>Currency</option>
                <option value={6}>Box</option>
                <option value={5}>Items</option>
                <option value={2}>Ksatriya</option>
                <option value={3}>KSA Skins</option>
                <option value={4}>Rune</option>
                <option value={11}>Frame</option>
                <option value={12}>Avatar</option>
              </select>
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="itemid">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Item Name</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <select name="itemID" onChange={handleChange}>
                {EventEnergyItemFunction({ itemtype })}
              </select>
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="amount">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Amount</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <Form.Control
                type="number"
                name="amount"
                onKeyPress={(event) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
                value={itemsForm.amount}
                placeholder="Set Amount..."
                onChange={handleChange}
              />
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="maxbuy">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Max Buy</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <Form.Control
                type="number"
                name="maxBuy"
                onKeyPress={(event) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
                value={itemsForm.maxBuy}
                placeholder="Set Max Buy..."
                onChange={handleChange}
              />
            </div>
          </div>
        </Form.Group>
        <div className="col-md-9 pb-3" align="right">
          {itemsForm.amount.length > 0 && itemsForm.maxBuy.length > 0 ? (
            <button
              className="btn btn-info btn-sm"
              onClick={(e) => {
                setArrayData([
                  ...arrayData,
                  {
                    item_type: parseInt(itemsForm.itemType),
                    item_id: parseInt(itemsForm.itemID),
                    amount: parseInt(itemsForm.amount),
                    max_buy: parseInt(itemsForm.maxBuy),
                  },
                ]);
                e.preventDefault();
              }}
            >
              <b>+ Add Item</b>
            </button>
          ) : (
            <button className="btn btn-info btn-sm" disabled>
              <b>+ Add Item</b>
            </button>
          )}
        </div>
        <Form.Group className="mb-3" controlId="missiontype">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Item Chosen</b>
              </Form.Label>
            </div>
            :&nbsp;&nbsp;&nbsp;
            <div className="row col-md-7">
              {AnniversaryShopItems({ arrayData, setArrayData })}
            </div>
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};
