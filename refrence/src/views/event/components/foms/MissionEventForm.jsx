import { useState } from "react";
import { Form } from "react-bootstrap";
import { GetEventList } from "src/api/EventEnergyAPI/eventEnergyGetRequest";

export const MissionEventForm = ({setMissionEventForm, missionEventForm}) => {
  const [eventList, setEventList] = useState([]);
  const handleChange = (e) => {
    const { name, value } = e.target;
    setMissionEventForm({ ...missionEventForm, [name]: value });
  };
  GetEventList({ setEventList });
  return (
    <div>
      <Form>
        <Form.Group className="mb-3" controlId="missiontype">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Event ID</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <select name="eventID" onChange={handleChange}>
                {eventList.map((type) => (
                  <option value={type.event_id}>
                    {type.event_id} - {type.event_name}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="target">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Start Date</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-4">
              <Form.Control
                type="date"
                name="start_date"
                value={missionEventForm.start_date}
                onKeyDown={(e) => e.preventDefault()}
                onChange={handleChange}
              />
            </div>
            <div className="col-md-4">
              <Form.Control
                type="time"
                name="start_time"
                value={missionEventForm.start_time}
                onChange={handleChange}
              />
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="target">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>End Date</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-4">
              <Form.Control
                type="date"
                name="end_date"
                value={missionEventForm.end_date}
                onKeyDown={(e) => e.preventDefault()}
                onChange={handleChange}
              />
            </div>
            <div className="col-md-4">
              <Form.Control
                type="time"
                name="end_time"
                value={missionEventForm.end_time}
                onChange={handleChange}
              />
            </div>
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};
