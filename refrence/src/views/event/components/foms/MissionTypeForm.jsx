import { Form } from "react-bootstrap";

export const AddMissionTypeForm = ({setMissionType}) => {
  return (
    <div>
      <Form>
        <Form.Group className="mb-3" controlId="missiontype">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Mission Type</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <Form.Control
                type="text"
                placeholder="Describe a Mission Type..."
                onChange={(e) => setMissionType(e.target.value)}
              />
            </div>
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};
