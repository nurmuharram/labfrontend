import React, { useEffect, useState } from 'react'
import { Form } from "react-bootstrap";
import { useDispatch, useSelector } from 'react-redux';
import { GetEventList } from 'src/api/EventEnergyAPI/eventEnergyGetRequest';
import { getItems } from 'src/redux/action/mailAction/getItemsAction';

export const ShopListForm = ({addEventShopForm, setAddEventShopForm}) => {
  const [eventList, setEventList] = useState([])
  GetEventList({ setEventList });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setAddEventShopForm({ ...addEventShopForm, [name]: value });
  };
  const dispatch = useDispatch()
  const items = useSelector((state) => state.items.items);
    useEffect(() => {
        dispatch(getItems());
    }, []);
  return (
    <div>
      <Form>
        <Form.Group className="mb-3" controlId="eventid">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Event ID</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <select name="eventID" onChange={handleChange}>
                {eventList.map((type) => (
                  <option value={type.event_id}>
                    {type.event_id} - {type.event_name}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="miscid">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Misc id</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <select name="miscID" onChange={handleChange}>
                {items.map((type) => (
                  <option value={type.misc_id}>
                    {type.misc_id} - {type.misc_name}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="startdate">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Start Date</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-4">
              <Form.Control
                type="date"
                name="start_date"
                value={addEventShopForm.start_date}
                onKeyDown={(e) => e.preventDefault()}
                onChange={handleChange}
              />
            </div>
            <div className="col-md-4">
              <Form.Control
                type="time"
                name="start_time"
                value={addEventShopForm.start_time}
                onChange={handleChange}
              />
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="enddate">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>End Date</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-4">
              <Form.Control
                type="date"
                name="end_date"
                value={addEventShopForm.end_date}
                onKeyDown={(e) => e.preventDefault()}
                onChange={handleChange}
              />
            </div>
            <div className="col-md-4">
              <Form.Control
                type="time"
                name="end_time"
                value={addEventShopForm.end_time}
                onChange={handleChange}
              />
            </div>
          </div>
        </Form.Group>
      </Form>
    </div>
  );
}
