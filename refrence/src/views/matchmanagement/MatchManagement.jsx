import React from 'react'
import MatchList from './MatchList'
function MatchManagement() {
  return (
    <div>
      <div className="col" align='center'>
        <div className="card" style={{ background: '#9bf54c' }}>
          <h4><b>Match Management</b></h4>
          <h6>
            <i>
              Manage Live Matches
            </i>
          </h6>
        </div>
      </div>
      <div className="container">
        <div className="col mb-4">
          <div className="card">
            <center>
            </center>
            <div className="card-body">
              <MatchList />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default MatchManagement
