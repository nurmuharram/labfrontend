import React, { useState, useRef } from "react";
import { MatchDataTable } from "./components/matchDataTable/MatchDataTable";
import { GetMatches, refreshMatches } from "src/api/matchManagementApi/matchManagementGetRequest";

const MatchList = () => {
  const [matches, setMatches] = useState([]);
  const [_matches, set_matches] = useState({
    room_id: "",
    room_name: "",
    karma: "",
    playing_time: "",
  });

  const [pageChange, setChangelist] = useState(0);

  const page = useRef();
  page.current = pageChange;

  const previousPage = () => {
    setChangelist(pageChange - 10);
    setTimeout(refreshMatches({setMatches, page}), 100);
  };
  const nextPage = () => {
    setChangelist(pageChange + 10);
    setTimeout(refreshMatches({setMatches, page}), 100);
  };

  GetMatches({setMatches, page})  

  const hidePreviousButton = () => {
    if (pageChange === 0) {
      return <></>;
    } else {
      return (
        <button className="btn btn-primary btn-sm" onClick={previousPage}>
          Previous Lists
        </button>
      );
    }
  };

  const hideNextButton = () => {
    if (Object.keys(matches && matches).length < 10) {
      return <></>;
    } else if (Object.keys(matches && matches).length >= 10) {
      return (
        <button className="btn btn-primary btn-sm" onClick={nextPage}>
          Next Lists
        </button>
      );
    }
  };

  return (
    <>
      <div className="container">
        <div className="col mb-4">
          <div className="card card-accent-primary">
            <div className="card-body">
              <div className="row">
                <div className="col" align="center">
                  <b>
                    <i>Room Lists</i>
                  </b>
                </div>
              </div>
              <br />
              {matches === null ? (
                <>
                  <div className="row">
                    <div className="col" align="center">
                      <h4>There is no data here... Yet.</h4>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <MatchDataTable matches={matches} />
                  <div className="row">
                    <div className="col" align="left">{hidePreviousButton()}</div>
                    <div className="col" align="right">{hideNextButton()}</div>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MatchList;
