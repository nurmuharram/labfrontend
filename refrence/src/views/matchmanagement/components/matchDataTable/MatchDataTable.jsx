import React from "react";
import { KillRoom } from "src/api/matchManagementApi/matchManagementGetRequest";
import DataTable from "src/components/table/DataTable";

export const MatchDataTable = ({ matches }) => {
  return (
    <div>
      <DataTable
        items={matches}
        fields={[
          {
            key: "room_id",
            _classes: "font-weight-bold",
            _style: { width: "9%" },
            filter: false,
          },
          {
            key: "room_name",
            filter: false,
          },
          {
            key: "match_id",
            filter: false,
          },
          {
            key: "game_mode",
            filter: false,
          },
          {
            key: "server_ip",
            filter: false,
          },
          {
            key: "server_port",
            filter: false,
          },
          {
            key: "start_time",
            filter: false,
          },
          {
            key: "Action",
            filter: false,
          },
        ]}
        clickableRows
        hover
        striped
        scopedSlots={{
          Action: (item) => {
            return <KillRoom item={item} />;
          },
          start_time: (item, index) => {
            const date = new Date(item.start_time && item.start_time);
            return <td>{date.toString()}</td>;
          },
        }}
      />
    </div>
  );
};
