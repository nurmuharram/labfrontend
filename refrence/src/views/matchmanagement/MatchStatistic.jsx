import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import {
    CCard,
    CCardBody,
    CContainer,
    CRow,
    CCol,
    CImg,
    CTabPane,
    CNav,
    CNavItem,
    CNavLink,
    CTabContent,
    CTabs,
    CDataTable,
    CBadge,
    CLink,
    CButton
} from '@coreui/react'
import axios from 'axios'

function MatchStatistic({ match }) {

    const history = useHistory();


    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getMatchData?room_id=${match.params.room_id}`, config).then(res => {
            const playerData = res.data;
            setData(playerData);
        });
    }, [match.params.room_id]);

    const [data, setData] = useState([])



    if (typeof (data[0]) === 'undefined') {
        return <></>
    }


    const win = data[0].win;

    const duration = data[0].game_duration / 60
    const durationresult = duration.toFixed(0)

    const redteam = () => {
        if (win === 1) {
            return "Win!"
        } else {
            return "Lose!"
        }
    };

    const blueteam = () => {
        if (win === 2) {
            return "Win!"
        } else {
            return "Lose!"
        }
    };

    const emptyobject = {
        game_duration: 0,
        game_mode: 0,
        match_id: 0,
        assist: 0,
        avatar_icon: 0,
        close_call_kill: 0,
        creep_kill: 0,
        damage_dealt: 0,
        damage_taken: 0,
        death: 0,
        double_kill: 0,
        first_blood: 0,
        frame: 0,
        game_duration: 0,
        game_mode: 0,
        gold: 0,
        highest_kill_streak: 0,
        is_afk: 0,
        is_leave: 0,
        kill: 0,
        ksatriya_damage_dealt: 0,
        ksatriya_id: 999,
        level: 0,
        mantra_id: 0,
        match_id: 0,
        minion_kill: 0,
        mvp: 0,
        mvp_badge: 0,
        penta_kill: 0,
        quadra_kill: 0,
        raksasha_controlled: 0,
        raksasha_kill: 0,
        raksasha_kill_assist: 0,
        room_id: 0,
        rune_activated: 0,
        slot_index: 0,
        start_time: null,
        tower_destroyed: 0,
        triple_kill: 0,
        user_id: 0,
        user_name: "--empty slot--",
        ward_placed: 0,
        win: 0,
        yaksha_kill: 0,
        yaksha_kill_assist: 0,
    }


    const slotIndex0 = data.find(obj => {
        return obj.slot_index === 0
    })

    const slotIndex1 = data.find(obj => {
        return obj.slot_index === 1
    })

    const slotIndex2 = data.find(obj => {
        return obj.slot_index === 2
    })

    const slotIndex3 = data.find(obj => {
        return obj.slot_index === 3
    })

    const slotIndex4 = data.find(obj => {
        return obj.slot_index === 4
    })



    const firstTeam = [slotIndex0 === undefined ? emptyobject :
        slotIndex0, slotIndex1 === undefined ? emptyobject :
        slotIndex1, slotIndex2 === undefined ? emptyobject :
        slotIndex2, slotIndex3 === undefined ? emptyobject :
        slotIndex3, slotIndex4 === undefined ? emptyobject :
        slotIndex4]

    const slotIndex5 = data.find(obj => {
        return obj.slot_index === 5
    })

    const slotIndex6 = data.find(obj => {
        return obj.slot_index === 6
    })

    const slotIndex7 = data.find(obj => {
        return obj.slot_index === 7
    })

    const slotIndex8 = data.find(obj => {
        return obj.slot_index === 8
    })

    const slotIndex9 = data.find(obj => {
        return obj.slot_index === 9
    })

    const secondTeam = [slotIndex5 === undefined ? emptyobject :
        slotIndex5, slotIndex6 === undefined ? emptyobject :
        slotIndex6, slotIndex7 === undefined ? emptyobject :
        slotIndex7, slotIndex8 === undefined ? emptyobject :
        slotIndex8, slotIndex9 === undefined ? emptyobject :
        slotIndex9]

    const firstTeamLength = (slotIndex0 === undefined ? 0 : 1) + (slotIndex1 === undefined ? 0 : 1) + (slotIndex2 === undefined ? 0 : 1) + (slotIndex3 === undefined ? 0 : 1) + (slotIndex4 === undefined ? 0 : 1)
    const secondTeamLength = (slotIndex5 === undefined ? 0 : 1) + (slotIndex6 === undefined ? 0 : 1) + (slotIndex7 === undefined ? 0 : 1) + (slotIndex8 === undefined ? 0 : 1) + (slotIndex9 === undefined ? 0 : 1)

    const pointsA = firstTeam.reduce((a, v) => a = a + v.kill, 0);
    const pointsB = secondTeam.reduce((a, v) => a = a + v.kill, 0);

    const mvpAverageA = (firstTeam.reduce((a, v) => a = a + v.mvp, 0)) / firstTeamLength;
    const mvpAverageB = (secondTeam.reduce((a, v) => a = a + v.mvp, 0)) / secondTeamLength;

    const kdaAveragekillA = (firstTeam.reduce((a, v) => a = a + v.kill, 0)) / firstTeamLength;
    const kdaAveragekillB = (secondTeam.reduce((a, v) => a = a + v.kill, 0)) / secondTeamLength;
    const kdaAveragedeathA = (firstTeam.reduce((a, v) => a = a + v.death, 0)) / firstTeamLength;
    const kdaAveragedeathB = (secondTeam.reduce((a, v) => a = a + v.death, 0)) / secondTeamLength;
    const kdaAverageassistA = (firstTeam.reduce((a, v) => a = a + v.assist, 0)) / firstTeamLength;
    const kdaAverageassistB = (secondTeam.reduce((a, v) => a = a + v.assist, 0)) / secondTeamLength;

    const totaldmgAverageA = (firstTeam.reduce((a, v) => a = a + v.damage_dealt, 0)) / firstTeamLength;
    const totaldmgAverageB = (secondTeam.reduce((a, v) => a = a + v.damage_dealt, 0)) / secondTeamLength;

    const goldAverageA = (firstTeam.reduce((a, v) => a = a + v.gold, 0)) / firstTeamLength;
    const goldAverageB = (secondTeam.reduce((a, v) => a = a + v.gold, 0)) / secondTeamLength;

    const wardsAverageA = (firstTeam.reduce((a, v) => a = a + v.ward_placed, 0)) / firstTeamLength;
    const wardsAverageB = (secondTeam.reduce((a, v) => a = a + v.ward_placed, 0)) / secondTeamLength;

    const csAverageminionkillA = (firstTeam.reduce((a, v) => a = a + v.minion_kill, 0));
    const csAverageminionkillB = (secondTeam.reduce((a, v) => a = a + v.minion_kill, 0));
    const csAveragecreepkillA = (firstTeam.reduce((a, v) => a = a + v.creep_kill, 0));
    const csAveragecreepkillB = (secondTeam.reduce((a, v) => a = a + v.creep_kill, 0));
    const csAverageA = (csAverageminionkillA + csAveragecreepkillA) / firstTeamLength;
    const csAverageB = (csAverageminionkillB + csAveragecreepkillB) / secondTeamLength;

    const minionkillAverageA = csAverageminionkillA / firstTeamLength;
    const minionkillAverageB = csAverageminionkillB / secondTeamLength;

    const creepAverageA = csAveragecreepkillA / firstTeamLength;
    const creepAverageB = csAveragecreepkillB / secondTeamLength;

    const damageAverageA = (firstTeam.reduce((a, v) => a = a + v.damage_taken, 0)) / firstTeamLength;
    const damageAverageB = (secondTeam.reduce((a, v) => a = a + v.damage_taken, 0)) / secondTeamLength;

    const raksashaAveragekillA = (firstTeam.reduce((a, v) => a = a + v.raksasha_kill, 0)) / firstTeamLength;
    const raksashaAveragekillB = (secondTeam.reduce((a, v) => a = a + v.raksasha_kill, 0)) / secondTeamLength;
    const raksashaAverageassistA = (firstTeam.reduce((a, v) => a = a + v.raksasha_kill_assist, 0)) / firstTeamLength;
    const raksashaAverageassistB = (secondTeam.reduce((a, v) => a = a + v.raksasha_kill_assist, 0)) / secondTeamLength;

    const yakshaAveragekillA = (firstTeam.reduce((a, v) => a = a + v.yaksha_kill, 0)) / firstTeamLength;
    const yakshaAveragekillB = (secondTeam.reduce((a, v) => a = a + v.yaksha_kill, 0)) / secondTeamLength;
    const yakshaAverageassistA = (firstTeam.reduce((a, v) => a = a + v.yaksha_kill_assist, 0)) / firstTeamLength;
    const yakshaAverageassistB = (secondTeam.reduce((a, v) => a = a + v.yaksha_kill_assist, 0)) / secondTeamLength;

    const towerdestroyAverageA = (firstTeam.reduce((a, v) => a = a + v.tower_destroyed, 0)) / firstTeamLength;
    const towerdestroyAverageB = (secondTeam.reduce((a, v) => a = a + v.tower_destroyed, 0)) / secondTeamLength;

    const gameMode = () => {
        if (data[0].game_mode === 1) {
            return 'Classic'
        } else if (data[0].game_mode === 2) {
            return 'Ranked (Blind Pick)'
        } else if (data[0].game_mode === 3) {
            return 'Ranked (Draft Pick)'
        } else {
            return '-'
        }
    };



    return (
        <div>
            <CCol align='center'>
                <CCard style={{ background: '#9bf54c' }}>
                    <h4><b>Match Statistic</b></h4>
                    <h6>
                        <i>
                            Match Details
                        </i>
                    </h6>
                </CCard>
            </CCol>
            <br />
            <CContainer fluid>
                <CButton color='info' variant='ghost' size='large' onClick={() => history.goBack()}>&#8592; Back to Previous Page </CButton>&nbsp;
                <hr />
                <CCard>
                    <CCardBody>
                        <CRow>
                            <CCol align='left'>
                                {gameMode()}
                                <br />
                                Game Duration: <i>{durationresult}mins</i>
                                <br />
                                <br />
                                <br />
                                <CRow md='6'>
                                    {slotIndex0 === undefined ? <></> : <CCol md='3' align='center'>
                                        <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${slotIndex0.ksatriya_id}.png`} fluid shape="rounded" width='110' align='left' />
                                        <small>{slotIndex0.user_name}</small>
                                    </CCol>}
                                    {slotIndex1 === undefined ? <></> : <CCol md='3' align='center'>
                                        <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${slotIndex1.ksatriya_id}.png`} fluid shape="rounded" width='110' align='left' />
                                        <small>{slotIndex1.user_name}</small>
                                    </CCol>}
                                    {slotIndex2 === undefined ? <></> : <CCol md='3' align='center'>
                                        <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${slotIndex2.ksatriya_id}.png`} fluid shape="rounded" width='110' align='left' />
                                        <small>{slotIndex2.user_name}</small>
                                    </CCol>}
                                    {slotIndex3 === undefined ? <></> : <CCol md='3' align='center'>
                                        <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${slotIndex3.ksatriya_id}.png`} fluid shape="rounded" width='110' align='left' />
                                        <small>{slotIndex3.user_name}</small>
                                    </CCol>}
                                    {slotIndex4 === undefined ? <></> : <CCol md='3' align='center'>
                                        <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${slotIndex4.ksatriya_id}.png`} fluid shape="rounded" width='110' align='left' />
                                        <small>{slotIndex4.user_name}</small>
                                    </CCol>}
                                </CRow>
                            </CCol>
                            <CCol align='center'>
                                <h5><span style={{ color: 'red' }}>Red</span> Team : {pointsA}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style={{ color: 'blue' }}>Blue</span> Team : {pointsB}</h5>
                                <br />
                                <span className='display-2'><b>VS</b></span>
                            </CCol>
                            <CCol align='right'>
                                Match ID: {data[0].match_id}
                                <br />
                                Start Time: {data[0].start_time}
                                <br />
                                <br />
                                <br />
                                <CRow>
                                    {slotIndex5 === undefined ? <></> : <CCol md='3' align='center'>
                                        <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${slotIndex5.ksatriya_id}.png`} fluid shape="rounded" width='110' align='left' />
                                        <small>{slotIndex5.user_name}</small>
                                    </CCol>}
                                    {slotIndex6 === undefined ? <></> : <CCol md='3' align='center'>
                                        <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${slotIndex6.ksatriya_id}.png`} fluid shape="rounded" width='110' align='left' />
                                        <small>{slotIndex6.user_name}</small>
                                    </CCol>}
                                    {slotIndex7 === undefined ? <></> : <CCol md='3' align='center'>
                                        <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${slotIndex7.ksatriya_id}.png`} fluid shape="rounded" width='110' align='left' />
                                        <small>{slotIndex7.user_name}</small>
                                    </CCol>}
                                    {slotIndex8 === undefined ? <></> : <CCol md='3' align='center'>
                                        <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${slotIndex8.ksatriya_id}.png`} fluid shape="rounded" width='110' align='left' />
                                        <small>{slotIndex8.user_name}</small>
                                    </CCol>}
                                    {slotIndex9 === undefined ? <></> : <CCol md='3' align='center'>
                                        <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${slotIndex9.ksatriya_id}.png`} fluid shape="rounded" width='110' align='left' />
                                        <small>{slotIndex9.user_name}</small>
                                    </CCol>}
                                </CRow>
                            </CCol>
                        </CRow>
                    </CCardBody>
                </CCard>
            </CContainer>

            <CContainer fluid>
                <CCard>
                    <CCardBody>
                        <CTabs activeTab='overview'>
                            <CNav variant="tabs">
                                <CNavItem>
                                    <CNavLink data-tab="overview">
                                        <h6><i>Overview</i></h6>
                                    </CNavLink>
                                </CNavItem>
                                <CNavItem>
                                    <CNavLink data-tab="ingameanalysis">
                                        <h6><i>In-Game Analysis</i></h6>
                                    </CNavLink>
                                </CNavItem>
                            </CNav>
                            <CTabContent>
                                <CTabPane data-tab="overview">
                                    <br />
                                    <CCol align='center'><h2><b>Red Team {redteam()}</b></h2></CCol>
                                    <CDataTable
                                        items={firstTeam}
                                        fields={[{
                                            key: 'red_team_players', filter: false, sorter: false, _style: { width: '16%', 'text-align': 'center' }
                                        },
                                        {
                                            key: 'ksatriya', filter: false, sorter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'MVP Score', filter: false, sorter: false, _style: { 'text-align': 'center' }
                                        },
                                        { key: 'KDA', filter: false, sorter: false, _style: { 'text-align': 'center' } },
                                        {
                                            key: 'total_damage', filter: true, sorter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'gold', filter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'Wards', filter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'CS', filter: false, _style: { 'text-align': 'center' }
                                        },

                                        ]}

                                        hover
                                        sorterValue={{row: '--empty slot--', hide: true}}
                                        striped
                                        border
                                        noItemsViewSlot={<div><h4><i>This match has no data so far...</i></h4></div>}
                                        sorter
                                        responsive
                                        filt
                                        captionSlot={
                                            <tr>
                                                <td style={{ 'left-border': '0px' }}></td>
                                                <td><b>Average</b></td>
                                                <td align='center'><b>{mvpAverageA.toFixed(2)}</b></td>
                                                <td align='center'><b>{kdaAveragekillA} / {kdaAveragedeathA} / {kdaAverageassistA}</b></td>
                                                <td align='center'><b>{totaldmgAverageA.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</b></td>
                                                <td align='center'><b>{goldAverageA.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</b></td>
                                                <td align='center'><b>{wardsAverageA.toFixed(2)}</b></td>
                                                <td align='center'><b>{csAverageA.toFixed(2)}</b></td>
                                            </tr>}
                                        scopedSlots={{
                                            'ksatriya':
                                                (item) => {
                                                    const KSA = () => {
                                                        if (item.ksatriya_id === 1) {
                                                            return 'Ilya';
                                                        } else if (item.ksatriya_id === 2) {
                                                            return 'Sveta';
                                                        } else if (item.ksatriya_id === 3) {
                                                            return 'Sena';
                                                        } else if (item.ksatriya_id === 4) {
                                                            return 'Ma`esh';
                                                        } else if (item.ksatriya_id === 5) {
                                                            return 'Myluta';
                                                        } else if (item.ksatriya_id === 6) {
                                                            return 'Nisha';
                                                        } else if (item.ksatriya_id === 7) {
                                                            return 'Jinno';
                                                        } else if (item.ksatriya_id === 8) {
                                                            return 'Neera';
                                                        } else if (item.ksatriya_id === 9) {
                                                            return 'Nala';
                                                        } else if (item.ksatriya_id === 10) {
                                                            return 'Sabara';
                                                        } else if (item.ksatriya_id === 11) {
                                                            return 'H`rtal';
                                                        } else if (item.ksatriya_id === 12) {
                                                            return 'Skar';
                                                        } else if (item.ksatriya_id === 13) {
                                                            return 'Haya';
                                                        } else if (item.ksatriya_id === 14) {
                                                            return 'Nada';
                                                        } else if (item.ksatriya_id === 15) {
                                                            return 'Saira';
                                                        } else if (item.ksatriya_id === 16) {
                                                            return 'Handaru';
                                                        } else if (item.ksatriya_id === 17) {
                                                            return 'Vijaya';
                                                        } else if (item.ksatriya_id === 18) {
                                                            return 'Tara';
                                                        } else if (item.ksatriya_id === 19) {
                                                            return 'Kanta';
                                                        } else if (item.ksatriya_id === 20) {
                                                            return 'Rajapatni';
                                                        } else if (item.ksatriya_id === 21) {
                                                            return 'Tarja';
                                                        } else if (item.ksatriya_id === 22) {
                                                            return 'Binara';
                                                        } else if (item.ksatriya_id === 23) {
                                                            return 'T`pala';
                                                        } else if (item.ksatriya_id === 24) {
                                                            return 'Khage';
                                                        } else if (item.ksatriya_id === 26) {
                                                            return 'Guning';
                                                        } else if (item.ksatriya_id === 27) {
                                                            return 'Nio';
                                                        } else if (item.ksatriya_id === 28) {
                                                            return 'Vyana';
                                                        } else if (item.ksatriya_id === 29) {
                                                            return 'Candravasi';
                                                        } else if (item.ksatriya_id === 30) {
                                                            return 'Lando';
                                                        } else if (item.ksatriya_id === 901) {
                                                            return 'Barda';
                                                        }

                                                    }
                                                    return (
                                                        <td>
                                                            {item.user_name === '--empty slot--' ? <></> : <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${item.ksatriya_id}.png`} fluid shape="rounded" width='110' align='left' />}
                                                            <br />
                                                            &nbsp;&nbsp;<span><i>{KSA()}</i></span>
                                                        </td>
                                                    )
                                                },
                                            'red_team_players':
                                                (item) => {
                                                    const mvp = () => {
                                                        if (item.mvp_badge === 4) {
                                                            return 'MVP'
                                                        } else {
                                                            return <></>
                                                        }
                                                    }

                                                    const afk = () => {
                                                        if (item.is_afk === 1) {
                                                            return 'AFK'
                                                        } else {
                                                            return <></>
                                                        }
                                                    }

                                                    const firstblood = () => {
                                                        if (item.first_blood === 1) {
                                                            return 'First Blood'
                                                        } else {
                                                            return <></>
                                                        }
                                                    };

                                                    const mvpbadge = () => {
                                                        switch (mvp) {
                                                            default: return 'info'
                                                        }
                                                    }

                                                    const afkbadge = () => {
                                                        switch (afk) {
                                                            default: return 'warning'
                                                        }
                                                    }

                                                    const firstbloodbadge = () => {
                                                        switch (firstblood) {
                                                            default: return 'danger'
                                                        }
                                                    }

                                                    return (
                                                        <td>
                                                            <CRow>
                                                                <CCol md='4'>
                                                                    {item.user_name === '--empty slot--' ? <></> : <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/avataricon/${item.avatar_icon}.png`} fluid shape="rounded" width='50' align='left' />}
                                                                </CCol>
                                                                <CCol md='7'>
                                                                    {item.user_name === '--empty slot--' ? <span><i><b>{item.user_name}</b></i></span> : <CLink onClick={() => history.push(`/playerdetail/${item.user_id}`)}><span><i><b>{item.user_name}</b></i></span></CLink>}<br />
                                                                    {item.mvp_badge === 4 ? <><CBadge color={mvpbadge(mvp())}>{mvp()}</CBadge><br /></> : <></>}
                                                                    {item.is_afk === 1 ? <><CBadge color={afkbadge(afk())}>{afk()}</CBadge><br /></> : <></>}
                                                                    {item.first_blood === 1 ? <CBadge color={firstbloodbadge(firstblood())}>{firstblood()}</CBadge> : <></>}
                                                                </CCol>
                                                            </CRow>

                                                        </td>
                                                    )
                                                },
                                            'CS':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.minion_kill + item.creep_kill}
                                                    </td>
                                                ),
                                            'KDA':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.kill}/{item.death}/{item.assist}
                                                    </td>
                                                ),
                                            'total_damage':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.damage_dealt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                    </td>
                                                ),
                                            'MVP Score':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.mvp.toFixed(2)}
                                                    </td>
                                                ),
                                            'gold':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.gold.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                    </td>
                                                ),
                                            'Wards':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.ward_placed}
                                                    </td>
                                                ),

                                        }}
                                    />
                                    <br />
                                    <CCol align='center'><h2><b>Blue Team {blueteam()}</b></h2></CCol>
                                    <CDataTable
                                        items={secondTeam}
                                        fields={[{
                                            key: 'blue_team_players', filter: false, sorter: false, _style: { width: '16%', 'text-align': 'center' }
                                        },
                                        {
                                            key: 'ksatriya', filter: false, sorter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'MVP Score', filter: false, sorter: false, _style: { 'text-align': 'center' }
                                        },
                                        { key: 'KDA', filter: false, sorter: false, _style: { 'text-align': 'center' } },
                                        {
                                            key: 'total_damage', filter: true, sorter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'gold', filter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'Wards', filter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'CS', filter: false, _style: { 'text-align': 'center' }
                                        },

                                        ]}

                                        hover
                                        sorterValue={{ column: "mvp_score", asc: false }}
                                        striped
                                        border
                                        noItemsViewSlot={<div><h4><i>This match has no data so far...</i></h4></div>}
                                        sorter
                                        captionSlot={
                                            <tr>
                                                <td style={{ 'left-border': '0px' }}></td>
                                                <td><b>Average</b></td>
                                                <td align='center'><b>{mvpAverageB.toFixed(2)}</b></td>
                                                <td align='center'><b>{kdaAveragekillB} / {kdaAveragedeathB} / {kdaAverageassistB}</b></td>
                                                <td align='center'><b>{totaldmgAverageB.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</b></td>
                                                <td align='center'><b>{goldAverageB.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</b></td>
                                                <td align='center'><b>{wardsAverageB.toFixed(2)}</b></td>
                                                <td align='center'><b>{csAverageB.toFixed(2)}</b></td>
                                            </tr>}
                                        scopedSlots={{
                                            'ksatriya':
                                                (item) => {
                                                    const KSA = () => {
                                                        if (item.ksatriya_id === 1) {
                                                            return 'Ilya';
                                                        } else if (item.ksatriya_id === 2) {
                                                            return 'Sveta';
                                                        } else if (item.ksatriya_id === 3) {
                                                            return 'Sena';
                                                        } else if (item.ksatriya_id === 4) {
                                                            return 'Ma`esh';
                                                        } else if (item.ksatriya_id === 5) {
                                                            return 'Myluta';
                                                        } else if (item.ksatriya_id === 6) {
                                                            return 'Nisha';
                                                        } else if (item.ksatriya_id === 7) {
                                                            return 'Jinno';
                                                        } else if (item.ksatriya_id === 8) {
                                                            return 'Neera';
                                                        } else if (item.ksatriya_id === 9) {
                                                            return 'Nala';
                                                        } else if (item.ksatriya_id === 10) {
                                                            return 'Sabara';
                                                        } else if (item.ksatriya_id === 11) {
                                                            return 'H`rtal';
                                                        } else if (item.ksatriya_id === 12) {
                                                            return 'Skar';
                                                        } else if (item.ksatriya_id === 13) {
                                                            return 'Haya';
                                                        } else if (item.ksatriya_id === 14) {
                                                            return 'Nada';
                                                        } else if (item.ksatriya_id === 15) {
                                                            return 'Saira';
                                                        } else if (item.ksatriya_id === 16) {
                                                            return 'Handaru';
                                                        } else if (item.ksatriya_id === 17) {
                                                            return 'Vijaya';
                                                        } else if (item.ksatriya_id === 18) {
                                                            return 'Tara';
                                                        } else if (item.ksatriya_id === 19) {
                                                            return 'Kanta';
                                                        } else if (item.ksatriya_id === 20) {
                                                            return 'Rajapatni';
                                                        } else if (item.ksatriya_id === 21) {
                                                            return 'Tarja';
                                                        } else if (item.ksatriya_id === 22) {
                                                            return 'Binara';
                                                        } else if (item.ksatriya_id === 23) {
                                                            return 'T`pala';
                                                        } else if (item.ksatriya_id === 24) {
                                                            return 'Khage';
                                                        } else if (item.ksatriya_id === 26) {
                                                            return 'Guning';
                                                        } else if (item.ksatriya_id === 27) {
                                                            return 'Nio';
                                                        } else if (item.ksatriya_id === 28) {
                                                            return 'Vyana';
                                                        } else if (item.ksatriya_id === 29) {
                                                            return 'Candravasi';
                                                        } else if (item.ksatriya_id === 30) {
                                                            return 'Lando';
                                                        } else if (item.ksatriya_id === 901) {
                                                            return 'Barda';
                                                        }

                                                    }
                                                    return (
                                                        <td>
                                                            {item.user_name === '--empty slot--' ? <></> : <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${item.ksatriya_id}.png`} fluid shape="rounded" width='110' align='left' />}
                                                            <br />
                                                            &nbsp;&nbsp;<span><i>{KSA()}</i></span>
                                                        </td>
                                                    )
                                                },
                                            'blue_team_players':
                                                (item) => {
                                                    const mvp = () => {
                                                        if (item.mvp_badge === 4) {
                                                            return 'MVP'
                                                        } else {
                                                            return <></>
                                                        }
                                                    }

                                                    const afk = () => {
                                                        if (item.is_afk === 1) {
                                                            return 'AFK'
                                                        } else {
                                                            return <></>
                                                        }
                                                    }

                                                    const firstblood = () => {
                                                        if (item.first_blood === 1) {
                                                            return 'First Blood'
                                                        } else {
                                                            return <></>
                                                        }
                                                    };

                                                    const mvpbadge = () => {
                                                        switch (mvp) {
                                                            default: return 'info'
                                                        }
                                                    }

                                                    const afkbadge = () => {
                                                        switch (afk) {
                                                            default: return 'warning'
                                                        }
                                                    }

                                                    const firstbloodbadge = () => {
                                                        switch (firstblood) {
                                                            default: return 'danger'
                                                        }
                                                    }

                                                    return (
                                                        <td>
                                                            <CRow>
                                                                <CCol md='4'>
                                                                    {item.user_name === '--empty slot--' ? <></> : <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/avataricon/${item.avatar_icon}.png`} fluid shape="rounded" width='50' align='left' />}
                                                                </CCol>
                                                                <CCol md='7'>
                                                                    {item.user_name === '--empty slot--' ? <span><i><b>{item.user_name}</b></i></span> : <CLink onClick={() => history.push(`/playerdetail/${item.user_id}`)}><span><i><b>{item.user_name}</b></i></span></CLink>}<br />
                                                                    {item.mvp_badge === 4 ? <><CBadge color={mvpbadge(mvp())}>{mvp()}</CBadge><br /></> : <></>}
                                                                    {item.is_afk === 1 ? <><CBadge color={afkbadge(afk())}>{afk()}</CBadge><br /></> : <></>}
                                                                    {item.first_blood === 1 ? <CBadge color={firstbloodbadge(firstblood())}>{firstblood()}</CBadge> : <></>}
                                                                </CCol>
                                                            </CRow>

                                                        </td>
                                                    )
                                                },
                                            'CS':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.minion_kill + item.creep_kill}
                                                    </td>
                                                ),
                                            'KDA':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.kill}/{item.death}/{item.assist}
                                                    </td>
                                                ),
                                            'total_damage':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.damage_dealt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                    </td>
                                                ),
                                            'MVP Score':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.mvp.toFixed(2)}
                                                    </td>
                                                ),
                                            'gold':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.gold.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                    </td>
                                                ),
                                            'Wards':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.ward_placed}
                                                    </td>
                                                ),

                                        }}
                                    />
                                </CTabPane>
                                <CTabPane data-tab="ingameanalysis">
                                    <br />
                                    <CCol align='center'><h2><b>Red Team {redteam()}</b></h2></CCol>
                                    <CDataTable
                                        items={firstTeam}
                                        fields={[{
                                            key: 'red_team_players', filter: false, sorter: false, _style: { width: '16%', 'text-align': 'center' }
                                        },
                                        {
                                            key: 'minion_kill', filter: false, sorter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'creep_kill', filter: false, sorter: false, _style: { 'text-align': 'center' }
                                        },
                                        { key: 'damage_taken', filter: false, _style: { 'text-align': 'center' } },
                                        {
                                            key: 'ksatriya_damage_dealt', filter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'Raksasha Kill/Assist', filter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'Yaksha Kill/Assist', filter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'tower_destroyed', filter: false, _style: { 'text-align': 'center' }
                                        },

                                        ]}

                                        hover
                                        sorterValue={{ column: "mvp_score", asc: false }}
                                        striped
                                        noItemsViewSlot={<div><h4><i>This match has no data so far...</i></h4></div>}
                                        sorter
                                        border
                                        captionSlot={
                                            <tr>
                                                <td><b>Average</b></td>
                                                <td align='center'><b>{minionkillAverageA.toFixed(2)}</b></td>
                                                <td align='center'><b>{creepAverageA}</b></td>
                                                <td align='center'><b>{damageAverageA.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</b></td>
                                                <td align='center'><b>{totaldmgAverageA.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</b></td>
                                                <td align='center'><b>{raksashaAveragekillA.toFixed(2)} / {raksashaAverageassistA.toFixed(2)}</b></td>
                                                <td align='center'><b>{yakshaAveragekillA.toFixed(2)} / {yakshaAverageassistA.toFixed(2)}</b></td>
                                                <td align='center'><b>{towerdestroyAverageA.toFixed(2)}</b></td>
                                            </tr>}
                                        scopedSlots={{
                                            'red_team_players':
                                                (item) => {
                                                    const mvp = () => {
                                                        if (item.mvp_badge === 4) {
                                                            return 'MVP'
                                                        } else {
                                                            return <></>
                                                        }
                                                    }

                                                    const afk = () => {
                                                        if (item.is_afk === 1) {
                                                            return 'AFK'
                                                        } else {
                                                            return <></>
                                                        }
                                                    }

                                                    const firstblood = () => {
                                                        if (item.first_blood === 1) {
                                                            return 'First Blood'
                                                        } else {
                                                            return <></>
                                                        }
                                                    };

                                                    const mvpbadge = () => {
                                                        switch (mvp) {
                                                            default: return 'info'
                                                        }
                                                    }

                                                    const afkbadge = () => {
                                                        switch (afk) {
                                                            default: return 'warning'
                                                        }
                                                    }

                                                    const firstbloodbadge = () => {
                                                        switch (firstblood) {
                                                            default: return 'danger'
                                                        }
                                                    }

                                                    return (
                                                        <td>
                                                            <CRow>
                                                                <CCol md='4'>
                                                                    {item.user_name === '--empty slot--' ? <></> : <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/avataricon/${item.avatar_icon}.png`} fluid shape="rounded" width='50' align='left' />}
                                                                </CCol>
                                                                <CCol md='7'>
                                                                    {item.user_name === '--empty slot--' ? <span><i><b>{item.user_name}</b></i></span> : <CLink onClick={() => history.push(`/playerdetail/${item.user_id}`)}><span><i><b>{item.user_name}</b></i></span></CLink>}<br />
                                                                    {item.mvp_badge === 4 ? <><CBadge color={mvpbadge(mvp())}>{mvp()}</CBadge><br /></> : <></>}
                                                                    {item.is_afk === 1 ? <><CBadge color={afkbadge(afk())}>{afk()}</CBadge><br /></> : <></>}
                                                                    {item.first_blood === 1 ? <CBadge color={firstbloodbadge(firstblood())}>{firstblood()}</CBadge> : <></>}
                                                                </CCol>
                                                            </CRow>

                                                        </td>
                                                    )
                                                },
                                            'Raksasha Kill/Assist':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.raksasha_kill}/{item.raksasha_kill_assist}
                                                    </td>
                                                ),
                                            'Yaksha Kill/Assist':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.yaksha_kill}/{item.yaksha_kill_assist}
                                                    </td>
                                                ),
                                            'minion_kill':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.minion_kill}
                                                    </td>
                                                ),
                                            'creep_kill':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.creep_kill}
                                                    </td>
                                                ),
                                            'damage_taken':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.damage_taken}
                                                    </td>
                                                ),
                                            'ksatriya_damage_dealt':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.ksatriya_damage_dealt}
                                                    </td>
                                                ),
                                            'tower_destroyed':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.tower_destroyed}
                                                    </td>
                                                ),

                                        }}
                                    />
                                    <br />
                                    <CCol align='center'><h2><b>Blue Team {blueteam()}</b></h2></CCol>
                                    <CDataTable
                                        items={secondTeam}
                                        fields={[{
                                            key: 'blue_team_players', filter: false, sorter: false, _style: { width: '16%', 'text-align': 'center' }
                                        },
                                        {
                                            key: 'minion_kill', filter: false, sorter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'creep_kill', filter: false, sorter: false, _style: { 'text-align': 'center' }
                                        },
                                        { key: 'damage_taken', filter: false, _style: { 'text-align': 'center' } },
                                        {
                                            key: 'ksatriya_damage_dealt', filter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'Raksasha Kill/Assist', filter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'Yaksha Kill/Assist', filter: false, _style: { 'text-align': 'center' }
                                        },
                                        {
                                            key: 'tower_destroyed', filter: false, _style: { 'text-align': 'center' }
                                        },

                                        ]}

                                        hover
                                        sorterValue={{ column: "mvp_score", asc: false }}
                                        striped
                                        noItemsViewSlot={<div><h4><i>This match has no data so far...</i></h4></div>}
                                        sorter
                                        border
                                        captionSlot={
                                            <tr>
                                                <td><b>Average</b></td>
                                                <td align='center'><b>{minionkillAverageB.toFixed(2)}</b></td>
                                                <td align='center'><b>{creepAverageB}</b></td>
                                                <td align='center'><b>{damageAverageB.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</b></td>
                                                <td align='center'><b>{totaldmgAverageB.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</b></td>
                                                <td align='center'><b>{raksashaAveragekillB.toFixed(2)} / {raksashaAverageassistB.toFixed(2)}</b></td>
                                                <td align='center'><b>{yakshaAveragekillB.toFixed(2)} / {yakshaAverageassistB.toFixed(2)}</b></td>
                                                <td align='center'><b>{towerdestroyAverageB.toFixed(2)}</b></td>
                                            </tr>}
                                        scopedSlots={{
                                            'blue_team_players':
                                                (item) => {
                                                    const mvp = () => {
                                                        if (item.mvp_badge === 4) {
                                                            return 'MVP'
                                                        } else {
                                                            return <></>
                                                        }
                                                    }

                                                    const afk = () => {
                                                        if (item.is_afk === 1) {
                                                            return 'AFK'
                                                        } else {
                                                            return <></>
                                                        }
                                                    }

                                                    const firstblood = () => {
                                                        if (item.first_blood === 1) {
                                                            return 'First Blood'
                                                        } else {
                                                            return <></>
                                                        }
                                                    };

                                                    const mvpbadge = () => {
                                                        switch (mvp) {
                                                            default: return 'info'
                                                        }
                                                    }

                                                    const afkbadge = () => {
                                                        switch (afk) {
                                                            default: return 'warning'
                                                        }
                                                    }

                                                    const firstbloodbadge = () => {
                                                        switch (firstblood) {
                                                            default: return 'danger'
                                                        }
                                                    }

                                                    return (
                                                        <td>
                                                            <CRow>
                                                                <CCol md='4'>
                                                                    {item.user_name === "--empty slot--" ? <></> : <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/avataricon/${item.avatar_icon}.png`} fluid shape="rounded" width='50' align='left' />}
                                                                </CCol>
                                                                <CCol md='7'>
                                                                    {item.user_name === '--empty slot--' ? <span><i><b>{item.user_name}</b></i></span> : <CLink onClick={() => history.push(`/playerdetail/${item.user_id}`)}><span><i><b>{item.user_name}</b></i></span></CLink>}<br />
                                                                    {item.mvp_badge === 4 ? <><CBadge color={mvpbadge(mvp())}>{mvp()}</CBadge><br /></> : <></>}
                                                                    {item.is_afk === 1 ? <><CBadge color={afkbadge(afk())}>{afk()}</CBadge><br /></> : <></>}
                                                                    {item.first_blood === 1 ? <CBadge color={firstbloodbadge(firstblood())}>{firstblood()}</CBadge> : <></>}
                                                                </CCol>
                                                            </CRow>

                                                        </td>
                                                    )
                                                },
                                            'Raksasha Kill/Assist':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.raksasha_kill}/{item.raksasha_kill_assist}
                                                    </td>
                                                ),
                                            'Yaksha Kill/Assist':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.yaksha_kill}/{item.yaksha_kill_assist}
                                                    </td>
                                                ),
                                            'minion_kill':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.minion_kill}
                                                    </td>
                                                ),
                                            'creep_kill':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.creep_kill}
                                                    </td>
                                                ),
                                            'damage_taken':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.damage_taken}
                                                    </td>
                                                ),
                                            'ksatriya_damage_dealt':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.ksatriya_damage_dealt}
                                                    </td>
                                                ),
                                            'tower_destroyed':
                                                (item) => (
                                                    <td align='center'>
                                                        {item.tower_destroyed}
                                                    </td>
                                                ),

                                        }}
                                    />
                                </CTabPane>
                                {/* <CTabPane data-tab="builds">
                                   Under Development
                                </CTabPane> */}
                            </CTabContent>
                        </CTabs>
                    </CCardBody>
                </CCard>
            </CContainer>
        </div>
    )
}

export default MatchStatistic
