import React, { useState } from "react";
import { PostMaintenance } from "src/api/maintenancemanagement/maintenancePostRequest";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from "src/components/modal";
import { Form } from "react-bootstrap";

function AddMaintenance({ data }) {
  const [addItemModal, setAddItemModal] = useState(false);
  const [startdate, setStartdate] = useState("2019-10-10");
  const [starttime, setStarttime] = useState("00:00");
  const [enddate, setEnddate] = useState("2019-10-10");
  const [endtime, setEndtime] = useState("00:00");
  const [reason, setMaintenanceReason] = useState("");

  return (
    <div>
      <button
        className="btn btn-primary"
        color="primary"
        onClick={() => setAddItemModal(!addItemModal)}
      >
        + Add Maintenance
      </button>
      <Modal
        show={addItemModal}
        onClose={() => setAddItemModal(!addItemModal)}
        color="primary"
        size="lg"
      >
        <ModalHeader>
          <ModalTitle>Add Maintenance Period</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <div className="col" align="left">
            <Form>
              <Form.Group className="mb-3" controlId="mtstart">
                <div className="row">
                  <div className="col-md-3">
                    <Form.Label>
                      <b>Maintenance Start</b>
                    </Form.Label>
                  </div>
                  :
                  <div className="col-md-4">
                    <input
                      className="form-control"
                      type="date"
                      onKeyDown={(e) => e.preventDefault()}
                      onChange={(e) => setStartdate(e.target.value)}
                    />
                  </div>
                  <div className="col-md-4">
                    <input
                      className="form-control"
                      type="time"
                      onChange={(e) => setStarttime(e.target.value)}
                    />
                  </div>
                </div>
              </Form.Group>
              <Form.Group className="mb-3" controlId="mtend">
                <div className="row">
                  <div className="col-md-3">
                    <Form.Label>
                      <b>Maintenance End</b>
                    </Form.Label>
                  </div>
                  :
                  <div className="col-md-4">
                    <input
                      className="form-control"
                      type="date"
                      onKeyDown={(e) => e.preventDefault()}
                      onChange={(e) => setEnddate(e.target.value)}
                    />
                  </div>
                  <div className="col-md-4">
                    <input
                      className="form-control"
                      type="time"
                      onChange={(e) => setEndtime(e.target.value)}
                    />
                  </div>
                </div>
              </Form.Group>
              <Form.Group className="mb-3" controlId="mtreason">
                <div className="row">
                  <div className="col-md-3">
                    <Form.Label>
                      <b>Maintenance Reason</b>
                    </Form.Label>
                  </div>
                  :
                  <div className="col-md-8">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Set Maintenance Reason"
                      onChange={(e) => setMaintenanceReason(e.target.value)}
                    />
                  </div>
                </div>
              </Form.Group>
            </Form>
          </div>
        </ModalBody>
        <ModalFooter>
          {startdate === "2019-10-10" ||
          starttime === "00:00" ||
          enddate === "2019-10-10" ||
          endtime === "00:00" ||
          reason === "" ? (
            <button className="btn btn-md btn-primary" disabled>
              Add Maintenance Period
            </button>
          ) : (
            <>
              {PostMaintenance({
                data,
                startdate,
                enddate,
                starttime,
                endtime,
                reason,
                setAddItemModal,
                addItemModal,
              })}
            </>
          )}
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default AddMaintenance;
