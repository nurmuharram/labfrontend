import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Collapse, Form } from "react-bootstrap";
import { connect } from "react-redux";
import { getMaintenanceAction } from "src/redux/action/maintenancemanagement/getMaintenanceAction";
import AddMaintenance from "./AddMaintenance";
import {
  UpdateMaintenanceEnddate,
  UpdateMaintenanceStartdate,
  UpdateReason,
} from "src/api/maintenancemanagement/maintenancePutRequest";
import { GetMaintenanceList } from "src/api/maintenancemanagement/maintenanceGetRequest";
import { toggleDetails } from "src/services/maintenanceMgmt.services/MaintenanceMgmt.function";
import DataTable from "src/components/table/DataTable";

const MaintenanceListFilter = (props) => {
  const history = useHistory();
  const [details, setDetails] = useState([]);
  const [maintenanceList, setMaintenanceList] = useState([]);

  const [newstartdate, setNewstartdate] = useState("2019-10-10");
  const [newstarttime, setNewstarttime] = useState("00:00");
  const [newenddate, setNewenddate] = useState("2019-10-10");
  const [newendtime, setNewendtime] = useState("00:00");
  const [newreason, setNewreason] = useState("");

  useEffect(() => {
    if (props.error?.response?.status === 401) {
      history.push("/login");
    }
  }, [props.error]);

  useEffect(() => {
    if (props.error?.response?.status === 502) {
      window.location.reload("/dashboard");
      return false;
    }
  }, [props.error]);

  GetMaintenanceList({ setMaintenanceList });

  return (
    <div>
      <div className="col" align="right">
        <AddMaintenance />
      </div>
      <br />
      <DataTable
        items={maintenanceList}
        fields={[
          {
            key: "mt_id",
            _classes: "font-weight-bold",
            _style: {
              width: "8%",
            },
          },
          {
            key: "reason",
            sorter: false,
          },
          {
            key: "start_date",
            filter: false,
          },
          {
            key: "end_date",
            filter: false,
          },
          {
            key: "show_details",
            label: "",
            _style: { width: "10%" },
            sorter: false,
            filter: false,
          },
        ]}
        columnFilter
        itemsPerPage={5}
        hover
        striped
        pagination
        sorterValue={{ column: "id", asc: false }}
        scopedSlots={{
          start_date: (item, index) => {
            const date = item.start_date;
            const splitdate = date.split(" ");
            const joindate = splitdate.join("T");
            const joindatez = joindate + ".000Z";

            const dateconvert = new Date(joindatez);

            return <td>{dateconvert.toString()}</td>;
          },
          end_date: (item, index) => {
            const date = item.end_date;
            const splitdate = date.split(" ");
            const joindate = splitdate.join("T");
            const joindatez = joindate + ".000Z";

            const dateconvert = new Date(joindatez);

            return <td>{dateconvert.toString()}</td>;
          },
          show_details: (item, index) => {
            return (
              <td className="py-2">
                <button
                  className="btn btn-outline-primary btn-sm"
                  onClick={() => {
                    toggleDetails(index, { setDetails, details });
                  }}
                >
                  {details.includes(index) ? "Close" : "Update"}
                </button>
              </td>
            );
          },
          details: (item, index) => {
            const id = item.mt_id;
            const start_date = item.start_date;
            const end_date = item.end_date;
            const _reason = item.reason;
            return (
              <Collapse in={details.includes(index)}>
                <div className="card-body">
                  <div className="row">
                    <div className="col" align="left">
                      <Form>
                        <Form.Group>
                          <Form.Group className="mb-3" controlId="mtstart">
                            <div className="row">
                              <div className="col-lg-2 col-md-3">
                                <Form.Label>
                                  <b>Maintenance Start</b>
                                </Form.Label>
                              </div>
                              :
                              <div className="col-lg-2 col-md-3">
                                <input
                                  className="form-control"
                                  type="date"
                                  onKeyDown={(e) => e.preventDefault()}
                                  onChange={(e) =>
                                    setNewstartdate(e.target.value)
                                  }
                                />
                              </div>
                              <div className="col-lg-2 col-md-3">
                                <input
                                  className="form-control"
                                  type="time"
                                  onChange={(e) =>
                                    setNewstarttime(e.target.value)
                                  }
                                />
                              </div>
                              <div className="col-md-2">
                                {newstartdate === "2019-10-10" ||
                                newstarttime === "00:00" ? (
                                  <button
                                    className="btn btn-md btn-primary"
                                    disabled
                                  >
                                    Update
                                  </button>
                                ) : (
                                  <>
                                    {UpdateMaintenanceStartdate({
                                      setMaintenanceList,
                                      id,
                                      end_date,
                                      _reason,
                                      newstartdate,
                                      newstarttime,
                                    })}
                                  </>
                                )}
                              </div>
                            </div>
                          </Form.Group>
                          <Form.Group className="mb-3" controlId="mtend">
                            <div className="row">
                              <div className="col-lg-2 col-md-3">
                                <Form.Label>
                                  <b>Maintenance End</b>
                                </Form.Label>
                              </div>
                              :
                              <div className="col-lg-2 col-md-3">
                                <input
                                  className="form-control"
                                  type="date"
                                  onKeyDown={(e) => e.preventDefault()}
                                  onChange={(e) =>
                                    setNewenddate(e.target.value)
                                  }
                                />
                              </div>
                              <div className="col-lg-2 col-md-3">
                                <input
                                  className="form-control"
                                  type="time"
                                  onChange={(e) =>
                                    setNewendtime(e.target.value)
                                  }
                                />
                              </div>
                              <div className="col-md-2">
                                {newenddate === "2019-10-10" ||
                                newendtime === "00:00" ? (
                                  <button
                                    className="btn btn-md btn-primary"
                                    disabled
                                  >
                                    Update
                                  </button>
                                ) : (
                                  <>
                                    {UpdateMaintenanceEnddate({
                                      setMaintenanceList,
                                      start_date,
                                      newenddate,
                                      newendtime,
                                      _reason,
                                      id,
                                    })}
                                  </>
                                )}
                              </div>
                            </div>
                          </Form.Group>
                          <Form.Group className="mb-2" controlId="mtreason">
                            <div className="row">
                              <div className="col-lg-2 col-md-3">
                                <Form.Label>
                                  <b>Maintenance Reason</b>
                                </Form.Label>
                              </div>
                              :
                              <div className="col-lg-4 col-md-6">
                                <input
                                  className="form-control"
                                  type="text"
                                  placeholder="Set Maintenance Reason"
                                  onChange={(e) => setNewreason(e.target.value)}
                                />
                              </div>
                              <div className="col-lg-2 col-md-2">
                                {newreason === "" ? (
                                  <button
                                    className="btn btn-md btn-primary"
                                    disabled
                                  >
                                    Update
                                  </button>
                                ) : (
                                  <>
                                    {UpdateReason({
                                      setMaintenanceList,
                                      start_date,
                                      end_date,
                                      newreason,
                                      id,
                                    })}
                                  </>
                                )}
                              </div>
                            </div>
                          </Form.Group>
                        </Form.Group>
                      </Form>
                    </div>
                  </div>
                </div>
              </Collapse>
            );
          },
        }}
      />
    </div>
  );
};
const mapDispatchToProps = (dispatch) => ({
  getMaintenance: () => dispatch(getMaintenanceAction()),
});
const mapStateToProps = (state) => ({
  isLoading: state.getMaintenanceReducer.isLoading,
  data: state.getMaintenanceReducer.data,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MaintenanceListFilter);
