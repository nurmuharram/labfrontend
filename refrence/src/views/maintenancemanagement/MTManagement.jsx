import React from "react";
import MTList from "./MTList";

function MaintenanceManagement() {
  return (
    <div className="MaintenanceManagement">
      <div className="card card-accent-primary">
        <div className="card-header">
          <div className="col" align="center">
            <h5>
              <b>Maintenance Manager</b>
            </h5>
          </div>
        </div>
        <div className="card-body">
          <MTList />
        </div>
      </div>
    </div>
  );
}

export default MaintenanceManagement;
