import React, { useState, useEffect } from 'react'

import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CCollapse,
    CDataTable,
    CFormGroup,
    CInput,
    CLabel,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    CRow,
    CSelect
} from '@coreui/react'

import axios from 'axios'
import CIcon from '@coreui/icons-react'

function LotusList() {

    const [addItemModal, setAddItemModal] = useState(false)
    const [deleteItemModal, setDeleteItemModal] = useState(false)
    const [lotusitemlist, setLotusitemlist] = useState([])
    const [lotusshoplist, setLotusshoplist] = useState([])
    const [lotusperiodlist, setLotusperiodlist] = useState([])
    const [arrayData, setArrayData] = useState([])

    const [details, setDetails] = useState([])

    const [getshopitem, setGetshopitem] = useState([])

    const [lotus_item_id, setLotus_item_id] = useState(1)
    const [lotusperiod, setLotusperiod] = useState()

    const [itemid, setItemid] = useState()


    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/lotus/getAll?count=10000&offset=0`, config).then(res => {
            const items = res.data;
            setLotusshoplist(items);
        });
    }, [])

    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/lotus/getAll`, config).then(res => {
            const items = res.data;
            setLotusshoplist(items);
        });
    }

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/lotus/getAllItem`, config).then(res => {
            const items = res.data;
            setLotusitemlist(items);
        });
    }, [])

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/lotus/getAllPeriod`, config).then(res => {
            const items = res.data;
            setLotusperiodlist(items);
        });
    }, [])

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/lotus/getItem?lotus_item_id=${lotus_item_id}`, config).then(res => {
            const items = res.data;
            setGetshopitem(items);
        });
    }, [lotus_item_id])

    const dropdownItems = lotusitemlist.map((item, index) => {
        return (<option key={item.shop_lotus_item_id} value={item.shop_lotus_item_id}>{item.item_type_name} - {item.item_name}</option>)
    });

    const dropdownPeriod = lotusperiodlist.map((item, index) => {
        return (<option key={item.shop_lotus_period_id} value={item.shop_lotus_period_id}>{item.start_date} - {item.end_date}</option>)
    });

    const showDataFromArray = arrayData && arrayData.map((data, index) => {
        return <><CButton color='info' className='btn-behance' size='sm' style={{ padding: '0.2rem' }} >
            {JSON.stringify(data.shop_lotus_item_id)} - {data.item_name} | <CIcon size='sm' name='cil-x' onClick={() => {
                arrayData.splice(index, 1);
                setArrayData([...arrayData]);
                alert(`Item removed from the list!`);
            }}></CIcon>
        </CButton> &nbsp;&nbsp;</>
    })

    const jsondata = arrayData && arrayData.map((data, index) => {
        return {
            items_lotus: {
                shop_lotus_item_id: data.shop_lotus_item_id,
                player_limit: data.player_limit
            }
        }
    })

    const addLottoToShop = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('shop_lotus_period_id', parseInt(lotusperiod))
        data.append('price_coin', JSON.stringify(jsondata))

        const config = {
            method: 'POST',
            url: `/api/shop/addBundle`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Added!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add!')
            });
        setTimeout(refreshData, 100)
    }

    const toggleDetails = (index) => {
        const position = details.indexOf(index)
        let newDetails = details.slice()
        if (position !== -1) {
            newDetails.splice(position, 1)
        } else {
            newDetails = [...details, index]
        }
        setDetails(newDetails)
    }

    const fields = [
        { key: 'id', _style: { width: '5%' }, filter: false },
        { key: 'item_type_name' },
        { key: 'item_name' },
        { key: 'amount' },
        { key: 'price' },
        { key: 'default_limit' },
        { key: 'start_date' },
        { key: 'end_date' },
        {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        }
    ]

    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            <CButton color='primary' onClick={() => setAddItemModal(!addItemModal)}>+ Add Lotus Shop</CButton>
                            <CModal
                                show={addItemModal}
                                onClose={() => setAddItemModal(!addItemModal)}
                                color="primary"
                                size="lg"
                            >
                                <CModalHeader closeButton>
                                    <CModalTitle>
                                        Add Lotus Shop
                                    </CModalTitle>
                                </CModalHeader>
                                <CModalBody>
                                    <CCol>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Select Lotus Period</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="7">
                                                <CSelect type='text' onChange={(e) => setLotusperiod(e.target.value)}>
                                                    {dropdownPeriod}
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Select Lotus Item</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="5">
                                                <CSelect type='text' onChange={(e) => setLotus_item_id(e.target.value)}>
                                                    {dropdownItems}
                                                </CSelect>
                                            </CCol>
                                            <CCol xs="12" md="2">
                                                <CButton color='primary' onClick={() => setArrayData([...arrayData, { shop_lotus_item_id: parseInt(lotus_item_id), player_limit: parseInt(getshopitem.default_limit), item_name: getshopitem.item_name }])}>+ Add</CButton>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Item(s) Chosen</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="6">
                                                {showDataFromArray}
                                            </CCol>

                                        </CFormGroup>
                                    </CCol>
                                </CModalBody>
                                <CModalFooter>
                                    <CButton color='primary' size='md' onClick={addLottoToShop}>Add to Lotus Shop</CButton>
                                </CModalFooter>
                            </CModal>
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={lotusshoplist}
                                fields={fields}
                                itemsPerPage={5}
                                pagination
                                sorter
                                columnFilter
                                scopedSlots={{
                                    "id":
                                        (item, index) => {
                                            return (
                                                <td>
                                                    {item.shop_lotus_item_id}
                                                </td>
                                            )
                                        },
                                    'show_details':
                                        (item, index) => {

                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        color="primary"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(index) }}
                                                    >
                                                        {details.includes(index) ? 'Close' : 'Edit Item'}
                                                    </CButton>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (item, index) => {

                                            /* UPDATE - PUT Request */
                                            const updateItem = async () => {
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('shop_lotus_period_id', item.shop_lotus_period_id);
                                                data.append('shop_lotus_item_id', itemid);
                                                data.append('player_limit', item.player_limit);

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/shop/lotus/update?shop_lotus_item_id=${item.shop_lotus_item_id}&shop_lotus_period_id=${item.shop_lotus_period_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };

                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        alert('Failed to Update!')
                                                    });
                                                setTimeout(refreshData, 100)
                                            };

                                            const updateItemPeriod = async () => {
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('shop_lotus_period_id', parseInt(lotusperiod));
                                                data.append('shop_lotus_item_id', item.shop_lotus_item_id);
                                                data.append('player_limit', item.player_limit);

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/shop/lotus/update?shop_lotus_item_id=${item.shop_lotus_item_id}&shop_lotus_period_id=${item.shop_lotus_period_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };

                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        alert('Failed to Update!')
                                                    });
                                                setTimeout(refreshData, 100)
                                            };

                                            const deleteItem = () => {

                                                const config = {
                                                    method: 'delete',
                                                    url: `/api/shop/lotus/delete?shop_lotus_item_id=${item.shop_lotus_item_id}&shop_lotus_period_id=${item.shop_lotus_period_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth')
                                                    }
                                                };

                                                axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Removed!');
                                                        setDeleteItemModal(false)
                                                    })
                                                    .catch((error) => {
                                                        alert('Failed to delete item!');
                                                    });
                                                setTimeout(refreshData, 100);
                                            };

                                            return (
                                                <CCollapse show={details.includes(index)}>
                                                    <CCardBody>
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <h4>
                                                                    <i>Edit/Update Item (<small>ID : {item.shop_lotus_item_id}</small>)</i>
                                                                </h4>
                                                            </CCol>
                                                            <CCol align='left' md='2'>
                                                                <CButton size="sm" color="danger" variant='outline' shape='square' onClick={() => setDeleteItemModal(!deleteItemModal)}>
                                                                    Delete Item</CButton>
                                                                <CModal
                                                                    show={deleteItemModal}
                                                                    onClose={() => setDeleteItemModal(!deleteItemModal)}
                                                                    color="danger"
                                                                    size="md"
                                                                >
                                                                    <CModalHeader closeButton>
                                                                        <CModalTitle>
                                                                            <b>Delete this item?</b>
                                                                        </CModalTitle>
                                                                    </CModalHeader>
                                                                    <CModalBody>

                                                                        <CCol align='center'>
                                                                            <h5>Are you sure?</h5>
                                                                        </CCol>

                                                                    </CModalBody>
                                                                    <CModalFooter>
                                                                        <CRow>
                                                                            <CCol align='center'>
                                                                                <CButton type="send" size="sm" color="danger" onClick={deleteItem} ><b>Delete</b></CButton>&nbsp;
                                                                                <CButton type="send" size="sm" color="secondary" onClick={() => setDeleteItemModal(!deleteItemModal)} ><b>Cancel</b></CButton>
                                                                            </CCol>
                                                                        </CRow>
                                                                    </CModalFooter>
                                                                </CModal>
                                                            </CCol>
                                                        </CRow>

                                                        <hr />
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <CFormGroup row>
                                                                    <CCol md="3">
                                                                        <CLabel htmlFor="input"><b>Select Lotus Period</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CSelect type='text' onChange={(e) => setLotusperiod(e.target.value)}>
                                                                            {dropdownPeriod}
                                                                        </CSelect>
                                                                    </CCol>
                                                                    <CCol xs="12" md="2">
                                                                        <CButton color='info' onClick={updateItemPeriod}>Update</CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <hr />
                                                                <CFormGroup row>
                                                                    <CCol md="3">
                                                                        <CLabel htmlFor="input"><b>Select Lotus Item</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CSelect type='text' onChange={(e) => setLotus_item_id(e.target.value)}>
                                                                            {dropdownItems}
                                                                        </CSelect>
                                                                    </CCol>
                                                                    <CCol xs="12" md="2">
                                                                        <CButton color='info' onClick={updateItem}>Update</CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                            </CCol>
                                                        </CRow>
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        },
                                    "start_date":
                                        (item, index) => {
                                            const date = new Date((item.start_date && item.start_date.split(' ').join('T')) + '.000Z')
                                            return (
                                                <td>
                                                    {date.toString()}
                                                </td>
                                            )
                                        },
                                    "end_date":
                                        (item, index) => {
                                            const date = new Date((item.end_date && item.end_date.split(' ').join('T')) + '.000Z')
                                            return (
                                                <td>
                                                    {date.toString()}
                                                </td>
                                            )
                                        }
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default LotusList