import React, { useState, useEffect } from 'react'

import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CCollapse,
    CDataTable,
    CFormGroup,
    CInput,
    CLabel,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    CRow
} from '@coreui/react'

import axios from 'axios'

function LotusPeriod() {

    const [addItemModal, setAddItemModal] = useState(false)
    const [deleteItemModal, setDeleteItemModal] = useState(false)
    const [lotusperiod, setLotusperiod] = useState([])

    const [details, setDetails] = useState([])

    const [startdate, setStartdate] = useState('2019-10-10')
    const [starttime, setStarttime] = useState('00:00')
    const convertTime = new Date(startdate + ' ' + starttime)
    const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')
    const [enddate, setEnddate] = useState('2019-10-10')
    const [endtime, setEndtime] = useState('00:00')
    const convertTimeEnd = new Date(enddate + ' ' + endtime)
    const endingdate = convertTimeEnd.toISOString().split('T').join(' ').replace('.000Z', '')

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/lotus/getAllPeriod`, config).then(res => {
            const items = res.data;
            setLotusperiod(items);
        });
    }, [])

    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/lotus/getAllPeriod`, config).then(res => {
            const items = res.data;
            setLotusperiod(items);
        });
    }

    const addLotusPeriod = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_date', startingdate)
        data.append('end_date', endingdate)

        const config = {
            method: 'POST',
            url: `/api/shop/lotus/addPeriod`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Added!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add!')
            });
        setTimeout(refreshData, 100)
    }

    const toggleDetails = (index) => {
        const position = details.indexOf(index)
        let newDetails = details.slice()
        if (position !== -1) {
            newDetails.splice(position, 1)
        } else {
            newDetails = [...details, index]
        }
        setDetails(newDetails)
    }

    const fields = [
        { key: 'shop_lotus_period_id', filter: false },
        { key: 'start_date' },
        { key: 'end_date' },
        {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        }
    ]

    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            <CButton color='primary' onClick={() => setAddItemModal(!addItemModal)}>+ Add Lotus Period</CButton>
                            <CModal
                                show={addItemModal}
                                onClose={() => setAddItemModal(!addItemModal)}
                                color="primary"
                                size="md"
                            >
                                <CModalHeader>
                                    <CModalTitle>
                                        Add Lotus Period
                                    </CModalTitle>
                                </CModalHeader>
                                <CModalBody>
                                    <CCol>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Start date</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="4">
                                                <CInput type='date' onKeyDown={(e) => e.preventDefault()} onChange={(e) => setStartdate(e.target.value)} />
                                            </CCol>
                                            <CCol xs="12" md="4">
                                                <CInput type='time' onChange={(e) => setStarttime(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>End date</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="4">
                                                <CInput type='date' onKeyDown={(e) => e.preventDefault()} onChange={(e) => setEnddate(e.target.value)} />
                                            </CCol>
                                            <CCol xs="12" md="4">
                                                <CInput type='time' onChange={(e) => setEndtime(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                    </CCol>
                                </CModalBody>
                                <CModalFooter>
                                    <CButton color='primary' size='md' onClick={addLotusPeriod}>Add</CButton>
                                </CModalFooter>
                            </CModal>
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={lotusperiod}
                                fields={fields}
                                itemsPerPage={5}
                                pagination
                                scopedSlots={{
                                    'show_details':
                                        (item, index) => {

                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        color="primary"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(index) }}
                                                    >
                                                        {details.includes(index) ? 'Close' : 'Edit Item'}
                                                    </CButton>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (item, index) => {

                                            /* UPDATE  - PUT Request */
                                            const updateStartdate = async () => {
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('start_date', startingdate);
                                                data.append('end_date', item.end_date);

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/shop/lotus/updatePeriod?id=${item.shop_lotus_period_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };

                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        alert('Failed to Update!')
                                                    });
                                                setTimeout(refreshData, 100)
                                            };

                                            const updateEnddate = async () => {
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('start_date', item.start_date);
                                                data.append('end_date', endingdate);

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/shop/lotus/updatePeriod?id=${item.shop_lotus_period_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };

                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        alert('Failed to Update!')
                                                    });
                                                setTimeout(refreshData, 100)
                                            };

                                            const deleteItem = () => {

                                                const config = {
                                                    method: 'delete',
                                                    url: `/api/shop/lotus/deletePeriod?id=${item.shop_lotus_period_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth')
                                                    }
                                                };

                                                axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Removed!');
                                                        setDeleteItemModal(false)
                                                    })
                                                    .catch((error) => {
                                                        alert('Failed to delete item!');
                                                    });
                                                setTimeout(refreshData, 100);
                                            };

                                            return (
                                                <CCollapse show={details.includes(index)}>
                                                    <CCardBody>
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <h4>
                                                                    <i>Edit/Update Item (<small>ID : {item.shop_lotus_period_id}</small>)</i>
                                                                </h4>
                                                            </CCol>
                                                            <CCol align='left' md='2'>
                                                                <CButton size="sm" color="danger" variant='outline' shape='square' onClick={() => setDeleteItemModal(!deleteItemModal)}>
                                                                    Delete Item</CButton>
                                                                <CModal
                                                                    show={deleteItemModal}
                                                                    onClose={() => setDeleteItemModal(!deleteItemModal)}
                                                                    color="danger"
                                                                    size="md"
                                                                >
                                                                    <CModalHeader closeButton>
                                                                        <CModalTitle>
                                                                            <b>Delete this item?</b>
                                                                        </CModalTitle>
                                                                    </CModalHeader>
                                                                    <CModalBody>

                                                                        <CCol align='center'>
                                                                            <h5>Are you sure?</h5>
                                                                        </CCol>

                                                                    </CModalBody>
                                                                    <CModalFooter>
                                                                        <CRow>
                                                                            <CCol align='center'>
                                                                                <CButton type="send" size="sm" color="danger" onClick={deleteItem} ><b>Delete</b></CButton>&nbsp;
                                                                                <CButton type="send" size="sm" color="secondary" onClick={() => setDeleteItemModal(!deleteItemModal)} ><b>Cancel</b></CButton>
                                                                            </CCol>
                                                                        </CRow>
                                                                    </CModalFooter>
                                                                </CModal>
                                                            </CCol>
                                                        </CRow>

                                                        <hr />
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>New Start date</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="2">
                                                                        <CInput type='date' onKeyDown={(e) => e.preventDefault()} onChange={(e) => setStartdate(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol xs="12" md="2">
                                                                        <CInput type='time' onChange={(e) => setStarttime(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol xs="12" md="2">
                                                                        <CButton color='info' onClick={updateStartdate}>Update</CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <hr />
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>New End date</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="2">
                                                                        <CInput type='date' onKeyDown={(e) => e.preventDefault()} onChange={(e) => setEnddate(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol xs="12" md="2">
                                                                        <CInput type='time' onChange={(e) => setEndtime(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol xs="12" md="2">
                                                                        <CButton color='info' onClick={updateEnddate}>Update</CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                            </CCol>
                                                        </CRow>
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        },
                                    "start_date":
                                        (item, index) => {
                                            const date = new Date((item.start_date && item.start_date.split(' ').join('T')) + '.000Z')
                                            return (
                                                <td>
                                                    {date.toString()}
                                                </td>
                                            )
                                        },
                                    "end_date":
                                        (item, index) => {
                                            const date = new Date((item.end_date && item.end_date.split(' ').join('T')) + '.000Z')
                                            return (
                                                <td>
                                                    {date.toString()}
                                                </td>
                                            )
                                        }
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default LotusPeriod