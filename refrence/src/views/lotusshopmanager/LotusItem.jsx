import React, { useState, useEffect } from 'react'

import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CFormGroup,
    CInput,
    CLabel,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    CRow,
    CSelect,
    CTextarea,
    CCollapse
} from '@coreui/react'

import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'
import CIcon from '@coreui/icons-react'

import { getBoxes } from 'src/redux/action/mailAction/getBoxesAction'
import { getKsatriyas } from 'src/redux/action/mailAction/getKsatriyasAction'
import { getItems } from 'src/redux/action/mailAction/getItemsAction'
import { getSkins } from 'src/redux/action/mailAction/getSkinsAction'
import { getRunes } from 'src/redux/action/mailAction/getRunesAction'
import { getFrames } from 'src/redux/action/mailAction/getFramesAction'
import { getAvatars } from 'src/redux/action/mailAction/getAvatarsAction'

function LotusItem() {

    const dispatch = useDispatch()

    const [details, setDetails] = useState([])

    const [addItemModal, setAddItemModal] = useState(false)
    const [deleteItemModal, setDeleteItemModal] = useState(false)
    const [lotusitem, setLotusitem] = useState([])
    const [arrayData, setArrayData] = useState([])

    const [itemtype, setItemtype] = useState()
    const [itemid, setItemid] = useState()
    const [amount, setAmount] = useState()
    const [price, setPrice] = useState()
    const [limit_default, setLimit_default] = useState()

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/lotus/getAllItem`, config).then(res => {
            const items = res.data;
            setLotusitem(items);
        });
    }, [])

    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/lotus/getAllItem`, config).then(res => {
            const items = res.data;
            setLotusitem(items);
        });
    }

    /* GET Boxes - Get Request */
    const boxes = useSelector((state) => state.boxes.boxes);

    useEffect(() => {
        dispatch(getBoxes());
    }, [dispatch]);

    /* GET Ksatriyas - Get Request */
    const ksatriyas = useSelector((state) => state.ksatriyas.ksatriyas);
    useEffect(() => {
        dispatch(getKsatriyas());
    }, [dispatch]);

    /* GET Items - Get Request */
    const items = useSelector((state) => state.items.items);
    useEffect(() => {
        dispatch(getItems());
    }, [dispatch]);

    /* GET skins - Get Request */
    const skins = useSelector((state) => state.skins.skins);
    useEffect(() => {
        dispatch(getSkins());
    }, [dispatch]);

    /* GET rune - Get Request */
    const runes = useSelector((state) => state.runes.runes);
    useEffect(() => {
        dispatch(getRunes());
    }, [dispatch]);

    /* GET frames - Get Request */
    const frames = useSelector((state) => state.frames.frames);
    useEffect(() => {
        dispatch(getFrames());
    }, [dispatch]);

    /* GET Avatar - Get Request */
    const avatars = useSelector((state) => state.avatars.avatars);
    useEffect(() => {
        dispatch(getAvatars());
    }, [dispatch]);

    const dropdownBoxes = boxes.map((box, index) => {
        return (<option key={box.box_id} value={box.box_id}>[Box] - {box.box_name}</option>)
    });

    /* KSA Mapping */
    const dropdownKsatriyas = ksatriyas.map((ksatriya, index) => {
        return (<option key={ksatriya.ksatriya_id} value={ksatriya.ksatriya_id}>[KSA] - {ksatriya.ksatriya_name}</option>)
    });

    /* items Mapping */
    const dropdownItems = items.map((item, index) => {
        return (<option key={item.misc_id} value={item.misc_id}>[Item] - {item.misc_name}</option>)
    });

    /* skins Mapping */
    const dropdownSkins = skins.map((item, index) => {
        const skinname = () => {
            if (item.ksatriya_skin_id === 100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 101) {
                return "Yanaka"
            } else if (item.ksatriya_skin_id === 300) {
                return "Default"
            } else if (item.ksatriya_skin_id === 301) {
                return "Arbalest Armor"
            } else if (item.ksatriya_skin_id === 302) {
                return "Homeroom Teacher"
            } else if (item.ksatriya_skin_id === 400) {
                return "Default"
            } else if (item.ksatriya_skin_id === 401) {
                return "Cybuff Funk"
            } else if (item.ksatriya_skin_id === 402) {
                return "Eternal Anarchy"
            } else if (item.ksatriya_skin_id === 500) {
                return "Default"
            } else if (item.ksatriya_skin_id === 600) {
                return "Default"
            } else if (item.ksatriya_skin_id === 601) {
                return "Thunder Phantom"
            } else if (item.ksatriya_skin_id === 602) {
                return "Badhayan Nyandi"
            } else if (item.ksatriya_skin_id === 700) {
                return "Default"
            } else if (item.ksatriya_skin_id === 701) {
                return "Patih Mada"
            } else if (item.ksatriya_skin_id === 800) {
                return "Default"
            } else if (item.ksatriya_skin_id === 801) {
                return "Guardian of Manthana"
            } else if (item.ksatriya_skin_id === 900) {
                return "Default"
            } else if (item.ksatriya_skin_id === 901) {
                return "Carimata Marauder"
            } else if (item.ksatriya_skin_id === 902) {
                return "Lady Jock"
            } else if (item.ksatriya_skin_id === 1000) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1001) {
                return "Cerberus"
            } else if (item.ksatriya_skin_id === 1100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1101) {
                return "Smokestack Outburst"
            } else if (item.ksatriya_skin_id === 1102) {
                return "Endless Angst"
            } else if (item.ksatriya_skin_id === 1200) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1201) {
                return "Divine Warlord"
            } else if (item.ksatriya_skin_id === 1202) {
                return "Undefined"
            } else if (item.ksatriya_skin_id === 1300) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1301) {
                return "Volcanic Lightning"
            } else if (item.ksatriya_skin_id === 1302) {
                return "Party Wrecker"
            } else if (item.ksatriya_skin_id === 1400) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1401) {
                return "Serene Melody"
            } else if (item.ksatriya_skin_id === 1500) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1501) {
                return "Brave Princess"
            } else if (item.ksatriya_skin_id === 1600) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1601) {
                return "Alighted Asura"
            } else if (item.ksatriya_skin_id === 1700) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1701) {
                return "Battle of Kudadu"
            } else if (item.ksatriya_skin_id === 1702) {
                return "Krtarajasa"
            } else if (item.ksatriya_skin_id === 1800) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1900) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1901) {
                return "Undefined"
            } else if (item.ksatriya_skin_id === 1903) {
                return "Knightly Trip"
            } else if (item.ksatriya_skin_id === 2000) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2001) {
                return "Singharajni"
            } else if (item.ksatriya_skin_id === 2100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2200) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2300) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2400) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2500) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2600) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2601) {
                return "Unswerving Fellowship"
            } else if (item.ksatriya_skin_id === 2700) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2702) {
                return "Lestari Mudan"
            } else if (item.ksatriya_skin_id === 2800) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2802) {
                return "Jinan"
            } else if (item.ksatriya_skin_id === 2900) {
                return "Default"
            } else if (item.ksatriya_skin_id === 3000) {
                return "Default"
            } else if (item.ksatriya_skin_id === 3002) {
                return "Champion of Diver"
            } else if (item.ksatriya_skin_id === 90100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 90101) {
                return "Black Suit"
            } else {
                return item.ksatriya_skin_id
            }
        }
        return (<option key={item.ksatriya_skin_id} value={item.ksatriya_skin_id}>[Skin] {item.ksatriya_name} - {skinname()}</option>)
    });

    /* rune Mapping */
    const dropdownRunes = runes.map((rune, index) => {
        return (<option key={rune.rune_id} value={rune.rune_id}>[Rune] - {rune.name} ({rune.description})</option>)
    });

    /* frames Mapping */
    const dropdownFrames = frames.map((frame, index) => {
        return (<option key={frame.frame_id} value={frame.frame_id}>[Frame] - {frame.description}</option>)
    });

    /* Avatar Mapping */
    const dropdownAvatars = avatars.map((avatar, index) => {
        return (<option key={avatar.avatar_id} value={avatar.avatar_id}>[Avatar] - {avatar.description}</option>)
    });

    const currency = () => {
        return (<><option key={'ori'} value={1}>[Currency] - Ori</option>
            <option key={'citrine'} value={2}>[Currency] - Citrine</option>
            <option key={'lotus'} value={3}>[Currency] - Lotus</option></>)
    }

    const dropdownItemChange = () => {
        if (itemtype === '1') {
            return (currency());
        } else if (itemtype === '6') {
            return (dropdownBoxes)
        } else if (itemtype === '5') {
            return (dropdownItems)
        } else if (itemtype === '2') {
            return (dropdownKsatriyas)
        } else if (itemtype === '3') {
            return (dropdownSkins)
        } else if (itemtype === '4') {
            return (dropdownRunes)
        } else if (itemtype === '11') {
            return (dropdownFrames)
        } else if (itemtype === '12') {
            return (dropdownAvatars)
        } else if (itemtype === '') {
            return <option>Choose Item Type First...</option>
        }
    };


    const showDataFromArray = arrayData && arrayData.map((data, index) => {

        const itemtypename = () => {
            if (data.item_type === 1) {
                return 'Currency';
            } else if (data.item_type === 6) {
                return 'Box'
            } else if (data.item_type === 5) {
                return 'Misc Items'
            } else if (data.item_type === 2) {
                return 'KSA'
            } else if (data.item_type === 3) {
                return 'KSA Skins'
            } else if (data.item_type === 4) {
                return 'Rune'
            } else if (data.item_type === 11) {
                return 'Frame'
            } else if (data.item_type === 12) {
                return 'Avatar'
            }
        };

        return <><CButton color='info' className='btn-behance' size='sm' style={{ padding: '0.2rem' }} >
            {itemtypename()} - {JSON.stringify(data.item_id)} ({data.amount}) | <CIcon size='sm' name='cil-x' onClick={() => {
                arrayData.splice(index, 1);
                setArrayData([...arrayData]);
                alert(`Item removed from the list!`);
            }}></CIcon>
        </CButton> &nbsp;&nbsp;</>
    })

    const jsonData = {
        items: arrayData
    }

    const addItem = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('items', JSON.stringify(jsonData))


        const config = {
            method: 'POST',
            url: `/api/shop/addBundle`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Added!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add Item!')
            });
        setTimeout(refreshData, 100)
    }

    const toggleDetails = (index) => {
        const position = details.indexOf(index)
        let newDetails = details.slice()
        if (position !== -1) {
            newDetails.splice(position, 1)
        } else {
            newDetails = [...details, index]
        }
        setDetails(newDetails)
    }

    const fields = [
        { key: 'id', _style: { width: '5%' }, filter: false },
        { key: 'item_type_name' },
        { key: 'item_name' },
        { key: 'amount' },
        { key: 'price' },
        { key: 'default_limit' },
        {
            key: 'show_details',
            label: '',
            _style: { width: '10%' },
            sorter: false,
            filter: false
        }
    ]


    return (
        <div>
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                            <CButton color='primary' onClick={() => setAddItemModal(!addItemModal)}>+ Add Lotus Item</CButton>
                            <CModal
                                show={addItemModal}
                                onClose={() => setAddItemModal(!addItemModal)}
                                color="primary"
                                size="lg"
                            >
                                <CModalHeader closeButton>
                                    <CModalTitle>
                                        Add Lotus Item
                                    </CModalTitle>
                                </CModalHeader>
                                <CModalBody>
                                    <CCol>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Item Type</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CSelect custom className="align-items-md-start" onChange={(e) => setItemtype(e.target.value)}>
                                                    <option value={''}>Choose Item Type</option>
                                                    <option value={1} >Currency</option>
                                                    <option value={6}  >Box</option>
                                                    <option value={5}  >Items</option>
                                                    <option value={2}  >Ksatriya</option>
                                                    <option value={3}  >KSA Skins</option>
                                                    <option value={4}  >Rune</option>
                                                    <option value={11} >Frame</option>
                                                    <option value={12} >Avatar</option>
                                                </CSelect>
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Item Name</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CSelect custom name="select" className="align-items-md-start" onChange={(e) => setItemid(e.target.value)}>
                                                    {dropdownItemChange()}
                                                </CSelect>
                                            </CCol>

                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Amount</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CInput type='number' onChange={(e) => setAmount(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Price</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CInput type='number' onChange={(e) => setPrice(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Limit Default</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                <CInput type='number' onChange={(e) => setLimit_default(e.target.value)} />
                                            </CCol>
                                        </CFormGroup>
                                        <CFormGroup row>
                                            <CCol xs="12" md="11" align='right'>
                                                <CButton size="sm" color="info" onClick={() => setArrayData([...arrayData, { item_type: parseInt(itemtype), item_id: parseInt(itemid), amount: parseInt(amount), price: parseInt(price), default_limit: parseInt(limit_default) }])} ><b>+ Add Item</b></CButton>
                                            </CCol>
                                        </CFormGroup>
                                        <hr />
                                        <CFormGroup row>
                                            <CCol md="3">
                                                <CLabel htmlFor="input"><b>Item(s) Attached</b></CLabel>
                                            </CCol>
                                            :
                                            <CCol xs="12" md="8">
                                                {showDataFromArray}
                                            </CCol>
                                        </CFormGroup>
                                    </CCol>
                                </CModalBody>
                                <CModalFooter>
                                    <CButton color='primary' size='md' onClick={addItem}>Add</CButton>
                                </CModalFooter>
                            </CModal>
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={lotusitem}
                                fields={fields}
                                pagination
                                sorter
                                itemsPerPage={5}
                                scopedSlots={{
                                    "id":
                                        (item, index) => {
                                            return (
                                                <td>
                                                    {item.shop_lotus_item_id}
                                                </td>
                                            )
                                        },
                                    'show_details':
                                        (item, index) => {

                                            return (
                                                <td className="py-2">
                                                    <CButton
                                                        color="primary"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={() => { toggleDetails(index) }}
                                                    >
                                                        {details.includes(index) ? 'Close' : 'Edit Item'}
                                                    </CButton>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (item, index) => {

                                            /* UPDATE - PUT Request */
                                            const updateItem = async () => {
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('item_type', parseInt(itemtype));
                                                data.append('item_id', parseInt(itemid));
                                                data.append('amount', item.amount);
                                                data.append('price', item.price);
                                                data.append('default_limit', item.default_limit);

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/shop/lotus/updateItem?lotus_item_id=${item.shop_lotus_item_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };

                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        alert('Failed to Update!')
                                                    });
                                                setTimeout(refreshData, 100)
                                            };

                                            const updateItemAmount = async () => {
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('item_type', item.item_type);
                                                data.append('item_id', item.item_id);
                                                data.append('amount', amount);
                                                data.append('price', item.price);
                                                data.append('default_limit', item.default_limit);

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/shop/lotus/updateItem?lotus_item_id=${item.shop_lotus_item_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };

                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        alert('Failed to Update!')
                                                    });
                                                setTimeout(refreshData, 100)
                                            };

                                            const updateItemPrice = async () => {
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('item_type', item.item_type);
                                                data.append('item_id', item.item_id);
                                                data.append('amount', item.amount);
                                                data.append('price', price);
                                                data.append('default_limit', item.default_limit);

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/shop/lotus/updateItem?lotus_item_id=${item.shop_lotus_item_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };

                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        alert('Failed to Update!')
                                                    });
                                                setTimeout(refreshData, 100)
                                            };

                                            const updateItemDefaultLimit = async () => {
                                                const FormData = require('form-data');
                                                const data = new FormData();
                                                data.append('item_type', item.item_type);
                                                data.append('item_id', item.item_id);
                                                data.append('amount', item.amount);
                                                data.append('price', item.price);
                                                data.append('default_limit', limit_default);

                                                const config = {
                                                    method: 'PUT',
                                                    url: `/api/shop/lotus/updateItem?lotus_item_id=${item.shop_lotus_item_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                                                    },
                                                    data: data
                                                };

                                                await axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Updated!')
                                                    })
                                                    .catch((error) => {
                                                        console.log(error);
                                                        alert('Failed to Update!')
                                                    });
                                                setTimeout(refreshData, 100)
                                            };

                                            const deleteItem = () => {

                                                const config = {
                                                    method: 'delete',
                                                    url: `/api/shop/lotus/deleteItem?lotus_item_id=${item.shop_lotus_item_id}`,
                                                    headers: {
                                                        Authorization: 'Bearer ' + localStorage.getItem('auth')
                                                    }
                                                };

                                                axios(config)
                                                    .then((response) => {
                                                        console.log(JSON.stringify(response.data));
                                                        alert('Item Removed!');
                                                        setDeleteItemModal(false)
                                                    })
                                                    .catch((error) => {
                                                        alert('Failed to delete item!');
                                                    });
                                                setTimeout(refreshData, 100);
                                            };

                                            return (
                                                <CCollapse show={details.includes(index)}>
                                                    <CCardBody>
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <h4>
                                                                    <i>Edit/Update Item (<small>ID : {item.shop_lotus_item_id}</small>)</i>
                                                                </h4>
                                                            </CCol>
                                                            <CCol align='left' md='2'>
                                                                <CButton size="sm" color="danger" variant='outline' shape='square' onClick={() => setDeleteItemModal(!deleteItemModal)}>
                                                                    Delete Item</CButton>
                                                                <CModal
                                                                    show={deleteItemModal}
                                                                    onClose={() => setDeleteItemModal(!deleteItemModal)}
                                                                    color="danger"
                                                                    size="md"
                                                                >
                                                                    <CModalHeader closeButton>
                                                                        <CModalTitle>
                                                                            <b>Delete this item?</b>
                                                                        </CModalTitle>
                                                                    </CModalHeader>
                                                                    <CModalBody>

                                                                        <CCol align='center'>
                                                                            <h5>Are you sure?</h5>
                                                                        </CCol>

                                                                    </CModalBody>
                                                                    <CModalFooter>
                                                                        <CRow>
                                                                            <CCol align='center'>
                                                                                <CButton type="send" size="sm" color="danger" onClick={deleteItem} ><b>Delete</b></CButton>&nbsp;
                                                                                <CButton type="send" size="sm" color="secondary" onClick={() => setDeleteItemModal(!deleteItemModal)} ><b>Cancel</b></CButton>
                                                                            </CCol>
                                                                        </CRow>
                                                                    </CModalFooter>
                                                                </CModal>
                                                            </CCol>
                                                        </CRow>

                                                        <hr />
                                                        <CRow>
                                                            <CCol align='left'>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Item Type</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CSelect custom className="align-items-md-start" onChange={(e) => setItemtype(e.target.value)}>
                                                                            <option value={''}>Choose Item Type</option>
                                                                            <option value={1} >Currency</option>
                                                                            <option value={6}  >Box</option>
                                                                            <option value={5}  >Items</option>
                                                                            <option value={2}  >Ksatriya</option>
                                                                            <option value={3}  >KSA Skins</option>
                                                                            <option value={4}  >Rune</option>
                                                                            <option value={11} >Frame</option>
                                                                            <option value={12} >Avatar</option>
                                                                        </CSelect>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Item Name</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CSelect custom name="select" className="align-items-md-start" onChange={(e) => setItemid(e.target.value)}>
                                                                            {dropdownItemChange()}
                                                                        </CSelect>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <CFormGroup>
                                                                    <CCol xs="12" md="6" align='right'>
                                                                        <CButton color='info' onClick={updateItem}>Update</CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <hr/>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Amount</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CInput type='text' placeholder='Set New Amount...' onChange={(e) => setAmount(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol xs="12" md="2">
                                                                        <CButton color='info' onClick={updateItemAmount}>Update</CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <hr/>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Price</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CInput type='text' placeholder='Set New Price...' onChange={(e) => setPrice(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol xs="12" md="2">
                                                                        <CButton color='info' onClick={updateItemPrice}>Update</CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                                <hr/>
                                                                <CFormGroup row>
                                                                    <CCol md="2">
                                                                        <CLabel htmlFor="input"><b>Limit Default</b></CLabel>
                                                                    </CCol>
                                                                    :
                                                                    <CCol xs="12" md="4">
                                                                        <CInput type='text' placeholder='Set New Limit...' onChange={(e) => setLimit_default(e.target.value)} />
                                                                    </CCol>
                                                                    <CCol xs="12" md="2">
                                                                        <CButton color='info' onClick={updateItemDefaultLimit}>Update</CButton>
                                                                    </CCol>
                                                                </CFormGroup>
                                                            </CCol>
                                                        </CRow>
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        }
                                }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </div>
    )
}

export default LotusItem