import React from 'react'

import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CContainer,
    CNav,
    CNavItem,
    CNavLink,
    CTabContent,
    CTabPane,
    CTabs
} from '@coreui/react'
import LotusList from './LotusList'
import LotusItem from './LotusItem'
import LotusPeriod from './LotusPeriod'

function LotusShop() {
    return (
        <div>
            <CCol align='center'>
                <CCard style={{ background: '#9bf54c' }}>
                    <h4><b>Lotus Shop Management</b></h4>
                    <h6><i>Lotus Shop Settings</i></h6>
                </CCard>
            </CCol>
            <br />
            <CContainer>
                <CCard accentColor='primary'>
                    <CCardHeader>
                        Lotus Shop
                    </CCardHeader>
                    <CCardBody>
                        <CTabs>
                            <CNav variant='tabs'>
                                <CNavItem>
                                    <CNavLink>
                                        Lotus List
                                    </CNavLink>
                                </CNavItem>
                                <CNavItem>
                                    <CNavLink>
                                        Lotus Item
                                    </CNavLink>
                                </CNavItem>
                                <CNavItem>
                                    <CNavLink>
                                        Lotus Period
                                    </CNavLink>
                                </CNavItem>
                            </CNav>
                            <CTabContent>
                                <CTabPane>
                                    <LotusList />
                                </CTabPane>
                                <CTabPane>
                                    <LotusItem />
                                </CTabPane>
                                <CTabPane>
                                    <LotusPeriod />
                                </CTabPane>
                            </CTabContent>
                        </CTabs>
                    </CCardBody>
                </CCard>
            </CContainer>
        </div>
    )
}

export default LotusShop
