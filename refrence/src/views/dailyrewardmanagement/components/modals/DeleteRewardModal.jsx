import { Modal } from "react-bootstrap";
import { DeleteReward } from "src/api/dailyrewardAPI/DailyRewardDeleteRequest";

export const DeleteRewardModal = ({
    id,
    _day,
    setDeleteItemModal,
    setDailyRewardList,
    deleteItemModal,
  }) => {
    return (
      <Modal
        show={deleteItemModal}
        onHide={() => setDeleteItemModal(!deleteItemModal)}
        animation={false}
      >
        <Modal.Header className="bg-danger" closeButton>
          <Modal.Title>You're About To Delete this Item?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="col" align="center">
            <h4>Are You Sure?</h4>
            <small>(item ID : {id})</small>
          </div>
          
        </Modal.Body>
        <Modal.Footer>
          <button
            className="btn btn-secondary btn-sm"
            onClick={() => setDeleteItemModal(!deleteItemModal)}
          >
            Cancel
          </button>
          {DeleteReward({ setDeleteItemModal, _day, id, setDailyRewardList })}
        </Modal.Footer>
      </Modal>
    );
  };
  