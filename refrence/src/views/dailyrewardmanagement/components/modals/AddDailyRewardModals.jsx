import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import { AddDailyRewards } from "src/api/dailyrewardAPI/DailyRewardPostRequest";
import { DailyLoot } from "src/services/dailyreward.services/dailyReward.function";
import DailyRewardsAddListDatatable from "../datatable/DailyRewardsAddListDatatable";
import { AddDailyRewardForm } from "../forms/AddDailyRewardForm";

function AddDailyRewardModals({
  addItemModal,
  setAddItemModal,
  setDaysCount,
  setItemtype,
  itemtype,
  itemid,
  daysCount,
  setDailyRewardLoot,
  dailyRewardLoot,
  setItemid,
  amount,
  setAmount,
  dailyid,
  setDailyid,
  day,
  setDay,
  setDailyRewardList
}) {
  return (
    <Modal
      show={addItemModal}
      size="xl"
      animation={false}
      onHide={() => setAddItemModal(!addItemModal)}
    >
      <Modal.Header className="bg-primary" closeButton>
        <Modal.Title>Add Daily Reward</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <AddDailyRewardForm
          setDaysCount={setDaysCount}
          setItemtype={setItemtype}
          itemtype={itemtype}
          setDailyRewardLoot={setDailyRewardLoot}
          dailyRewardLoot={dailyRewardLoot}
          setDailyid={setDailyid}
          dailyid={dailyid}
        />
        <hr />
        {daysCount === 0 ? (
          <></>
        ) : (
          <DailyRewardsAddListDatatable
            setItemtype={setItemtype}
            itemtype={itemtype}
            daysCount={daysCount}
            setDailyRewardLoot={setDailyRewardLoot}
            dailyRewardLoot={dailyRewardLoot}
            itemid={itemid}
            setItemid={setItemid}
            amount={amount}
            setAmount={setAmount}
            day={day}
            setDay={setDay}
          />
        )}
        <br />
        {DailyLoot({ dailyRewardLoot, setDailyRewardLoot })}
        {dailyRewardLoot.length > 2 ? (
          <div>
            <br />
            <div align="right">
              <button
                className="btn btn-danger btn-sm"
                onClick={() => setDailyRewardLoot([])}
              >
                Remove All
              </button>
            </div>
          </div>
        ) : (
          <></>
        )}
      </Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => setAddItemModal(!addItemModal)}
        >
          Cancel
        </button>
        {dailyRewardLoot.length === parseInt(daysCount) ? (
          <AddDailyRewards
            dailyid={dailyid}
            setDailyRewardList={setDailyRewardList}
            setAddItemModal={setAddItemModal}
            dailyRewardLoot={dailyRewardLoot}
          />
        ) : (
          <>
            <button className="btn btn-md btn-primary" disabled>
              Add Rewards
            </button>
          </>
        )}
      </Modal.Footer>
    </Modal>
  );
}

export default AddDailyRewardModals;
