import { Form } from "react-bootstrap";
import { UpdateReward } from "src/api/dailyrewardAPI/DailyRewardPutRequest";
import { DailyRewardUpdateFunction } from "src/services/dailyreward.services/dailyReward.function";

export const UpdateDailyRewardForm = ({
  setDailyRewardList,
  newitemtype,
  setNewitemtype,
  newitemid,
  setNewitemid,
  newamount,
  setNewamount,
  id,
  _day,
}) => {
  return (
    <Form>
      <Form.Group className="mb-3" controlId="formDailyRewardsItemType">
        <div className="row">
          <div className="col-lg-2 col-md-3">
            <Form.Label>
              <b>Item Type</b>
            </Form.Label>
          </div>
          :
          <div className="col-lg-5 col-md-8">
            <Form.Select
              className="col-md-9"
              aria-label="Select"
              onChange={(e) => setNewitemtype(e.target.value)}
            >
              <option value={""}>Choose Item Type</option>
              <option value={1}>Currency</option>
              <option value={6}>Box</option>
              <option value={5}>Items</option>
              <option value={2}>Ksatriya</option>
              <option value={3}>KSA Skins</option>
              <option value={4}>Rune</option>
              <option value={11}>Frame</option>
              <option value={12}>Avatar</option>
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formDailyRewardsItemID">
        <div className="row">
          <div className="col-lg-2 col-md-3">
            <Form.Label>
              <b>Item ID</b>
            </Form.Label>
          </div>
          :
          <div className="col-lg-5 col-md-8">
            <Form.Select
              className="col-md-9"
              aria-label="Select"
              onChange={(e) => setNewitemid(e.target.value)}
            >
              {DailyRewardUpdateFunction({ newitemtype, newitemid })}
            </Form.Select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formDailyRewardsItemAmount">
        <div className="row">
          <div className="col-lg-2 col-md-3">
            <Form.Label>
              <b>Amount</b>
            </Form.Label>
          </div>
          :
          <div className="col-lg-3 col-md-6">
            <Form.Control
              type="number"
              placeholder="Set Item Amount..."
              onChange={(e) => setNewamount(e.target.value)}
            />
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formDailyRewardsItemAmount">
        <div className="row">
          {newitemtype === "" || newamount === "" ? (
            <div className="col-lg-6 col-md-9" align="right">
              <button className="btn btn-md btn-info" disabled>
                Update
              </button>
            </div>
          ) : (
            <div className="col-md-6" align="right">
              {UpdateReward({
                setDailyRewardList,
                newitemtype,
                setNewitemtype,
                newitemid,
                setNewitemid,
                newamount,
                setNewamount,
                id,
                _day,
              })}
            </div>
          )}
        </div>
      </Form.Group>
    </Form>
  );
};
