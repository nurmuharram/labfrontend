import { useState } from "react";
import { Form } from "react-bootstrap";
import { GetDailyMonthYear } from "src/api/dailyrewardAPI/DailyRewardGetRequest";

export const AddDailyRewardForm = ({ setDaysCount, setDailyid }) => {
  const [monthYear, setMonthYear] = useState([]);

  GetDailyMonthYear({ setMonthYear });

  const dropdownMonthYear = monthYear.map((item, index) => {
    return (
      <option value={item.daily_id}>
        {item.month === 1
          ? "Jan"
          : item.month === 2
          ? "Feb"
          : item.month === 3
          ? "Mar"
          : item.month === 4
          ? "Apr"
          : item.month === 5
          ? "May"
          : item.month === 6
          ? "Jun"
          : item.month === 7
          ? "Jul"
          : item.month === 8
          ? "Aug"
          : item.month === 9
          ? "Sep"
          : item.month === 10
          ? "Oct"
          : item.month === 11
          ? "Nov"
          : item.month === 12
          ? "Dec"
          : item.month}{" "}
        - {item.year}
      </option>
    );
  });

  return (
    <Form>
      <Form.Group className="mb-3" controlId="formYearMonth">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Month-Year</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-4">
            <select onChange={(e) => setDailyid(e.target.value)}>{dropdownMonthYear}</select>
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formNumberDays">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>Number of Days</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-4">
            <select onChange={(e) => setDaysCount(e.target.value)}>
              <option value={28}>28 Days</option>
              <option value={29}>29 Days</option>
              <option value={30}>30 Days</option>
              <option value={31} selected>31 Days</option>
            </select>
          </div>
        </div>
      </Form.Group>
    </Form>
  );
};
