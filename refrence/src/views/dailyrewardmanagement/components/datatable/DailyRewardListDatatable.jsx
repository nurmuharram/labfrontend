import React, { useState } from "react";
import { Collapse } from "react-bootstrap";
import { GetDailyRewardList } from "src/api/dailyrewardAPI/DailyRewardGetRequest";
import DataTable from "src/components/table/DataTable";
import { toggleDetails } from "src/services/dailyreward.services/dailyReward.function";
import { UpdateDailyRewardForm } from "../forms/UpdateDailyRewardForm";
import { DeleteRewardModal } from "../modals/DeleteRewardModal";

function DailyRewardListDatatable({
  dailyRewardList,
  setDailyRewardList,
  newitemtype,
  setNewitemtype,
  newitemid,
  setNewitemid,
  newamount,
  setNewamount,
}) {
  const [deleteItemModal, setDeleteItemModal] = useState(false);
  const [details, setDetails] = useState([]);
  GetDailyRewardList({ setDailyRewardList });

  const fields = [
    { key: "id", _style: { width: "7%" } },
    { key: "day" },
    { key: "month" },
    { key: "year" },
    { key: "item_type_name" },
    { key: "item_name" },
    { key: "amount" },
    {
      key: "show_details",
      label: "",
      _style: { width: "14%" },
      sorter: false,
      filter: false,
    },
  ];

  return (
    <div>
      <DataTable
        items={dailyRewardList}
        fields={fields}
        itemsPerPage={10}
        sorterValue={{ column: "year", asc: false }}
        pagination
        scopedSlots={{
          id: (item, index) => {
            return <td>{item.daily_id}</td>;
          },
          month: (item, index) => {
            return (
              <td>
                {item.month === 1
                  ? "January"
                  : item.month === 2
                  ? "February"
                  : item.month === 3
                  ? "March"
                  : item.month === 4
                  ? "April"
                  : item.month === 5
                  ? "May"
                  : item.month === 6
                  ? "June"
                  : item.month === 7
                  ? "July"
                  : item.month === 8
                  ? "August"
                  : item.month === 9
                  ? "September"
                  : item.month === 10
                  ? "October"
                  : item.month === 11
                  ? "November"
                  : item.month === 12
                  ? "December"
                  : item.month}
              </td>
            );
          },
          show_details: (item, index) => {
            return (
              <td className="py-2">
                <button
                  className="btn btn-outline-primary btn-sm"
                  type="button"
                  onClick={() => {
                    toggleDetails(index, { setDetails, details });
                  }}
                >
                  {details.includes(index) ? "Hide" : "Edit/Update"}
                </button>
              </td>
            );
          },
          details: (item, index) => {
            const id = item.daily_id;
            const _day = item.day;
            return (
              <Collapse in={details.includes(index)}>
                <div className="card-body">
                  <div className="row">
                    <div className="col" align="left">
                      <h4>
                        <i>
                          Edit/Update Item (<small>ID : {id}</small>)
                        </i>
                      </h4>
                    </div>
                    <div className="col-lg-5 col-md-3" align="left">
                      <button
                        className="btn btn-outline-danger btn-sm"
                        type="button"
                        onClick={() => setDeleteItemModal(!deleteItemModal)}
                      >
                        Delete Item
                      </button>
                      <DeleteRewardModal
                        id={id}
                        _day={_day}
                        setDeleteItemModal={setDeleteItemModal}
                        setDailyRewardList={setDailyRewardList}
                        deleteItemModal={deleteItemModal}
                      />
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col" align="left">
                      <UpdateDailyRewardForm
                        id={id}
                        _day={_day}
                        setDailyRewardList={setDailyRewardList}
                        newitemtype={newitemtype}
                        setNewitemtype={setNewitemtype}
                        newitemid={newitemid}
                        setNewitemid={setNewitemid}
                        newamount={newamount}
                        setNewamount={setNewamount}
                      />
                    </div>
                  </div>
                </div>
              </Collapse>
            );
          },
        }}
      />
    </div>
  );
}

export default DailyRewardListDatatable;
