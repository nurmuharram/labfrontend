import React, { useState } from "react";
import DataTable from "src/components/table/DataTable";
import { DailyRewardFunction } from "src/services/dailyreward.services/dailyReward.function";
import days from "../../days";
import Table from "react-bootstrap/Table";
import { Button, Form } from "react-bootstrap";

function DailyRewardsAddListDatatable({
  setItemtype,
  itemtype,
  daysCount,
  setDailyRewardLoot,
  dailyRewardLoot,
  itemid,
  setItemid,
  amount,
  setAmount,
  day,
  setDay,
}) {
  const [itemTypeName, setItemTypeName] = useState("");
  const [itemName, setItemName] = useState();
  const data = days;
  const fields = [
    { key: "day", _style: { width: "10%" } },
    { key: "item_type" },
    { key: "item_name" },
    { key: "amount" },
    {
      key: "option",
      label: "",
      _style: { width: "10%" },
      sorter: false,
      filter: false,
    },
  ];

  function userExists(day) {
    return dailyRewardLoot.some(function(item) {
      return item.day === day;
    }); 
  }

  const ValidCheck = () => {
    if (itemtype === '' || amount === ''){
      return (<button disabled className="btn btn-primary">Add</button>)
    } else {
      return (<button
        className="btn btn-primary"
        onClick={() =>
          setDailyRewardLoot([
            ...dailyRewardLoot,
            {
              day: parseInt(day),
              item_type: parseInt(itemtype),
              item_id: parseInt(itemid),
              amount: parseInt(amount),
            },
          ])
        }
      >
        Add
      </button>)
    }
  }

  return (
    <div className="fade-in">
      <div align="center">
        <Form.Label>
          <i>
            <b>Add Rewards</b>
          </i>
        </Form.Label>
      </div>
      <Table size="sm" responsive>
        <thead>
          <th>Day</th>
          <th>Item Type</th>
          <th>Item Name</th>
          <th>Amount</th>
          <th> </th>
        </thead>
        <tbody>
          <tr>
            <td>
              <select onChange={(e) => setDay(e.target.value)}>
                {days
                  .map((day) => {
                    return <option value={day.day}>Day {day.day}</option>;
                  })
                  .slice(
                    0,
                    daysCount === "28"
                      ? 28
                      : daysCount === "29"
                      ? 29
                      : daysCount === "30"
                      ? 30
                      : daysCount === "31"
                      ? 31
                      : 31
                  )}
              </select>
            </td>
            <td>
              <select
                className="form-select"
                onChange={(e) => {
                  setItemtype(e.target.value);
                }}
              >
                <option value={""}>Choose Item Type</option>
                <option value={1} title="currency">
                  Currency
                </option>
                <option value={6}>Box</option>
                <option value={5}>Items</option>
                <option value={2}>Ksatriya</option>
                <option value={3}>KSA Skins</option>
                <option value={4}>Rune</option>
                <option value={11}>Frame</option>
                <option value={12}>Avatar</option>
              </select>
            </td>
            <td>
              <select onChange={(e) => setItemid(e.target.value)}>
                {DailyRewardFunction({ itemtype })}
              </select>
            </td>
            <td className="col-md-2">
              <input
                type="number"
                className="form-control form-control-md"
                placeholder="Set Item Amount..."
                onChange={(e) => setAmount(e.target.value)}
              />
            </td>
            <td>
              {userExists(parseInt(day)) === false ? (
                ValidCheck()
              ) : (
                <div>&#10004;</div>
              )}
            </td>
          </tr>
        </tbody>
      </Table>
      <hr />
    </div>
  );
}

export default DailyRewardsAddListDatatable;
