import React, { useState } from "react";
import DailyRewardListDatatable from "./components/datatable/DailyRewardListDatatable";
import AddDailyRewardModals from "./components/modals/AddDailyRewardModals";

function DailyRewardManagement() {
  const [addItemModal, setAddItemModal] = useState(false);
  const [daysCount, setDaysCount] = useState(0);
  const [dailyRewardList, setDailyRewardList] = useState([]);

  const [itemtype, setItemtype] = useState("");
  const [itemid, setItemid] = useState(1);
  const [amount, setAmount] = useState("");
  const [dailyid, setDailyid] = useState(1);
  const [day, setDay] = useState(1);

  const [newitemtype, setNewitemtype] = useState("");
  const [newitemid, setNewitemid] = useState(1);
  const [newamount, setNewamount] = useState("");

  const [dailyRewardLoot, setDailyRewardLoot] = useState([]);

  return (
    <div>
      <div className="container-fluid">
        <div className="card card-accent-primary">
          <div className="card-header" align="center">
            <h5>
              <b>Daily Reward Settings</b>
            </h5>
          </div>
          <div className="card-body">
            <div className="col">
              <button
                type="button"
                className="btn btn-primary"
                onClick={() => setAddItemModal(!addItemModal)}
              >
                + Add Daily Reward
              </button>
              <AddDailyRewardModals
                addItemModal={addItemModal}
                setAddItemModal={setAddItemModal}
                setDaysCount={setDaysCount}
                setItemtype={setItemtype}
                itemtype={itemtype}
                itemid={itemid}
                setItemid={setItemid}
                amount={amount}
                setAmount={setAmount}
                dailyid={dailyid}
                setDailyid={setDailyid}
                daysCount={daysCount}
                setDailyRewardLoot={setDailyRewardLoot}
                dailyRewardLoot={dailyRewardLoot}
                day={day}
                setDay={setDay}
                setDailyRewardList={setDailyRewardList}
              />
            </div>
            <br />
            <div className="col">
              <DailyRewardListDatatable
                dailyRewardList={dailyRewardList}
                setDailyRewardList={setDailyRewardList}
                newitemtype={newitemtype}
                setNewitemtype={setNewitemtype}
                newitemid={newitemid}
                setNewitemid={setNewitemid}
                newamount={newamount}
                setNewamount={setNewamount}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DailyRewardManagement;
