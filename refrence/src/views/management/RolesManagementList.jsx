import React, { useState, useEffect } from "react";
import { CBadge, CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CCollapse, CDataTable, CFormGroup, CLabel, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CRow, CSwitch } from "@coreui/react";


import { useHistory } from 'react-router'
import RoleRegister from "./RolesRegister";
import axios from "axios";
import DataTable from "src/components/table/DataTable";
import { Modal, ModalBody, ModalFooter, ModalHeader, ModalTitle } from "src/components/modal";


function RolesManagement(props) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const history = useHistory();

  const [details, setDetails] = useState([])
  const [data, setData] = useState([])
  const [permissions, setPermissions] = useState([])

  const [deleteItemModal, setDeleteItemModal] = useState(false)

  const [modifyuser, setModifyuser] = useState()
  const [shopcontrol, setShopcontrol] = useState()
  const [playerreportcont, setPlayerreportcont] = useState()
  const [sendmail, setSendmail] = useState()
  const [matches, setMatches] = useState()
  const [ksarotation, setKsarotation] = useState()
  const [playerreports, setPlayerreports] = useState()
  const [banplayerchat, setBanplayerchat] = useState()
  const [vouchercontrol, setVouchercontrol] = useState()
  const [judgecontrol, setJudgecontrol] = useState()
  const [userstats, setUserstats] = useState()

  useEffect(() => {
    if (props.error === "Unauthorized") {
      history.push("login")
    }
  }, [props.error])
  console.log('ini utk error', props.error)

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/role/getAll`, config).then(res => {
      const items = res.data;
      setData(items);
    });
  }, [])

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/role/getAllRolesPermission`, config).then(res => {
      const items = res.data;
      setPermissions(items);
    });
  }, [])

  const refreshrolelist = () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/role/getAllRolesPermission`, config).then(res => {
      const items = res.data;
      setPermissions(items);
    });
  }

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }

  /* Table Data */
  const fields = [
    { key: 'role_id', _style: { width: '5%' } },
    { key: 'role_name', _style: { width: '10%' } },
    { key: 'permission_name', _style: { width: '30%' } },
    {
      key: 'show_details',
      label: '',
      _style: { width: '15%' },
      sorter: false,
      filter: false
    }
    /* { key: 'Approve Report', _style: { width: '10%' } } */
  ]

  return (
    <div>
      <div className="card">
        <div className="card-body">
        <DataTable
            items={permissions}
            fields={fields}
            hover
            scopedSlots={{
              'permission_name':
                (item, index) => {
                  return (
                    <td>
                      {item.permission_name && item.permission_name.split(",").map(desc =>
                        <CBadge color="info" style={{ width: '8rem', marginRight: '0.5rem' }}>
                          {desc}
                        </CBadge>
                      )}
                    </td>
                  )
                },
              'show_details':
                (item, index) => {


                  const deleteRole = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/delete?id=${item.role_id}`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },

                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                        alert('Role Deleted')
                        setDeleteItemModal(false)
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed Delete Role!')
                      });
                    // setTimeout(refreshrolelist, 100)
                  }


                  return (
                    <td className="py-1">
                      {item.id === '1' ? <></> : <CButton
                        color="info"
                        shape="round"
                        size="sm"
                        variant="outline"
                        onClick={() => { toggleDetails(index) }}
                      >
                        {details.includes(index) ? 'Close' : 'Edit Permission'}
                      </CButton>}

                      <hr />
                      <button
                            className="btn btn-outline-danger btn-sm"
                            type="button"
                            onClick={() => setDeleteItemModal(!deleteItemModal)}
                          >
                            Delete Role
                      </button>

                      <Modal
                            show={deleteItemModal}
                            onClose={() => setDeleteItemModal(!deleteItemModal)}
                            color="primary"
                            size="md"
                          >
                            <ModalHeader className="bg-danger" closeButton>
                              <ModalTitle>You're About To Delete this Item?</ModalTitle>
                            </ModalHeader>
                            <ModalBody>
                              <div className="col" align="center">
                                <h4>Are You Sure?</h4>
                                <small>(item ID : {item.id})</small>
                              </div>

                            </ModalBody>
                            <ModalFooter>
                              <button
                                className="btn btn-secondary btn-sm"
                                onClick={() => setDeleteItemModal(!deleteItemModal)}
                              >
                                Cancel
                              </button>
                              <button
                                className="btn btn-danger btn-sm"
                                onClick={deleteRole}
                              >
                                Delete
                              </button>
                            </ModalFooter>
                          </Modal>
                    </td>

                  )
                },
              'details':
                (item, index) => {

                  const permissionname = item.permission_name && item.permission_name.split(",").map(desc => {
                    return desc;
                  });

                  const switchOnModifyuser = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=1`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },

                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffModifyuser = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=1`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },

                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnSendmail = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=4`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffSendmail = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=4`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnShop = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=2`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffShop = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=2`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnPlayerReportcont = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=3`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffPlayerReportCont = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=3`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnMatches = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=5`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffMatches = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=5`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnKsarotation = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=6`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffKsarotation = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=6`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnplayerreports = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=7`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffplayerreports = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=7`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnbanchat = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=8`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffbanchat = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=8`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnvoucher = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=9`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffvoucher = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=9`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnjudgecont = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=10`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffjudgecont = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=10`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnuserstats = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=11`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffuserstats = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=11`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnseasonmanagement = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=12`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffseasonmanagement = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=12`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnguildmanagement = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=13`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffguildmanagement = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=13`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnnews = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=14`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffnews = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=14`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnSeason = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=12`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffSeason = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=12`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnEvent = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=15`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffEvent = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=15`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnMT = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=16`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffMT = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=16`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOnDailyReward = () => {
                    const config = {
                      method: 'POST',
                      url: `/api/role/addPermissionToRole?role_id=${item.role_id}&permission_id=17`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  const switchOffDailyReward = () => {
                    const config = {
                      method: 'DELETE',
                      url: `/api/role/removePermissionFromRole?role_id=${item.role_id}&permission_id=17`,
                      headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      },
                    };

                    axios(config)
                      .then((response) => {
                        console.log(JSON.stringify(response.data));
                      })
                      .catch((error) => {
                        console.log(error);
                        alert('Failed!')
                      });
                    setTimeout(refreshrolelist, 100)
                  }

                  return (
                    <CCollapse show={details.includes(index)}>
                      <CCardHeader>
                        <CCol align='center'><i><b>Edit this Role Permission</b></i></CCol>
                      </CCardHeader>
                      <CCardBody>
                        <CRow>
                          <CCol align='left' md="4">
                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Modify User</CLabel>
                              </CCol>

                              {item.role_id === 1 ? <CCol xs="12" md="3">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('modify_user')}
                                  disabled />
                              </CCol> : <CCol xs="12" md="3">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('modify_user')}
                                  onChange={permissionname && permissionname.includes('modify_user') === false ? switchOnModifyuser : permissionname === null ? switchOnModifyuser : switchOffModifyuser} />
                              </CCol>}
                            </CFormGroup>

                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Shop Control</CLabel>
                              </CCol>

                              <CCol xs="12" md="3">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('shop_control')}
                                  onChange={permissionname && permissionname.includes('shop_control') === false ? switchOnShop : permissionname === null ? switchOnShop : switchOffShop} />
                              </CCol>
                            </CFormGroup>

                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Player Report Control</CLabel>
                              </CCol>

                              <CCol xs="12" md="3">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('player_report_control')}
                                  onChange={permissionname && permissionname.includes('player_report_control') === false ? switchOnPlayerReportcont : permissionname === null ? switchOnPlayerReportcont : switchOffPlayerReportCont} />
                              </CCol>
                            </CFormGroup>

                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Send Mail</CLabel>
                              </CCol>

                              <CCol xs="12" md="3">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname === null ? false : permissionname && permissionname.includes('send_mail')}
                                  onChange={permissionname && permissionname.includes('send_mail') === false ? switchOnSendmail : permissionname === null ? switchOnSendmail : switchOffSendmail} />
                              </CCol>
                            </CFormGroup>

                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Matches</CLabel>
                              </CCol>

                              <CCol xs="12" md="3">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('matches')}
                                  onChange={permissionname && permissionname.includes('matches') === false ? switchOnMatches : permissionname === null ? switchOnMatches : switchOffMatches} />
                              </CCol>
                            </CFormGroup>

                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">KSA Rotation</CLabel>
                              </CCol>

                              <CCol xs="12" md="3">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('ksa_rotation')}
                                  onChange={permissionname && permissionname.includes('ksa_rotation') === false ? switchOnKsarotation : permissionname === null ? switchOnKsarotation : switchOffKsarotation} />
                              </CCol>
                            </CFormGroup>
                          </CCol>

                          <CCol align='left' md="4">
                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Player Reports</CLabel>
                              </CCol>

                              <CCol xs="12" md="1">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('player_reports')}
                                  onChange={permissionname && permissionname.includes('player_reports') === false ? switchOnplayerreports : permissionname === null ? switchOnplayerreports : switchOffplayerreports} />
                              </CCol>
                            </CFormGroup>

                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Ban Players' Chat</CLabel>
                              </CCol>

                              <CCol xs="12" md="1">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('blacklist_player_chat')}
                                  onChange={permissionname && permissionname.includes('blacklist_player_chat') === false ? switchOnbanchat : permissionname === null ? switchOnbanchat : switchOffbanchat} />
                              </CCol>
                            </CFormGroup>

                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Voucher Control</CLabel>
                              </CCol>

                              <CCol xs="12" md="1">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('voucher_control')}
                                  onChange={permissionname && permissionname.includes('voucher_control') === false ? switchOnvoucher : permissionname === null ? switchOnvoucher : switchOffvoucher} />
                              </CCol>
                            </CFormGroup>

                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Judge Control</CLabel>
                              </CCol>

                              <CCol xs="12" md="1">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('judge_control')}
                                  onChange={permissionname && permissionname.includes('judge_control') === false ? switchOnjudgecont : permissionname === null ? switchOnjudgecont : switchOffjudgecont} />
                              </CCol>
                            </CFormGroup>
                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">User Statistics</CLabel>
                              </CCol>

                              <CCol xs="12" md="1">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('user_statistics')}
                                  onChange={permissionname && permissionname.includes('user_statistics') === false ? switchOnuserstats : permissionname === null ? switchOnuserstats : switchOffuserstats} />
                              </CCol>
                            </CFormGroup>
                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">News Management</CLabel>
                              </CCol>

                              <CCol xs="12" md="1">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('news_management')}
                                  onChange={permissionname && permissionname.includes('news_management') === false ? switchOnnews : permissionname === null ? switchOnnews : switchOffnews} />
                              </CCol>
                            </CFormGroup>
                          </CCol>

                          <CCol>
                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Season Management</CLabel>
                              </CCol>

                              <CCol xs="12" md="1">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('season_management')}
                                  onChange={permissionname && permissionname.includes('season_management') === false ? switchOnSeason : permissionname === null ? switchOnSeason : switchOffSeason} />
                              </CCol>
                            </CFormGroup>

                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Guild Management</CLabel>
                              </CCol>

                              <CCol xs="12" md="1">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('guild_management')}
                                  onChange={permissionname && permissionname.includes('guild_management') === false ? switchOnguildmanagement : permissionname === null ? switchOnguildmanagement : switchOffguildmanagement} />
                              </CCol>
                            </CFormGroup>

                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Event Management</CLabel>
                              </CCol>

                              <CCol xs="12" md="1">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('event_management')}
                                  onChange={permissionname && permissionname.includes('event_management') === false ? switchOnEvent : permissionname === null ? switchOnEvent : switchOffEvent} />
                              </CCol>
                            </CFormGroup>

                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Maintenance</CLabel>
                              </CCol>

                              <CCol xs="12" md="1">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('maintenance')}
                                  onChange={permissionname && permissionname.includes('maintenance') === false ? switchOnMT : permissionname === null ? switchOnMT : switchOffMT} />
                              </CCol>
                            </CFormGroup>

                            <CFormGroup row>
                              <CCol md="6" align='left'>
                                <CLabel htmlFor="input">Daily Reward Management</CLabel>
                              </CCol>

                              <CCol xs="12" md="1">
                                <CSwitch className={'mx-1'} shape={'pill'} color={'success'} labelOn={'\u2713'} labelOff={'\u2715'}
                                  checked={permissionname === null ? false : permissionname && permissionname.includes('daily_reward_management')}
                                  onChange={permissionname && permissionname.includes('daily_reward_management') === false ? switchOnDailyReward : permissionname === null ? switchOnDailyReward : switchOffDailyReward} />
                              </CCol>
                            </CFormGroup>
                          </CCol>
                        </CRow>


                      </CCardBody>
                    </CCollapse>
                  )
                }
            }}
          />

              <CCardFooter>
                <CCol align='right'>
                  <CButton color="primary" onClick={handleShow}>
                    Add New Roles
                  </CButton>
                  <CModal
                    show={show}
                    onHide={handleClose}
                    backdrop="static"
                    keyboard={false}
                  >
                    <CModalHeader closeButton>
                      <CModalTitle>Add New Role</CModalTitle>
                    </CModalHeader>
                    <CModalBody>
                      <RoleRegister />
                    </CModalBody>
                    <CModalFooter>
                      <CButton color="secondary" onClick={handleClose}>
                        Close
                      </CButton>
                    </CModalFooter>
                  </CModal>
                </CCol>
              </CCardFooter>
        </div>
      </div>

    </div>
  );
}

export default RolesManagement;