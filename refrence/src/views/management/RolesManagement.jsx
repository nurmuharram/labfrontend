
import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CContainer,
  CFormGroup,
  CLabel,
  CInput,
  CValidFeedback,
  CInvalidFeedback
  
} from '@coreui/react'

import RolesManagementList from './RolesManagementList'

function RolesManagement() {

  


  return (
    <div className="RolesManagement">
      <hr />
      {/* <h6>
        <i>Send player(s) RolesManagements and gifts</i>
      </h6> */}

      <CContainer fluid>
        
      
          <CCard accentColor="primary">
     
            <CCardBody> 

            <CTabs>
                <CNav variant="tabs">
                 
                  <CNavItem>
                    <CNavLink>
                      Roles List
                    </CNavLink>
                  </CNavItem>
                

                </CNav>
                <CTabContent>

                  {/*IMPORT Roles List*/}
                  <CTabPane>
                    <RolesManagementList />
                  </CTabPane>
                  {/* Send RolesManagement to Player(s) Tab */}
                  <CTabPane>
                  </CTabPane>
                  
                </CTabContent>
              </CTabs>



            </CCardBody>
          </CCard>

      </CContainer>
    </div>
  )
}

export default RolesManagement
