import React, { useState, useEffect } from 'react';
import axios from "axios";
import {
    CCard,
    CCardBody,
    CCardHeader,
    CTabs,
    CNav,
    CNavItem,
    CNavLink,
    CTabContent,
    CTabPane,
    CCardFooter,
    CCol,
    CRow,
    CForm,
    CModal,
    CModalHeader,
    CModalTitle,
    CFormGroup,
    CLabel,
    CTextarea,
    CSelect,
    CInput,
    CButton
    
  } from '@coreui/react'
  import { currentInfoAction } from "src/redux/action/currentInfoAction/currentInfoAction";
  import { useHistory, useLocation } from "react-router-dom";
  import { connect, useSelector, useDispatch } from "react-redux";
  
function UpdatePassword(props){
    const history =useHistory();
    const dispatch = useDispatch();

    useEffect(()=>{
        props.getInfo()
        console.log('ini data',props.data)
      },[])
   
      
    // const [fullName, setFullName] = useState('');
    // const [email, setEmail] = useState('');
    // const [role, setRole] = useState(1);
    const [id, setID] = useState(props.data.id);
    const [password, setPassword] = useState('');

    const [loading, setLoading] = useState(false);
    const [isError, setIsError] = useState(false);
    const [data, setData] = useState(null);



    const handleSubmit =  (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        
        // data.append('name', fullName)
        // data.append('email', email)
        data.append('password', password)
        data.append('id', id)
        // data.append('role_id', role)
        const config = {
            method: 'PUT',
            url: `api/user/updateUserPassword?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
                "Content-Type": "multipart/form-data"
            },
            data: data
        };

        // const handleSubmit =  (e) => {
        //     e.preventDefault()
        //     const FormData = require('form-data');
        //     const data = new FormData();
        //     var fs = require('fs');
        //     data.append('type', type)
        //     data.append('fullName', fullName)
        //     data.append('titleIN', titleIN)
        //     data.append('banner',fs.createReadStream (banner))
        //     data.append('contentEN',fs.createReadStream(contentEN))
        //     data.append('contentIN',fs.createReadStream (contentIN))
        //     data.append('release_date', release_date)


        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Update Password succeded')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed, check again! ')
            });
    }


    return (
        <>
            <CCard>


                <CCardBody>

                        <CCardBody >
                            <CForm action="" method="post" encType="multipart/form-data" className="form-horizontal" >
                                

                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="text-input" ><b>Full Name</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CInput id="fullName" name="subject" placeholder={props.data.name} disabled />
                                    </CCol>
                                </CFormGroup>


                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="text-input"><b>Email</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CInput id="email" name="subject"  placeholder={props.data.email} disabled />
                                    </CCol>
                                </CFormGroup>




                                <CFormGroup row>
                                    <CCol md="3" xs="9">
                                        <CLabel htmlFor="text-input"><b>Role</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CInput id="fullName" name="subject" placeholder={props.data.role_name} disabled />
                                    </CCol>
                                </CFormGroup>  

                                <CFormGroup row>
                                    <CCol md="3" xs="9">
                                        <CLabel htmlFor="text-input"><b>ID</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CInput placeholder={props.data.id} onChange={e => setID(e.target.value)} disabled/>
                                    </CCol>
                                </CFormGroup>  

                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="password-input"><b>Password</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CInput type="password" id="password" name="subject"  onChange={e => setPassword(e.target.value)} placeholder="Password" />
                                    </CCol>
                                </CFormGroup>

                                <CButton color="primary"  className="mr-1"type="submit" onClick={handleSubmit} >
                                    Update Password
                                </CButton>
                            </CForm>
                        </CCardBody>
                        <CCardFooter>

                        </CCardFooter>
                </CCardBody>

            </CCard>
        </>
    )


}

const mapDispatchToProps = (dispatch) => ({
    getInfo: () => dispatch(currentInfoAction()),
  });
  const mapStateToProps = (state) => ({
    isLoading: state.currentInfoReducer.isLoading,
    error: state.currentInfoReducer.error,
    data: state.currentInfoReducer.data,
  });


export default connect(mapStateToProps, mapDispatchToProps)(UpdatePassword);
