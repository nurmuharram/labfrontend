import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { Button, Modal } from "react-bootstrap";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CContainer,
} from "@coreui/react";

import toast, { Toaster } from 'react-hot-toast';

import { useSelector, useDispatch } from "react-redux";
import { connect } from "react-redux";
import { getUsers} from "src/redux/action/userManagementAction";
import { getRoles, rolesManagementAction } from "src/redux/action/rolesManagementAction/rolesManagementAction";
import UserRegister from "./UserRegister";

const UserManagementListFilter = (props) => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const history = useHistory();
  const queryPage = useLocation().search.match(/page=([0-9]+)/, "");
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1);
  const [page, setPage] = useState(currentPage);

  const pageChange = (newPage) => {
    currentPage !== newPage && history.push(`/?page=${newPage}`);
  };
 /*  useEffect(() => {
    props.getUser();
    console.log("ini data", props.data);
    setTimeout(100)
  }, []); */
  useEffect(() => {
    currentPage !== page && setPage(currentPage);
  }, [currentPage, page]);

  useEffect(() => {
    if (props.error?.response?.status===401) {
    history.push("/login")
    }
    },[props.error])
    // console.log('ini utk error',props.error?.response)

  useEffect(() => {
    if (props.error?.response?.status===502) {
      window.location.reload("/dashboard")
      return false;
    }
    },[props.error])  

  const dispatch = useDispatch();
  const data = useSelector((state) => state.users.users);
  const roles = useSelector((state) => state.roles.roles);
  useEffect(() => {
    dispatch(getRoles())
  }, [])
  useEffect(() => {
    dispatch(getUsers());
  }, []);


  const refreshlist = () => {
    dispatch(getUsers());
  };

  return (
    <>
      <CContainer>
        <Toaster 
        containerStyle={{
          top: 100,
          left: 300,
          bottom: 20,
          right: 20,
        }}
        />
        <CDataTable
          items={data}
          fields={[
            {
              key: "id",
              _classes: "font-weight-bold",
              _style: {
                width: "9%",
              },
            },
            "name",
            {
              key: "email",
              filter: false,
            },
            {
              key: "role_name",
              filter: false,
            },
            {
              key: "Role_id",
              filter: false,
              sorter: false,
            },
            
          ]}
          columnFilter
          //tableFilter
          activePage={page}
          itemsPerPageSelect
          clickableRows={pageChange}
          itemsPerPage={5}
          hover
          striped
          sorter
          pagination
          // onRowClick={(item) =>
          //   history.push(`/usermanagementdetail/${item.id}`)
          // }
        />

        <p align="right">
          <Button variant="success" onClick={handleShow}>
            Register New User{" "}
          </Button>{" "}
        </p>
      </CContainer>{" "}
      {/* <!--- Model Box ---> */}{" "}
      <div className="model_box">
        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title> Register New User </Modal.Title>{" "}
          </Modal.Header>{" "}
          <Modal.Body>
            <UserRegister refreshlist={refreshlist} setShow={setShow} roles={roles} />

            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close{" "}
              </Button>{" "}
            </Modal.Footer>
          </Modal.Body>
        </Modal>
        {/* Model Box Finsihs */}{" "}
      </div>{" "}
    </>
  );
};

export default UserManagementListFilter
