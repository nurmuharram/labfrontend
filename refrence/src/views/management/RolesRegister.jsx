import React, { useState, useEffect } from 'react';
import axios from "axios";
import {
    CCard,
    CCardBody,
    CCardHeader,
    CTabs,
    CNav,
    CNavItem,
    CNavLink,
    CTabContent,
    CTabPane,
    CCardFooter,
    CCol,
    CRow,
    CForm,
    CModal,
    CModalHeader,
    CModalTitle,
    CFormGroup,
    CLabel,
    CTextarea,
    CSelect,
    CInput,
    CButton
    
  } from '@coreui/react'

  
export default function RoleRegister(props) {
    
   
    const [role_name, setRoleName] = useState('');
    // const [id, setRoleID] = useState('');
    const [description, setDescription] = useState('');

    const [loading, setLoading] = useState(false);
    const [isError, setIsError] = useState(false);
    const [data, setData] = useState(null);



    const handleSubmit =  (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        
        data.append('role_name', role_name)
        // data.append('id', id)
        data.append('description', description)
        const config = {
            method: 'POST',
            url: `api/role/add`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
                "Content-Type": "multipart/form-data"
            },
            data: data
        };
        // const handleSubmit =  (e) => {
        //     e.preventDefault()
        //     const FormData = require('form-data');
        //     const data = new FormData();
        //     var fs = require('fs');
        //     data.append('type', type)
        //     data.append('fullName', fullName)
        //     data.append('titleIN', titleIN)
        //     data.append('banner',fs.createReadStream (banner))
        //     data.append('contentEN',fs.createReadStream(contentEN))
        //     data.append('contentIN',fs.createReadStream (contentIN))
        //     data.append('release_date', release_date)


        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('New Role Added!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add New Role!')
            });
    }


    return (
        <>
            <CCard>


                <CCardBody>

                        <CCardBody>
                            <CForm action="" method="post" encType="multipart/form-data" className="form-horizontal" >
                                

                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="text-input"><b>Role Name</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CInput name="subject" onChange={e => setRoleName(e.target.value)} placeholder="e.g: game_master" />
                                    </CCol>
                                </CFormGroup>

{/* 
                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="text-input"><b>Role ID</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CInput type="number" id="number" name="subject"  onChange={e => setRoleID(e.target.value)} placeholder="Role ID Must Be Number" />
                                    </CCol>
                                </CFormGroup> */}


                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="text-input"><b>Description</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CInput type="text" id="number" name="subject"  onChange={e => setDescription(e.target.value)} placeholder=" e.g: Game Master " />
                                    </CCol>
                                </CFormGroup>


                                <CButton color="primary"  className="mr-1"type="submit" onClick={handleSubmit} >
                                    Add New Role
                                </CButton>
                            </CForm>
                        </CCardBody>
                        <CCardFooter>

                        </CCardFooter>
                </CCardBody>

            </CCard>
        </>
    )
}
