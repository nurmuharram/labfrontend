import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CContainer,
  
} from '@coreui/react'

import RolesManagement from './RolesManagement'
import UserManagementPage from './UserManagement'

function UserManagement() {

  


  return (
    <div className="UserManagement">
      <CCol align='center'>
        <CCard style={{ background: '#9bf54c' }}>
          <h4>
          <b>Management Dashboard</b>
          </h4>
          <h6>
            <i>User/Role/Permission Settings</i>
          </h6>
        </CCard>
      </CCol>
      <br />

      <CContainer fluid>
        <CCol className="mb-4">
          <CCard accentColor="primary">
     
            <CCardBody> 
              


            <CTabs>
                <CNav variant="tabs">
                 
                  <CNavItem>
                    <CNavLink>
                      User Management
                    </CNavLink>
                  </CNavItem>
                  <CNavItem>
                    <CNavLink>
                      Roles Management
                    </CNavLink>
                  </CNavItem>

                </CNav>
                <CTabContent>

                  {/*IMPORT User List*/}
                  <CTabPane>
                    <UserManagementPage />
                  </CTabPane>
                  {/* Send UserManagement to Player(s) Tab */}
                  <CTabPane>
                    <RolesManagement/>
                  </CTabPane>
                  <CTabPane>


                  </CTabPane>
                  <CTabPane>

                  </CTabPane>
                </CTabContent>
              </CTabs>



            </CCardBody>
          </CCard>
        </CCol>
      </CContainer>
    </div>
  )
}

export default UserManagement
