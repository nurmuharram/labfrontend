import React, { useState, useEffect } from "react";
import { Button, Modal, Input, Form } from "react-bootstrap";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CCardFooter,
  CCol,
  CRow,
  CForm,
  CModal,
  CModalHeader,
  CModalTitle,
  CFormGroup,
  CLabel,
  CTextarea,
  CSelect,
  CInput,
  CButton,
} from "@coreui/react";
import { useHistory, useLocation } from "react-router-dom";
import { connect, useSelector, useDispatch } from "react-redux";
import axios from "axios";
import { rolesManagementAction, getRoles } from "src/redux/action/rolesManagementAction/rolesManagementAction";
import { getUsers } from "src/redux/action/userManagementAction";
//import { userManagementAction } from "src/redux/action/userManagementAction";
function AssignRoleToUser(props) {
  const dispatch = useDispatch();

  const [pageNumber, setPageNumber] = useState(0);
  const newsPerPage = 5;
  const pagesVisited = pageNumber * newsPerPage;

  // const AssignRoleToUser = useSelector((state) => state.data);
  // useEffect(() => {
  //     dispatch(getUserAction());
  // }, []);
  const [user_id, setUserid] = useState("");
  const [role_id, setRoleid] = useState(1);
  // const [newsSearch, setNewsSearch] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();

    data.append("user_id", user_id);
    data.append("role_id", role_id);
    const config = {
        method: 'POST',
        url: `api/role/addRoleToUser?user_id=${user_id}&role_id=${role_id}`,
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
            "Content-Type": "multipart/form-data"
        },
        data: data
    };

    axios(config)
        .then((response) => {
            console.log(JSON.stringify(response.data));
            alert('Assign Role Success')
        })
        .catch((error) => {
            console.log(error);
            alert('Failed to Assign Role!')
        });
  };
  const users = useSelector((state) => state.users.users)
  useEffect(() => {
    dispatch(getUsers())
  }, [])

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const roles = useSelector((state) => state.roles.roles);
  useEffect(()=>{
    dispatch(getRoles())
    console.log('ini data',props.data)
  },[])
  //const pageCount = Math.ceil(props.dataUserManagement.length / newsPerPage);
  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };


  return (
    <div class="container ">
      <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
        <div>
            <center>
            <CCardBody>
              <CCardBody>
                <CForm
                  action=""
                  method="post"
                  encType="multipart/form-data"
                  className="form-horizontal"
                >
                  <CFormGroup row>
                    <CCol md="3" xs="9">
                      <CLabel htmlFor="text-input">
                        <b>Select User</b>
                      </CLabel>
                    </CCol>
                    <CCol xs="12" md="9">
                      <select  class="form-control form-control"onChange={(e) => setUserid(e.target.value)}>
                      <option disabled>Select User</option>
                        {users.length > 0 &&
                          users.map((item, index) => {
                            return (
                             
                              <option value={item.id}>
                                {item.id} - {item.name}
                              </option>
                            );
                          })}

                        {/* {listDropdown} */}
                        {/* <option values='1'>Whats's New</option> */}
                      </select>
                    </CCol>
                  </CFormGroup>

                  <CFormGroup row>
                    <CCol md="3" xs="9">
                      <CLabel htmlFor="text-input">
                        <b>Select Role</b>
                      </CLabel>
                    </CCol>
                    <CCol xs="12" md="9">
                      <select  class="form-control form-control"onChange={(e) => setRoleid(e.target.value)}>
                      <option disabled >Select Role</option>
                        {roles.length > 0 &&
                          roles.map((item, index) => {
                            return (
                             
                              <option value={item.id}>
                                {item.role_name} - {item.description}
                              </option>
                            );
                          })}

                        {/* {listDropdown} */}
                        {/* <option values='1'>Whats's New</option> */}
                      </select>
                    </CCol>
                  </CFormGroup>

                  {/* <CFormGroup row>
                                    <CCol md="3" xs="9">
                                        <CLabel htmlFor="text-input"><b>Select Role</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                      <CSelect onChange={e => setRoleid(e.target.value)}> 
                                            <option disabled >Select Your Role</option>
                                            <option value='1'>Super Admin</option>
                                            <option value='2' >Marketing</option>
                                            <option value='3' >Game Master</option>
                                      </CSelect>

                                    </CCol>
                                </CFormGroup>   */}

                  <CButton
                    color="primary"
                    className="mr-1"
                    type="submit"
                    onClick={handleSubmit}
                  >
                    Assign Role
                  </CButton>
                </CForm>
              </CCardBody>
              <CCardFooter></CCardFooter>
            </CCardBody>
          </center>
        </div>
        <div class="row">
          <div class="table-responsive ">
            {/* SEARCH MENU */}
            {/* Search, belum fixed, cuma narik data 1 laman saja */}
            {/* <input type='text' placeholder='Search' onChange={(e) => { setNewsSearch(e.target.value) }}/>
             */}

            {/* <div class="col-sm-2 offset-sm-9  mt-1 mb-1 text-gred">
            <Button variant="primary" onClick={handleShow}>
              Add New news
            </Button>
          </div> */}
          </div>
        </div>

        {/* <!--- Model Box ---> */}
        <div className="model_box">
          <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
          >
            <Modal.Header closeButton>
              <Modal.Title>Add New news</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <form>
                <div class="form-group">
                  <input
                    class="form-control"
                    id="title"
                    aria-describedby="newsTitle"
                    placeholder="News Title"
                  />
                </div>
                <button type="submit" class="btn btn-success mt-4">
                  Register
                </button>
              </form>
            </Modal.Body>

            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>

          {/* Model Box Finsihs */}
        </div>
      </div>
    </div>
  );
}

// 1st set redux
const mapDispatchToProps = (dispatch) => ({
  //getUser: () => dispatch(userManagementAction()),
  //getRoles: () => dispatch(rolesManagementAction()),
});

const mapStateToProps = (state) => ({
  isLoading: state.userManagementReducer.isLoading,
  error: state.userManagementReducer.error,
  dataUserManagement: state.userManagementReducer.data,
  dataRolesManagement: state.rolesManagementReducer.data,
  // data: state.userManagementReducer.data,
});

export default connect(mapStateToProps, mapDispatchToProps)(AssignRoleToUser);
