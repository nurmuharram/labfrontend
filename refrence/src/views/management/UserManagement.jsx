
import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CContainer,
  CFormGroup,
  CLabel,
  CInput,
  CValidFeedback,
  CInvalidFeedback
  
} from '@coreui/react'

import UserManagementList from './AssignRoleToUser'
import UserManagementListFilter from './UserManagementListFilter'
import AssignRoleToUser from './AssignRoleToUser'
import UpdateRoleToUser from './UpdateRoleToUser'


function RolesManagement() {

  


  return (
    <div className="RolesManagement">
      <hr />
      {/* <h6>
        <i>Send player(s) RolesManagements and gifts</i>
      </h6> */}

      <CContainer fluid>
        
      
          <CCard accentColor="primary">
     
            <CCardBody> 

            <CTabs>
                <CNav variant="tabs">
                 
                  <CNavItem>
                    <CNavLink>
                      Users List
                    </CNavLink>
                  </CNavItem>

                  <CNavItem>
                    <CNavLink>
                      Update Role 
                    </CNavLink>
                  </CNavItem>


                  <CNavItem>
                    <CNavLink>
                      Assign Role 
                    </CNavLink>
                  </CNavItem>

                </CNav>
                <CTabContent>

                  {/*IMPORT Roles List*/}
                  <CTabPane>
                  <UserManagementListFilter />
                  </CTabPane>
                  {/* Assign Roles to User(s) Tab */}


                  <CTabPane>
                  <UpdateRoleToUser />
                  </CTabPane>

                  <CTabPane>
                  <AssignRoleToUser />
                  </CTabPane>

                  <CTabPane>

                  </CTabPane>
                </CTabContent>
              </CTabs>



            </CCardBody>
          </CCard>

      </CContainer>
    </div>
  )
}

export default RolesManagement
