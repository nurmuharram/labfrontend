import React, { useState, useEffect } from 'react';
import axios from "axios";
import {
    CCard,
    CCardBody,
    CCardHeader,
    CTabs,
    CNav,
    CNavItem,
    CNavLink,
    CTabContent,
    CTabPane,
    CCardFooter,
    CCol,
    CRow,
    CForm,
    CModal,
    CModalHeader,
    CModalTitle,
    CFormGroup,
    CLabel,
    CTextarea,
    CSelect,
    CInput,
    CButton

} from '@coreui/react'
import { rolesManagementAction } from "src/redux/action/rolesManagementAction/rolesManagementAction";
import { useHistory, useLocation } from "react-router-dom";
import { connect, useSelector, useDispatch } from "react-redux";
import toast from 'react-hot-toast';

function UserRegister({ refreshlist, setShow, roles }) {
    const dispatch = useDispatch();

    const [role, setRole] = useState('1');

    const initialValues = { name: "", email: "", password: "" };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmit, setIsSubmit] = useState(false);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (Object.keys(formErrors).length === 0) {
            register(e);
        }
    };

    useEffect(() => {
        setFormErrors(validate(formValues));
    }, [formValues])

    const validate = (values) => {
        const errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.name) {
            errors.name = "*Name is required!";
        }
        if (!values.email) {
            errors.email = "*Email is required!";
        } else if (!regex.test(values.email)) {
            errors.email = "*This is not a valid email format!";
        }
        if (!values.password) {
            errors.password = "*Password is required";
        } else if (values.password.length < 4) {
            errors.password = "*Password must be more than 4 characters";
        } else if (values.password.length > 16) {
            errors.password = "*Password cannot exceed more than 16 characters";
        }
        return errors;
    };

    const register = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();

        data.append('name', formValues.name)
        data.append('email', formValues.email)
        data.append('password', formValues.password)
        data.append('role_id', role)
        const config = {
            method: 'POST',
            url: `api/user/register`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
                "Content-Type": "multipart/form-data"
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                toast.success('Register New User Succeded');
                setShow(false);
            })
            .catch((error) => {
                console.log(error);
                toast.error('Failed to Register New User!')
            });
        setTimeout(refreshlist, 1000);
    }


    return (
        <>
            <CCard>
                <CCardBody>

                    <CCardBody>
                        <CForm action="" method="post" encType="multipart/form-data" className="form-horizontal" onSubmit={handleSubmit} >


                            <CFormGroup row>
                                <CCol md="3">
                                    <CLabel htmlFor="text-input"><b>Full Name</b></CLabel>
                                </CCol>
                                <CCol xs="12" md="9">
                                    <CInput id="fullName" name="name" value={formValues.name} onChange={handleChange} placeholder="Full Name" />
                                    <small style={{ color: '#ff0000' }}><i>{formErrors.name}</i></small>
                                </CCol>
                            </CFormGroup>


                            <CFormGroup row>
                                <CCol md="3">
                                    <CLabel htmlFor="text-input"><b>Email</b></CLabel>
                                </CCol>
                                <CCol xs="12" md="9">
                                    <CInput type="email" id="email" name="email" value={formValues.email} onChange={handleChange} placeholder="Email" />
                                    <small style={{ color: '#ff0000' }}><i>{formErrors.email}</i></small>
                                </CCol>

                            </CFormGroup>



                            <CFormGroup row>
                                <CCol md="3">
                                    <CLabel htmlFor="password-input"><b>Password</b></CLabel>
                                </CCol>
                                <CCol xs="12" md="9">
                                    <CInput type="password" id="password" maxLength={16} name="password" value={formValues.password} onChange={handleChange} placeholder="Password" />
                                    <small style={{ color: '#ff0000' }}><i>{formErrors.password}</i></small>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup row>
                                <CCol md="3" xs="9">
                                    <CLabel htmlFor="text-input"><b>Role</b></CLabel>
                                </CCol>
                                <CCol xs="12" md="9">
                                    <CSelect onChange={e => setRole(e.target.value)}>
                                        {/* <option disabled >Select Your Role</option>
                                            <option value='1'>Super Admin</option>
                                            <option value='2' >Marketing</option>
                                            <option value='3' >Game Master</option> */}
                                        <option disabled >Select Role</option>
                                        {roles.length > 0 &&
                                            roles.map((item, index) => {
                                                return (
                                                    <option value={item.id}>
                                                        {item.role_name} - {item.description}
                                                    </option>
                                                );
                                            })}
                                    </CSelect>

                                </CCol>
                            </CFormGroup>

                            {Object.keys(formErrors).length === 0 ?
                                <><CButton color="primary" className="mr-1" onClick={register} >Register</CButton></>
                                : <><CButton color="primary" className="mr-1" >Register</CButton></>}
                        </CForm>
                    </CCardBody>
                    <CCardFooter>

                    </CCardFooter>
                </CCardBody>

            </CCard>
        </>
    )


}

/* const mapDispatchToProps = (dispatch) => ({
    getRoles: () => dispatch(rolesManagementAction()),
  });
  
  const mapStateToProps = (state) => ({
    isLoading: state.rolesManagementReducer.isLoading,
    error: state.rolesManagementReducer.error,
    dataRolesManagement: state.rolesManagementReducer.data,
    // data: state.userManagementReducer.data,
  }); */


export default UserRegister;
