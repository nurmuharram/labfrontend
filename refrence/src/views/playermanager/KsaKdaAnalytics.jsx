import React, { useState } from 'react'
import {
    CCard, 
    CCol, 
    CDataTable, 
    CFormGroup, 
    CImg, 
    CRow, 
    CSelect
} from '@coreui/react'
import { GetUserKsaKdaStats } from 'src/api/playerDashboardAPI/playerDashboardGetRequest'

function KsaKdaAnalytics({ id }) {

    const [ksakdalist, setKsakdalist] = useState([])
    const [gamemode, setGamemode] = useState('1')

    GetUserKsaKdaStats({ setKsakdalist, gamemode, id })

    const fields = [
        {
            key: 'ksa_img',
            label: '',
            sorter: false,
            filter: false,
        },
        {
            key: 'detail',
            label: '',
            sorter: false,
            filter: false,
        }
    ]

    return (
        <div>
            <u><b>Most Ksatriya K/D/A Rate</b></u>
            <hr />
            <section>
                <CSelect className="col-sm-6" onChange={(e) => setGamemode(e.target.value)}>
                    <option value={1} >Classic</option>
                    <option value={2} >Ranked (Blind Pick)</option>
                    <option value={3} >Ranked (Draft Pick)</option>
                </CSelect>
                <br />
                <CCard borderColor="primary">
                    <CDataTable
                        items={ksakdalist && ksakdalist.sort(function (low, high) {
                            return high.kill_death_rate - low.kill_death_rate
                        })}
                        fields={fields}
                        header={false}
                        itemsPerPage={3}
                        pagination
                        scopedSlots={{
                            "ksa_img":
                                (item, index) => {
                                    return (
                                        <td>
                                            <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${item.ksatriya_id}.png`} fluid shape="rounded" width='130' align='left' />
                                        </td>
                                    )
                                },
                            "detail":
                                (item, index) => {
                                    return (
                                        <td>
                                            <CRow>
                                                <CCol align="left">
                                                <h5><u><i><b>{item.ksatriya_name}</b></i></u></h5>
                                                    <i>Kill {''}</i>
                                                    : {item.kill_count}
                                                    <br />
                                                    <i>Death {''}</i>
                                                    : {item.death_count}
                                                    <br />
                                                    <i>Assist {''}</i>
                                                    : {item.assist_count}
                                                    <br/>
                                                    <i>KDA Rate {''}</i>
                                                    : {item.kill_death_rate} %
                                                </CCol>
                                            </CRow>
                                        </td>
                                    )
                                }
                        }}
                    />
                </CCard>
            </section>
        </div>
    )
}

export default KsaKdaAnalytics
