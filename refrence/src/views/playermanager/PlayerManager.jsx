import React, { useState, useEffect, useRef } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CContainer,
  CButton,
  CFormGroup,
  CInputGroup,
  CInput,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CForm

} from '@coreui/react'


import { useSelector, useDispatch } from 'react-redux'

import axios from 'axios'


const getBadge = karma => {
  if (karma <= 100 && karma >= 81) {
    return 'success'
  } else if (karma <= 80 && karma >= 71) {
    return 'warning'
  } else if (karma <= 70) {
    return 'danger'

  }
}


const PlayerManager = () => {
  const history = useHistory()
  const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
  const [page, setPage] = useState(currentPage)

  const pageChange = newPage => {
    currentPage !== newPage && history.push(`/?page=${newPage}`)
  }

  useEffect(() => {
    currentPage !== page && setPage(currentPage)
  }, [currentPage, page])

  //const dispatch = useDispatch();
  //const players = useSelector((state) => state.players.players);
  /* useEffect(() => {
    dispatch(getPlayers());
  }, []); */

  const [errorsubmitid, setErrorsubmitid] = useState('')

  const [players, setPlayers] = useState([])
  const playerschange = useRef()
  playerschange.current = players;

  const [_players, set_players] = useState({
    user_id: '',
    user_name: '',
    karma: '',
    playing_time: ''
  })

  const nullskip = () => {
    if (players == null) {
      return <>Player is not exist</>
    } else {
      return players
    }
  }


  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/players/getplayers?count=10&offset=${listchange.current}`, config).then(res => {
      const players = res.data;
      setPlayers(players);
    });
  }, []);

  const refreshPlayerslist = () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/players/getplayers?count=10&offset=${listchange.current}`, config).then(res => {
      const items = res.data;
      setPlayers(items);
    });
  }

  const refreshPlayersdefault = () => {
    const config = {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
        }
    }
    axios.get(`api/players/getplayers?count=10&offset=0`, config).then(res => {
        const items = res.data;
        setPlayers(items);
    })
}

  const [changelist, setChangelist] = useState(0);

  const listchange = useRef();
  listchange.current = changelist;

  const decrementforprev = () => { setChangelist(changelist - 10); setTimeout(refreshPlayerslist, 100) }
  const incrementfornext = () => { setChangelist(changelist + 10); setTimeout(refreshPlayerslist, 100) }

  const prevbttn = () => {
    return <CButton className="info" size="sm" color="primary" onClick={decrementforprev}>Previous Lists</CButton>;
}

const nextbttn = () => {
    return <CButton className="info" size="sm" color="primary" onClick={incrementfornext} >Next Lists</CButton>;
}

const hideprevbttn = () => {

  if (changelist === 0) {
      return <></>
  } else if (Object.keys(players).length < 5){
    return <></>
  } else {
      return prevbttn();
  }
};

const hidenextbttn = () => {

    if (Object.keys(players).length < 10) {
        return <></>
    } else if (Object.keys(players).length >= 10) {
        return nextbttn();
    }
};

  const [uninput, setUninput] = useState('')
  const uninputchange = useRef();
  uninputchange.current = uninput;

  const [inputid, setInputid] = useState('')
  const inputidchange = useRef();
  inputidchange.current = inputid;

  const [inputrefid, setInputrefid] = useState('')
  const inputrefidchange = useRef();
  inputrefidchange.current = inputrefid;

  const submitusername = () => {

    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/players/getplayerbyname?user_name=${uninputchange.current}`, config).then(res => {
      const players = res.data;
      setPlayers(players);
    })
  }

  const submitid = () => {

    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/players/getplayer?user_id=${inputidchange.current}`, config).then(res => {
      const players = res.data;
      setPlayers(Object([players]));
    })
    .catch((error) => {
      const federror = error;
      setErrorsubmitid(federror)
  });
  }


  const submitrefid = () => {

    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/players/getplayerbyrefid?ref_id=${inputrefidchange.current}`, config).then(res => {
      const players = res.data;
      setPlayers(players);
    });
  }


  const idinput = () => {
    return <><CInput type="text" placeholder="Search by Player ID here..." value={inputidchange.current} onChange={(e) => setInputid(e.target.value.replace(/\D/,''))}/><CButton color="primary" size="sm" onClick={!inputidchange.current.length<1 ? submitid : refreshPlayerslist}>Search</CButton></>;
  }

  const usernameinput = () => {
    return <><CInput placeholder="Search by Username here..." value={uninputchange.current} onChange={(e) => setUninput(e.target.value)} /><CButton color="primary" size="sm" onClick={!uninputchange.current.length<1 ? submitusername : refreshPlayersdefault}>Search</CButton></>;
  }

  const referralidinput = () => {
    return <><CInput placeholder="Search by Referral ID here..." value={inputrefidchange.current} onChange={(e) => setInputrefid(e.target.value)} /><CButton color="primary" size="sm" onClick={!inputrefidchange.current.length<1 ? submitrefid : refreshPlayerslist}>Search</CButton></>;
  }

  const [searchby, setSearchby] = useState('')

  const changesearchby = () => {

    if (searchby === 'Player ID') {
      return idinput();
    } else if (searchby === 'Username') {
      return usernameinput();
    } else if (searchby === 'Referral ID') {
      return referralidinput();
  }};

  const namesearchby = () => {
    if (searchby === 'Player ID') {
      return 'PID';
    } else if (searchby === 'Username') {
      return 'Username';
    } else if (searchby === 'Referral ID') {
      return 'Referral ID';
  }};
  

  const items = [_players]


  const searchTable = () => {
    return <><CDataTable
      items={items}
      fields={[{
        key: 'user_id', _classes: 'font-weight-bold', filter: false
      },
      {
        key: 'user_name', filter: false
      },
      {
        key: 'karma', filter: false
      },
      {
        key: 'playing_time', filter: false
      },
      ]}
      clickableRows
      pagination={false}
      hover
      striped
      onRowClick={(item) => history.push(`/playerdetail/${item.user_id}`)}
      scopedSlots={{
        'karma':
          (item) => (
            <td>
              <CBadge color={getBadge(item.karma)}>
                {item.karma}
              </CBadge>
            </td>
          )
      }}
    /></>;
  }

  const tableallplayers = () => {
    return <>
      <CDataTable
        items={playerschange.current}
        fields={[{
          key: 'user_id', _classes: 'font-weight-bold', _style: { width: '9%' }, filter: false
        },
        {
          key: 'user_name', filter: false
        },
        {
          key: 'karma', filter: false
        },
        {
          key: 'last_login', filter: false
        },
        ]}
        clickableRows
        hover
        striped
        onRowClick={(item) => history.push(`/playerdetail/${item.user_id}`)}
        scopedSlots={{
          'karma':
            (item) => (
              <td>
                <CBadge color={getBadge(item.karma)}>
                  {item.karma}
                </CBadge>
              </td>
            )
        }}
      />
    </>
  }
  

  const changetable = () => {

    if (_players === '') {
      return <></>;
    } else {
      return searchTable();
    }
  };

  return (
    <>
      <CCol align='center'>
        <CCard style={{ background: '#9bf54c' }}>
          <h4><b>Player Manager</b></h4>
          <h6><i>View Player's Lists</i></h6>
        </CCard>
      </CCol>
      <br />
      <CContainer>
        <CCol className="mb-4">
          <CCard borderColor="success">
            <CCardHeader>
              <h5> Players List </h5>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol>
                  <CCard>
                    <CCardHeader>
                      <i>Click to show Player Details</i>

                    </CCardHeader>
                    <CCardBody>
                      <CFormGroup row>
                        <CCol md="9" lg='5' sm='12'>
                          <CInputGroup>
                            <CDropdown className="input-group-prepend">
                              <CDropdownToggle caret color="primary">
                                Search by {namesearchby()}
                              </CDropdownToggle>
                              <CDropdownMenu>
                                <CDropdownItem id="Username" onClick={(e) => setSearchby(e.target.id)}>Player Username</CDropdownItem>
                                <CDropdownItem id="Player ID" onClick={(e) => setSearchby(e.target.id)}>Player ID</CDropdownItem>
                                <CDropdownItem id="Referral ID" onClick={(e) => setSearchby(e.target.id)}>Referral ID</CDropdownItem>

                              </CDropdownMenu>
                            </CDropdown>
                            {changesearchby()}
                          </CInputGroup>
                        </CCol>

                      </CFormGroup>                    
                      
                      <CRow>
                        <CCol align="center">
                          <b><i>Player lists</i></b>
                        </CCol>
                      </CRow>
                      <br />
                      {players === null? <><CRow>
                        <CCol align='center'>
                          <h4>Ooopss... No Players Found! Check Your "Search By" or spelling</h4>
                        </CCol>
                        </CRow></>:tableallplayers()}
                      <CRow>
                        {players === null ? <></>:
                        <><CCol align='left'>
                        {hideprevbttn()}
                      </CCol>
                      <CCol align='right'>
                        {hidenextbttn()}
                      </CCol></>}
                      </CRow>

                    </CCardBody>
                  </CCard>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CContainer>
    </>
  )

}

export default PlayerManager
