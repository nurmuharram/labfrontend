import React, { useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import InventoryAvatars from "../../InventoryAvatars";
import InventoryBox from "../../InventoryBox";
import InventoryFrames from "../../InventoryFrames";
import InventoryKsatriyas from "../../InventoryKsatriyas";
import InventoryMiscItems from "../../InventoryMiscItems";
import InventoryRunes from "../../InventoryRunes";
import InventorySkinFragments from "../../InventorySkinFragments";
import InventoryVahana from "../../InventoryVahana";

function InventoryTabs({ user_id }) {
  const [key, setKey] = useState("invbox");

  return (
    <Tabs
      id="controlled-tab"
      activeKey={key}
      onSelect={(k) => setKey(k)}
      className="mb-3"
      variant="pills"
      color="red"
    >
      <Tab eventKey="invbox" title="Box">
        <InventoryBox user_id={user_id} />
      </Tab>
      <Tab eventKey="invavatars" title="Avatars">
        <InventoryAvatars user_id={user_id} />
      </Tab>
      <Tab eventKey="invframes" title="Frames">
        <InventoryFrames user_id={user_id} />
      </Tab>
      <Tab eventKey="invksa" title="Ksatriyas">
        <InventoryKsatriyas user_id={user_id} />
      </Tab>
      <Tab eventKey="invskinfragments" title="Skin Fragments">
        <InventorySkinFragments user_id={user_id} />
      </Tab>
      <Tab eventKey="invmiscitem" title="Misc Items">
        <InventoryMiscItems user_id={user_id} />
      </Tab>
      <Tab eventKey="invrunes" title="Runes">
        <InventoryRunes user_id={user_id} />
      </Tab>
      <Tab eventKey="invvahana" title="Vahanas">
        <InventoryVahana user_id={user_id} />
      </Tab>
    </Tabs>
  );
}

export default InventoryTabs;
