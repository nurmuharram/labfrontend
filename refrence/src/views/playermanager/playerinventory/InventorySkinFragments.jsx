import React from "react";
import { useState } from "react";
import { GetInvSkinFragments } from "src/api/playerDashboardAPI/playerInventoryAPI/playerInventoryGetRequests";
import DataTable from "src/components/table/DataTable";

function InventorySkinFragments({ user_id }) {
  const [skinFragments, setSkinFragments] = useState([]);
  GetInvSkinFragments({ user_id, setSkinFragments });
  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <DataTable items={skinFragments} itemsPerPage={5} pagination />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InventorySkinFragments;
