import React, { useState } from "react";
import { GetInvKsatriyas } from "src/api/playerDashboardAPI/playerInventoryAPI/playerInventoryGetRequests";
import DataTable from "src/components/table/DataTable";

function InventoryKsatriyas({ user_id }) {
  const [ksa, setKsa] = useState([]);
  GetInvKsatriyas({ user_id, setKsa });
  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <DataTable itemsPerPage={5} items={ksa} pagination />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InventoryKsatriyas;
