import React, { useState } from "react";
import { GetInvavatars } from "src/api/playerDashboardAPI/playerInventoryAPI/playerInventoryGetRequests";
import DataTable from "src/components/table/DataTable";

function InventoryAvatars({ user_id }) {
  const [avatars, setAvatars] = useState([]);
  GetInvavatars({ user_id, setAvatars });
  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <DataTable items={avatars} pagination itemsPerPage={5} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InventoryAvatars;
