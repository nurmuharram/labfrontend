import React from "react";
import InventoryTabs from "./components/tabs/InventoryTabs";

function PlayerInventory({ user_id }) {
  return (
    <div>
      <div className="card card-accent-primary">
        <div className="card-header" align="center">
          <h4>Players' Inventory</h4>
        </div>
        <div className="card-body">
          <InventoryTabs user_id={user_id}/>
        </div>
      </div>
    </div>
  );
}

export default PlayerInventory;
