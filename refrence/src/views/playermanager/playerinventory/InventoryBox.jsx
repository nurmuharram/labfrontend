import React from "react";
import { useState } from "react";
import { GetInvBoxes } from "src/api/playerDashboardAPI/playerInventoryAPI/playerInventoryGetRequests";
import DataTable from "src/components/table/DataTable";

function InventoryBox({ user_id }) {
  const [boxes, setBoxes] = useState([]);
  GetInvBoxes({ user_id, setBoxes });
  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <DataTable 
              items={boxes}
              itemsPerPage={5}
              pagination
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InventoryBox;
