import React, { useState } from "react";
import { GetInvMiscItems } from "src/api/playerDashboardAPI/playerInventoryAPI/playerInventoryGetRequests";
import DataTable from "src/components/table/DataTable";

function InventoryMiscItems({ user_id }) {
  const [miscItems, setMiscItems] = useState([]);
  GetInvMiscItems({ user_id, setMiscItems });
  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <DataTable itemsPerPage={5} items={miscItems} pagination />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InventoryMiscItems;
