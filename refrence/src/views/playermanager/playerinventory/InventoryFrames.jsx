import React from "react";
import { useState } from "react";
import { GetInvFrames } from "src/api/playerDashboardAPI/playerInventoryAPI/playerInventoryGetRequests";
import DataTable from "src/components/table/DataTable";

function InventoryFrames({ user_id }) {
  const [frames, setFrames] = useState([]);
  GetInvFrames({ user_id, setFrames });
  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <DataTable items={frames} itemsPerPage={5} pagination />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InventoryFrames;
