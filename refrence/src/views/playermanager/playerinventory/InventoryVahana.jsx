import React, { useState } from "react";
import { GetInvVahana } from "src/api/playerDashboardAPI/playerInventoryAPI/playerInventoryGetRequests";
import DataTable from "src/components/table/DataTable";

function InventoryVahana({ user_id }) {
  const [vahana, setVahana] = useState([]);
  GetInvVahana({ user_id, setVahana });
  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <DataTable items={vahana} itemsPerPage={5} pagination />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InventoryVahana;
