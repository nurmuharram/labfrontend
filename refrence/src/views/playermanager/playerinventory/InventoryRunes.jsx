import React from "react";
import { useState } from "react";
import { GetInvRunes } from "src/api/playerDashboardAPI/playerInventoryAPI/playerInventoryGetRequests";
import DataTable from "src/components/table/DataTable";

function InventoryRunes({ user_id }) {
  const [runes, setRunes] = useState([]);
  GetInvRunes({ user_id, setRunes });
  return (
    <div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <DataTable items={runes} itemsPerPage={5} pagination />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InventoryRunes;
