import React, { useEffect, useState, useRef } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CContainer,
  CNavLink,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CTabContent,
  CTabPane,
  CNav,
  CNavItem,
  CButton,
  CTabs,
  CModal,
  CModalTitle,
  CBadge,
  CDataTable,
  CImg,
  CInput,
  CSelect,
  CLink,
} from '@coreui/react'


import CIcon from '@coreui/icons-react'


import { useSelector, useDispatch } from 'react-redux'

import { getPlayers } from 'src/redux/action/mailAction/getPlayersAction';
import axios from 'axios';
import KsaKdaAnalytics from './KsaKdaAnalytics'
import PlayerInventory from './playerinventory/PlayerInventory'


const getBadge = report_type => {
  switch (report_type) {
    case 'Inappropriate Name': return 'warning'
    case 'Inappropriate Name, Inappropriate Profile Picture': return 'info'
    case 'Inappropriate Profile Picture': return 'danger'
    default: return 'primary'
  }
}


const PlayerDashboard = ({ match }) => {

  const history = useHistory()
  const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
  const [page, setPage] = useState(currentPage)

  const user_id = match.params.user_id


  const pageChange = newPage => {
    currentPage !== newPage && history.push(`/?page=${newPage}`)
  }

  useEffect(() => {
    currentPage !== page && setPage(currentPage)
  }, [currentPage, page])

  const dispatch = useDispatch();
  const players = useSelector((state) => state.players.players);
  useEffect(() => {
    dispatch(getPlayers());
  }, []);

  const player = players && players.find(player => player.user_id.toString() === match.params.user_id)
  const playerDetails = player ? Object.entries(player) :
    [['user_id', (<span><CIcon className="text-muted" name="cui-icon-ban" /> Not found</span>)]];


  const [primary, setPrimary] = useState(false);
  const [unbanchatmodal, setUnbanchatmodal] = useState(false);
  //const [secondary, setSecondary] = useState(false)
  //const [tertiary, setTertiary] = useState(false)
  const [fourth, setFourth] = useState(false);
  const [fifth, setFifth] = useState(false);
  const [karmapointsmodal, setKarmapointsmodal] = useState(false);
  const [fulldetailsmodal, setFulldetailsmodal] = useState(false);
  const [changeGamemode, setChangeGamemode] = useState(1)
  const [changelist, setChangelist] = useState(0);

  const [_player, set_player] = useState([]);
  const [reports, setReports] = useState([]);
  const [pdata, setPdata] = useState([{}]);
  const [matchHistory, setMatchHistory] = useState([])
  const [reporterreports, setReporterreports] = useState([]);
  const [reportedrepots, setReportedrepots] = useState([]);
  const [lastlogin, setLastlogin] = useState([]);
  const [totalmatch, setTotalmatch] = useState([]);
  const [ksaowned, setKsaowned] = useState([]);
  const [topUpHistory, setTopUpHistory] = useState([])
  const id = match.params.user_id;


  const [blacklistdata, setBlacklistdata] = useState({
    blacklist_user_id: '',
    target_user_id: '',
    blacklist_date: ''
  });

  const [matchstats, setMatchstats] = useState([])
  const [rank, setRank] = useState([])

  const kchange = useRef();
  kchange.current = pdata.karma;
  const [karma, setKarma] = useState(kchange.current)

  const banChatbttn = () => {

    if (blacklistdata.target_user_id == 0) {
      return banButton();
    } else {
      return unbanButton();
    }
  };

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/stats/getUserMatchHistory?count=10&offset=${changelist}&user_id=${match.params.user_id}&game_mode=${changeGamemode}`, config).then(res => {
      const playerData = res.data;
      setMatchHistory(playerData);
    });
  }, [match.params.user_id, changeGamemode, changelist]);


  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/stats/getUserMatchStats?user_id=${match.params.user_id}`, config).then(res => {
      const playerData = res.data;
      setMatchstats(playerData);
    });
  }, [match.params.user_id]);

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/stats/getUserLastLogin?user_id=${match.params.user_id}`, config).then(res => {
      const playerData = res.data;
      setLastlogin(playerData);
    });
  }, [match.params.user_id]);

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/stats/getUserTotalMatch?user_id=${match.params.user_id}`, config).then(res => {
      const playerData = res.data;
      setTotalmatch(playerData);
    });
  }, [match.params.user_id]);

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/stats/getUserKsaOwned?user_id=${match.params.user_id}`, config).then(res => {
      const playerData = res.data;
      setKsaowned(playerData);
    });
  }, [match.params.user_id]);

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/stats/getUserSeasonRankStats?user_id=${match.params.user_id}`, config).then(res => {
      const playerData = res.data;
      setRank(playerData);
    });
  }, [match.params.user_id]);


  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/players/getplayer?user_id=${match.params.user_id}`, config).then(res => {
      const playerData = res.data;
      setPdata(playerData);
    });
  }, [match.params.user_id]);

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/report/getBlacklist?target_id=${id}`, config).then(res => {
      const items = res.data;
      setBlacklistdata(items);
    });
  }, [id])



  const banButton = () => {
    return <CButton color="danger" size="sm" onClick={() => setPrimary(!primary)}>Mute Player</CButton>
      ;
  }

  const unbanButton = () => {
    return <CButton color="danger" size="sm" onClick={() => setUnbanchatmodal(!unbanchatmodal)}>Unmute Player
    </CButton>;
  }


  /* PROFILE REPORTS BY REPORTED USER */
  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/report/getProfileReportsbyReportedUser?user_id=${match.params.user_id}`, config).then(res => {
      const items = res.data;
      set_player(items);
    });
  }, [match.params.user_id])

  /* PROFILE REPORTS BY REPORTER USER */
  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/report/getProfileReportsbyReporterUser?user_id=${match.params.user_id}`, config).then(res => {
      const items = res.data;
      setReports(items);
    });
  }, [match.params.user_id])

  /* REPORTS BY REPORTED USER */
  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/report/getReportsbyReportedUser?count=1000&offset=0&user_id=${match.params.user_id}`, config).then(res => {
      const items = res.data;
      setReportedrepots(items);
    });
  }, [match.params.user_id])

  /* REPORTS BY REPORTER USER */
  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/report/getReportsbyReporterUser?count=1000&offset=0&user_id=${match.params.user_id}`, config).then(res => {
      const items = res.data;
      setReporterreports(items);
    });
  }, [match.params.user_id])

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/stats/getUserTopUpHistory?count=1000&offset=0&user_id=${match.params.user_id}`, config).then(res => {
      const items = res.data;
      setTopUpHistory(items);
    });
  }, [match.params.user_id])


  const refreshPlayerData = () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/players/getplayer?user_id=${match.params.user_id}`, config).then(res => {
      const playerData = res.data;
      setPdata(playerData);
    });
  }


  const refreshReportdata = () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/report/getBlacklist?target_id=${id}`, config).then(res => {
      const items = res.data;
      setBlacklistdata(items);
    });
  }

  const banPlayerChat = () => {
    const data = new FormData();
    data.append('target_user_id', id);

    const config = {
      method: 'post',
      url: '/api/report/blacklistPlayer',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('auth'),

      },
      data: data
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert('Player Muted!');
        setPrimary(false);


      })
      .catch((error) => {
        console.log(error);
        alert('Failed to mute player!')
      });
    setTimeout(refreshReportdata, 500);

  }



  const releaseBanPlayerChat = () => {
    const config = {
      method: 'delete',
      url: `/api/report/unblacklistPlayer?user_id=0&target_id=${id}`,
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('auth'),
      }
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert('Player un-mute sucessfully!')
        setUnbanchatmodal(false)
      })
      .catch((error) => {
        console.log(error);
        alert('Failed to Un-mute Player!')
      });
    setTimeout(refreshReportdata, 500);
  }



  const updatePlayerAvatar = () => {
    const data = new FormData();
    data.append('avatar', '1');

    const config = {
      method: 'put',
      url: `/api/players/updateplayeravatar?user_id=${id}`,
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('auth'),

      },
      data: data
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert('Players` Avatar Reset!')
        setFourth(false)
      })
      .catch((error) => {
        console.log(error);
        alert('Failed to reset!')
      });
    setTimeout(refreshPlayerData, 500)
  }


  const updatePlayerName = () => {
    const config = {
      method: 'put',
      url: `/api/players/updateplayernameauto?user_id=${id}`,
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('auth'),
      },
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert('Players` name updated!');
        setFifth(false);

      })
      .catch((error) => {
        console.log(error);
        alert('Failed to update name!')
      });
    setTimeout(refreshPlayerData, 500)
  }

  const updatePlayerKarma = (e) => {
    e.preventDefault()
    const FormData = require('form-data');
    const data = new FormData();
    data.append('karma', karma)
    const config = {
      method: 'put',
      url: `/api/players/updateplayerkarma?user_id=${id}`,
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('auth'),
      },
      data: data
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert('Karma Points Updated!');
        setKarmapointsmodal(false);

      })
      .catch((error) => {
        console.log(error);
        alert('Failed to adjust karma points!')
      });
    setTimeout(refreshPlayerData, 500)
  };

  const winrate = matchstats.map((winratio) => {
    return winratio.win_rate
  });

  const wincount = matchstats.map((winratio) => {
    return <small><i>({winratio.win_count} / {winratio.match_count} Matches)</i></small>
  });

  const loserate = matchstats.map((loseratio) => {
    return loseratio.lose_rate
  });

  const losecount = matchstats.map((loseratio) => {
    return <small><i>({loseratio.lose_count} / {loseratio.match_count} Matches)</i></small>
  });

  const tableProfileReported = () => {
    return (
      <>
        <i><b>Profile Reports by Reported User</b></i>
        <CDataTable
          items={_player}
          fields={[{
            key: 'reported_user', filter: false, _classes: 'font-italic', sorter: false
          },
          { key: 'reporter_user', _classes: 'font-weight-bold' },
          {
            key: 'report_type', filter: false
          },
          {
            key: 'report_date', filter: false
          },

          ]}
          columnFilter
          activePage={page}
          itemsPerPageSelect
          clickableRows
          itemsPerPage={5}
          hover
          sorterValue={{ column: "report_date", asc: false }}
          striped
          noItemsViewSlot={<div><h4><i>This Player has no report so far...</i></h4></div>}
          sorter
          pagination
          onRowClick={(item) => history.push(`/playerdetail/${item.reporter_user_id}`)}
          scopedSlots={{
            'report_type':
              (item) => (
                <td>
                  <CBadge color={getBadge(item.report_type)}>
                    {item.report_type}
                  </CBadge>
                </td>
              )
          }}
        /></>
    )
  };

  const tableProfileReporter = () => {
    return (
      <>
        <i><b>Profile Reports by Reporter User</b></i>
        <CDataTable
          items={reports}
          fields={[{ key: 'reporter_user', filter: false, _classes: 'font-italic', sorter: false },
          {
            key: 'reported_user', _classes: 'font-weight-bold',
          },
          {
            key: 'report_type', filter: false
          },
          {
            key: 'report_date', filter: false
          },

          ]}
          columnFilter
          activePage={page}
          itemsPerPageSelect
          clickableRows
          itemsPerPage={5}
          hover
          sorterValue={{ column: "report_date", asc: false }}
          striped
          noItemsViewSlot={<div><h4><i>This Player has no report so far...</i></h4></div>}
          sorter
          pagination
          onRowClick={(item) => history.push(`/playerdetail/${item.reported_user_id}`)}
          scopedSlots={{
            'report_type':
              (item) => (
                <td>
                  <CBadge color={getBadge(item.report_type)}>
                    {item.report_type}
                  </CBadge>
                </td>
              )
          }}
        /></>
    )
  };

  const [listchanges, setListchanges] = useState('1')
  const tchange = useRef();
  tchange.current = listchanges;

  const tableChange = () => {
    if (tchange.current === '1') {
      return tableProfileReported();
    } else if (tchange.current === '2') {
      return tableProfileReporter();
    }
  };

  const tableReported = () => {
    return (
      <>
        <i><b>In-Game Reports by Reported User</b></i>
        <CDataTable
          items={reportedrepots}
          fields={[{
            key: 'room_id', filter: false, _classes: 'font-italic', sorter: false
          },
          {
            key: 'reported_user_name', filter: false, _classes: 'font-italic', sorter: false
          },
          { key: 'reporter_user_name', _classes: 'font-weight-bold' },
          {
            key: 'description', filter: false
          },
          {
            key: 'message', filter: false
          },
          {
            key: 'report_date', filter: false
          },

          ]}
          columnFilter
          activePage={page}
          itemsPerPageSelect
          clickableRows
          itemsPerPage={5}
          hover
          sorterValue={{ column: "report_date", asc: false }}
          striped
          noItemsViewSlot={<div><h4><i>This Player has no report so far...</i></h4></div>}
          sorter
          pagination
          onRowClick={(item) => history.push(`/playerdetail/${item.reporter_user_id}`)}
          scopedSlots={{
            'description':
              (item) => (
                <td>
                  <CBadge color={getBadge(item.description)}>
                    {item.description}
                  </CBadge>
                </td>
              )
          }}
        /></>
    )
  };

  const tableReporter = () => {
    return (
      <>
        <i><b>In-Game Reports by Reporter User</b></i>
        <CDataTable
          items={reporterreports}
          fields={[{
            key: 'room_id', filter: false, _classes: 'font-italic', sorter: false
          },
          { key: 'reporter_user_name', filter: false, _classes: 'font-weight-bold' },
          {
            key: 'reported_user_name', filter: true, _classes: 'font-italic', sorter: false
          },
          {
            key: 'description', filter: false
          },
          {
            key: 'message', filter: false
          },
          {
            key: 'report_date', filter: false
          },


          ]}
          columnFilter
          activePage={page}
          itemsPerPageSelect
          clickableRows
          itemsPerPage={5}
          hover
          sorterValue={{ column: "report_date", asc: false }}
          striped
          noItemsViewSlot={<div><h4><i>This Player has no report so far...</i></h4></div>}
          sorter
          pagination
          onRowClick={(item) => history.push(`/playerdetail/${item.reported_user_id}`)}
          scopedSlots={{
            'description':
              (item) => (
                <td>
                  <CBadge color={getBadge(item.description)}>
                    {item.description}
                  </CBadge>
                </td>
              ),
            'message':
              (item) => {
                const message = () => {
                  if (item.message === "") {
                    return "-"
                  } else {
                    return item.message
                  }
                }
                return (
                  <td>
                    {message()}
                  </td>
                )
              }
          }}
        /></>
    )
  };

  const [listReportschanges, setListReportschanges] = useState('1')
  const trchange = useRef();
  trchange.current = listReportschanges;

  const tableReportsChange = () => {
    if (trchange.current === '1') {
      return tableReported();
    } else if (trchange.current === '2') {
      return tableReporter();
    }
  };

  const cardMatch = matchHistory && matchHistory.map((match) => {

    const duration = match.game_duration / 60
    const durationresult = duration.toFixed(0)

    const gameMode = () => {
      if (match.game_mode === 1) {
          return 'Classic'
      } else if (match.game_mode === 2) {
          return 'Ranked (Blind Pick)'
      } else if (match.game_mode === 3) {
          return 'Ranked (Draft Pick)'
      }
  };

    return <>
      <CCard color="success" className="text-white text-center">
        <CCardBody align="center">
          <CRow>
            <CCol align='left'>
              {gameMode()}
              <br />
              Game Duration: <i>{durationresult}mins</i>
              <br />
              <br />
              <CRow md='6' lg='6'>
                <CCol md='3' lg='2' align='center'>
                  <CImg className="border-dark shadow-lg mx-auto" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${match.slot0_ksa}.png`} fluid shape="rounded" width='70' alt='Responsive Image' align='left' />
                </CCol>
                <CCol md='3' lg='2' align='center'>
                  <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${match.slot1_ksa}.png`} fluid shape="rounded" width='70' align='left' />
                </CCol>
                <CCol md='3' lg='2' align='center'>
                  <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${match.slot2_ksa}.png`} fluid shape="rounded" width='70' align='left' />
                </CCol>
                <CCol md='3' lg='2' align='center'>
                  <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${match.slot3_ksa}.png`} fluid shape="rounded" width='70' align='left' />
                </CCol>
                <CCol md='3' lg='2' align='center'>
                  <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${match.slot4_ksa}.png`} fluid shape="rounded" width='70' align='left' />
                </CCol>
              </CRow>
            </CCol>
            <CCol align='center' md='2'>
              <span className='display-3'><b>VS</b></span>
            </CCol>
            <CCol align='right'>
              Room ID: {match.room_id}
              <br />
              Start Time: {match.start_time}
              <br />
              <br />
              <CRow>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <CCol md='3' lg='2' align='center'>
                  <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${match.slot5_ksa}.png`} fluid shape="rounded" width='70' align='right' />
                </CCol>
                <CCol md='3' lg='2' align='center'>
                  <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${match.slot6_ksa}.png`} fluid shape="rounded" width='70' align='right' />
                </CCol>
                <CCol md='3' lg='2' align='center'>
                  <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${match.slot7_ksa}.png`} fluid shape="rounded" width='70' align='right' />
                </CCol>
                <CCol md='3' lg='2' align='center'>
                  <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${match.slot8_ksa}.png`} fluid shape="rounded" width='70' align='right' />
                </CCol>
                <CCol md='3' lg='2' align='center'>
                  <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${match.slot9_ksa}.png`} fluid shape="rounded" width='70' align='right' />
                </CCol>
              </CRow>
            </CCol>
          </CRow>
          <CCol align='center' md='12'>
              <CLink onClick={() => history.push(`/matchdetails/${match.room_id}`)} ><u>See match detail...</u></CLink>
            </CCol>
        </CCardBody>
      </CCard>
    </>
  });

  

  const listchange = useRef();
  listchange.current = changelist;

  const decrementforprev = () => { setChangelist(changelist - 10);  }
  const incrementfornext = () => { setChangelist(changelist + 10);  }

  const prevbttn = () => {
    return <CButton className="info" size="sm" color="primary" onClick={decrementforprev}>Previous Lists</CButton>;
}

const nextbttn = () => {
    return <CButton className="info" size="sm" color="primary" onClick={incrementfornext} >Next Lists</CButton>;
}

const hideprevbttn = () => {

  if (changelist === 0) {
      return <></>
  } else if (Object.keys(matchHistory).length <= 10){
    return prevbttn()
  } 
};

const hidenextbttn = () => {

    if (Object.keys(matchHistory).length < 10) {
        return <></>
    } else if (Object.keys(matchHistory).length >= 10) {
        return nextbttn();
    }
};

const fields = [
  { key: 'item_type_name' },
  { key: 'item_name' },
  { key: 'amount' },
  { key: 'price', _style: { width: '10%' } },
  { key: 'request_date' },
  { key: 'trx_id', _style: { width: '10%' } },
]




  return (
    <div>
      <CCol align='center'>
        <CCard style={{ background: '#9bf54c' }}>
          <h4><b>Player Dashboard</b></h4>
          <h6>
            <i>
              Manage Players' Data
            </i>
          </h6>
        </CCard>
      </CCol>
      <br />
      <CContainer fluid>
        <CRow>
          <CCol>
          <CButton color='info' variant='ghost' size='large' onClick={() => history.goBack()}>&#8592; Back to Previous Page </CButton>&nbsp;
          <hr/>
            <CCard accentColor='primary'>
              <CCardHeader>
                <CRow>
                  <CCol>
                    <i>Player ID: {match.params.user_id}</i>
                  </CCol>
                </CRow>
              </CCardHeader>
              <CCardBody>
                <CRow>
                  <CCol className='align-self-center' md='3' lg='3'>
                    <CCol align='center'>
                      <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/avataricon/${pdata.avatar_icon}.png`} fluid shape="rounded" alt={pdata.user_name} width='130px' />
                    </CCol>
                    <br />
                    <CCol align='center'>
                      <CButton color='danger' size='sm' onClick={() => setFourth(!fourth)}>Reset Avatar</CButton>
                      <CModal
                        show={fourth}
                        onClose={() => setFourth(!fourth)}
                        color="danger"
                      >
                        <CModalHeader closeButton>
                          <CModalTitle>Reset Avatar</CModalTitle>
                        </CModalHeader>
                        <CModalBody align="center">
                          <u align="center">Are you sure to reset this players' Avatar?</u>
                        </CModalBody>
                        <CModalFooter>
                          <CButton color="danger" size="sm" onClick={updatePlayerAvatar}>
                            Yes, Reset
                          </CButton>{' '}
                          <CButton color="secondary" size="sm" onClick={() => setFourth(!fourth)}>
                            No, Cancel
                          </CButton>
                        </CModalFooter>
                      </CModal> <br />
                      <CButton color="info" size="sm" onClick={() => setKarmapointsmodal(!karmapointsmodal)}>Set Karma Points</CButton>
                      <CModal
                        show={karmapointsmodal}
                        onClose={() => setKarmapointsmodal(!karmapointsmodal)}
                        color="info"
                      >
                        <CModalHeader closeButton>
                          <CModalTitle>Set Karma Points</CModalTitle>
                        </CModalHeader>
                        <CModalBody align="center">
                          <CCol>
                            <u align="center">Adjust Player's Karma Points</u>
                          </CCol>
                          <br />
                          <CCol>


                            <CInput type="number" min={0} max={100} maxLength={3} placeholder={pdata.karma} onChange={(e) => (setKarma(e.target.value))} />
                          </CCol>
                          {/* <CButton color='light'>{karma}</CButton> */}
                        </CModalBody>
                        <CModalFooter>
                          <CButton color="info" size="sm" onClick={updatePlayerKarma} >
                            Set Karma Points
                          </CButton>{' '}
                          <CButton color="secondary" size="sm" onClick={() => setKarmapointsmodal(!karmapointsmodal)}>
                            Cancel
                          </CButton>
                        </CModalFooter>
                      </CModal>
                    </CCol>
                  </CCol>
                  <CCol lg='5' md='7'>
                    <div className='row'>
                      <div className='col-lg-6'>
                        <h3>{pdata.user_name}{' '}<br /><CButton variant='outline' color='info' size='sm' onClick={() => setFifth(!fifth)}><i>Generate a new username</i></CButton>
                          <CModal
                            show={fifth}
                            onClose={() => setFifth(!fifth)}
                            color="info"
                          >
                            <CModalHeader closeButton>
                              <CModalTitle>Generate a new username for <i>PID {pdata.user_id}</i></CModalTitle>
                            </CModalHeader>
                            <CModalBody align="center">
                              <h5><u align="center">Are you sure?</u></h5>
                            </CModalBody>
                            <CModalFooter>
                              <CButton color="danger" size="sm" onClick={updatePlayerName}>
                                Generate!
                              </CButton>{' '}
                              <CButton color="secondary" size="sm" onClick={() => setFifth(!fifth)}>
                                No, Cancel
                              </CButton>
                            </CModalFooter>
                          </CModal>
                        </h3><small>(PID:{match.params.user_id})</small>
                      </div>
                    </div>
                    <div className="bd-example">
                      <dl className="row">
                        <dt className="col-lg-3 col-md-5">Rank</dt>
                        {rank === null ? <dd className="col-lg-6 col-md-6">:&nbsp;&nbsp;&nbsp;Unranked</dd> : rank && rank.map(item => (
                          <dd className="col-lg-6 col-md-6">:&nbsp;&nbsp;&nbsp;{
                            item.rank === 0 ? 'No Rank' :
                              item.rank === 1 ? 'Ardharathi' :
                                item.rank === 2 ? 'Rathi' :
                                  item.rank === 3 ? 'Ekarathi' :
                                    item.rank === 4 ? 'Atirathi' :
                                      item.rank === 5 ? 'Maharathi' :
                                        item.rank === 6 ? 'Atimaharathi' :
                                          item.rank === 7 ? 'Mahamaharathi' : ''
                          } {
                              item.tier
                            }</dd>
                        ))}&nbsp;
                        <dt className="col-lg-3 col-md-5">Karma</dt>
                        <dd className="col-lg-6 col-md-6">:&nbsp;&nbsp;&nbsp;{pdata.karma}&nbsp;</dd>&nbsp;
                        <dt className="col-lg-3 col-md-5">Total Win Rate</dt>
                        <dd className="col-lg-6 col-md-6">:&nbsp;&nbsp;&nbsp;{winrate} % &nbsp;&nbsp;{wincount}</dd>&nbsp;
                        <dt className="col-lg-3 col-md-5">Total Lose Rate</dt>
                        <dd className="col-lg-6 col-md-6">:&nbsp;&nbsp;&nbsp;{loserate} % &nbsp;&nbsp;{losecount}</dd>&nbsp;
                        <dt className="col-lg-3 col-md-5">Referral ID</dt>
                        <dd className="col-lg-6 col-md-6">:&nbsp;&nbsp;&nbsp;<b><i>{pdata.referal_id}</i></b></dd>
                        <dd className="col-lg-6 col-md-6"><CLink onClick={() => setFulldetailsmodal(!fulldetailsmodal)}><u>See {pdata.user_name} full details...</u></CLink>
                          <CModal
                            show={fulldetailsmodal}
                            onClose={() => setFulldetailsmodal(!fulldetailsmodal)}
                            color="info"
                          >
                            <CModalHeader closeButton>
                              <CModalTitle>{pdata.user_name} Details</CModalTitle>
                            </CModalHeader>
                            <CModalBody align="center">
                              <CImg className="border-dark shadow-lg" src={`${process.env.PUBLIC_URL}/assets/img/avataricon/${pdata.avatar_icon}.png`} fluid shape="rounded" alt={pdata.user_name} width='70' height='70' align='center' />
                              <br />
                              <b>{pdata.user_name}</b>

                              <CRow>
                                <CCol align='left'>
                                  <br />
                                  <dl className="row">
                                    <dt className="col-sm-4">Rank</dt>
                                    {rank === null ? <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Unranked</dd> : rank && rank.map(item => (
                                      <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{
                                        item.rank === 0 ? 'No Rank' :
                                          item.rank === 1 ? 'Ardharathi' :
                                            item.rank === 2 ? 'Rathi' :
                                              item.rank === 3 ? 'Ekarathi' :
                                                item.rank === 4 ? 'Atirathi' :
                                                  item.rank === 5 ? 'Maharathi' :
                                                    item.rank === 6 ? 'Atimaharathi' :
                                                      item.rank === 7 ? 'Mahamaharathi' : ''
                                      } {
                                          item.tier
                                        }</dd>
                                    ))}
                                    <dt className="col-sm-4">Karma</dt>
                                    <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{pdata.karma}&nbsp;</dd>
                                    <dt className="col-sm-4">Signup / Login with</dt>
                                    <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{pdata.account_type}</dd>
                                    <dt className="col-sm-4">Total Win Rate</dt>
                                    <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{winrate} % &nbsp;&nbsp;{wincount}</dd>
                                    <dt className="col-sm-4">Total Lose Rate</dt>
                                    <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{loserate} % &nbsp;&nbsp;{losecount}</dd>
                                    <dt className="col-sm-4">Total Play Hours</dt>
                                    <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{pdata.playing_time}{' '}<i>hrs</i></dd>
                                    <dt className="col-sm-4">Total Games</dt>
                                    <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{totalmatch.total_games} <i>Games</i></dd>
                                    <dt className="col-sm-4">Referral ID</dt>
                                    <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><i>{pdata.referal_id}</i></b></dd>
                                    <dt className="col-sm-4">Register Date</dt>
                                    <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{pdata.register_date}</dd>
                                    <dt className="col-sm-4">Last Login</dt>
                                    <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{lastlogin.last_login}</dd>
                                    <dt className="col-sm-4">Total KSA Owned</dt>
                                    <dd className="col-sm-6">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ksaowned.ksa_owned} <i>KSAs</i></dd>
                                  </dl>
                                </CCol>
                              </CRow>
                            </CModalBody>
                            <CModalFooter>
                              <CButton color="secondary" size="md" onClick={() => setFulldetailsmodal(!fulldetailsmodal)}>
                                Close
                              </CButton>
                            </CModalFooter>
                          </CModal></dd>
                      </dl>
                    </div>
                  </CCol>
                  <CCol md='2' lg='3'>
                    <CRow className='justify-content-start'>
                      {banChatbttn()}
                      <CModal
                        show={primary}
                        onClose={() => setPrimary(!primary)}
                        color="info"
                      >
                        <CModalHeader closeButton>
                          <CModalTitle>Mute "{pdata.user_name}" from Chatting</CModalTitle>
                        </CModalHeader>
                        <CModalBody align="center">

                          <u align="center">Are you sure to mute "{pdata.user_name}"?</u>
                        </CModalBody>
                        <CModalFooter>
                          <CButton color="danger" size="sm" onClick={banPlayerChat}>
                            Yes, Mute <i>{pdata.user_name}</i>
                          </CButton>{' '}
                          <CButton color="secondary" size="sm" onClick={() => setPrimary(!primary)}>
                            No, Cancel
                          </CButton>
                        </CModalFooter>
                      </CModal>

                      <CModal
                        show={unbanchatmodal}
                        onClose={() => setUnbanchatmodal(!unbanchatmodal)}
                        color="info"
                      >
                        <CModalHeader closeButton>
                          <CModalTitle>Unmute "{pdata.user_name}" from Chatting</CModalTitle>
                        </CModalHeader>
                        <CModalBody align="center">

                          <u align="center">Are you sure to unmute "{pdata.user_name}"?</u>
                        </CModalBody>
                        <CModalFooter>
                          <CButton color="danger" size="sm" onClick={releaseBanPlayerChat}>
                            Yes, Unmute <i>{pdata.user_name}</i>
                          </CButton>{' '}
                          <CButton color="secondary" size="sm" onClick={() => setUnbanchatmodal(!unbanchatmodal)}>
                            No, Cancel
                          </CButton>
                        </CModalFooter>
                      </CModal>
                    </CRow>
                    {/* <br />
                    <CRow>
                      <CButton color="info" size="sm" onClick={() => setSecondary(!secondary)} className="btn-pill">
                        Ban Player's Profile Picture
                      </CButton>
                      <CModal
                        show={secondary}
                        onClose={() => setSecondary(!secondary)}
                        color="info"
                      >
                        <CModalHeader closeButton>
                          <CModalTitle>Ban Player from Changing Their Profile Picture</CModalTitle>
                        </CModalHeader>
                        <CModalBody align="center">

                          <u align="center">Are you sure to ban this Player?</u>
                        </CModalBody>
                        <CModalFooter>
                          <CCol align="left">
                            <CButton color="info" size="sm" onClick={() => setSecondary(!secondary)}>
                              Release Ban
                            </CButton>{' '}
                          </CCol>
                          <CButton color="danger" size="sm" onClick={() => setSecondary(!secondary)}>
                            Yes, Ban This player
                          </CButton>{' '}
                          <CButton color="secondary" size="sm" onClick={() => setSecondary(!secondary)}>
                            No, Cancel
                          </CButton>
                        </CModalFooter>
                      </CModal>

                    </CRow>
                    <br />

                    <CRow>

                      <CButton color="info" size="sm" onClick={() => setTertiary(!tertiary)} className="btn-pill">
                        Ban Player's Username
                      </CButton>
                      <CModal
                        show={tertiary}
                        onClose={() => setTertiary(!tertiary)}
                        color="info"
                      >
                        <CModalHeader closeButton>
                          <CModalTitle>Ban Player from Changing Their Username</CModalTitle>
                        </CModalHeader>
                        <CModalBody align="center">

                          <u align="center">Are you sure to ban this Player?</u>
                        </CModalBody>
                        <CModalFooter>
                          <CCol align="left">
                            <CButton color="info" size="sm" onClick={() => setTertiary(!tertiary)}>
                              Release Ban
                            </CButton>{' '}
                          </CCol>
                          <CButton color="danger" size="sm" onClick={() => setTertiary(!tertiary)}>
                            Yes, Ban This player
                          </CButton>{' '}
                          <CButton color="secondary" size="sm" onClick={() => setTertiary(!tertiary)}>
                            No, Cancel
                          </CButton>
                        </CModalFooter>
                      </CModal>
                    </CRow> */}
                  </CCol>
                </CRow>

              </CCardBody>
            </CCard>
            <CTabs activeTab="ingameoverview">

              <CCard accentColor='info'>
                <CNav variant="tabs">
                  <CNavItem>
                    <CNavLink data-tab="ingameoverview">
                      <h6><i>In-Game Overview</i></h6>
                    </CNavLink>
                  </CNavItem>
                  <CNavItem>
                    <CNavLink data-tab="playerreports">
                      <h6><i>Player Reports</i></h6>
                    </CNavLink>
                  </CNavItem>
                  <CNavItem>
                    <CNavLink data-tab="inventory">
                      <h6><i>Inventory</i></h6>
                    </CNavLink>
                  </CNavItem>
                  <CNavItem>
                    <CNavLink data-tab="purchase">
                      <h6><i>Purchase & Top Up</i></h6>
                    </CNavLink>
                  </CNavItem>
                </CNav>
              </CCard>
              <hr />
              <CTabContent>
                <CTabPane data-tab="ingameoverview">
                  <CRow>
                    <CCol md="12" lg='4' className="mb-4">
                      <CCard>
                        <CCardHeader align='center'>
                          <h4>Ksatriya Analytics</h4>
                        </CCardHeader>
                        <CCardBody align="center">
                          <KsaKdaAnalytics id={id}/>
                        </CCardBody>
                      </CCard>

                    </CCol>
                    <CCol xs="5" md="12" lg='8' className="mb-4">
                      <CCard>
                        <CCardHeader>
                          <CCol align='center'><h4>Match History</h4></CCol>
                        </CCardHeader>
                      </CCard>
                      <CSelect className="col-sm-3" onChange={(e) => setChangeGamemode(e.target.value)}>
                        <option value={1} >Classic</option>
                        <option value={2} >Ranked (Blind Pick)</option>
                        <option value={3} >Ranked (Draft Pick)</option>
                      </CSelect>
                      <br/>
                      {matchHistory === null ? <CCard>
                        <CCardBody>
                          <CRow>
                            <CCol align='center'>
                              <h4>No Data</h4>
                            </CCol>
                          </CRow>
                        </CCardBody>
                      </CCard>:cardMatch}
                      
                        <CRow>
                        {matchHistory === null ? <></> : <><CCol align='left'>
                          {hideprevbttn()}
                        </CCol>
                        <CCol align='right'>
                          {hidenextbttn()}
                        </CCol></>}
                        </CRow>
                    
                    </CCol>
                   
                      
                  </CRow>
                </CTabPane>
                <CTabPane data-tab="playerreports">
                  <CRow>
                    <CCol>
                      <CCard>
                        <CCardBody align="center">
                          <CRow>
                            <CCol align="left">
                              <CCol md="4" lg="3">
                                <b><i>Set Players' Profile Reports as</i></b>
                                <CSelect onChange={(e) => setListchanges(e.target.value)}>
                                  <option value={1}>Reported</option>
                                  <option value={2}>Reporter</option>
                                </CSelect>
                                <br />
                              </CCol>
                            </CCol>
                          </CRow>
                          {tableChange()}
                        </CCardBody>
                      </CCard>
                      <CCard>
                        <CCardBody align="center">
                          <CRow>
                            <CCol align="left">
                              <CCol md="4" lg="3">
                                <b><i>Set Players' Reports as</i></b>
                                <CSelect onChange={(e) => setListReportschanges(e.target.value)}>
                                  <option value={1}>Reported</option>
                                  <option value={2}>Reporter</option>
                                </CSelect>
                                <br />
                              </CCol>
                            </CCol>
                          </CRow>
                          {tableReportsChange()}
                        </CCardBody>
                      </CCard>

                    </CCol>
                  </CRow>
                </CTabPane>
                <CTabPane data-tab="inventory">
                  <div className='row'>
                    <div className='col'>
                    <PlayerInventory user_id={user_id}/>
                    </div>
                  </div>
                </CTabPane>
                <CTabPane data-tab="purchase">
                  <CRow>
                    <CCol md="12" lg="6">
                      <CCard accentColor="info">
                        <CCardHeader>
                          <b><i>Purchase History</i></b>
                        </CCardHeader>
                        <CCardBody>
                          Data will shown in here
                        </CCardBody>
                      </CCard>
                    </CCol>
                    <CCol md="12" lg="6">
                      <CCard accentColor="info">
                        <CCardHeader>
                          <b> <i>Top-Up History</i></b>
                        </CCardHeader>
                        <CCardBody>
                          <CDataTable
                          items={topUpHistory}
                          fields={fields}
                          pagination
                          itemsPerPage={5}
                          sorter
                          />
                        </CCardBody>
                      </CCard>
                    </CCol>
                  </CRow>
                </CTabPane>
              </CTabContent>
            </CTabs>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}


export default PlayerDashboard