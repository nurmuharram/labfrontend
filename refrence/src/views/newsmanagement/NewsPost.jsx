import React, { useState } from 'react';
import axios from "axios";
import {
    CCard,
    CCardBody,
    CCardHeader,
    CTabs,
    CNav,
    CNavItem,
    CNavLink,
    CTabContent,
    CTabPane,
    CCardFooter,
    CCol,
    CRow,
    CForm,
    CModal,
    CModalHeader,
    CModalTitle,
    CFormGroup,
    CLabel,
    CTextarea,
    CSelect,
    CInput,
    CButton
    
  } from '@coreui/react'

  
export default function NewsPost() {
    
    const [date, setDate] = useState('2019-10-10')
    const [time, setTime] = useState('00:00')
    const release_date = date + " " + time + ':' + '00'


    const adate = new Date(release_date)

    const toiso = adate.toISOString();

    const splitdate = toiso.split('T')

    const fixdate = splitdate.join(' ')

    const [newsName, setNewsName] = useState('');
    const [titleEN, setTitleEN] = useState('');
    const [titleIN, setTitleIN] = useState('');
    const [type, setType] = useState(1);
    const [banner, setBanner] = useState('');
    const [contentEN, setContentEN] = useState('');
    const [contentIN, setContentIN] = useState('');
    const [loading, setLoading] = useState(false);
    const [isError, setIsError] = useState(false);
    const [data, setData] = useState(null);
    

    const handleSubmit =  (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('name', newsName)
        data.append('type', type)
        data.append('titleEN', titleEN)
        data.append('titleIN', titleIN)
        data.append('banner', banner)
        data.append('contentEN', contentEN)
        data.append('contentIN', contentIN)
        data.append('release_date', fixdate.replace('.000Z', ''))

        const config = {
            method: 'POST',
            url: `api/news/addDetail`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
                "Content-Type": "multipart/form-data"
            },
            data: data
        };

        // const handleSubmit =  (e) => {
        //     e.preventDefault()
        //     const FormData = require('form-data');
        //     const data = new FormData();
        //     var fs = require('fs');
        //     data.append('type', type)
        //     data.append('titleEN', titleEN)
        //     data.append('titleIN', titleIN)
        //     data.append('banner',fs.createReadStream (banner))
        //     data.append('contentEN',fs.createReadStream(contentEN))
        //     data.append('contentIN',fs.createReadStream (contentIN))
        //     data.append('release_date', release_date)


        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('News Submited')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Submit News!')
            });
    }

    return (
        <>
            <CCard>


                <CCardBody>

                        <CCardBody>
                            <CForm action="" method="post" encType="multipart/form-data" className="form-horizontal" >



                                <CFormGroup row>
                                    <CCol md="3" xs="9">
                                        <CLabel htmlFor="text-input"><b>Categories</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <select class="form-control form-control" onChange={e => setType(e.target.value)}>
                                            <option disabled>Select News Categories</option>
                                            <option value='1'>1 - What's New</option>
                                            <option value='2'>2 - Updates</option>
                                            <option value='3'>3 - Events</option>
                                            <option value='4'>4 - Esports</option>
                                        </select>
                                    </CCol>
                                </CFormGroup>                                

                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="text-input"><b>News Name</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CInput maxLength={45} id="newsName" name="subject" onChange={e => setNewsName(e.target.value)} placeholder="News Name for GM Tools" />
                                    </CCol>
                                </CFormGroup>

                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="text-input"><b>In-Game Title (English)</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CInput maxLength={200} id="titleEN" name="subject" onChange={e => setTitleEN(e.target.value)} placeholder="English Title In-Game" />
                                    </CCol>
                                </CFormGroup>


                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="text-input"><b>In-Game Title (Indonesia)</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <CInput maxLength={200} id="titleIN" name="subject"  onChange={e => setTitleIN(e.target.value)} placeholder="Indonesian Title In-Game" />
                                    </CCol>
                                </CFormGroup>

                                <CFormGroup row>
                                    <CCol md="3">
                                        <CLabel htmlFor="textarea-input" name="newscontent"><b>Banner</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                        <form>
                                            <div class="form-group">
                                                <label for="exampleFormControlFile1"><b>File format (.jpg) Max. Size 10MB</b></label>
                                                <input type="file" name="file "accept=".txt" class="form-control-file" 
                                                id="banner" onChange={e => setBanner(e.target.files[0])}
                                                />
                                            </div>
                                        </form>
                                    </CCol>



                                    <CCol md="3">
                                        <CLabel htmlFor="textarea-input" name="newscontent"><b>News English</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                    <form>
                                        <div class="form-group">
                                            <label for="exampleFormControlFile1">File format (.txt)</label>
                                            <input type="file" accept=".txt" class="form-control-file" 
                                            id="contentEN" onChange={e => setContentEN(e.target.files[0])}                                            
                                            />
                                        </div>
                                        </form>
                                    </CCol>

                                    <CCol md="3">
                                        <CLabel htmlFor="textarea-input" name="newscontent"><b>News Indonesia</b></CLabel>
                                    </CCol>
                                    <CCol xs="12" md="9">
                                    <form>
                                        <div class="form-group">
                                            <label for="exampleFormControlFile1">File format (.txt)</label>
                                            <input type="file" accept=".txt" class="form-control-file" 
                                            id="contentIN" onChange={e => setContentIN(e.target.files[0])}                                            
                                            />
                                        </div>
                                        </form>
                                    </CCol>

                                    <CCol md="3">
                                        <CLabel htmlFor="textarea-input" name="newscontent"><b>Release Date</b></CLabel>
                                    </CCol>
                                    <CCol xs="10" md="3">
                                    
                                    <form>
                                        <div class="form-group">
                                        <p>
                                            <input type="date" onKeyDown={(e) => e.preventDefault()}  class="form-control-file" 
                                             onChange={e => setDate(e.target.value)}                                            
                                            />
   Example: 05 / 21 / 2022
                                        </p>
                                        
                                            <p>
                                            <input type="time" class="form-control-file" id="currentTime" 
                                             onChange={e => setTime(e.target.value)}                                            
                                            />
                                        Example : 01 : 20 : AM
                                        </p>
                                        </div>
                                    </form>

                                    
                                    </CCol>

                                </CFormGroup>

   

                                <CButton color="primary"  className="mr-1"type="submit" onClick={handleSubmit} >
                                    Post News
                                </CButton>
                            </CForm>
                        </CCardBody>
                        <CCardFooter>

                        </CCardFooter>
                </CCardBody>

            </CCard>
        </>
        // <div>
        //       <form>
        //         <div class="form-group">
        //           <input
        //             class="form-control"
        //             id="title"
        //             aria-describedby="newsTitle"
        //             placeholder="News Title"
        //           />
        //         </div>
        //         <button type="submit" class="btn btn-success mt-4">
        //           Post News
        //         </button>
        //       </form>
        // </div>
    )
}
