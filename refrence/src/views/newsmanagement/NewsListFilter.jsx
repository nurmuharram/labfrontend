import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import {
  CCol,
  CRow,
  CFormGroup,
  CCollapse,
  CButton,
  CLabel,
} from "@coreui/react";
import axios from "axios";

import { useSelector, useDispatch } from "react-redux";
import { connect } from "react-redux";
import { getNewsAction } from "src/redux/action/newsmanagement/getNewsAction";
import { Modal, ModalBody, ModalFooter, ModalHeader, ModalTitle } from "src/components/modal";
import DataTable from "src/components/table/DataTable";

const NewsListFilter = (props) => {
  const [content, setContent] = useState("");
  const [type, setType] = useState('');
  const [banner, setBanner] = useState("");
  const [deleteItemModal, setDeleteItemModal] = useState(false)
  console.log(content, type)

  // const [show, setShow] = useState(false);
  // const handleClose = () => setShow(false);
  // const handleShow = () => setShow(true);
  const history = useHistory();
  const queryPage = useLocation().search.match(/page=([0-9]+)/, "");
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1);
  const [page, setPage] = useState(currentPage);
  const [details, setDetails] = useState([])

  const [date, setDate] = useState('2019-10-10')
  const [time, setTime] = useState('00:00')
  
  const release_date = date + " " + time + ':' + '00'

  const adate = new Date(release_date)

  const toiso = adate.toISOString();

  const splitdate = toiso.split('T')

  const fixdate = splitdate.join(' ')

  const pageChange = (newPage) => {
    currentPage !== newPage && history.push(`/?page=${newPage}`);
  };
  useEffect(() => {
    props.getNews();
    console.log("ini data", props.data);
    setTimeout(100)
  }, []);
  useEffect(() => {
    currentPage !== page && setPage(currentPage);
  }, [currentPage, page])

  useEffect(() => {
    if (props.error?.response?.status === 502) {
      window.location.reload("/dashboard")
      return false;
    }
  }, [props.error])

  const dispatch = useDispatch();
  const data = useSelector((state) => state.getNewsReducer.data);
  useEffect(() => {
    dispatch(getNewsAction());
  }, []);
  console.log("response user filter", data);

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }


  return (
    <>
    <div className="container">
    <DataTable
          items={data}
          fields={[
            {
              key: "id",
              _classes: "font-weight-bold",
              _style: {
                width: "6%",
              },
            },
            {
              key: "name",

            },
            {
              key: 'release_date',
              filter: false,
            },
            {
              key: "type_name",
              filter: false,
              sorter: false,

            },
            {
              key: 'show_details',
              label: '',
              _style: { width: '10%' },
              sorter: false,
              filter: false
            }
          ]}
          columnFilter
          //tableFilter
          activePage={page}
          itemsPerPageSelect
          clickableRows={pageChange}
          itemsPerPage={5}
          hover
          striped
          sorter
          pagination
          sorterValue={{ column: "id", asc: false }}
          scopedSlots={{
            'release_date':
              (item, index) => {
                const date = item.release_date
                const splitdate = date.split(' ')
                const joindate = splitdate.join('T')
                const joindatez = joindate + '.000Z'

                const dateconvert = new Date(joindatez);


                return (
                  <td>
                    {dateconvert.toString()}
                  </td>
                )
              },
            'show_details':
              (item, index) => {

                return (
                  <td className="py-2">
                    <CButton
                      color="primary"
                      variant="outline"
                      shape="square"
                      size="sm"
                      onClick={() => { toggleDetails(index) }}
                    >
                      {details.includes(index) ? 'Close' : 'Edit News'}
                    </CButton>
                  </td>
                )
              },
            'details':
              (item, index) => {

                const handleSubmitBanner = (e) => {
                  e.preventDefault();
                  const FormData = require("form-data");
                  const data = new FormData();

                  data.append("banner", banner);
                  const config = {
                    method: 'PUT',
                    url: `api/news/updateDetailBanner?id=${item.id}`,
                    headers: {
                      Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      "Content-Type": "multipart/form-data"
                    },
                    data: data
                  };

                  axios(config)
                    .then((response) => {
                      console.log(JSON.stringify(response.data));
                      alert('News Banner Submited')
                    })
                    .catch((error) => {
                      console.log(error);
                      alert('Failed to Submit News!')
                    });
                };


                const handleSubmit = (e) => {
                  e.preventDefault();
                  const FormData = require("form-data");
                  const data = new FormData();

                  data.append("content", content);
                  const config = {
                    method: 'PUT',
                    url: `api/news/updateDetailContent?id=${item.id}&lang=${type}`,
                    headers: {
                      Authorization: 'Bearer ' + localStorage.getItem('auth'),
                      "Content-Type": "multipart/form-data"
                    },
                    data: data
                  };

                  axios(config)
                    .then((response) => {
                      console.log(JSON.stringify(response.data));
                      alert('News Content Submited')
                    })
                    .catch((error) => {
                      console.log(error);
                      alert('Failed to Submit News!')
                    });
                };



                const updateItem = async () => {
                  const FormData = require('form-data');
                  const data = new FormData();
                  data.append('release_date', fixdate.replace('.000Z', ''))

                  const config = {
                    method: 'PUT',
                    url: `/api/news/updateReleaseDate?id=${item.id}`,
                    headers: {
                      Authorization: 'Bearer ' + localStorage.getItem('auth'),
                    },
                    data: data
                  };

                  await axios(config)
                    .then((response) => {
                      console.log(JSON.stringify(response.data));
                      alert('Item Updated!')
                    })
                    .catch((error) => {
                      console.log(error);
                      alert('Failed to Update!')
                    });
                  /* setTimeout(refreshData, 100) */
                };

                const deleteItem = () => {

                  const config = {
                    method: 'delete',
                    url: `/api/news/delete?id=${item.id}`,
                    headers: {
                      Authorization: 'Bearer ' + localStorage.getItem('auth')
                    }
                  };

                  axios(config)
                    .then((response) => {
                      console.log(JSON.stringify(response.data));
                      alert('Item Removed!');
                      setDeleteItemModal(false)
                    })
                    .catch((error) => {
                      alert('Failed!');
                    });
                  /*  setTimeout(refreshList, 100); */
                };

                return (
                  <CCollapse show={details.includes(index)}>
                    <div className="card-body">
                      <CRow>
                        <CCol align='left'>
                          <h4>
                            <i>Edit/Update (<small>ID : {item.id}</small>)</i>
                          </h4>
                        </CCol>
                        <CCol align='right'>
                          <button
                            className="btn btn-outline-danger btn-sm"
                            type="button"
                            onClick={() => setDeleteItemModal(!deleteItemModal)}
                          >
                            Delete News
                          </button>
                          <Modal
                            show={deleteItemModal}
                            onClose={() => setDeleteItemModal(!deleteItemModal)}
                            color="primary"
                            size="md"
                          >
                            <ModalHeader className="bg-danger" closeButton>
                              <ModalTitle>You're About To Delete this Item?</ModalTitle>
                            </ModalHeader>
                            <ModalBody>
                              <div className="col" align="center">
                                <h4>Are You Sure?</h4>
                                <small>(item ID : {item.id})</small>
                              </div>

                            </ModalBody>
                            <ModalFooter>
                              <button
                                className="btn btn-secondary btn-sm"
                                onClick={() => setDeleteItemModal(!deleteItemModal)}
                              >
                                Cancel
                              </button>
                              <button
                                className="btn btn-danger btn-sm"
                                onClick={deleteItem}
                              >
                                Delete
                              </button>
                            </ModalFooter>
                          </Modal>
                        </CCol>
                      </CRow>


                      <hr />
                      <CRow>
                        <CCol>


                          <hr />

                          <CRow>
                            <CCol md="12" xl="6" lg="12">

                              <CFormGroup row>
                                <CCol md="4">
                                  <CLabel htmlFor="textarea-input" name="newscontent"><b>New Release Date</b></CLabel>
                                </CCol>
                                <CCol xs="10" md="4">
                                  <form>
                                    <div class="form-group">
                                      <input defaultValue={item.fixdate} type="date" onKeyDown={(e) => e.preventDefault()} class="form-control-file" id="date" onChange={e => setDate(e.target.value)}
                                      />
                                      <br></br>
                                      <input type="time" class="form-control-file" id="time" onChange={e => setTime(e.target.value)}
                                      />
                                    </div>
                                  </form>
                                  <CButton
                                    color="info"
                                    className="mr-1"
                                    type="submit"
                                    onClick={updateItem}
                                  >
                                    Update Release Date
                                  </CButton>
                                </CCol>

                              </CFormGroup>

                              <CFormGroup row>
                                <CCol lg="4" md="4">
                                  <CLabel htmlFor="text-input"><b>Languages</b></CLabel>
                                </CCol>
                                <CCol xs="12" md="5">
                                  <select class="form-control form-control" onChange={e => setType(e.target.value)}>
                                    <option disabled>Select News languages</option>
                                    <option value='en'>English</option>
                                    <option value='in'>Indonesia</option>

                                  </select>
                                </CCol>
                              </CFormGroup>


                              <CFormGroup row>
                                <CCol md="4">
                                  <CLabel htmlFor="textarea-input" name="newscontent">
                                    <b>News Content</b>
                                  </CLabel>
                                </CCol>
                                <CCol lg="8" xl="6" md="7">
                                  <center>
                                    <form>
                                      <div class="form-group">
                                        <input
                                          type="file"
                                          name="file "
                                          accept=".txt"
                                          class="form-control-file"
                                          onChange={(e) => setContent(e.target.files[0])}
                                        />
                                      </div>
                                    </form>
                                  </center>

                                  <CButton
                                    color="info"
                                    className="mr-1"
                                    type="submit"
                                    onClick={handleSubmit}
                                  >
                                    Update Content
                                  </CButton>
                                </CCol>

                              </CFormGroup>



                            </CCol>

                            <CCol md="12" xl='6' lg='12'>

                              <CFormGroup row>
                                <CCol lg="4" md='4'>
                                  <CLabel htmlFor="textarea-input" name="newscontent">
                                    <b>Banner</b>
                                  </CLabel>
                                </CCol>
                                <CCol xl="8" lg="7" md='7'>
                                  <center>
                                    <form>
                                      <div class="form-group">
                                        <input
                                          type="file"
                                          name="file "
                                          accept=".txt"
                                          class="form-control-file"
                                          id="banner"
                                          onChange={(e) => setBanner(e.target.files[0])}
                                        />
                                      </div>
                                    </form>
                                  </center>

                                  <CButton
                                    color="info"
                                    className="mr-1"
                                    type="submit"
                                    onClick={handleSubmitBanner}
                                  >
                                    Update Banner
                                  </CButton>
                                </CCol>
                              </CFormGroup>
                            </CCol>
                          </CRow>
                        </CCol>
                      </CRow>
                    </div>

                  </CCollapse>
                )
              },
          }}
        />
    </div>
    </>
  );
};
const mapDispatchToProps = (dispatch) => ({
  getNews: () => dispatch(getNewsAction()),
});
const mapStateToProps = (state) => ({
  isLoading: state.getNewsReducer.isLoading,
  data: state.getNewsReducer.data,
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsListFilter);