import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CContainer,
  
} from '@coreui/react'

import UpdateNewsBanner from './UpdateNewsBanner'
import UpdateNewsContent from './UpdateNewsContent'

function NewsManagement() {

  


  return (
    <div className="NewsManagement">
      <hr />
    <center>
      <h3>
        <b>Update News</b>
      </h3>
    </center>  
      <hr />
      {/* <h6>
        <i>Send player(s) NewsManagements and gifts</i>
      </h6> */}

      <CContainer>
          <CCard accentColor="primary">
        
            <CCardBody>
              <CTabs>
                <CNav variant="tabs">
                  <CNavItem>
                    <CNavLink>
                      Update News Banner
                    </CNavLink>
                  </CNavItem>
                  <CNavItem>
                    <CNavLink>
                      Update News Content
                    </CNavLink>
                  </CNavItem>
                </CNav>
                <CTabContent>

                  {/*IMPORT NEWS LIST*/}
                  <CTabPane>
                    <UpdateNewsBanner />
                  </CTabPane>
                  {/* Send NewsManagement to Player(s) Tab */}
                  <CTabPane>
                    <UpdateNewsContent />
                  </CTabPane>
                  <CTabPane>


                  </CTabPane>
                  <CTabPane>

                  </CTabPane>
                </CTabContent>
              </CTabs>
            </CCardBody>
          </CCard>

      </CContainer>
    </div>
  )
}

export default NewsManagement
