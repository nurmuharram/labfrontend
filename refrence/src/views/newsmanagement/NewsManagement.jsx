import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CContainer,

} from '@coreui/react'

import NewsPost from './NewsPost'
import NewsListFilter from './NewsListFilter'

function NewsManagement() {




  return (
    <div className="NewsManagement">
      <CCol align='center'>
        <CCard style={{ background: '#9bf54c' }}>
          <h4>
            <b>News Management</b>
          </h4>
          <h6>
            <i>News Manager</i>
          </h6>
        </CCard>
      </CCol>

      <CContainer>
        <CCol className="mb-4">
          <CCard accentColor="black">
            <center>
            </center>
            <CCardBody>
              <CTabs>
                <CNav variant="tabs">
                  <CNavItem>
                    <CNavLink>
                      News List
                    </CNavLink>
                  </CNavItem>
                  <CNavItem>
                    <CNavLink>
                      Post News
                    </CNavLink>
                  </CNavItem>
                </CNav>
                <CTabContent>

                  {/*IMPORT NEWS LIST*/}
                  <CTabPane>
                    <NewsListFilter />
                  </CTabPane>
                  {/* Send NewsManagement to Player(s) Tab */}
                  <CTabPane>
                    <NewsPost />
                  </CTabPane>

                </CTabContent>
              </CTabs>
            </CCardBody>
          </CCard>
        </CCol>
      </CContainer>
    </div>
  )
}

export default NewsManagement
