import React, { useState, useEffect } from "react";
import { Button, Modal, Input, Form } from "react-bootstrap";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CCardFooter,
  CCol,
  CRow,
  CForm,
  CModal,
  CModalHeader,
  CModalTitle,
  CFormGroup,
  CLabel,
  CTextarea,
  CSelect,
  CInput,
  CButton,
} from "@coreui/react";
import { useHistory, useLocation } from "react-router-dom";
import { connect, useSelector, useDispatch } from "react-redux";
import axios from "axios";
import { getNewsAction } from "src/redux/action/newsmanagement/getNewsAction";
function UpdateNewsBanner(props) {
  const dispatch = useDispatch();

  const [pageNumber, setPageNumber] = useState(0);
  const newsPerPage = 5;
  const pagesVisited = pageNumber * newsPerPage;

  // const UpdateNewsBanner = useSelector((state) => state.data);
  // useEffect(() => {
  //     dispatch(getNewsAction());
  // }, []);
  const [newsid, setNewsID] = useState("");
  const [banner, setBanner] = useState("");
  // const [newsSearch, setNewsSearch] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();

    data.append("id", newsid);
    data.append("banner", banner);
    const config = {
        method: 'PUT',
        url: `api/news/updateDetailBanner?id=${newsid}`,
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
            "Content-Type": "multipart/form-data"
        },
        data: data
    };

    axios(config)
        .then((response) => {
            console.log(JSON.stringify(response.data));
            alert('News Banner Submited')
        })
        .catch((error) => {
            console.log(error);
            alert('Failed to Submit News!')
        });
  };

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  useEffect(() => {
    props.getNews();
    console.log("ini data", props.data);
  }, []);

  const pageCount = Math.ceil(props.data.length / newsPerPage);
  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  return (
    <div class="container ">
      <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
        <div>
            <center>
            <CCardBody>
              <CCardBody>
                <CForm
                  action=""
                  method="post"
                  encType="multipart/form-data"
                  className="form-horizontal"
                >
                  <CFormGroup row>
                    <CCol md="3" xs="9">
                      <CLabel htmlFor="text-input">
                        <b>Select News</b>
                      </CLabel>
                    </CCol>
                    <CCol xs="12" md="9">
                      <select  class="form-control form-control"onChange={(e) => setNewsID(e.target.value)}>
                      <option disabled>Select News</option>
                        {props.data.length > 0 &&
                          props.data.map((item, index) => {
                            return (
                             
                              <option value={item.id}>
                                {item.id}-{item.name}
                              </option>
                            );
                          })}

                        {/* {listDropdown} */}
                        {/* <option values='1'>Whats's New</option> */}
                      </select>
                    </CCol>
                  </CFormGroup>

                  <CFormGroup row>
                    <CCol md="3">
                      <CLabel htmlFor="textarea-input" name="newscontent">
                        <b>Banner</b>
                      </CLabel>
                    </CCol>
                    <CCol xs="12" md="9">
                        <center>
                      <form>
                        <div class="form-group">
                          <input
                            type="file"
                            name="file "
                            accept=".txt"
                            class="form-control-file"
                            
                            id="banner"
                            onChange={(e) => setBanner(e.target.files[0])}
                          />
                        </div>
                      </form>
                      </center>
                    </CCol>
                  </CFormGroup>

                  <CButton
                    color="primary"
                    className="mr-1"
                    type="submit"
                    onClick={handleSubmit}
                  >
                    Update News
                  </CButton>
                </CForm>
              </CCardBody>
              <CCardFooter></CCardFooter>
            </CCardBody>
          </center>
        </div>
        <div class="row">
          <div class="table-responsive ">
            {/* SEARCH MENU */}
            {/* Search, belum fixed, cuma narik data 1 laman saja */}
            {/* <input type='text' placeholder='Search' onChange={(e) => { setNewsSearch(e.target.value) }}/>
             */}

            {/* <div class="col-sm-2 offset-sm-9  mt-1 mb-1 text-gred">
            <Button variant="primary" onClick={handleShow}>
              Add New news
            </Button>
          </div> */}
          </div>
        </div>

        {/* <!--- Model Box ---> */}
        <div className="model_box">
          <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
          >
            <Modal.Header closeButton>
              <Modal.Title>Add New news</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <form>
                <div class="form-group">
                  <input
                    class="form-control"
                    id="title"
                    aria-describedby="newsTitle"
                    placeholder="News Title"
                  />
                </div>
                <button type="submit" class="btn btn-success mt-4">
                  Register
                </button>
              </form>
            </Modal.Body>

            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>

          {/* Model Box Finsihs */}
        </div>
      </div>
    </div>
  );
}

// 1st set redux
const mapDispatchToProps = (dispatch) => ({
  getNews: () => dispatch(getNewsAction()),
});
const mapStateToProps = (state) => ({
  isLoading: state.getNewsReducer.isLoading,
  data: state.getNewsReducer.data,
});

export default connect(mapStateToProps, mapDispatchToProps)(UpdateNewsBanner);
