import React from "react";
import { useState } from "react";
import { GetMostCompletedMission } from "src/api/MissionStatsApi/MissionStatsGetRequests";
import DataTable from "src/components/table/DataTable";

function MostCompletedMission() {
  const [mostCompletedMission, setMostCompletedMission] = useState([]);
  GetMostCompletedMission({ setMostCompletedMission });

  return (
    <div>
      <div className="card card-accent-info">
        <div className="card-header" align="center">
          <b>
            <u>Most Completed Mission</u>
          </b>
        </div>
        <div className="card-body">
          <DataTable items={mostCompletedMission} itemsPerPage={5} pagination />
        </div>
      </div>
    </div>
  );
}

export default MostCompletedMission;
