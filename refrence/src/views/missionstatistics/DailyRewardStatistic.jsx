import { CChartPie } from "@coreui/react-chartjs";
import React from "react";
import { useState } from "react";
import { GetDailyReward } from "src/api/MissionStatsApi/MissionStatsGetRequests";

function DailyRewardStatistic() {
  const [dailyRewardCount, setDailyRewardCount] = useState([]);

  GetDailyReward({ setDailyRewardCount });

  const count = dailyRewardCount.map((count, index) => {
    return count.done_count;
  });

  const points = dailyRewardCount.map((point, index) => {
    return point.reward_desc;
  });

  return (
    <div>
      <div className="card card-accent-info">
        <div className="card-header" align="center">
          <b>
            <u>Daily Reward Statistic</u>
          </b>
        </div>
        <div className="card-body">
          <CChartPie
            datasets={[
              {
                backgroundColor: [
                  "#B42828",
                  "#00FF66",
                  "#000AFF",
                  "FF00B8",
                  "#FFA800",
                ],
                data: count,
              },
            ]}
            labels={points}
            options={{
              tooltips: {
                enabled: true,
              },
            }}
          />
        </div>
      </div>
    </div>
  );
}

export default DailyRewardStatistic;
