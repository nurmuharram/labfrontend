import React, { useState } from "react";
import { GetWeeklyReward } from "src/api/MissionStatsApi/MissionStatsGetRequests";

function WeeklyRewardCount() {
  const [weeklyRewardsCount, setWeeklyRewardsCount] = useState([]);
  GetWeeklyReward({ setWeeklyRewardsCount });

  const Count = weeklyRewardsCount.map((count, index) => {
    return (
      <div className="col-md-12" align="center">
        <h1>
          {count.reward_desc} : {count.done_count}
        </h1>
        <br />
      </div>
    );
  });
  return (
    <div>
      <div className="card card-accent-info">
        <div className="card-header" align="center">
          <b>
            <u>Weekly Reward Count</u>
          </b>
        </div>
        <div className="card-body">
          <br />
          {Count}
          <br />
        </div>
      </div>
    </div>
  );
}

export default WeeklyRewardCount;
