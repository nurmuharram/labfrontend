import React from "react";
import DailyRewardStatistic from "./DailyRewardStatistic";
import MostCompletedMission from "./MostCompletedMission";
import MostNotCompletedMission from "./MostNotCompletedMission";
import WeeklyRewardCount from "./WeeklyRewardCount";

function MissionStatistic() {
  return (
    <div>
      <div>
        <div className="col" align="center">
          <div className="card" style={{ background: "#9bf54c" }}>
            <h4>
              <b>Mission Statistic</b>
            </h4>
            <h6>
              <i>View Mission Stats</i>
            </h6>
          </div>
        </div>
        <br />
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-6 col-md-12">
              <DailyRewardStatistic />
              <MostCompletedMission />
            </div>
            <div className="col-lg-6 col-md-12">
              <WeeklyRewardCount />
              <MostNotCompletedMission />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MissionStatistic;
