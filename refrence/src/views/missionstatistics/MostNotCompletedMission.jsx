import React, { useState } from "react";
import { GetMostNotCompletedMission } from "src/api/MissionStatsApi/MissionStatsGetRequests";
import DataTable from "src/components/table/DataTable";

function MostNotCompletedMission() {
  const [mostNotCompletedMission, setMostNotCompletedMission] = useState([]);
  GetMostNotCompletedMission({ setMostNotCompletedMission });
  return (
    <div>
      <div className="card card-accent-info">
        <div className="card-header" align="center">
          <b>
            <u>Most Not Completed Mission</u>
          </b>
        </div>
        <div className="card-body">
          <DataTable
            items={mostNotCompletedMission}
            itemsPerPage={5}
            pagination
          />
        </div>
      </div>
    </div>
  );
}

export default MostNotCompletedMission;
