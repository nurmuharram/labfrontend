import React from "react";
import { Form } from "react-bootstrap";
import { UpdateForceUpdateCodeVersion, UpdateForceUpdateCreateDate, UpdateForceUpdatePlatform, UpdateForceUpdateVersionString } from "src/api/forceupdateAPI/forceUpdatePutRequest";

export const ForceUpdateForm = ({addForceUpdateForm, setAddForceUpdateForm}) => {
    const handleChange = (e) => {
        const { name, value } = e.target;
        setAddForceUpdateForm({ ...addForceUpdateForm, [name]: value });
      };
  return (
    <div>
      <Form>
        <Form.Group className="mb-3" controlId="description">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Version String</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-8">
              <Form.Control
                type="text"
                name="VersionString"
                value={addForceUpdateForm.VersionString}
                placeholder="Set Version String..."
                onChange={handleChange}
              />
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="description">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Code Version</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-8">
              <Form.Control
                type="number"
                name="CodeVersion"
                onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                value={addForceUpdateForm.CodeVersion}
                placeholder="Set Code Version..."
                onChange={handleChange}
              />
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="target">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Create Time</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-4">
              <Form.Control
                type="date"
                name="CreateDate"
                value={addForceUpdateForm.CreateDate}
                onKeyDown={(e) => e.preventDefault()}
                onChange={handleChange}
              />
            </div>
            <div className="col-md-4">
              <Form.Control
                type="time"
                name="CreateTime"
                value={addForceUpdateForm.CreateTime}
                onChange={handleChange}
              />
            </div>
          </div>
        </Form.Group>
        <Form.Group className="mb-3" controlId="missiontype">
          <div className="row">
            <div className="col-md-3">
              <Form.Label>
                <b>Platform</b>
              </Form.Label>
            </div>
            :
            <div className="col-md-6">
              <select name="Platform" onChange={handleChange}>
                  <option value={"Android"}>
                    Android
                  </option>
                  <option value={"iOS"}>
                    iOS
                  </option>
              </select>
            </div>
          </div>
        </Form.Group>
      </Form>
    </div>
  );
};


export const UpdateForceUpdateForm = ({id, updateForceForm, setUpdateForceForm, setForceUpdateList}) => {
  const handleChange = (e) => {
      const { name, value } = e.target;
      setUpdateForceForm({ ...updateForceForm, [name]: value });
    };
return (
  <div>
    <Form>
      <Form.Group className="mb-3" controlId="description">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>New Version String</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-4">
            <Form.Control
              type="text"
              name="VersionString"
              value={updateForceForm.VersionString}
              placeholder="Set Version String..."
              onChange={handleChange}
            />
          </div>
          <div className="col-md-4">
            <UpdateForceUpdateVersionString
              id={id}
              updateForceForm={updateForceForm}
              setUpdateForceForm={setUpdateForceForm}
              setForceUpdateList={setForceUpdateList}
            />
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="description">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>New Code Version</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-4">
            <Form.Control
              type="number"
              name="CodeVersion"
              onKeyPress={(event) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              value={updateForceForm.CodeVersion}
              placeholder="Set New Code Version..."
              onChange={handleChange}
            />
          </div>
          <div className="col-md-4">
            <UpdateForceUpdateCodeVersion
              id={id}
              updateForceForm={updateForceForm}
              setUpdateForceForm={setUpdateForceForm}
              setForceUpdateList={setForceUpdateList}
            />
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="target">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>New Create Time</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-2">
            <Form.Control
              type="date"
              name="CreateDate"
              value={updateForceForm.CreateDate}
              onKeyDown={(e) => e.preventDefault()}
              onChange={handleChange}
            />
          </div>
          <div className="col-md-2">
            <Form.Control
              type="time"
              name="CreateTime"
              value={updateForceForm.CreateTime}
              onChange={handleChange}
            />
          </div>
          <div className="col-md-4">
            <UpdateForceUpdateCreateDate
              id={id}
              updateForceForm={updateForceForm}
              setUpdateForceForm={setUpdateForceForm}
              setForceUpdateList={setForceUpdateList}
            />
          </div>
        </div>
      </Form.Group>
      <Form.Group className="mb-3" controlId="missiontype">
        <div className="row">
          <div className="col-md-3">
            <Form.Label>
              <b>New Platform</b>
            </Form.Label>
          </div>
          :
          <div className="col-md-1">
            <select name="Platform" onChange={handleChange}>
              <option value={"Android"}>Android</option>
              <option value={"iOS"}>iOS</option>
            </select>
          </div>
          <div className="col-md-4">
            <UpdateForceUpdatePlatform
              id={id}
              updateForceForm={updateForceForm}
              setUpdateForceForm={setUpdateForceForm}
              setForceUpdateList={setForceUpdateList}
            />
          </div>
        </div>
      </Form.Group>
    </Form>
  </div>
);
};