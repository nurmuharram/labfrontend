import React, { useState } from "react";
import { AddForceUpdate } from "src/api/forceupdateAPI/forcePostRequest.";
import { DeleteForceUpdateItem } from "src/api/forceupdateAPI/forceUpdateDeleteRequest";
import { Modal, ModalBody, ModalFooter, ModalHeader, ModalTitle } from "src/components/modal";
import { ForceUpdateForm } from "../form/ForceUpdateForm";

export const AddForceModal = ({
  setAddItemModal,
  addItemModal,
  setForceUpdateList,
}) => {
  const todayDate = new Date().toISOString().split("T")[0];
  const [addForceUpdateForm, setAddForceUpdateForm] = useState({
    VersionString: "0.0.0000",
    CodeVersion: "",
    CreateDate: todayDate,
    CreateTime: "00:00",
    Platform: "Android",
  });

  return (
    <div>
      <Modal
        show={addItemModal}
        onClose={() => setAddItemModal(!addItemModal)}
        color="primary"
        size="md"
      >
        <ModalHeader closeButton>
          <ModalTitle>Add Force Update</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <ForceUpdateForm
            addForceUpdateForm={addForceUpdateForm}
            setAddForceUpdateForm={setAddForceUpdateForm}
          />
        </ModalBody>
        <ModalFooter>
          {" "}
          <button
            type="button"
            className="btn btn-secondary"
            onClick={() => setAddItemModal(!addItemModal)}
          >
            Close
          </button>
          {addForceUpdateForm.CodeVersion.length > 0 ? (
            <AddForceUpdate
              setAddItemModal={setAddItemModal}
              addForceUpdateForm={addForceUpdateForm}
              setForceUpdateList={setForceUpdateList}
            />
          ) : (
            <button className="btn btn-primary btn-md" disabled>
              + Add
            </button>
          )}
        </ModalFooter>
      </Modal>
    </div>
  );
};


export const DeleteForceUpdateModal = ({
  id,
  setDeleteItemModal,
  setForceUpdateList,
  deleteItemModal,
}) => {
  return (
    <Modal
      show={deleteItemModal}
      onClose={() => setDeleteItemModal(!deleteItemModal)}
      color="primary"
        size="md"
    >
      <ModalHeader className="bg-danger" closeButton>
        <ModalTitle>You're About To Delete this Item?</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="col" align="center">
          <h4>Are You Sure?</h4>
          <small>(item ID : {id})</small>
        </div>
        
      </ModalBody>
      <ModalFooter>
        <button
          className="btn btn-secondary btn-sm"
          onClick={() => setDeleteItemModal(!deleteItemModal)}
        >
          Cancel
        </button>
        {DeleteForceUpdateItem({ setDeleteItemModal, id, setForceUpdateList })}
      </ModalFooter>
    </Modal>
  );
};
