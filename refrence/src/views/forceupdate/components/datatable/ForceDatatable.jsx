import React, { useState } from "react";
import { Collapse } from "react-bootstrap";
import DataTable from "src/components/table/DataTable";
import { toggleDetails } from "src/services/forceupdate.services/ForceUpdate.function";
import { UpdateForceUpdateForm } from "../form/ForceUpdateForm";
import { DeleteForceUpdateModal } from "../modal/ForceModal";

export const ForceDatatable = ({ forceUpdateList, setForceUpdateList }) => {
  const [details, setDetails] = useState([])
  const [deleteItemModal, setDeleteItemModal] = useState(false)
  const todayDate = new Date().toISOString().split("T")[0];
  const [updateForceForm, setUpdateForceForm] = useState({
    CodeVersion: "",
    CreateDate: todayDate,
    CreateTime: "00:00",
    Platform: "Android",
    VersionString: "",
  });
  const fields = [
    { key: 'version_id', _style: { width: '10%' } },
    { key: 'version_string', _style: { width: '30%' } },
    { key: 'code_version', _style: { width: '20%' } },
    { key: 'create_time', _style: { width: '20%' } },
    { key: 'platform', _style: { width: '10%' } },
    {
        key: 'show_details',
        label: '',
        _style: { width: '10%' },
        sorter: false,
        filter: false
    }
]
  return (
    <div>
      <DataTable
        items={forceUpdateList}
        fields={fields}
        itemsPerPage={5}
        pagination
        columnFilter
        sorterValue={{ column: "version_id", asc: false }}
        scopedSlots={{
          show_details: (item, index) => {
            return (
              <td className="py-2">
                <button
                  className="btn btn-square btn-outline-info btn-sm"
                  onClick={() => {
                    toggleDetails(index, { setDetails, details });
                  }}
                >
                  {details.includes(index) ? "Hide" : "Options"}
                </button>
              </td>
            );
          },
          details: (item, index) => {
            const id = item.version_id
        
            return (<Collapse in={details.includes(index)}>
            <div className="card-body">
              <div className="row">
                <div className="col" align="left">
                  <h4>
                    <i>
                      Edit/Update Item (<small>ID : {id}</small>)
                    </i>
                  </h4>
                </div>
                <div className="col-lg-5 col-md-4" align="left">
                  <button
                    className="btn btn-outline-danger"
                    type="button"
                    onClick={() => setDeleteItemModal(!deleteItemModal)}
                  >
                    Delete Item
                  </button>
                  <DeleteForceUpdateModal
                    id={id}
                    setDeleteItemModal={setDeleteItemModal}
                    setForceUpdateList={setForceUpdateList}
                    deleteItemModal={deleteItemModal}
                  />
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col" align="left">
                  <UpdateForceUpdateForm
                    id={id}
                    updateForceForm={updateForceForm}
                    setUpdateForceForm={setUpdateForceForm}
                    setForceUpdateList={setForceUpdateList}
                  />
                </div>
              </div>
            </div>
          </Collapse>);
          },
        }}
      />
    </div>
  );
};
