import React, { useState } from "react";
import { GetForceUpdateList } from "src/api/forceupdateAPI/forceGetRequest";
import { ForceDatatable } from "./components/datatable/ForceDatatable";
import { AddForceModal } from "./components/modal/ForceModal";

function ForceUpdateMain() {
  const [forceUpdateList, setForceUpdateList] = useState([]);
  const [addItemModal, setAddItemModal] = useState(false);
  GetForceUpdateList({ setForceUpdateList });
  return (
    <div>
      <div>
        <div className="container-fluid">
          <div className="card card-accent-primary">
            <div className="card-header" align="center">
              <h5>
                <b>Force Update</b>
              </h5>
            </div>
            <div className="card-body">
              <div className="col">
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => setAddItemModal(!addItemModal)}
                >
                  + Add Force Update
                </button>
                <AddForceModal addItemModal={addItemModal} setAddItemModal={setAddItemModal} />
              </div>
              <br />
              <div className="col">
                <ForceDatatable
                  forceUpdateList={forceUpdateList}
                  setForceUpdateList={setForceUpdateList}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ForceUpdateMain;
