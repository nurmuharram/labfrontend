import React, { useState, useEffect, useRef } from 'react'
import {
    CCardBody,
    CCardHeader,
    CContainer,
    CCard,
    CCol,
    CDataTable,
    CInput,
    CButton,
    CFormGroup,
    CRow
} from '@coreui/react'

import { useHistory } from 'react-router'

import axios from 'axios';
import { GetPastMatchList } from 'src/api/pastMatchApi/PastMatchGetApi'
import { SearchRoomData } from 'src/api/pastMatchApi/PastMatchGetApi'
import { RefreshData } from 'src/api/pastMatchApi/PastMatchGetApi'
import CardHeader from 'react-bootstrap/esm/CardHeader';
import DataTable from 'src/components/table/DataTable';

function PastMatchData() {

    const history = useHistory();

    const [matchData, setMatchData] = useState([]);
    const [changeData, setChangedata] = useState('')

    const inputChange = useRef();
    inputChange.current = changeData;

    GetPastMatchList({setMatchData})
    SearchRoomData({inputChange, setMatchData}) 

    // useEffect(() => {
    //     const config = {
    //         headers: {
    //             Authorization: 'Bearer ' + localStorage.getItem('auth'),
    //         }
    //     }
    //     axios.get(`/api/stats/getRoomList?room_id?&offset=0&count=1000`, config).then(res => {
    //         const matchData = res.data;
    //         setMatchData(matchData);
    //     });
    // }, []);

    // const searchroom = () => {
    //     const config = {
    //         headers: {
    //             Authorization: 'Bearer ' + localStorage.getItem('auth'),
    //         }
    //     }
    //     axios.get(`/api/stats/getRoomList?room_id=${inputChange.current}?&offset=0&count=10`, config).then(res => {
    //         const matchData = res.data;
    //         setMatchData(matchData);
    //     });
    // }

    GetPastMatchList({setMatchData})
    RefreshData({setMatchData})

    // const refreshData = () => {
    //     const config = {
    //         headers: {
    //             Authorization: 'Bearer ' + localStorage.getItem('auth'),
    //         }
    //     }
    //     axios.get(`/api/stats/getRoomList?room_id?&offset=0&count=10000`, config).then(res => {
    //         const matchData = res.data;
    //         setMatchData(matchData);
    //     });
    // }

    const fields = [
        { key: 'room_id', _style: { width: '10%' }, sorter: false },
        { key: 'match_id', _style: { width: '10%' } },
        { key: 'room_name', _style: { width: '20%' } },
        { key: 'create_time', _style: { width: '20%' } },
        { key: 'game_duration', _style: { width: '15%' } },
        { key: 'game_mode', _style: { width: '20%' } },
    ]

    return (
        <div>
            <div className="col" align='center'>
                <div className="card" style={{ background: '#9bf54c' }}>

                    <h4>
                        <b>Past Match Data</b>
                    </h4>
                    <h6>
                        <i>View Past Match Data</i>
                    </h6>

                </div>
            </div>

            <br />
            <div className="container">
                <div className="col">
                    <div className="card card-accent-primary">
                        <div className="card-header">

                            <h5>Past Match Data List</h5>

                        </div>

                        <div className="card-body">
                            <div className="col">
                                <br />
                                    <CFormGroup row>
                                        <input class="form-control" className='col-md-2' placeholder="Search by Room ID here..." type="text"  onChange={(e) => setChangedata(e.target.value.replace(/\D/, ''))}/>
                                        <button className='btn btn-primary btn-sm ' onClick={inputChange.current.length < 1 ? RefreshData({setMatchData}) : SearchRoomData({inputChange, setMatchData, RefreshData})} >Search Room </button>     
                                    </CFormGroup>
                            </div>

                                {matchData === null ? <>
                                <div className="row">
                                    <div className="col" align='center'>
                                        <h4>Ooopss... No Room Found!</h4>
                                        <button className='btn btn-primary btn-sm ' onClick={RefreshData({setMatchData})} >Reload Data </button>                                              

                                    </div>
                              
                                </div>

                                </> : <><DataTable
                                    items={matchData}
                                    fields={fields}
                                    itemsPerPageSelect
                                    clickableRows
                                    onRowClick={(item) => history.push(`/matchdetails/${item.room_id}`)}
                                    itemsPerPage={10}
                                    hover
                                    sorter
                                    pagination
                                    scopedSlots={{
                                        'game_duration':
                                            (item) => {
                                                const duration = item.game_duration / 60
                                                const durationresult = duration.toFixed(0)

                                                return (
                                                    <td>
                                                        {durationresult} mins
                                                    </td>
                                                )
                                            },
                                        'game_mode':
                                            (item) => {
                                                const gameMode = () => {
                                                    if (item.game_mode === 1) {
                                                        return 'Classic'
                                                    } else if (item.game_mode === 2) {
                                                        return 'Ranked (Blind Pick)'
                                                    } else if (item.game_mode === 3) {
                                                        return 'Ranked (Draft Pick)'
                                                    } else if (item.game_mode === 101) {
                                                        return 'Tutorial'
                                                    }
                                                };
                                                return (
                                                    <td>
                                                        {gameMode()}
                                                    </td>
                                                )
                                            },
                                        "create_time":
                                            (item, index) => {
                                                const date = new Date((item.create_time && item.create_time.split(' ').join('T')) + '.000Z')
                                                return (
                                                    <td>
                                                        {date.toString()}
                                                    </td>
                                                )
                                            }
                                    }}
                                /></>}
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    )
}

export default PastMatchData
