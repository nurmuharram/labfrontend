import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { tagPropType } from '../utils/helper.js'
import { Context } from './Modal'

const ModalHeader = props=>{

  const {
    tag: Tag,
    className,
    //
    innerRef,
    closeButton,
    ...attributes
  } = props

  const { close } = useContext(Context)
  
  //render


  const classes = classNames(
    className,
    'modal-header'
  )

  return (
    <Tag className={classes} {...attributes} ref={innerRef}>
      {props.children}
      {closeButton && <button type="button" class="close" aria-label="Close" onClick={close}>
  <span aria-hidden="true">&times;</span>
</button>}
    </Tag>
  )

}

ModalHeader.propTypes = {
  tag: tagPropType,
  children: PropTypes.node,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.array, PropTypes.object]),
  //
  innerRef: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  closeButton: PropTypes.bool
};

ModalHeader.defaultProps = {
  tag: 'header'
};

export default ModalHeader
