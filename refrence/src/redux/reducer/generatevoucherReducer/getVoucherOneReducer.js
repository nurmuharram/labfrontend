const initialState = {
    voucherone: [],
    loading: false,
    error: null,
  };
  
  const voucherone = (state = initialState, action) => {
    switch (action.type) {
      case "GET_VOUCHER_ONE_REQUEST":
        return { ...state, loading: true };
      case "GET_VOUCHER_ONE_SUCCESS":
        return { ...state, loading: false, voucherone: action.voucherone };
      case "GET_VOUCHER_ONE_FAILED":
        return { ...state, loading: false, error: action.message };
      default:
        return state;
    }
  };
  
  export default voucherone;