const initialState = {
    vouchers: [],
    loading: false,
    error: null,
  };
  
  const vouchers = (state = initialState, action) => {
    switch (action.type) {
      case "GET_VOUCHERS_REQUEST":
        return { ...state, loading: true };
      case "GET_VOUCHERS_SUCCESS":
        return { ...state, loading: false, vouchers: action.vouchers };
      case "GET_VOUCHERS_FAILED":
        return { ...state, loading: false, error: action.message };
      default:
        return state;
    }
  };
  
  export default vouchers;