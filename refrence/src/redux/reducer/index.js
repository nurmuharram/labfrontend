import { combineReducers } from "redux";
import loginReducer from './loginReducer';
import templates from "./mailReducer/getmailtemplReducer";
import mails from "./mailReducer/getMailsReducer";
import loginmails from "./mailReducer/getLoginMailReducer";
import attachments from "./mailReducer/getAttachmentsReducer";
import players from "./mailReducer/getPlayersReducer.";
import ksatriyas from "./mailReducer/getKsatriyasReducer";
import ksatriyaFragments from "./mailReducer/getKsatriyaFragmentReducer";
import boxes from "./mailReducer/getBoxesReducer";
import skins from "./mailReducer/getSkinsReducer";
import skinParts from "./mailReducer/getSkinPartsReducer";
import runes from "./mailReducer/getRunesReducer";
import frames from "./mailReducer/getFramesReducer";
import avatars from "./mailReducer/getAvatarsReducer";
import customs from "./mailReducer/getCustomsReducer";
import userManagementReducer from "./userManagementReducer";
import items from "./mailReducer/getItemsReducer";
import getNewsReducer from "./newsmanagement/getNewsReducer";
import currentInfoReducer from "./currentInfoReducer/currentInfoReducer";
import rolesManagementReducer from "./rolesmanagement/rolesManagementReducer";
import users from "./userManagementReducer";
import roles from "./rolesmanagement/rolesManagementReducer";
import vtemplates from "./generatevoucherReducer/getVoucherTemplateReducer";
import vouchers from "./generatevoucherReducer/getVouchersReducer";
import voucherone from "./generatevoucherReducer/getVoucherOneReducer";
import getMaintenanceReducer from "./maintenancemanagement/getMaintenanceReducer";


export default combineReducers({
    loginReducer, templates, mails, loginmails, attachments, players, ksatriyas, boxes, skins, runes, frames, avatars, 
    customs, userManagementReducer, getNewsReducer, items, currentInfoReducer, rolesManagementReducer, users, roles, vtemplates, vouchers, voucherone, getMaintenanceReducer, skinParts, ksatriyaFragments


})