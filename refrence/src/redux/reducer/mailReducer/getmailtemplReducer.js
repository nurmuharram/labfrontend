const initialState = {
    templates: [],
    temploading: false,
    error: null,
  };
  
  const templates = (state = initialState, action) => {
    switch (action.type) {
      case "GET_TEMPLATES_REQUEST":
        return { ...state, temploading: true };
      case "GET_TEMPLATES_SUCCESS":
        return { ...state, temploading: false, templates: action.templates };
      case "GET_TEMPLATES_FAILED":
        return { ...state, temploading: false, error: action.message };
      default:
        return state;
    }
  };
  
  export default templates;