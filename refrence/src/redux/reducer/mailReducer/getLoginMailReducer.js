const initialState = {
    loginmails: [],
    loading: false,
    error: null,
  };
  
  const loginmails = (state = initialState, action) => {
    switch (action.type) {
      case "GET_LOGIN_MAIL_REQUEST":
        return { ...state, loading: true };
      case "GET_LOGIN_MAIL_SUCCESS":
        return { ...state, loading: false, loginmails: action.loginmails };
      case "GET_LOGIN_MAIL_FAILED":
        return { ...state, loading: false, error: action.message };
      default:
        return state;
    }
  };
  
  export default loginmails;