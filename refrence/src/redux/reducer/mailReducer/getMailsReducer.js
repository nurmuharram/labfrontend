const initialState = {
    mails: [],
    loading: false,
    error: null,
  };
  
  const mails = (state = initialState, action) => {
    switch (action.type) {
      case "GET_MAIL_REQUEST":
        return { ...state, loading: true };
      case "GET_MAIL_SUCCESS":
        return { ...state, loading: false, mails: action.mails };
      case "GET_MAIL_FAILED":
        return { ...state, loading: false, error: action.message };
      default:
        return state;
    }
  };
  
  export default mails;