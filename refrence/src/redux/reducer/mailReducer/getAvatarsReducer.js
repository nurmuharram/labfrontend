const initialState = {
    avatars: [],
    loading: false,
    error: null,
  };
  
  const avatars = (state = initialState, action) => {
    switch (action.type) {
      case "GET_AVATARS_REQUEST":
        return { ...state, loading: true };
      case "GET_AVATARS_SUCCESS":
        return { ...state, loading: false, avatars: action.avatars };
      case "GET_AVATARS_FAILED":
        return { ...state, loading: false, error: action.message };
      default:
        return state;
    }
  };
  
  export default avatars;