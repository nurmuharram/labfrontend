const initialState = {
    customs: [],
    loading: false,
    error: null,
  };
  
  const customs = (state = initialState, action) => {
    switch (action.type) {
      case "GET_CUSTOMS_REQUEST":
        return { ...state, loading: true };
      case "GET_CUSTOMS_SUCCESS":
        return { ...state, loading: false, customs: action.customs };
      case "GET_CUSTOMS_FAILED":
        return { ...state, loading: false, error: action.message };
      default:
        return state;
    }
  };
  
  export default customs;