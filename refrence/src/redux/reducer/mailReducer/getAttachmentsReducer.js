const initialState = {
    attachments: [],
    loading: false,
    error: null,
  };
  
  const attachments = (state = initialState, action) => {
    switch (action.type) {
      case "GET_ATTACHMENTS_REQUEST":
        return { ...state, loading: true };
      case "GET_ATTACHMENTS_SUCCESS":
        return { ...state, loading: false, attachments: action.attachments };
      case "GET_ATTACHMENTS_FAILED":
        return { ...state, loading: false, error: action.message };
      default:
        return state;
    }
  };
  
  export default attachments;