import { call, put, takeLatest } from "redux-saga/effects";
import fetchGetLoginMail from "src/api/mailAPI/getLoginMailApi";

function* handleGetLoginMail() {
  try {
    const loginmails = yield call(fetchGetLoginMail);
    yield put({ type: "GET_LOGIN_MAIL_SUCCESS", loginmails: loginmails });
  } catch (err) {
    yield put({ type: "GET_LOGIN_MAIL_FAILED", message: err.message });
  }
}

function* watcherLoginMailSaga() {
  yield takeLatest("GET_LOGIN_MAIL_REQUEST", handleGetLoginMail);
}

export default watcherLoginMailSaga;