import { call, put, takeLatest } from "redux-saga/effects";
import fetchGetTemplates from "src/api/mailAPI/getmailtemplApi";

function* handleGetTemplates() {
  try {
    const templates = yield call(fetchGetTemplates);
    yield put({ type: "GET_TEMPLATES_SUCCESS", templates: templates });
  } catch (err) {
    yield put({ type: "GET_TEMPLATES_FAILED", message: err.message });
  }
}

function* watcherTemplateSaga() {
  yield takeLatest("GET_TEMPLATES_REQUEST", handleGetTemplates);
}

export default watcherTemplateSaga;