import { call, put, takeLatest } from "redux-saga/effects";
import fetchGetCustoms from "src/api/mailAPI/getCustommessageApi";

function* handleGetCustoms() {
  try {
    const customs = yield call(fetchGetCustoms);
    yield put({ type: "GET_CUSTOMS_SUCCESS", customs: customs });
  } catch (err) {
    yield put({ type: "GET_CUSTOMS_FAILED", message: err.message });
  }
}

function* watcherCustomsSaga() {
  yield takeLatest("GET_CUSTOMS_REQUEST", handleGetCustoms);
}

export default watcherCustomsSaga;