import { call, put, takeLatest } from "redux-saga/effects";
import fetchGetAttachments from "src/api/mailAPI/getAttachments";

function* handleGetAttachments() {
  try {
    const attachments = yield call(fetchGetAttachments);
    yield put({ type: "GET_ATTACHMENTS_SUCCESS", attachments: attachments });
  } catch (err) {
    yield put({ type: "GET_ATTACHMENTS_FAILED", message: err.message });
  }
}

function* watcherAttachmentsSaga() {
  yield takeLatest("GET_ATTACHMENTS_REQUEST", handleGetAttachments);
}

export default watcherAttachmentsSaga;