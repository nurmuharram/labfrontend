import { call, put, takeLatest } from "redux-saga/effects";
import fetchGetMails from "src/api/mailAPI/getMailsApi";

function* handleGetMails() {
  try {
    const mails = yield call(fetchGetMails);
    yield put({ type: "GET_MAIL_SUCCESS", mails: mails });
  } catch (err) {
    yield put({ type: "GET_MAIL_FAILED", message: err.message });
  }
}

function* watcherMailSaga() {
  yield takeLatest("GET_MAIL_REQUEST", handleGetMails);
}

export default watcherMailSaga;