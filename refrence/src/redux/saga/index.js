import { all } from 'redux-saga/effects';
import loginWatcher from './loginSaga';
import watcherTemplateSaga from './mailSaga/getmailtemplSaga';
import watcherMailSaga from './mailSaga/getMailsSaga';
import watcherLoginMailSaga from './mailSaga/getLoginMailSaga';
import watcherAttachmentsSaga from './mailSaga/getAttachmentsSaga';
import watcherPlayersSaga from './mailSaga/getPlayersSaga';
import watcherKsatriyasSaga from './mailSaga/getKsatriyasSaga';
import watcherBoxesSaga from './mailSaga/getBoxesSaga';
import watcherSkinsSaga from './mailSaga/getSkinsSaga';
import watcherRunesSaga from './mailSaga/getRunesSaga';
import watcherFramesSaga from './mailSaga/getFramesSaga';
import watcherAvatarsSaga from './mailSaga/getAvatarsSaga';
import watcherCustomsSaga from './mailSaga/getCustomsSaga';
import userManagementSaga from './userManagementSaga';
import watcherItemsSaga from './mailSaga/getItemsSaga';

import getNewsSaga from './newsmanagement/getNewsSaga';
import currentInfoSaga from './currentInfo/currentInfoSaga';
import rolesManagementSaga from './rolesmanagement/rolesManagementSaga';
import watcherUsersSaga from './userManagementSaga';
import watcherVoucherTemplateSaga from './generatevoucherSaga/getVoucherTemplateSaga';
import watcherVouchers from './generatevoucherSaga/getVouchersSaga';
import watcherVoucherOne from './generatevoucherSaga/getVoucherOneSaga';
import getMaintenanceSaga from './maintenancemanagement/getMaintenanceSaga';
import watcherKsatriyaFragmentSaga from './mailSaga/getKsatriyaFragmentSaga';
import watcherSkinPartsSaga from './mailSaga/getSkinPartsSaga';

export default function* rootSaga() {
  yield all([
    loginWatcher(),
    watcherTemplateSaga(),
    watcherMailSaga(),
    userManagementSaga(),
    watcherLoginMailSaga(),
    watcherAttachmentsSaga(),
    watcherPlayersSaga(),
    watcherKsatriyasSaga(),
    watcherBoxesSaga(),
    watcherSkinsSaga(),
    watcherRunesSaga(),
    watcherFramesSaga(),
    watcherAvatarsSaga(),
    watcherCustomsSaga(),
    watcherItemsSaga(),
    getNewsSaga(),
    currentInfoSaga(),
    rolesManagementSaga(),
    watcherUsersSaga(),
    watcherVoucherTemplateSaga(),
    watcherVouchers(),
    watcherVoucherOne(),
    getMaintenanceSaga(),
    watcherKsatriyaFragmentSaga(),
    watcherSkinPartsSaga()

  ]);
}
