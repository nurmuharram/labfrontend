import { call, put, takeLatest } from "redux-saga/effects";
import fetchGetVouchers from "src/api/generatevoucherAPI/getVouchersAPI";

function* handleGetVouchers() {
  try {
    const vouchers = yield call(fetchGetVouchers);
    yield put({ type: "GET_VOUCHERS_SUCCESS", vouchers: vouchers });
  } catch (err) {
    yield put({ type: "GET_VOUCHERS_FAILED", message: err.message });
  }
}

function* watcherVouchers() {
  yield takeLatest("GET_VOUCHERS_REQUEST", handleGetVouchers);
}

export default watcherVouchers;