import { call, put, takeLatest } from "redux-saga/effects";
import fetchGetVoucherOne from "src/api/generatevoucherAPI/getVoucherOneAPI";

function* handleGetVoucherOne() {
  try {
    const voucherone = yield call(fetchGetVoucherOne);
    yield put({ type: "GET_VOUCHER_ONE_SUCCESS", voucherone: voucherone });
  } catch (err) {
    yield put({ type: "GET_VOUCHER_ONE_FAILED", message: err.message });
  }
}

function* watcherVoucherOne() {
  yield takeLatest("GET_VOUCHER_ONE_REQUEST", handleGetVoucherOne);
}

export default watcherVoucherOne;