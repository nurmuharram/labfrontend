import { currentInfoTypes } from "src/redux/action/types/currentInfoTypes/currentInfoTypes";

export const currentInfoAction = () => {
  return {type: currentInfoTypes.GET_INFO}
};
/* eslint-disable no-unused-expressions */