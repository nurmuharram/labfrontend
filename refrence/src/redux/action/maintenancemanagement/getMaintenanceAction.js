import {getMaintenanceTypes } from "src/redux/action/types/maintenancemanagement/getMaintenanceTypes";

export const getMaintenanceAction = () => {
  return {type: getMaintenanceTypes.GET_MAINTENANCE}
};
/* eslint-disable no-unused-expressions */