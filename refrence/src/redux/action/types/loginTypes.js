export const loginTypes = {
    LOGIN_REQUEST:'LOGIN_REQUEST',
    LOGIN_SUCCESS:'LOGIN_SUCCESS',
    LOGIN_FAILED:'LOGIN_FAILED'
}