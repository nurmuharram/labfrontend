import {getNewsTypes } from "src/redux/action/types/newsmanagement/getNewsTypes";

export const getNewsAction = () => {
  return {type: getNewsTypes.GET_NEWS}
};
/* eslint-disable no-unused-expressions */