import { CButton } from '@coreui/react';
import React, {useState, useEffect, useRef} from 'react'

import axios from 'axios';
import GuildMemberCheckinLog from 'src/views/guildmanagement/GuildMemberCheckinLog';
import { GetGuildMemberCheckinLogsList } from 'src/api/guildManagementApi/GuildGetApi'

function GuildMemberCheckinLogsservices({match}) {

    const [guildMemberCheckin, setGuildMemberCheckinLogsList] = useState([])
    const [changeList, setchangeList] = useState(0);

    const listChange = useRef();
    listChange.current = changeList;

    GetGuildMemberCheckinLogsList({
        match,setGuildMemberCheckinLogsList
    })


    const refreshdata = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/guild/getGuildMemberCheckInLogs?guild_id=${match.params.guild_id}&count=10&offset=${listChange.current}`, config).then(res => {
            const data = res.data;
            setGuildMemberCheckinLogsList(data);
        });
    }

    const previousPage = () => { setchangeList(changeList - 10); setTimeout(refreshdata, 100) }
    const nextPage = () => { setchangeList(changeList + 10); setTimeout(refreshdata, 100) }

    const previousButton = () => {
        return <CButton className="info" size="sm" color="primary" onClick={previousPage}>Previous Lists</CButton>;
    }

    const nextButton = () => {
        return <CButton className="info" size="sm" color="primary" onClick={nextPage} >Next Lists</CButton>;
    }

    const hidePreviousButton = () => {

        if (changeList === 0) {
            return <></>
        } else {
            return previousButton();
        }
    };

    const hideNextButton = () => {

        if (Object.keys(guildMemberCheckin).length < 10) {
            return <></>
        } else if (Object.keys(guildMemberCheckin).length >= 10) {
            return nextButton();
        }
    };
    
    return (
        <>
            <GuildMemberCheckinLog guildmembercheckin={guildMemberCheckin} hideNextButton={hideNextButton} hidePreviousButton={hidePreviousButton} />
        </>
    )
}

export default GuildMemberCheckinLogsservices
