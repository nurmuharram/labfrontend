import { CButton } from '@coreui/react';
import React, { useState, useEffect, useRef } from 'react'

import axios from 'axios';
import GuildMemberRankLogs from 'src/views/guildmanagement/GuildMemberRankLogs';
import { GetGuildMemberRankLogsServices} from 'src/api/guildManagementApi/GuildGetApi'

function GuildMemberRankLogsServices({match}) {

    const [guildRankLogs, setGuildRankLogs] = useState([])
    const [changeList, setChangelist] = useState(0);

    const listChange = useRef();
    listChange.current = changeList;

    GetGuildMemberRankLogsServices({
        match,setGuildRankLogs
    })


    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/guild/getGuildMemberRankLogs?guild_id=${match.params.guild_id}&count=10&offset=${listChange.current}`, config).then(res => {
            const data = res.data;
            setGuildRankLogs(data);
        });
    }

    const previousPage = () => { setChangelist(changeList - 10); setTimeout(refreshData, 100) }
    const nextPage = () => { setChangelist(changeList + 10); setTimeout(refreshData, 100) }

    const previousButton = () => {
        return <CButton className="info" size="sm" color="primary" onClick={previousPage}>Previous Lists</CButton>;
    }

    const nextButton = () => {
        return <CButton className="info" size="sm" color="primary" onClick={nextPage} >Next Lists</CButton>;
    }

    const hidePreviousButton = () => {

        if (changeList === 0) {
            return <></>
        } else {
            return previousButton();
        }
    };

    const hideNextButton = () => {

        if (Object.keys(guildRankLogs).length < 10) {
            return <></>
        } else if (Object.keys(guildRankLogs).length >= 10) {
            return nextButton();
        }
    };

    return (
        <>
            <GuildMemberRankLogs guildRankLogs={guildRankLogs} hidePreviousButton={hidePreviousButton} hideNextButton={hideNextButton} />
        </>
    )
}

export default GuildMemberRankLogsServices
