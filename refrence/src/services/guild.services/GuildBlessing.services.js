import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import GuildBlessing from 'src/views/guildmanagement/GuildBlessing';
import { GetGuildBlessing } from 'src/api/guildManagementApi/GuildGetApi';

function GuildBlessingservices({match}) {

    const [guildBlessings, setGuildBlessings] = useState([])
    const [changeList, setChangeList] = useState(0);

    const listChange = useRef();
    listChange.current = changeList;

    GetGuildBlessing({
        match,setGuildBlessings
    })

    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/guild/getGuildBlessingsLog?guild_id=${match.params.guild_id}&count=10&offset=${listChange.current}`, config).then(res => {
            const data = res.data;
            setGuildBlessings(data);
        });
    }

    const previousPage = () => { setChangeList(changeList - 10); setTimeout(refreshData, 100) }
    const nextPage = () => { setChangeList(changeList + 10); setTimeout(refreshData, 100) }

    const previousButton = () => {
        return <button className='btn btn-info btn-sm btn-primary' onClick={previousPage}>Previous Lists</button>;
    }

    const nextButton = () => {
        return <button className='btn btn-info btn-sm btn-primary' onClick={nextPage} >Next Lists</button>;
    }

    const hidePreviousButton = () => {

        if (changeList === 0) {
            return <></>
        } else {
            return previousButton();
        }
    };

    const hideNextButton = () => {

        if (Object.keys(guildBlessings).length < 10) {
            return <></>
        } else if (Object.keys(guildBlessings).length >= 10) {
            return nextButton();
        }
    };
    
    return (
        <div>
            <GuildBlessing guildblessings={guildBlessings} hideNextButton={hideNextButton} hidePreviousButton={hidePreviousButton}/>
        </div>
    )
}

export default GuildBlessingservices
