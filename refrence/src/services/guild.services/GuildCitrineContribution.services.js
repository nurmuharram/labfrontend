import React, { useState, useRef } from 'react';
import axios from 'axios';
import GuildCitrineContribution from 'src/views/guildmanagement/GuildCitrineContribution';
import { GetGuildCitrineContributionList } from 'src/api/guildManagementApi/GuildGetApi'


function GuildCitrineContributionservices({match}) {

    const [citrineContribution, setGetGuildCitrineContribution] = useState([])
    const [changeList, setChangeList] = useState(0);

    const listChange = useRef();
    listChange.current = changeList;
    
    GetGuildCitrineContributionList({
        match,setGetGuildCitrineContribution
    })

    const refreshdata = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/guild/getGuildCitrineContribution?guild_id=${match.params.guild_id}&count=10&offset=${listChange.current}`, config).then(res => {
            const data = res.data;
            setGetGuildCitrineContribution(data);
        });
    }

    const previousPage = () => { setChangeList(changeList - 10); setTimeout(refreshdata, 100) }
    const nextPage = () => { setChangeList(changeList + 10); setTimeout(refreshdata, 100) }

    const previousButton = () => {
        return <button className='btn btn-info btn-sm btn-primary' onClick={previousPage}>Previous Lists</button>;

    }

    const nextButton = () => {
        return <button className='btn btn-info btn-sm btn-primary' onClick={nextPage} >Next Lists</button>;
    }

    const hidePreviousButton = () => {

        if (changeList === 0) {
            return <></>
        } else {
            return previousButton();
        }
    };

    const hideNextButton = () => {

        if (Object.keys(citrineContribution).length < 10) {
            return <></>
        } else if (Object.keys(citrineContribution).length >= 10) {
            return nextButton();
        }
    };

    return (
        <>
            <GuildCitrineContribution citrineContribution={citrineContribution} hideNextButton={hideNextButton} hidePreviousButton={hidePreviousButton}/>
        </>
    )
}

export default GuildCitrineContributionservices
