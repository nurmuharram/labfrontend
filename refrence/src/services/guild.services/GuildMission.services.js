import { CButton } from '@coreui/react';
import React, {useState, useEffect, useRef} from 'react'

import axios from 'axios';
import GuildMission from 'src/views/guildmanagement/GuildMission';
import { GetGuildMissionServices } from 'src/api/guildManagementApi/GuildGetApi';

function GuildMissionServices({match}) {
    const [guildMissions, setGuildMissionServices] = useState([])
    const [changeList, setchangeList] = useState(0);

    const listChange = useRef();
    listChange.current = changeList;


    
    GetGuildMissionServices({
        match,setGuildMissionServices
    })

    const refreshdata = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/guild/getGuildMisisons?guild_id=${match.params.guild_id}&count=10&offset=${listChange.current}`, config).then(res => {
            const data = res.data;
            setGuildMissionServices(data);
        });
    }

    const previousPage = () => { setchangeList(changeList - 10); setTimeout(refreshdata, 100) }
    const nextPage = () => { setchangeList(changeList + 10); setTimeout(refreshdata, 100) }

    const previousButton = () => {
        return <CButton className="info" size="sm" color="primary" onClick={previousPage}>Previous Lists</CButton>;
    }

    const nextButton = () => {
        return <CButton className="info" size="sm" color="primary" onClick={nextPage} >Next Lists</CButton>;
    }

    const hidePreviousButton = () => {

        if (changeList === 0) {
            return <></>
        } else {
            return previousButton();
        }
    };

    const hideNextButton = () => {

        if (Object.keys(guildMissions).length < 10) {
            return <></>
        } else if (Object.keys(guildMissions).length >= 10) {
            return nextButton();
        }
    };
    return (
        <>
            <GuildMission match={match} guildmissions={guildMissions} changeList={changeList} listChange={listChange} hideNextButton={hideNextButton} hidePreviousButton={hidePreviousButton}/>
        </>
    )
}

export default GuildMissionServices
