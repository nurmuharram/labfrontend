import { CButton } from '@coreui/react';
import React, {useRef, useEffect, useState} from 'react'
import axios from 'axios';
import GuildMemberLogs from 'src/views/guildmanagement/GuildMemberLogs';
import { GetGuildMemberLogsServices} from 'src/api/guildManagementApi/GuildGetApi'

function GuildMemberLogsServices({match}) {
    const [guildMemberLogs, setGuildMemberLogs] = useState([])
    const [changeList, setchangeList] = useState(0);

    const listChange = useRef();
    listChange.current = changeList;

    GetGuildMemberLogsServices({
        match,setGuildMemberLogs
    })


    const refreshdata = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/guild/getGuildMemberLogs?guild_id=${match.params.guild_id}&count=10&offset=${listChange.current}`, config).then(res => {
            const data = res.data;
            setGuildMemberLogs(data);
        });
    }

    const previousPage = () => { setchangeList(changeList - 10); setTimeout(refreshdata, 100) }
    const nextPage = () => { setchangeList(changeList + 10); setTimeout(refreshdata, 100) }

    const previousButton = () => {
        return <CButton className="info" size="sm" color="primary" onClick={previousPage}>Previous Lists</CButton>;
    }

    const nextButton = () => {
        return <CButton className="info" size="sm" color="primary" onClick={nextPage} >Next Lists</CButton>;
    }

    const hidePreviousButton = () => {

        if (changeList === 0) {
            return <></>
        } else {
            return previousButton();
        }
    };

    const hideNextButton = () => {

        if (Object.keys(guildMemberLogs).length < 10) {
            return <></>
        } else if (Object.keys(guildMemberLogs).length >= 10) {
            return nextButton();
        }
    };
    return (
        <>
            <GuildMemberLogs guildmemberlogs={guildMemberLogs} hidePreviousButton={hidePreviousButton} hideNextButton={hideNextButton} />
        </>
    )
}

export default GuildMemberLogsServices
