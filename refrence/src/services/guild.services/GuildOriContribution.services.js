import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import GuildOriContribution from 'src/views/guildmanagement/GuildOriContribution';


function GuildOriContributionservices({match}) {

    const [oricontribution, setOriContribution] = useState([])
    const [changelist, setChangelist] = useState(0);

    const listChange = useRef();
    listChange.current = changelist;

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/guild/getGuildOriContribution?guild_id=${match.params.guild_id}&count=10&offset=0`, config).then(res => {
            const data = res.data;
            setOriContribution(data);
        });
    }, [])

    const refreshdata = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/guild/getGuildOriContribution?guild_id=${match.params.guild_id}&count=10&offset=${listChange.current}`, config).then(res => {
            const data = res.data;
            setOriContribution(data);
        });
    }

    const previousPage = () => { setChangelist(changelist - 10); setTimeout(refreshdata, 100) }
    const nextPage = () => { setChangelist(changelist + 10); setTimeout(refreshdata, 100) }

    const previousButton = () => {
        return <button className='btn btn-info btn-sm btn-primary' onClick={previousPage}>Previous Lists</button>;
    }

    const nextButton = () => {
        return <button className='btn btn-info btn-sm btn-primary' onClick={nextPage} >Next Lists</button>;
    }

    const hidePreviousButton = () => {

        if (changelist === 0) {
            return <></>
        } else {
            return previousButton();
        }
    };

    const hideNextButton = () => {

        if (Object.keys(oricontribution).length < 10) {
            return <></>
        } else if (Object.keys(oricontribution).length >= 10) {
            return nextButton();
        }
    };

    return (
        <>
            <GuildOriContribution oricontribution={oricontribution} hideNextButton={hideNextButton} hidePreviousButton={hidePreviousButton} />
        </>
    )
}

export default GuildOriContributionservices
