import React, {useEffect, useState} from 'react'

import axios from 'axios'
import { useHistory } from 'react-router-dom'
import GuildMembers from 'src/views/guildmanagement/GuildMembers'
import { GetGuildMembersServices } from 'src/api/guildManagementApi/GuildGetApi'


function GuildMembersservices({match}) {
    const history = useHistory()


    const [guildMembers, setGuildMembersServices] = useState([])

    GetGuildMembersServices({
        match,setGuildMembersServices
    })
    return (
        <>
          <GuildMembers history={history} match={match} guildMembers={guildMembers}/>
        </>
    )
}

export default GuildMembersservices
