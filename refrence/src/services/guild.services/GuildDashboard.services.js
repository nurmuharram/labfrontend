import React, { useEffect, useState } from 'react'

import axios from 'axios'
import { useHistory } from 'react-router-dom'
import GuildDashboard from 'src/views/guildmanagement/GuildDashboard'


function GuildDashboardServices ({match}, props) {
    const history = useHistory()

    const [guildDdata, setGuilddata] = useState([])

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/guild/getGuild?guild_id=${match.params.guild_id}`, config).then(res => {
            const data = res.data;
            setGuilddata(data);
        });
    }, [])

    return(
        <>
        <GuildDashboard history={history} guilddata={guildDdata} match={match} />
        </>
    )
}
export default GuildDashboardServices
