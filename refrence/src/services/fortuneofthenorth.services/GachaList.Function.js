export const toggleDetails = (index, { setDetails, details }) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
        newDetails.splice(position, 1)
    } else {
        newDetails = [...details, index]
    }
    setDetails(newDetails)
}