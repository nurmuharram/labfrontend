import CIcon from "@coreui/icons-react";

export const Maildata = ({maildata, setMaildata}) =>{

    const showDataFromArray = maildata && maildata.map((data, index) => {
        const rankdesc = () => {
            if (data.rank === 1) {
                return 'Ardha-rathi';
            } else if (data.rank === 2) {
                return 'Rathi'
            } else if (data.rank === 3) {
                return 'Ekarathi'
            } else if (data.rank === 4) {
                return 'Atirathi'
            } else if (data.rank === 5) {
                return 'Maharathi'
            } else if (data.rank === 6) {
                return 'Atimaharati'
            } else if (data.rank === 7) {
                return 'Mahamaharathi'
            }
        };
        return <><button type="button" className="btn btn-info" style={{ padding: '0.2rem' }} >
            {rankdesc()} - {JSON.stringify(data.mail_template)} | <CIcon size='sm' name='cil-x' onClick={() => {
                maildata.splice(index, 1);
                setMaildata([...maildata]);
                alert(`Item removed from the list!`);
            }}></CIcon>
        </button> &nbsp;&nbsp;</>
    })
    return showDataFromArray
} 

export const toggleDetails = (index, { setDetails, details }) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
        newDetails.splice(position, 1)
    } else {
        newDetails = [...details, index]
    }
    setDetails(newDetails)
}