import CIcon from "@coreui/icons-react";

export const KsatriyaList = ({ksatriyas, ksaPagesVisited, arrayData, setArrayData, ksaPerPage}) => {
    const displayKsatriyas = ksatriyas
    .slice(ksaPagesVisited, ksaPagesVisited + ksaPerPage)
    .map((ksatriya, index) => {
      return (
        <>
          <div className="col-sm-6 col-lg-3">
            <div className="col" align="center">
              <img
                className="border-dark shadow-lg rounded"
                src={`${process.env.PUBLIC_URL}/assets/img/ksa_icon/${ksatriya.ksatriya_id}.png`}
                fluid
                shape="rounded"
                width="110"
                align="center"
                style={{ "pointer-events": "all" }}
              />
            </div>
            <div className="col" align="center">
              <b>{ksatriya.ksatriya_name}</b>
            </div>
            <div className="col" align="center">
              <button
                className="btn btn-info btn-sm"
                key={ksatriya.ksatriya_id}
                value={ksatriya.ksatriya_id}
                id={ksatriya.ksatriya_name}
                color="info"
                size="sm"
                onClick={(e) =>
                  {setArrayData([
                    ...arrayData,
                    {
                      ksatriya_id: parseInt(ksatriya.ksatriya_id),
                      name: ksatriya.ksatriya_name,
                    },
                  ]); e.preventDefault()}
                }
              >
                Select KSA
              </button>
            </div>
            <br />
          </div>{" "}
        </>
      );
    });

    return displayKsatriyas
}

export const showSelectedKsatriyas = ({arrayData, setArrayData}) => {
    const showDataFromArray =
    arrayData &&
    arrayData.map((data, index) => {
      return (
        <>
          <button
            className="btn btn-info btn-sm btn-behance"
            style={{ padding: "0.2rem" }}
          >
            {data.ksatriya_id} - {data.name}{" "}
            <CIcon
              size="sm"
              name="cil-x"
              onClick={() => {
                arrayData.splice(index, 1);
                setArrayData([...arrayData]);
                alert(`Item removed from the list!`);
              }}
            ></CIcon>
          </button>{" "}
          &nbsp;&nbsp;
        </>
      );
    });
    return showDataFromArray
}