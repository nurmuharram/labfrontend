import React, { useEffect, useState } from 'react'
import axios from 'axios'
import LottoLoot from 'src/views/shopmanagement/svakalocus/LottoLoot'

function LottoLootservices() {

    const [lottoloot, setLottoloot] = useState([])
    const [lottolist, setLottolist] = useState([])

    const [lottoitem, setLottoitem] = useState([])
    const [addItemModal, setAddItemModal] = useState(false)
    const [deleteItemModal, setDeleteItemModal] = useState(false)

    const [details, setDetails] = useState([])
    
    const [lotto_id, setLotto_id] = useState()
    const [lotto_item_id, setLotto_item_id] = useState()
    const [amount, setAmount] = useState()

    useEffect(() => {
        const config = {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
          }
        }
        axios.get(`/api/lotto/getLoots`, config).then(res => {
          const items = res.data;
          setLottoloot(items);
        });
      }, [])

      const refreshData = () => {
        const config = {
            headers: {
              Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
          }
          axios.get(`/api/lotto/getLoots`, config).then(res => {
            const items = res.data;
            setLottoloot(items);
          });
      }

      useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/lotto/getLottos`, config).then(res => {
            const items = res.data;
            setLottolist(items);
        });
    }, [])

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/lotto/getItems`, config).then(res => {
            const items = res.data;
            setLottoitem(items);
        });
    }, [])

      const addLottoLoot = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('lotto_id', parseInt(lotto_id));
        data.append('lotto_item_id', parseInt(lotto_item_id))
        data.append('amount', amount)


        const config = {
            method: 'POST',
            url: `/api/lotto/addLoot`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Lotto added to Loot Pool!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to add!')
            });
        setTimeout(refreshData, 100)
    }

    const toggleDetails = (index) => {
        const position = details.indexOf(index)
        let newDetails = details.slice()
        if (position !== -1) {
            newDetails.splice(position, 1)
        } else {
            newDetails = [...details, index]
        }
        setDetails(newDetails)
    }


    const dropdownLottoId = lottolist.map((lotto, index) => {
        return (<option key={lotto.lotto_id} value={lotto.lotto_id}>{lotto.lotto_id}</option>)
    });

    const dropdownLottoItemId = lottoitem.map((lotto, index) => {
        return (<option key={lotto.lotto_item_id} value={lotto.lotto_item_id}>{lotto.lotto_item_id} - {lotto.item_name}</option>)
    });

    return (
        <div>
            <LottoLoot
            lottoloot={lottoloot}
            addItemModal={addItemModal}
            setAddItemModal={setAddItemModal}
            deleteItemModal={deleteItemModal}
            setDeleteItemModal={setDeleteItemModal}
            toggleDetails={toggleDetails}
            details={details}
            setLotto_id={setLotto_id}
            setLotto_item_id={setLotto_item_id}
            setAmount={setAmount}
            amount={amount}
            addLottoLoot={addLottoLoot}
            refreshData={refreshData}
            dropdownLottoId={dropdownLottoId}
            dropdownLottoItemId={dropdownLottoItemId}
            />
        </div>
    )
}

export default LottoLootservices
