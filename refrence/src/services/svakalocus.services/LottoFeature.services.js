import React, { useEffect, useState } from 'react'
import axios from 'axios'
import LottoFeature from 'src/views/shopmanagement/svakalocus/LottoFeature'


function LottoFeatureservices() {
    const [lottofeature, setLottofeature] = useState([])
    const [addItemModal, setAddItemModal] = useState(false)

    const [lottolist, setLottolist] = useState([])
    const [lottoitem, setLottoitem] = useState([])

    const [lotto_id, setLotto_id] = useState()
    const [lotto_item_id, setLotto_item_id] = useState()
    const [priority, setPriority] = useState()

    const [details, setDetails] = useState([])
    const [deleteItemModal, setDeleteItemModal] = useState(false)

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/lotto/getFeatures`, config).then(res => {
            const items = res.data;
            setLottofeature(items);
        });
    }, [])

    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/lotto/getFeatures`, config).then(res => {
            const items = res.data;
            setLottofeature(items);
        });
    }

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/lotto/getLottos`, config).then(res => {
            const items = res.data;
            setLottolist(items);
        });
    }, [])

    useEffect(() => {
        const config = {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
          }
        }
        axios.get(`/api/lotto/getItems`, config).then(res => {
          const items = res.data;
          setLottoitem(items);
        });
      }, [])

    const toggleDetails = (index) => {
        const position = details.indexOf(index)
        let newDetails = details.slice()
        if (position !== -1) {
            newDetails.splice(position, 1)
        } else {
            newDetails = [...details, index]
        }
        setDetails(newDetails)
    }

    const dropdownLottoId = lottolist.map((lotto, index) => {
        return (<option key={lotto.lotto_id} value={lotto.lotto_id}>{lotto.lotto_id}</option>)
    });

    const dropdownLottoItemId = lottoitem.map((lotto, index) => {
        return (<option key={lotto.lotto_item_id} value={lotto.lotto_item_id}>{lotto.lotto_item_id} - {lotto.item_name}</option>)
    });

    const addLottoFeatured =  (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('lotto_id', lotto_id);
        data.append('lotto_item_id', lotto_item_id)
        data.append('priority', priority)


        const config = {
            method: 'POST',
            url: `/api/lotto/addFeature`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Lotto added to Featured!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to add!')
            });
            setTimeout(refreshData, 100)
    }
    return (
        <div>
            <LottoFeature 
            lottofeature={lottofeature}
            addItemModal={addItemModal}
            setAddItemModal={setAddItemModal}
            setLotto_id={setLotto_id}
            setLotto_item_id={setLotto_item_id}
            setPriority={setPriority}
            deleteItemModal={deleteItemModal}
            setDeleteItemModal={setDeleteItemModal}
            details={details}
            setDetails={setDetails}
            toggleDetails={toggleDetails}
            dropdownLottoId={dropdownLottoId}
            dropdownLottoItemId={dropdownLottoItemId}
            addLottoFeatured={addLottoFeatured}
            refreshData={refreshData}
            lotto_id={lotto_id}
            lotto_item_id={lotto_item_id}
            priority={priority}
            />
        </div>
    )
}

export default LottoFeatureservices
