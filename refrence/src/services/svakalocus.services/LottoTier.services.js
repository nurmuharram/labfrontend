import React, { useEffect, useState } from 'react'
import axios from 'axios'
import LottoTier from 'src/views/shopmanagement/svakalocus/LottoTier'

function LottoTierservices() {
    const [lottotier, setLottotier] = useState([])

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/lotto/getColors`, config).then(res => {
      const items = res.data;
      setLottotier(items);
    });
  }, [])
    return (
        <div>
           <LottoTier
           lottotier={lottotier}
           /> 
        </div>
    )
}

export default LottoTierservices
