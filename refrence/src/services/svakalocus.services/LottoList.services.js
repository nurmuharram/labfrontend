import React, { useEffect, useState } from 'react'

import axios from 'axios'
import LottoList from 'src/views/shopmanagement/svakalocus/LottoList'

function LottoListservices() {
    const [lottolist, setLottolist] = useState([])
    const [addItemModal, setAddItemModal] = useState(false)

    const [startdate, setStartdate] = useState('2019-10-10')
    const [starttime, setStarttime] = useState('00:00')
    const start_date = startdate + ' ' + starttime + ':' + '00'
    const [enddate, setEnddate] = useState('2019-10-10')
    const [endtime, setEndtime] = useState('00:00')
    const end_date = enddate + " " + endtime + ':' + '00'

    const adatestart = new Date(start_date)

    const toisostart = adatestart.toISOString();

    const splitdatestart = toisostart.split('T')

    const fixdatestart = splitdatestart.join(' ')

    const adateend = new Date(end_date)

    const toisoend = adateend.toISOString();

    const splitdateend = toisoend.split('T')

    const fixdateend = splitdateend.join(' ')

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/lotto/getLottos`, config).then(res => {
            const items = res.data;
            setLottolist(items);
        });
    }, [])

    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/lotto/getLottos`, config).then(res => {
            const items = res.data;
            setLottolist(items);
        });
    }

    const addLottoPeriod =  (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_date', fixdatestart.replace('.000Z', ''));
        data.append('end_date', fixdateend.replace('.000Z', ''))


        const config = {
            method: 'POST',
            url: `/api/lotto/addLottos`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Lotto Period Created!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to add!')
            });
            setTimeout(refreshData, 100)
    }
    return (
        <div>
            <LottoList 
            lottolist={lottolist} 
            addItemModal={addItemModal}
            setAddItemModal={setAddItemModal}
            setStartdate={setStartdate}
            setStarttime={setStarttime}
            setEnddate={setEnddate}
            setEndtime={setEndtime}
            addLottoPeriod={addLottoPeriod}
            />
        </div>
    )
}

export default LottoListservices
