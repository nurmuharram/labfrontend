const { CButton } = require("@coreui/react");


const prevbttn = ({ setChangelist, changelist }) => {
    return <CButton className="info" size="sm" color="primary" onClick={() => { setChangelist(changelist - 10) }}>Previous Lists</CButton>;
}

const nextbttn = ({ setChangelist, changelist }) => {
    return <CButton className="info" size="sm" color="primary" onClick={() => { setChangelist(changelist + 10) }}>Next Lists</CButton>;
}

export const hideprevbttn = ({ setChangelist, changelist, topuplist }) => {
    if (changelist === 0) {
        return <></>
    } else if (topuplist.length <= 10) {
        return prevbttn({ setChangelist, changelist })
    }
};

export const hidenextbttn = ({ setChangelist, changelist, topuplist }) => {
    if (topuplist.length < 10) {
        return <></>
    } else if (topuplist.length >= 10) {
        return nextbttn({ setChangelist, changelist });
    }
};