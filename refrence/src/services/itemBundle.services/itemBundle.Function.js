import CIcon from '@coreui/icons-react';
import React from 'react'

export const showItemSelected = ({ arrayData, setArrayData }) => {
    const showDataFromArray = arrayData && arrayData.map((data, index) => {

        const itemtypename = () => {
            if (data.item_type === 1) {
                return 'Currency';
            } else if (data.item_type === 6) {
                return 'Box'
            } else if (data.item_type === 5) {
                return 'Misc Items'
            } else if (data.item_type === 2) {
                return 'KSA'
            } else if (data.item_type === 3) {
                return 'KSA Skins'
            } else if (data.item_type === 4) {
                return 'Rune'
            } else if (data.item_type === 11) {
                return 'Frame'
            } else if (data.item_type === 12) {
                return 'Avatar'
            }
        };

        return <><button
            className="btn btn-info btn-sm btn-behance"
            style={{ padding: "0.2rem" }}
            onClick={(e) => e.preventDefault()}
        >
            {itemtypename()} - {JSON.stringify(data.item_id)} ({data.amount}) | <CIcon size='sm' name='cil-x' onClick={() => {
                arrayData.splice(index, 1);
                setArrayData([...arrayData]);
                alert(`Item removed from the list!`)
            }}></CIcon>
        </button> &nbsp;&nbsp;</>
    })
    return showDataFromArray
}
