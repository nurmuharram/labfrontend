import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getKsatriyas } from "src/redux/action/mailAction/getKsatriyasAction";

export const DropdownKsatriya = () => {
    const dispatch = useDispatch()
    const ksatriyas = useSelector((state) => state.ksatriyas.ksatriyas);
    useEffect(() => {
        dispatch(getKsatriyas());
    }, []);

return ksatriyas.map((ksa) =><option key={ksa.ksatriya_id} value={ksa.ksatriya_id}>{ksa.ksatriya_id} - {ksa.ksatriya_name}</option>)
}

export const toggleDetails = (index, { setDetails, details }) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
        newDetails.splice(position, 1)
    } else {
        newDetails = [...details, index]
    }
    setDetails(newDetails)
}