import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { getKsatriyas } from 'src/redux/action/mailAction/getKsatriyasAction';
import { getBoxes } from 'src/redux/action/mailAction/getBoxesAction';
import { getSkins } from 'src/redux/action/mailAction/getSkinsAction';
import { getRunes } from 'src/redux/action/mailAction/getRunesAction'
import { getFrames } from 'src/redux/action/mailAction/getFramesAction';
import { getAvatars } from 'src/redux/action/mailAction/getAvatarsAction';
import { getItems } from 'src/redux/action/mailAction/getItemsAction';
import { CButton } from '@coreui/react';
import CIcon from '@coreui/icons-react';


export const EventEnergyItemFunction = ({ itemtype }) => {

    const dispatch = useDispatch()

    /* GET Boxes - Get Request */
    const boxes = useSelector((state) => state.boxes.boxes);

    useEffect(() => {
        dispatch(getBoxes());
    }, []);

    /* GET Ksatriyas - Get Request */
    const ksatriyas = useSelector((state) => state.ksatriyas.ksatriyas);
    useEffect(() => {
        dispatch(getKsatriyas());
    }, []);

    /* GET Items - Get Request */
    const items = useSelector((state) => state.items.items);
    useEffect(() => {
        dispatch(getItems());
    }, []);

    /* GET skins - Get Request */
    const skins = useSelector((state) => state.skins.skins);
    useEffect(() => {
        dispatch(getSkins());
    }, []);

    /* GET rune - Get Request */
    const runes = useSelector((state) => state.runes.runes);
    useEffect(() => {
        dispatch(getRunes());
    }, []);

    /* GET frames - Get Request */
    const frames = useSelector((state) => state.frames.frames);
    useEffect(() => {
        dispatch(getFrames());
    }, []);

    /* GET Avatar - Get Request */
    const avatars = useSelector((state) => state.avatars.avatars);
    useEffect(() => {
        dispatch(getAvatars());
    }, []);

    /* Box Mapping */
    const dropdownBoxes = boxes.map((box, index) => {
        return (<option key={box.box_id} value={box.box_id}>[Box] - {box.box_name}</option>)
    });

    /* KSA Mapping */
    const dropdownKsatriyas = ksatriyas.map((ksatriya, index) => {
        return (<option key={ksatriya.ksatriya_id} value={ksatriya.ksatriya_id}>[KSA] - {ksatriya.ksatriya_name}</option>)
    });

    /* items Mapping */
    const dropdownItems = items.map((item, index) => {
        return (<option key={item.misc_id} value={item.misc_id}>[Item] - {item.misc_name}</option>)
    });

    /* skins Mapping */
    const dropdownSkins = skins.map((item, index) => {
        const skinname = () => {
            if (item.ksatriya_skin_id === 100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 101) {
                return "Yanaka"
            } else if (item.ksatriya_skin_id === 300) {
                return "Default"
            } else if (item.ksatriya_skin_id === 301) {
                return "Arbalest Armor"
            } else if (item.ksatriya_skin_id === 302) {
                return "Homeroom Teacher"
            } else if (item.ksatriya_skin_id === 400) {
                return "Default"
            } else if (item.ksatriya_skin_id === 401) {
                return "Cybuff Funk"
            } else if (item.ksatriya_skin_id === 402) {
                return "Eternal Anarchy"
            } else if (item.ksatriya_skin_id === 500) {
                return "Default"
            } else if (item.ksatriya_skin_id === 600) {
                return "Default"
            } else if (item.ksatriya_skin_id === 601) {
                return "Thunder Phantom"
            } else if (item.ksatriya_skin_id === 602) {
                return "Badhayan Nyandi"
            } else if (item.ksatriya_skin_id === 700) {
                return "Default"
            } else if (item.ksatriya_skin_id === 701) {
                return "Patih Mada"
            } else if (item.ksatriya_skin_id === 800) {
                return "Default"
            } else if (item.ksatriya_skin_id === 801) {
                return "Guardian of Manthana"
            } else if (item.ksatriya_skin_id === 900) {
                return "Default"
            } else if (item.ksatriya_skin_id === 901) {
                return "Carimata Marauder"
            } else if (item.ksatriya_skin_id === 902) {
                return "Lady Jock"
            } else if (item.ksatriya_skin_id === 1000) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1001) {
                return "Cerberus"
            } else if (item.ksatriya_skin_id === 1100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1101) {
                return "Smokestack Outburst"
            } else if (item.ksatriya_skin_id === 1102) {
                return "Endless Angst"
            } else if (item.ksatriya_skin_id === 1200) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1201) {
                return "Divine Warlord"
            } else if (item.ksatriya_skin_id === 1202) {
                return "Undefined"
            } else if (item.ksatriya_skin_id === 1300) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1301) {
                return "Volcanic Lightning"
            } else if (item.ksatriya_skin_id === 1302) {
                return "Party Wrecker"
            } else if (item.ksatriya_skin_id === 1400) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1401) {
                return "Serene Melody"
            } else if (item.ksatriya_skin_id === 1500) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1501) {
                return "Brave Princess"
            } else if (item.ksatriya_skin_id === 1600) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1601) {
                return "Alighted Asura"
            } else if (item.ksatriya_skin_id === 1700) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1701) {
                return "Battle of Kudadu"
            } else if (item.ksatriya_skin_id === 1702) {
                return "Krtarajasa"
            } else if (item.ksatriya_skin_id === 1800) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1900) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1901) {
                return "Undefined"
            } else if (item.ksatriya_skin_id === 1903) {
                return "Knightly Trip"
            } else if (item.ksatriya_skin_id === 2000) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2001) {
                return "Singharajni"
            } else if (item.ksatriya_skin_id === 2100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2200) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2300) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2400) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2500) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2600) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2601) {
                return "Unswerving Fellowship"
            } else if (item.ksatriya_skin_id === 2700) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2702) {
                return "Lestari Mudan"
            } else if (item.ksatriya_skin_id === 2800) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2802) {
                return "Jinan"
            } else if (item.ksatriya_skin_id === 2900) {
                return "Default"
            } else if (item.ksatriya_skin_id === 3000) {
                return "Default"
            } else if (item.ksatriya_skin_id === 3002) {
                return "Champion of Diver"
            } else if (item.ksatriya_skin_id === 90100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 90101) {
                return "Black Suit"
            } else {
                return item.ksatriya_skin_id
            }
        }
        return (<option key={item.ksatriya_skin_id} value={item.ksatriya_skin_id}>[Skin] {item.ksatriya_name} - {skinname()}</option>)
    });

    /* rune Mapping */
    const dropdownRunes = runes.map((rune, index) => {
        return (<option key={rune.rune_id} value={rune.rune_id}>[Rune] - {rune.name} ({rune.description})</option>)
    });

    /* frames Mapping */
    const dropdownFrames = frames.map((frame, index) => {
        return (<option key={frame.frame_id} value={frame.frame_id}>[Frame] - {frame.description}</option>)
    });

    /* Avatar Mapping */
    const dropdownAvatars = avatars.map((avatar, index) => {
        return (<option key={avatar.avatar_id} value={avatar.avatar_id}>[Avatar] - {avatar.description}</option>)
    });

    const currency = () => {
        return (<><option key={'ori'} value={1}>[Currency] - Ori</option>
            <option key={'citrine'} value={2}>[Currency] - Citrine</option>
            <option key={'lotus'} value={3}>[Currency] - Lotus</option></>)
    }

    const dropdownItemChange = () => {
        if (itemtype === '1') {
            return (currency());
        } else if (itemtype === '6') {
            return (dropdownBoxes)
        } else if (itemtype === '5') {
            return (dropdownItems)
        } else if (itemtype === '2') {
            return (dropdownKsatriyas)
        } else if (itemtype === '3') {
            return (dropdownSkins)
        } else if (itemtype === '4') {
            return (dropdownRunes)
        } else if (itemtype === '11') {
            return (dropdownFrames)
        } else if (itemtype === '12') {
            return (dropdownAvatars)
        } else if (itemtype === '12') {
            return (dropdownAvatars)
        } else if (itemtype === '') {
            return <option disabled>Select Item Type First...</option>
        }
    };
    return (
        <>{dropdownItemChange()}</>
    )
}

export const toggleDetails = (index, { setDetails, details }) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
        newDetails.splice(position, 1)
    } else {
        newDetails = [...details, index]
    }
    setDetails(newDetails)
}


export const ItemReward = ({arrayData, setArrayData}) =>{
    const showDataFromArray = arrayData && arrayData.map((data, index) => {

        const itemtypename = () => {
            if (data.item_type === 1) {
                return 'Currency';
            } else if (data.item_type === 6) {
                return 'Box'
            } else if (data.item_type === 5) {
                return 'Misc Items'
            } else if (data.item_type === 2) {
                return 'KSA'
            } else if (data.item_type === 3) {
                return 'KSA Skins'
            } else if (data.item_type === 4) {
                return 'Rune'
            } else if (data.item_type === 11) {
                return 'Frame'
            } else if (data.item_type === 12) {
                return 'Avatar'
            }
        };

        return <><CButton color='info' className='btn-behance' size='sm' style={{ padding: '0.2rem' }} >
            {itemtypename()} - {JSON.stringify(data.item_id)} ({data.amount}) | <CIcon size='sm' name='cil-x' onClick={() => {
                arrayData.splice(index, 1);
                setArrayData([...arrayData]);
                alert(`Item removed from the list!`);
            }}></CIcon>
        </CButton> &nbsp;&nbsp;</>
    })
    return showDataFromArray
} 

export const TargetEnergy_ = ({target_energy, setTarget_energy}) =>{
    const showDataFromArray = target_energy && target_energy.map((data, index) => {

        return <><CButton color='info' className='btn-behance' size='sm' style={{ padding: '0.2rem' }} >
            {JSON.stringify(data.target_energy)} | <CIcon size='sm' name='cil-x' onClick={() => {
                target_energy.splice(index, 1);
                setTarget_energy([...target_energy]);
                alert(`Item removed from the list!`);
            }}></CIcon>
        </CButton> &nbsp;&nbsp;</>
    })
    return showDataFromArray
} 

export const EventEnergyItemUpdate = ({ newitemtype }) => {

    const dispatch = useDispatch()

    /* GET Boxes - Get Request */
    const boxes = useSelector((state) => state.boxes.boxes);

    useEffect(() => {
        dispatch(getBoxes());
    }, []);

    /* GET Ksatriyas - Get Request */
    const ksatriyas = useSelector((state) => state.ksatriyas.ksatriyas);
    useEffect(() => {
        dispatch(getKsatriyas());
    }, []);

    /* GET Items - Get Request */
    const items = useSelector((state) => state.items.items);
    useEffect(() => {
        dispatch(getItems());
    }, []);

    /* GET skins - Get Request */
    const skins = useSelector((state) => state.skins.skins);
    useEffect(() => {
        dispatch(getSkins());
    }, []);

    /* GET rune - Get Request */
    const runes = useSelector((state) => state.runes.runes);
    useEffect(() => {
        dispatch(getRunes());
    }, []);

    /* GET frames - Get Request */
    const frames = useSelector((state) => state.frames.frames);
    useEffect(() => {
        dispatch(getFrames());
    }, []);

    /* GET Avatar - Get Request */
    const avatars = useSelector((state) => state.avatars.avatars);
    useEffect(() => {
        dispatch(getAvatars());
    }, []);

    /* Box Mapping */
    const dropdownBoxes = boxes.map((box, index) => {
        return (<option key={box.box_id} value={box.box_id}>[Box] - {box.box_name}</option>)
    });

    /* KSA Mapping */
    const dropdownKsatriyas = ksatriyas.map((ksatriya, index) => {
        return (<option key={ksatriya.ksatriya_id} value={ksatriya.ksatriya_id}>[KSA] - {ksatriya.ksatriya_name}</option>)
    });

    /* items Mapping */
    const dropdownItems = items.map((item, index) => {
        return (<option key={item.misc_id} value={item.misc_id}>[Item] - {item.misc_name}</option>)
    });

    /* skins Mapping */
    const dropdownSkins = skins.map((item, index) => {
        const skinname = () => {
            if (item.ksatriya_skin_id === 100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 101) {
                return "Yanaka"
            } else if (item.ksatriya_skin_id === 300) {
                return "Default"
            } else if (item.ksatriya_skin_id === 301) {
                return "Arbalest Armor"
            } else if (item.ksatriya_skin_id === 302) {
                return "Homeroom Teacher"
            } else if (item.ksatriya_skin_id === 400) {
                return "Default"
            } else if (item.ksatriya_skin_id === 401) {
                return "Cybuff Funk"
            } else if (item.ksatriya_skin_id === 402) {
                return "Eternal Anarchy"
            } else if (item.ksatriya_skin_id === 500) {
                return "Default"
            } else if (item.ksatriya_skin_id === 600) {
                return "Default"
            } else if (item.ksatriya_skin_id === 601) {
                return "Thunder Phantom"
            } else if (item.ksatriya_skin_id === 602) {
                return "Badhayan Nyandi"
            } else if (item.ksatriya_skin_id === 700) {
                return "Default"
            } else if (item.ksatriya_skin_id === 701) {
                return "Patih Mada"
            } else if (item.ksatriya_skin_id === 800) {
                return "Default"
            } else if (item.ksatriya_skin_id === 801) {
                return "Guardian of Manthana"
            } else if (item.ksatriya_skin_id === 900) {
                return "Default"
            } else if (item.ksatriya_skin_id === 901) {
                return "Carimata Marauder"
            } else if (item.ksatriya_skin_id === 902) {
                return "Lady Jock"
            } else if (item.ksatriya_skin_id === 1000) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1001) {
                return "Cerberus"
            } else if (item.ksatriya_skin_id === 1100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1101) {
                return "Smokestack Outburst"
            } else if (item.ksatriya_skin_id === 1102) {
                return "Endless Angst"
            } else if (item.ksatriya_skin_id === 1200) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1201) {
                return "Divine Warlord"
            } else if (item.ksatriya_skin_id === 1202) {
                return "Undefined"
            } else if (item.ksatriya_skin_id === 1300) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1301) {
                return "Volcanic Lightning"
            } else if (item.ksatriya_skin_id === 1302) {
                return "Party Wrecker"
            } else if (item.ksatriya_skin_id === 1400) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1401) {
                return "Serene Melody"
            } else if (item.ksatriya_skin_id === 1500) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1501) {
                return "Brave Princess"
            } else if (item.ksatriya_skin_id === 1600) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1601) {
                return "Alighted Asura"
            } else if (item.ksatriya_skin_id === 1700) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1701) {
                return "Battle of Kudadu"
            } else if (item.ksatriya_skin_id === 1702) {
                return "Krtarajasa"
            } else if (item.ksatriya_skin_id === 1800) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1900) {
                return "Default"
            } else if (item.ksatriya_skin_id === 1901) {
                return "Undefined"
            } else if (item.ksatriya_skin_id === 1903) {
                return "Knightly Trip"
            } else if (item.ksatriya_skin_id === 2000) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2001) {
                return "Singharajni"
            } else if (item.ksatriya_skin_id === 2100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2200) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2300) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2400) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2500) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2600) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2601) {
                return "Unswerving Fellowship"
            } else if (item.ksatriya_skin_id === 2700) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2702) {
                return "Lestari Mudan"
            } else if (item.ksatriya_skin_id === 2800) {
                return "Default"
            } else if (item.ksatriya_skin_id === 2802) {
                return "Jinan"
            } else if (item.ksatriya_skin_id === 2900) {
                return "Default"
            } else if (item.ksatriya_skin_id === 3000) {
                return "Default"
            } else if (item.ksatriya_skin_id === 3002) {
                return "Champion of Diver"
            } else if (item.ksatriya_skin_id === 90100) {
                return "Default"
            } else if (item.ksatriya_skin_id === 90101) {
                return "Black Suit"
            } else {
                return item.ksatriya_skin_id
            }
        }
        return (<option key={item.ksatriya_skin_id} value={item.ksatriya_skin_id}>[Skin] {item.ksatriya_name} - {skinname()}</option>)
    });

    /* rune Mapping */
    const dropdownRunes = runes.map((rune, index) => {
        return (<option key={rune.rune_id} value={rune.rune_id}>[Rune] - {rune.name} ({rune.description})</option>)
    });

    /* frames Mapping */
    const dropdownFrames = frames.map((frame, index) => {
        return (<option key={frame.frame_id} value={frame.frame_id}>[Frame] - {frame.description}</option>)
    });

    /* Avatar Mapping */
    const dropdownAvatars = avatars.map((avatar, index) => {
        return (<option key={avatar.avatar_id} value={avatar.avatar_id}>[Avatar] - {avatar.description}</option>)
    });

    const currency = () => {
        return (<><option key={'ori'} value={1}>[Currency] - Ori</option>
            <option key={'citrine'} value={2}>[Currency] - Citrine</option>
            <option key={'lotus'} value={3}>[Currency] - Lotus</option></>)
    }

    const dropdownItemChange = () => {
        if (newitemtype === '1') {
            return (currency());
        } else if (newitemtype === '6') {
            return (dropdownBoxes)
        } else if (newitemtype === '5') {
            return (dropdownItems)
        } else if (newitemtype === '2') {
            return (dropdownKsatriyas)
        } else if (newitemtype === '3') {
            return (dropdownSkins)
        } else if (newitemtype === '4') {
            return (dropdownRunes)
        } else if (newitemtype === '11') {
            return (dropdownFrames)
        } else if (newitemtype === '12') {
            return (dropdownAvatars)
        } else if (newitemtype === '12') {
            return (dropdownAvatars)
        } else if (newitemtype === '') {
            return <option disabled>Select Item Type First...</option>
        }
    };
    return (
        <>{dropdownItemChange()}</>
    )
}