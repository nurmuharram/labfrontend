import CIcon from "@coreui/icons-react";

export const MissionEventReward = ({arrayData, setArrayData}) =>{
    const showDataFromArray = arrayData && arrayData.map((data, index) => {

        const itemtypename = () => {
            if (data.item_type === 1) {
                return 'Currency';
            } else if (data.item_type === 6) {
                return 'Box'
            } else if (data.item_type === 5) {
                return 'Misc Items'
            } else if (data.item_type === 2) {
                return 'KSA'
            } else if (data.item_type === 3) {
                return 'KSA Skins'
            } else if (data.item_type === 4) {
                return 'Rune'
            } else if (data.item_type === 11) {
                return 'Frame'
            } else if (data.item_type === 12) {
                return 'Avatar'
            }
        };

        return <div className="pb-1"><button className='btn btn-info btn-sm btn-behance' onClick={(e) => e.preventDefault()} style={{ padding: '0.2rem' }} >
            {itemtypename()} - {JSON.stringify(data.item_id)} ({data.amount}) | <CIcon size='sm' name='cil-x' onClick={() => {
                arrayData.splice(index, 1);
                setArrayData([...arrayData]);
                alert(`Item removed from the list!`);
            }}></CIcon>
        </button> &nbsp;&nbsp;</div>
    })
    return showDataFromArray
} 

export const AnniversaryShopItems = ({arrayData, setArrayData}) =>{
    const showDataFromArray = arrayData && arrayData.map((data, index) => {

        const itemtypename = () => {
            if (data.item_type === 1) {
                return 'Currency';
            } else if (data.item_type === 6) {
                return 'Box'
            } else if (data.item_type === 5) {
                return 'Misc Items'
            } else if (data.item_type === 2) {
                return 'KSA'
            } else if (data.item_type === 3) {
                return 'KSA Skins'
            } else if (data.item_type === 4) {
                return 'Rune'
            } else if (data.item_type === 11) {
                return 'Frame'
            } else if (data.item_type === 12) {
                return 'Avatar'
            }
        };

        return <div className="pb-1"><button className='btn btn-info btn-sm btn-behance' onClick={(e) => e.preventDefault()} style={{ padding: '0.2rem' }} >
            {itemtypename()} - {JSON.stringify(data.item_id)} (amt: {data.amount}) (mxb: {data.max_buy}) | <CIcon size='sm' name='cil-x' onClick={() => {
                arrayData.splice(index, 1);
                setArrayData([...arrayData]);
                alert(`Item removed from the list!`);
            }}></CIcon>
        </button> &nbsp;&nbsp;</div>
    })
    return showDataFromArray
} 

export const AnniversaryShopItemsDetails = ({arrayData, setArrayData}) =>{
    const showDataFromArray = arrayData && arrayData.map((data, index) => {
        return <div className="pb-1"><button className='btn btn-info btn-sm btn-behance' onClick={(e) => e.preventDefault()} style={{ padding: '0.2rem' }} >
            Item: {JSON.stringify(data.shop_item_id)} (Price: {data.price}) | <CIcon size='sm' name='cil-x' onClick={() => {
                arrayData.splice(index, 1);
                setArrayData([...arrayData]);
                alert(`Item removed from the list!`);
            }}></CIcon>
        </button> &nbsp;&nbsp;</div>
    })
    return showDataFromArray
} 