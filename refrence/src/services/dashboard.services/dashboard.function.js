export const ksaList = ({item}) => {
    if (item.ksatriya_id === 1) {
        return 'Ilya';
    } else if (item.ksatriya_id === 2) {
        return 'Sveta';
    } else if (item.ksatriya_id === 3) {
        return 'Sena';
    } else if (item.ksatriya_id === 4) {
        return 'Ma`esh';
    } else if (item.ksatriya_id === 5) {
        return 'Myluta';
    } else if (item.ksatriya_id === 6) {
        return 'Nisha';
    } else if (item.ksatriya_id === 7) {
        return 'Jinno';
    } else if (item.ksatriya_id === 8) {
        return 'Neera';
    } else if (item.ksatriya_id === 9) {
        return 'Nala';
    } else if (item.ksatriya_id === 10) {
        return 'Sabara';
    } else if (item.ksatriya_id === 11) {
        return 'H`rtal';
    } else if (item.ksatriya_id === 12) {
        return 'Skar';
    } else if (item.ksatriya_id === 13) {
        return 'Haya';
    } else if (item.ksatriya_id === 14) {
        return 'Nada';
    } else if (item.ksatriya_id === 15) {
        return 'Saira';
    } else if (item.ksatriya_id === 16) {
        return 'Handaru';
    } else if (item.ksatriya_id === 17) {
        return 'Vijaya';
    } else if (item.ksatriya_id === 18) {
        return 'Tara';
    } else if (item.ksatriya_id === 19) {
        return 'Kanta';
    } else if (item.ksatriya_id === 20) {
        return 'Rajapatni';
    } else if (item.ksatriya_id === 21) {
        return 'Tarja';
    } else if (item.ksatriya_id === 22) {
        return 'Binara';
    } else if (item.ksatriya_id === 23) {
        return 'T`pala';
    } else if (item.ksatriya_id === 24) {
        return 'Khage';
    } else if (item.ksatriya_id === 26) {
        return 'Guning';
    } else if (item.ksatriya_id === 27) {
        return 'Nio';
    } else if (item.ksatriya_id === 28) {
        return 'Vyana';
    } else if (item.ksatriya_id === 29) {
        return 'Candravasi';
    } else if (item.ksatriya_id === 30) {
        return 'Lando';
    } else if (item.ksatriya_id === 901) {
        return 'Barda';
    }

}

export const _date = ({concurrentUsers}) => {
    const _date = concurrentUsers && concurrentUsers.map((dateTime, index) => {
        const month = () => {
            if (dateTime.month === 1) {
                return 'Jan';
            } else if (dateTime.month === 2) {
                return 'Feb';
            } else if (dateTime.month === 3) {
                return 'Mar';
            } else if (dateTime.month === 4) {
                return 'Apr';
            } else if (dateTime.month === 5) {
                return 'May';
            } else if (dateTime.month === 6) {
                return 'Jun';
            } else if (dateTime.month === 7) {
                return 'Jul';
            } else if (dateTime.month === 8) {
                return 'Aug';
            } else if (dateTime.month === 9) {
                return 'Sep';
            } else if (dateTime.month === 10) {
                return 'Oct';
            } else if (dateTime.month === 11) {
                return 'Nov';
            } else if (dateTime.month === 12) {
                return 'Dec';
            }
        }

        const hour = () => {
            if (dateTime.hour === 1) {
                return '01';
            } else if (dateTime.hour === 2) {
                return '02';
            } else if (dateTime.hour === 3) {
                return '03';
            } else if (dateTime.hour === 4) {
                return '04';
            } else if (dateTime.hour === 5) {
                return '05';
            } else if (dateTime.hour === 6) {
                return '06';
            } else if (dateTime.hour === 7) {
                return '07';
            } else if (dateTime.hour === 8) {
                return '08';
            } else if (dateTime.hour === 9) {
                return '09';
            } else if (dateTime.hour === 0) {
                return '00';
            }
            else {
                return dateTime.hour;
            }
        }

        const minute = () => {
            if (dateTime.minute === 1) {
                return '01';
            } else if (dateTime.minute === 2) {
                return '02';
            } else if (dateTime.minute === 3) {
                return '03';
            } else if (dateTime.minute === 4) {
                return '04';
            } else if (dateTime.minute === 5) {
                return '05';
            } else if (dateTime.minute === 6) {
                return '06';
            } else if (dateTime.minute === 7) {
                return '07';
            } else if (dateTime.minute === 8) {
                return '08';
            } else if (dateTime.minute === 9) {
                return '09';
            } else if (dateTime.minute === 0) {
                return '00';
            }
            else {
                return dateTime.minute;
            }
        }
        return [dateTime.day + ' ' + month() + ' ' + dateTime.year + ' ' + '@' + hour() + ':' + minute()]
    })
    return _date
}

export const date = ({dailyUsers}) => {
    const date = dailyUsers && dailyUsers.map((dailyuser, index) => {
        const month = () => {
            if (dailyuser.month === 1) {
                return 'Jan';
            } else if (dailyuser.month === 2) {
                return 'Feb';
            } else if (dailyuser.month === 3) {
                return 'Mar';
            } else if (dailyuser.month === 4) {
                return 'Apr';
            } else if (dailyuser.month === 5) {
                return 'May';
            } else if (dailyuser.month === 6) {
                return 'Jun';
            } else if (dailyuser.month === 7) {
                return 'Jul';
            } else if (dailyuser.month === 8) {
                return 'Aug';
            } else if (dailyuser.month === 9) {
                return 'Sep';
            } else if (dailyuser.month === 10) {
                return 'Oct';
            } else if (dailyuser.month === 11) {
                return 'Nov';
            } else if (dailyuser.month === 12) {
                return 'Dec';
            }
        }
        return [dailyuser.day + ' ' + month() + ' ' + dailyuser.year]
    })
    return date
}