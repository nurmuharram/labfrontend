import React from 'react';

// const Toaster = React.lazy(() => import('./views/notifications/toaster/Toaster'));
// const Tables = React.lazy(() => import('./views/base/tables/Tables'));
// const Tabs = React.lazy(() => import('./views/base/tabs/Tabs'));
// const Tooltips = React.lazy(() => import('./views/base/tooltips/Tooltips'));
// const BrandButtons = React.lazy(() => import('./views/buttons/brand-buttons/BrandButtons'));
// const ButtonDropdowns = React.lazy(() => import('./views/buttons/button-dropdowns/ButtonDropdowns'));
// const ButtonGroups = React.lazy(() => import('./views/buttons/button-groups/ButtonGroups'));
// const Buttons = React.lazy(() => import('./views/buttons/buttons/Buttons'));
// const Charts = React.lazy(() => import('./views/charts/Charts'));
// const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
// const CoreUIIcons = React.lazy(() => import('./views/icons/coreui-icons/CoreUIIcons'));
// const Flags = React.lazy(() => import('./views/icons/flags/Flags'));
// const Brands = React.lazy(() => import('./views/icons/brands/Brands'));
// const Alerts = React.lazy(() => import('./views/notifications/alerts/Alerts'));
// const Badges = React.lazy(() => import('./views/notifications/badges/Badges'));
// const Modals = React.lazy(() => import('./views/notifications/modals/Modals'));
// const Colors = React.lazy(() => import('./views/theme/colors/Colors'));
// const Typography = React.lazy(() => import('./views/theme/typography/Typography'));
// const Widgets = React.lazy(() => import('./views/widgets/Widgets'));
// const Users = React.lazy(() => import('./views/users/Users'));
// const User = React.lazy(() => import('./views/users/User'));
const Mail = React.lazy(() => import('./views/mail/Mail'));
const Management = React.lazy(() => import('./views/management/Management'));
const UserManagementDetail = React.lazy(() => import('./views/management/usermanagementdetail'));

const SvakaLocus = React.lazy(() => import('./views/shopmanagement/svakalocus/SvakaLocus'));
const NewsManagement = React.lazy(() => import('./views/newsmanagement/NewsManagement'));
const PlayerManager = React.lazy(() => import('./views/playermanager/PlayerManager'));
const UsersDashboard = React.lazy(() => import('./views/dashboard/usersdashboard/UsersDashboard'));
const KsatriyaDashboard = React.lazy(() => import('./views/dashboard/ksatriyadashboard/KsatriyaDashboard'));
const ServerMonitor = React.lazy(() => import('./views/dashboard/servermonitor/ServerMonitor'));
const PlayerDashboard = React.lazy(() => import ('./views/playermanager/PlayerDashboard'));
const Dashboard = React.lazy(() => import ('./views/dashboard/Dashboard'));
const PlayersReports = React.lazy(() => import ('./views/playersreports/PlayersReports'));
const ProfileDetail = React.lazy(() => import ('./views/management/profiledetail/ProfileDetail'));
const MatchManagement = React.lazy(() => import('./views/matchmanagement/MatchManagement'));
const MatchStatistic = React.lazy(() => import ('./views/matchmanagement/MatchStatistic'))
const Voucher = React.lazy(() => import ('./views/generatevoucher/Voucher'))
const ItemManager = React.lazy(() => import ('./views/shopmanagement/shopmanager/ShopManager'))
const PastMatchData = React.lazy(() => import ('./views/pastmatchdata/PastMatchData'))
const ItemBundle = React.lazy(() => import ('./views/shopmanagement/itembundle/ItemBundle'))
const GuildManagement = React.lazy(() => import ('./views/guildmanagement/GuildManagement'))
const GuildDashboard = React.lazy(() => import ('./services/guild.services/GuildDashboard.services'))
const LotusShopManager = React.lazy(() => import ('./views/lotusshopmanager/LotusShop'))
const ShopStatistic = React.lazy(() => import ('./views/shopmanagement/shopstatistics/ShopStatistic'))
const FortuneoftheNorth = React.lazy(() => import ('./views/fortuneofthenorth/FortuneoftheNorth'))
const EventEnergy = React.lazy(() => import ('./views/event/EventMainPage'))
const SeasonEnd = React.lazy(() => import ('./views/seasonend/SeasonEnd'))
const MissionStatistics = React.lazy(() => import ('./views/missionstatistics/MissionStatistics'))
const GameManager = React.lazy(() => import ('./views/gamemanager/GameManager'))
const MaintenanceManagement = React.lazy(() => import ('./views/maintenancemanagement/MTManagement'))
// const Logout = React.lazy(() => import('./views/pages/login/Login'));

const routes = [

  { path: '/dashboard', exact: true, name: 'Home', component: Dashboard },
  { path: '/playermanager', name: 'PlayerManager', component: PlayerManager },
  { path: '/mail', name: 'Mail', component: Mail },
  { path: '/management', name: 'Management', component: Management },
  { path: '/profiledetail', name: 'Profile Detail', component: ProfileDetail },
  { path: '/newsmanagement', name: 'News Management', component: NewsManagement },
  { path: '/shop/svakalocus', name: 'SvakaLocus', component: SvakaLocus },
  { path: '/newsmanagement', name: 'NewsManagement', component: NewsManagement },
  { path: '/matchmanagement', name: 'MatchManagement', component: MatchManagement },
  //{ path: '/users/:playerID', exact: true, name: 'User Details', component: User },
  { path: '/usersdashboard', exact: true, name: 'Users', component: UsersDashboard },
  { path: '/ksatriyadashboard', exact: true, name: 'Ksatriya', component: KsatriyaDashboard},
  { path: '/servermonitor', exact: true, name: 'Server Monitor', component: ServerMonitor},
  { path: '/playermanager', exact: true, name: 'PlayerManager', component: PlayerManager },
  { path: '/generatevoucher', exact: true, name: 'Generate Voucher', component: Voucher },
  { path: '/shop/itemmanager', exact: true, name: 'Item Manager', component: ItemManager },
  { path: '/shop/itembundle', exact: true, name: 'Item Bundle', component: ItemBundle },
  { path: '/shop/svakalocus', exact: true, name: 'Svaka Locus', component: SvakaLocus},
  { path: '/shop/lotusshop', exact: true, name: 'Lotus Shop Manager', component: LotusShopManager },
  { path: '/pastmatchdata', exact: true, name: 'Past Match Data', component: PastMatchData },

  { path: '/playersreports', name: 'Players Reports', component: PlayersReports },
  { path: '/playerdetail/:user_id', exact: true, name: 'Player Details', component: PlayerDashboard },
  { path: '/matchdetails/:room_id', exact: true, name: 'Match Statistic', component: MatchStatistic },
  { path: '/usermanagementdetail/:id', exact: true, name: 'UserManagementDetail', component: UserManagementDetail },

  { path: '/guildmanagement', exact: true, name: 'Guild Management', component: GuildManagement },
  { path: '/guilddashboard/:guild_id', exact: true, name: 'Guild Dashboard', component: GuildDashboard },

  { path: '/shopstatistics', exact: true, name: 'Guild Dashboard', component: ShopStatistic },
  { path: '/fortuneofthenorth', exact: true, name: 'Fortune of the North', component: FortuneoftheNorth },
  { path: '/eventenergy', exact: true, name: 'Event Energy', component: EventEnergy },
  { path: '/seasonend', exact: true, name: 'Season End', component: SeasonEnd },
  { path: '/missionstats', exact: true, name: 'Season End', component: MissionStatistics },
  { path: '/gamemanager', exact: true, name: 'Game Manager', component: GameManager },
  { path: '/maintenancemanagement', exact: true, name: 'Maintenance Management', component: MaintenanceManagement }
  // { path: '/login', exact: true, name: 'Logout', component: Logout},
  
  
  // { path: '/theme', name: 'Theme', component: Colors, exact: true },
  // { path: '/theme/colors', name: 'Colors', component: Colors },
  // { path: '/theme/typography', name: 'Typography', component: Typography },
  // { path: '/base', name: 'Base', component: Cards, exact: true },
  // { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  // { path: '/base/cards', name: 'Cards', component: Cards },
  // { path: '/base/carousels', name: 'Carousel', component: Carousels },
  // { path: '/base/collapses', name: 'Collapse', component: Collapses },
  // { path: '/base/forms', name: 'Forms', component: BasicForms },
  // { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  // { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  // { path: '/base/navbars', name: 'Navbars', component: Navbars },
  // { path: '/base/navs', name: 'Navs', component: Navs },
  // { path: '/base/paginations', name: 'Paginations', component: Paginations },
  // { path: '/base/popovers', name: 'Popovers', component: Popovers },
  // { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  // { path: '/base/switches', name: 'Switches', component: Switches },
  // { path: '/base/tables', name: 'Tables', component: Tables },
  // { path: '/base/tabs', name: 'Tabs', component: Tabs },
  // { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  // { path: '/buttons', name: 'Buttons', component: Buttons, exact: true },
  // { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  // { path: '/buttons/button-dropdowns', name: 'Dropdowns', component: ButtonDropdowns },
  // { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  // { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  // { path: '/charts', name: 'Charts', component: Charts },
  // { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  // { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  // { path: '/icons/flags', name: 'Flags', component: Flags },
  // { path: '/icons/brands', name: 'Brands', component: Brands },
  // { path: '/notifications', name: 'Notifications', component: Alerts, exact: true },
  // { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  // { path: '/notifications/badges', name: 'Badges', component: Badges },
  // { path: '/notifications/modals', name: 'Modals', component: Modals },
  // { path: '/notifications/toaster', name: 'Toaster', component: Toaster },
  // { path: '/widgets', name: 'Widgets', component: Widgets },
   
];

export default routes;
