import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
        <a href="http://www.anantarupa.com/" target="_blank" rel="noopener noreferrer">Anantarupa Team</a>
        <span className="ml-1">&copy; 2021</span>
      </div>
      <div className="mfs-auto">
        <span className="mr-1">Powered by</span>
        <a href="http://www.anantarupa.com/" target="_blank" rel="noopener noreferrer">Anantarupa Studio</a>
      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
