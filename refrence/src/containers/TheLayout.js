import React, { useEffect, useState } from 'react'
import {
  TheContent,
  TheSidebar,
  TheFooter,
  TheHeader
} from './index'
import { useHistory } from 'react-router'
import {GetCurrentInfo} from 'src/api/currentinfo/currentInfoandPermissionGetRequests'
const TheLayout = (props) => {

  const [_show, set_show] = useState(window.innerWidth > 1080 ? true : window.innerWidth <= 1080 ? false : false)

  const [wwidth, setWwidth] = useState()
  const history = useHistory ();

  // const [currentInfo, setCurrentInfo] = useState([]);

  // GetCurrentInfo({
  //   setCurrentInfo
  // })
  // console.log(currentInfo.length);

  // useEffect(() => {
  //   if (currentInfo.length === 0) {
  //     localStorage.removeItem('auth')
  //     history.push('/login')
  //   }
  // }, [currentInfo])

  useEffect(() => {
    window.addEventListener("resize", function () {
      return setWwidth(window.innerWidth);
    });
  }, [window.innerWidth])
  
  useEffect(() => {
    if (props.error?.response?.status === 401) {
      localStorage.removeItem('auth')
      history.push('/login')
    }
  }, [props.error])
  useEffect(() => {
    if (wwidth > 1080) {
      return set_show(true)
    } else if (wwidth <= 1080) {
      return set_show(false)
    }
  }, [wwidth])

  return (
    <div className="c-app c-default-layout">
      <TheSidebar _show={_show} set_show={set_show} />
      <div className="c-wrapper">
        <TheHeader _show={_show} set_show={set_show} />
        <div className="c-body">
          <TheContent />
        </div>
        <TheFooter />
      </div>
    </div>
  )
}

export default TheLayout;
