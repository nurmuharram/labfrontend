import React, { useEffect, useRef, useState } from "react";
import { useHistory } from 'react-router'
import { useSelector, useDispatch, connect } from 'react-redux'
import {
  CHeader,
  CToggler,
  CHeaderBrand,
  CHeaderNav,
  CHeaderNavItem,
  CCol,
  // CHeaderNavLink,
  // CSubheader,
  // CBreadcrumbRouter,
  // CLink
} from '@coreui/react'
import { currentInfoAction } from "src/redux/action/currentInfoAction/currentInfoAction";
import CIcon from '@coreui/icons-react'

// routes config
// import routes from '../routes'

import {
  TheHeaderDropdown,
  TheHeaderDropdownMssg,
  // TheHeaderDropdownNotif,
  // TheHeaderDropdownTasks
} from './index'
import { GetCurrentInfo } from "src/api/currentinfo/currentInfoandPermissionGetRequests";
import jwtDecode from "jwt-decode";
import toast from "react-hot-toast";
import axios from 'axios'
import { duration } from "moment";

const TheHeader = ({ _show, set_show }) => {
  const dispatch = useDispatch()
  const sidebarShow = useSelector(state => state.sidebarShow)
  const [currentInfo, setCurrentInfo] = useState({})
  const [decodedToken, setDecodedToken] = useState([])
  const history = useHistory()

  GetCurrentInfo({ setCurrentInfo })

  const token = localStorage.getItem('auth')
  const [currentInfoError, setCurrentInfoError] = useState()
  const [permission, setPermission] = useState([])


  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/getCurrentUserLogin`, config).then(res => {
      const data = res.data;
      setCurrentInfo(data);
    })
    .catch((error) => {
      setCurrentInfoError(error.message)
  });
  }, [])
  console.log(currentInfoError)
  useEffect(() => {
    if(currentInfoError === 'Request failed with status code 401') {
      localStorage.removeItem('auth')
      history.push('/login')
      
    } else if(currentInfoError === 'Request failed with status code 502'){
      localStorage.removeItem('auth')
      history.push('/login')
    }
  }, [currentInfoError])

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/getRolePermission?role_id=${currentInfo.Role_id}`, config).then(res => {
      const data = res.data;
      setPermission(data);
    });
  }, [currentInfo.Role_id])

  const _permission = permission && permission.map(item => item.permission_id).filter((value, index, self) => self.indexOf(value) === index);

  
  // Token Check if Exist or null
  useEffect(() => {
    if (token === null) {
      return ''
    } else {
      const decodedToken = jwtDecode(token)
      return setDecodedToken(decodedToken)
    }
  }, [])

  const calculateTimeLeft = () => {
    let tokenExpiry = new Date(decodedToken.exp * 1000)
    let difference = +tokenExpiry - +new Date();

    let timeLeft = {}
    if (difference > 0) {
      timeLeft = {
        hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
        minutes: Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60)
      };
    }
    return timeLeft;
  }
  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

  useEffect(() => {
    const timer = setTimeout(() => {
      setTimeLeft(calculateTimeLeft());
    }, 1000);
    return () => clearTimeout(timer);
  });
  const timerComponents = [];
  Object.keys(timeLeft).forEach((interval) => {
    if (!timeLeft[interval]) {
      return;
    }
    timerComponents.push(
      <span>
        {timeLeft[interval]} {interval}{" "}
      </span>
    );
  });

  const logout = () => {
    localStorage.removeItem('auth');
    // alert('Your Session Has Expired, Please Relogin.');
    history.push('/login');
  }

  useEffect(() => {
    if (timeLeft.minutes === 0 && timeLeft.seconds === 3) {
      return setTimeout(logout(), 2000)
    }
  }, [timeLeft])

  const expireWarning = () => {
    toast((t) => (
      <div className="container-fluid">
        <div>
          <div align='center'>
            <div className="card-title">
              <b>Warning!</b> &#x26A0;
            </div>
          </div>
          <div align='center'>
            <p color="red">Your Session is About to Expired in <b style={{ color: "red" }}>{timeLeft.minutes} mins.</b> </p>
            <hr />
            <button className="btn btn-secondary btn-sm" onClick={() => toast.dismiss(t.id)}>
              Dismiss
            </button>

          </div>
        </div>
      </div>
    ),
      {
        duration: 10000,
        position: "top-right"
      });
  }

  return (
    <CHeader withSubheader>
      <CToggler
        inHeader
        className="ml-md-3 d-lg-none"
        onClick={() => set_show(!_show)}
      />
      <CToggler
        inHeader
        className="ml-3 d-md-down-none"
        onClick={() => set_show(!_show)}
      />

      {timeLeft.minutes === 5 && timeLeft.seconds === 0 ? expireWarning() : <></>}

      <CHeaderBrand className="mx-auto d-lg-none" to="/">

        <CIcon name="logo" height="48" alt="Logo" />
      </CHeaderBrand>

      <CHeaderNav className="d-md-down-none mr-auto">
        <CHeaderNavItem className="px-3" >
          {/* <CHeaderNavLink to="/dashboard">Dashboard</CHeaderNavLink> */}
          {timerComponents.length ? <small>Your Session Expires in : {timerComponents}</small> :
            timerComponents.length === 0 ? <div class="spinner-border spinner-border-sm" role="status">
              <span class="sr-only">Loading...</span>
            </div> : <small>Your Session Has Expired, Logging Out User...</small>}
        </CHeaderNavItem>

      </CHeaderNav>

      <CHeaderNav className="px-3">
        <h6>Welcome</h6> <h6>&nbsp;<b>{currentInfo.name}</b></h6>&nbsp;&nbsp;&nbsp;&nbsp;
        {/* <TheHeaderDropdownNotif/>
        <TheHeaderDropdownTasks/> */}
        <TheHeaderDropdownMssg />
        <TheHeaderDropdown />

      </CHeaderNav>
    </CHeader>
  )
}
export default TheHeader; 
