import React from 'react'
import CIcon from '@coreui/icons-react'

const navNews = {
        _tag: 'CSidebarNavItem',
        name: 'News Management',
        to: '/newsmanagement',
        icon: 'cil-calendar',
      }
export default navNews