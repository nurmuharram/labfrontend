const navEvent = {
    _tag: 'CSidebarNavItem',
    name: 'Events',
    to: '/eventenergy',
    icon: 'cil-user',
  }
  export default navEvent