import React from 'react'
import CIcon from '@coreui/icons-react'

const navDashboard =  {
    _tag: 'CSidebarNavDropdown',
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
    _children: [
       {
         _tag: 'CSidebarNavItem',
         name: 'Users',
         to: '/usersdashboard',
         icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
       },
    
       {
         _tag: 'CSidebarNavItem',
         name: 'Ksatriya',
         to: '/ksatriyadashboard',
         icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
       },
    
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Server Monitor',
      //   to: '/dashboard/servermonitor',
      //   icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
      // },
    ]
  }
  export default navDashboard