const navMission = {
    _tag: 'CSidebarNavItem',
    name: 'Mission Statistics',
    to: '/missionstats',
    icon: 'cil-speedometer',
  }
export default navMission