import React from 'react'
import CIcon from '@coreui/icons-react'

const navReports = {
    _tag: 'CSidebarNavItem',
    name: 'Player Reports',
    to: '/playersreports',
    icon: 'cil-warning',
  }
  export default navReports