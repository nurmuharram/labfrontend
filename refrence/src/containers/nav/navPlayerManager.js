import React from 'react'
import CIcon from '@coreui/icons-react'

const navPlayerManager = {
    _tag: 'CSidebarNavItem',
    name: 'Player Manager',
    to: '/playermanager',
    icon: 'cil-pencil',
  }
  export default navPlayerManager