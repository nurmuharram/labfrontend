import React from 'react'
import CIcon from '@coreui/icons-react'

const navManagement= {
    _tag: 'CSidebarNavItem',
    name: 'Management',
    to: '/management',
    icon: 'cil-user',
  }
  export default navManagement