import React from 'react'
import CIcon from '@coreui/icons-react'

const navShop = {
    _tag: 'CSidebarNavDropdown',
    name: 'Shop',
    icon: <CIcon name="cil-basket" customClasses="c-sidebar-nav-icon"/>,
    _children: [
       {
         _tag: 'CSidebarNavItem',
         name: 'Items Manager',
         to: '/shop/itemmanager',
         icon: <CIcon name="cil-cart" customClasses="c-sidebar-nav-icon"/>,
       },
    
       {
         _tag: 'CSidebarNavItem',
         name: 'Bundle',
         to: '/shop/itembundle',
         icon: <CIcon name="cil-cart" customClasses="c-sidebar-nav-icon"/>,
       },
       {
        _tag: 'CSidebarNavItem',
        name: 'Svaka Locus',
        to: '/shop/svakalocus',
        icon: <CIcon name="cil-cart" customClasses="c-sidebar-nav-icon"/>,
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Lotus Shop',
        to: '/shop/lotusshop',
        icon: <CIcon name="cil-cart" customClasses="c-sidebar-nav-icon"/>,
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Shop Statistic',
        to: '/shopstatistics',
        icon: <CIcon name="cil-cart" customClasses="c-sidebar-nav-icon"/>,
      }
    ]
}
export default navShop