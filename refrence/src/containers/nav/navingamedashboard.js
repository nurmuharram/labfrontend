import React from 'react'
import CIcon from '@coreui/icons-react'

export const navIngamedashboard = {
    _tag: 'CSidebarNavDropdown',
    name: 'Match Management',
    icon: <CIcon name="cil-puzzle" customClasses="c-sidebar-nav-icon"/>,
    _children: [
       {
        _tag: 'CSidebarNavItem',
        name: 'Live Matches',
        to: '/matchmanagement',
        icon: 'cil-user',
       },
    
       {
        _tag: 'CSidebarNavItem',
        name: 'Past Match Data',
        to: '/pastmatchdata',
        icon: 'cil-magnifying-glass',
       },    
    
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Server Monitor',
      //   to: '/dashboard/servermonitor',
      //   icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
      // },
    ]
  }