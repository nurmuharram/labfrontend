import React from 'react'
import CIcon from '@coreui/icons-react'

const navKsaRotation = {
    _tag: 'CSidebarNavItem',
    name: 'KSA Rotation Manager',
    to: '/ksarotationmanager',
    icon: 'cil-user-follow',
  }
  export default navKsaRotation