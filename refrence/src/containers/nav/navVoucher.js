import React from 'react'
import CIcon from '@coreui/icons-react'

const navVoucher = {
    _tag: 'CSidebarNavItem',
    name: 'Generate Voucher',
    to: '/generatevoucher',
    icon: 'cil-tags',
  }
export default navVoucher