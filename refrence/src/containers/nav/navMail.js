import React from 'react'
import CIcon from '@coreui/icons-react'

const navMail = {
    _tag: 'CSidebarNavItem',
    name: 'Mail',
    to: '/mail',
    icon: 'cil-envelope-closed',
  }
  export default navMail