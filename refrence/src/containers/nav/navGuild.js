import React from 'react'
import CIcon from '@coreui/icons-react'

const navGuild = {
    _tag: 'CSidebarNavItem',
    name: 'Guild Management',
    to: '/guildmanagement',
    icon: 'cil-user',
  }
export default navGuild