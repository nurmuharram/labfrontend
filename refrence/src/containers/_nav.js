import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav =  [
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
    _children: [
       {
         _tag: 'CSidebarNavItem',
         name: 'Users',
         to: '/usersdashboard',
         icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
       },
    
       {
         _tag: 'CSidebarNavItem',
         name: 'Ksatriya',
         to: '/ksatriyadashboard',
         icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
       },
    
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Server Monitor',
      //   to: '/dashboard/servermonitor',
      //   icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
      // },
    ]
  },


  {
    _tag: 'CSidebarNavItem',
    name: 'Player Manager',
    to: '/playermanager',
    icon: 'cil-pencil',
  },

  {
    _tag: 'CSidebarNavItem',
    name: 'Management',
    to: '/management',
    icon: 'cil-user',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Mail',
    to: '/mail',
    icon: 'cil-envelope-closed',
  },

  {
    _tag: 'CSidebarNavDropdown',
    name: 'In-Game Dashboard',
    icon: <CIcon name="cil-puzzle" customClasses="c-sidebar-nav-icon"/>,
    _children: [
       {
        _tag: 'CSidebarNavItem',
        name: 'Match Management',
        to: '/matchmanagement',
        icon: 'cil-user',
       },
    
       {
        _tag: 'CSidebarNavItem',
        name: 'Past Match Data',
        to: '/pastmatchdata',
        icon: 'cil-magnifying-glass',
       },
       {
        _tag: 'CSidebarNavItem',
        name: 'Player Reports',
        to: '/playersreports',
        icon: 'cil-warning',
      },
    
    
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Server Monitor',
      //   to: '/dashboard/servermonitor',
      //   icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
      // },
    ]
  },

  {
    _tag: 'CSidebarNavItem',
    name: 'News Management',
    to: '/newsmanagement',
    icon: 'cil-calendar',
  },

  {
    _tag: 'CSidebarNavItem',
    name: 'Generate Voucher',
    to: '/generatevoucher',
    icon: 'cil-tags',
  },

  

  {
    _tag: 'CSidebarNavDropdown',
    name: 'Shop',
    icon: <CIcon name="cil-basket" customClasses="c-sidebar-nav-icon"/>,
    _children: [
       {
         _tag: 'CSidebarNavItem',
         name: 'Items Manager',
         to: '/shop/itemmanager',
         icon: <CIcon name="cil-cart" customClasses="c-sidebar-nav-icon"/>,
       },
    
       {
         _tag: 'CSidebarNavItem',
         name: 'Item Bundle',
         to: '/shop/itembundle',
         icon: <CIcon name="cil-cart" customClasses="c-sidebar-nav-icon"/>,
       },
    
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Server Monitor',
      //   to: '/dashboard/servermonitor',
      //   icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
      // },
    ]
  },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Svaka Lotus',
  //   to: '/svakalotus',
  //   icon: 'cil-laptop',
  // },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Featured bundle',
  //   to: '/featuredbundle',
  //   icon: 'cil-laptop',
  // },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Logout',
  //   to: '/login',
  //   icon: '',
  // }, 
]

export default _nav
