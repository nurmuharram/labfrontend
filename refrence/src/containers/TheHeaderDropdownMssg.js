import React, { useState, useEffect } from 'react'
import {
  CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg,
  CRow,
  CCol,
  CLink,
  CDropdownDivider
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import axios from 'axios'


const TheHeaderDropdownMssg = () => {

  const tempdata = '0'

  const [ksarot, setKsarot] = useState()
  const [gacha, setGacha] = useState()
  const [lotto, setLotto] = useState()
  const [lotus, setLotus] = useState()
  const [season, setSeason] = useState()
  const [dailyreward, setDailyreward] = useState()

  useEffect(() => {
    const config = {
      headers: {
          Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
  }
  axios.get(`/api/warning/ksaRot`, config).then(res => {
      const ksaRot = res.data;
      setKsarot(ksaRot);
  });
  }, [])

  useEffect(() => {
    const config = {
      headers: {
          Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
  }
  axios.get(`/api/warning/gacha`, config).then(res => {
      const gacha = res.data;
      setGacha(gacha);
  });
  }, [])

  useEffect(() => {
    const config = {
      headers: {
          Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
  }
  axios.get(`/api/warning/lotto`, config).then(res => {
      const lotto = res.data;
      setLotto(lotto);
  });
  }, [])

  useEffect(() => {
    const config = {
      headers: {
          Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
  }
  axios.get(`/api/warning/lotus`, config).then(res => {
      const lotus = res.data;
      setLotus(lotus);
  });
  }, [])

  useEffect(() => {
    const config = {
      headers: {
          Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
  }
  axios.get(`/api/warning/season`, config).then(res => {
      const season = res.data;
      setSeason(season);
  });
  }, [])

  useEffect(() => {
    const config = {
      headers: {
          Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
  }
  axios.get(`/api/warning/dailyreward`, config).then(res => {
      const dailyreward = res.data;
      setDailyreward(dailyreward);
  });
  }, [])


  const ksarottemplate = () => {
    return (
      <><CDropdownDivider/><CDropdownItem href="#">
      <div className="message">
            <div>
              <small className="text-muted"><i>System</i></small>
            </div>
            <div className="text-truncate font-weight-bold">Warning!</div>
            <div className="small text-muted text-truncate"><h6><b>KSA Free Rotation</b> period is about to end!</h6>
            </div>
          </div>
          </CDropdownItem>
           <CDropdownDivider/></>
    )
  }

  const gachatemplate = () => {
    return (
      <><CDropdownDivider/><CDropdownItem href="#">
      <div className="message">
            <div>
              <small className="text-muted"><i>System</i></small>
            </div>
            <div className="text-truncate font-weight-bold">Warning!</div>
            <div className="small text-muted text-truncate"><h6><b>Svaka Locus</b> period is about to end!</h6>
            </div>
          </div>
          </CDropdownItem>
          <CDropdownDivider/></>
    )
  }

  const lottotemplate = () => {
    return (
      <><CDropdownDivider/><CDropdownItem href="#">
      <div className="message">
            <div>
              <small className="text-muted"><i>System</i></small>
            </div>
            <div className="text-truncate font-weight-bold">Warning!</div>
            <div className="small text-muted text-truncate"><h6><b>Fortune of the North</b> period is about to end!</h6>
            </div>
          </div>
          </CDropdownItem>
          <CDropdownDivider/></>
    )
  }

  const lotustemplate = () => {
    return (
      <><CDropdownDivider/><CDropdownItem href="#">
      <div className="message">
            <div>
              <small className="text-muted"><i>System</i></small>
            </div>
            <div className="text-truncate font-weight-bold">Warning!</div>
            <div className="small text-muted text-truncate"><h6><b>Lotus Shop</b> period is about to end!</h6>
            </div>
          </div>
          </CDropdownItem>
          <CDropdownDivider/></>
    )
  }

  const seasontemplate = () => {
    return (
      <><CDropdownDivider/><CDropdownItem href="#">
      <div className="message">
            <div>
              <small className="text-muted"><i>System</i></small>
            </div>
            <div className="text-truncate font-weight-bold">Warning!</div>
            <div className="small text-muted text-truncate"><h6><b>The Season</b> is about to end!</h6>
            </div>
          </div>
          </CDropdownItem>
          <CDropdownDivider/></>
    )
  }

  const dailyrewardtemplate = () => {
    return (
      <><CDropdownDivider/><CDropdownItem href="#">
      <div className="message">
            <div>
              <small className="text-muted"><i>System</i></small>
            </div>
            <div className="text-truncate font-weight-bold">Warning!</div>
            <div className="small text-muted text-truncate"><h6><b>Daily Reward</b> Period is about to end!</h6>
            </div>
          </div>
          </CDropdownItem>
          <CDropdownDivider/></>
    )
  }

  const warningksarot = () => {
    if (ksarot === '1') {
      return ksarottemplate()
    } else if (ksarot === '0') {
      return <></>
    }
  }

  const warninggacha = () => {
    if (gacha === '1') {
      return gachatemplate()
    } else if (gacha === '0') {
      return <></>
    }
  }

  const warninglotto = () => {
    if (lotto === '1') {
      return lottotemplate()
    } else if (lotto === '0') {
      return <></>
    }
  }

  const warninglotus = () => {
    if (lotus === '1') {
      return lotustemplate()
    } else if (lotus === '0') {
      return <></>
    }
  }

  const warningseason = () => {
    if (season === '1') {
      return seasontemplate()
    } else if (season === '0') {
      return <></>
    }
  }

  const warningdailyreward = () => {
    if (dailyreward === '1') {
      return dailyrewardtemplate()
    } else if (dailyreward === '0') {
      return <></>
    }
  }


  const countksa = () => {
    if (ksarot === '1') {
      return 1
    } else if (ksarot === '0') {
      return 0
    } else if (ksarot === undefined) {
      return 0
    }
  }

  const countgacha = () => {
    if (gacha === '1') {
      return 1
    } else if (gacha === '0') {
      return 0
    } else if (gacha === undefined) {
      return 0
    }
  }

  const countlotto = () => {
    if (lotto === '1') {
      return 1
    } else if (lotto === '0') {
      return 0
    } else if (lotto === undefined) {
      return 0
    }
  }

  const countlotus = () => {
    if (lotus === '1') {
      return 1
    } else if (lotus === '0') {
      return 0
    } else if (lotus === undefined) {
      return 0
    }
  }

  const countseason = () => {
    if (season === '1') {
      return 1
    } else if (season === '0') {
      return 0
    } else if (season === undefined) {
      return 0
    }
  }

  const countdailyreward = () => {
    if (dailyreward === '1') {
      return 1
    } else if (dailyreward === '0') {
      return 0
    } else if (dailyreward === undefined) {
      return 0
    }
  }
  
  const itemsCount = countksa() + countgacha() + countlotto() + countlotus() + countseason() + countdailyreward()


  return (
    
    <CDropdown
      inNav
      className="c-header-nav-item mx-2"
      direction="down"
    >
      
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <CIcon size='xl' name="cil-bell" />
        {itemsCount === 0 ? <></> : <CBadge shape="pill" color="danger">{itemsCount}</CBadge>}
      </CDropdownToggle>
      
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem
          header
          tag="div"
          color="light"
        >
          <CRow>
            <CCol align='left'>
            <strong>Notifications</strong>
            </CCol>
            {/* <CCol align='right'>
            <CLink>Mark all as read</CLink> 
            </CCol> */}
          </CRow>
        </CDropdownItem>
        {warningksarot()} 
        {warninggacha()}
        {warninglotto()}
        {warninglotus()}
        {warningseason()}
        {warningdailyreward()}
        {/* <CDropdownItem href="#" className="text-center border-top"><strong>View all notifications</strong></CDropdownItem> */}
      </CDropdownMenu>
    </CDropdown> 
  )
}

export default TheHeaderDropdownMssg