import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from '@coreui/react'

import axios from 'axios'

// import CIcon from '@coreui/icons-react'

// sidebar nav config
import navigation from './_nav'
import navVoucher from './nav/navVoucher'
import navDashboard from './nav/navDashboard'
import navShop from './nav/navShop'
import {navIngamedashboard} from './nav/navingamedashboard'
import navManagement from './nav/navManagement'
import navMail from './nav/navMail'
import navPlayerManager from './nav/navPlayerManager'
import navNews from './nav/navNews'
import navReports from './nav/navReports'
import navGuild from './nav/navGuild'
import navKsaRotation from './nav/navKsaRotation'
import navEvent from './nav/navEvent'
import navMission from './nav/navMission'
import { useHistory } from 'react-router-dom'


const TheSidebar = ({_show, set_show}) => {
  const dispatch = useDispatch()
  const show = useSelector(state => state.sidebarShow)
  
  const [currentInfo, setCurrentInfo] = useState({})
  const [currentInfoError, setCurrentInfoError] = useState()
  const [permission, setPermission] = useState([])
  const history = useHistory();


  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/getCurrentUserLogin`, config).then(res => {
      const data = res.data;
      setCurrentInfo(data);
    })
    .catch((error) => {
      setCurrentInfoError(error.message)
  });
  }, [])
  console.log(currentInfoError)
  // useEffect(() => {
  //   if(currentInfoError === 'Request failed with status code 401') {
  //     localStorage.removeItem('auth')
  //     history.push('/login')
      
  //   } else if(currentInfoError === 'Request failed with status code 502'){
  //     localStorage.removeItem('auth')
  //     history.push('/login')
  //   }
  // }, [currentInfoError])

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/getRolePermission?role_id=${currentInfo.Role_id}`, config).then(res => {
      const data = res.data;
      setPermission(data);
    });
  }, [currentInfo.Role_id])

  const _permission = permission && permission.map(item => item.permission_id).filter((value, index, self) => self.indexOf(value) === index);

  const VoucherControl = () => {
    if (_permission && _permission.includes(9) === true) {
      return navVoucher
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }

  const userstats = () => {
    if (_permission && _permission.includes(11) === true) {
      return navDashboard
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }

  const shop = () => {
    if (_permission && _permission.includes(2) === true) {
      return navShop
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }

  const playerReportsControl = () => {
    if (_permission && _permission.includes(3) === true) {
      return navPlayerManager
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }

  const modifyUser = () => {
    if (_permission && _permission.includes(1) === true) {
      return navManagement
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }
  const mail = () => {
    if (_permission && _permission.includes(4) === true) {
      return navMail
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }

  const matchesManagement = () => {
    if (_permission && _permission.includes(5) === true) {
      return navIngamedashboard
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }

  const reports = () => {
    if (_permission && _permission.includes(7) === true) {
      return navReports
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }

  const news = () => {
    if (_permission && _permission.includes(14) === true) {
      return navNews
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }

  const guild = () => {
    if (_permission && _permission.includes(13) === true) {
      return navGuild
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }

  const ksarot = () => {
    if (_permission && _permission.includes(6) === true) {
      return navKsaRotation
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }

  const event = () => {
    if (_permission && _permission.includes(15) === true) {
      return navEvent
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }

  const mission = () => {
    if (_permission && _permission.includes(11) === true) {
      return navMission
    } else {
      return {
        _tag: 'CSidebarNavMinimizer',
        name: '',
        to: '',
        icon: '',
      }
    }
  }
  

  const data = [
    userstats(),
    modifyUser(),
    playerReportsControl(),
    guild(),
    {
      _tag: 'CSidebarNavItem',
      name: 'Game Manager',
      to: '/gamemanager',
      icon: 'cil-speedometer',
    },
    news(),
    matchesManagement(),
    mail(),
    shop(),
    // event(),
    mission(),

    // ksarot(),
    reports(),
    VoucherControl(),
  ]

  return (
    <CSidebar
      show={_show}
      className=" bg-gradient-dark"
      onShowChange={() => set_show(!_show)}
    >
      <CSidebarBrand className="d-md-down-none" to="/">

        <img className="c-sidebar-brand-full" height={105} 
          src={`${process.env.PUBLIC_URL}/avatars/Logo_big.png`}
          alt="logo" />

        <img className="c-sidebar-brand-minimized" height={35} name="sygnet"
          src={`${process.env.PUBLIC_URL}/avatars/logo_small.png`}
          alt="logo-small" />

      </CSidebarBrand>
      <CSidebarNav>

        <CCreateElement
          items={data}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle,
            CSidebarMinimizer
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none"/>
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
