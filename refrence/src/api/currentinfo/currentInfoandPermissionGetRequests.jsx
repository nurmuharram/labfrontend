import { useEffect } from "react";
import axios from "axios";

export const GetCurrentInfo = ({ setCurrentInfo }) => {
    useEffect(() => {
        const config = {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
          }
        }
        axios.get(`/api/getCurrentUserLogin`, config).then(res => {
          const data = res.data;
          setCurrentInfo(data);
        });
      }, [])
};

export const GetCurrentPermission = ({ setPermission, currentInfo }) => {
    useEffect(() => {
        const config = {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
          }
        }
        axios.get(`api/getRolePermission?role_id=${currentInfo.Role_id}`, config).then(res => {
          const data = res.data;
          setPermission(data);
        });
      }, [currentInfo.Role_id])
};