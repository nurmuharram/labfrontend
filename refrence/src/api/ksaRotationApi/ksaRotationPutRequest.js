import axios from "axios";
import { GetKsaRotationList } from "./ksaRotationGetRequest";

export const UpdateKsaRotation = ({
    setKsaRotation,
    newStartDate,
    newStartTime,
    newEndDate,
    newEndTime,
    ksatriyaId,
    id
}) => {
    const convertNewStartDateToUtc = new Date(newStartDate + " " + newStartTime);
    const convertedNewStartDate = convertNewStartDateToUtc
        .toISOString()
        .split("T")
        .join(" ")
        .replace(".000Z", "");
    const convertNewEndDateToUtc = new Date(newEndDate + " " + newEndTime);
    const convertedNewEndDate = convertNewEndDateToUtc
        .toISOString()
        .split("T")
        .join(" ")
        .replace(".000Z", "");
    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/ksatriya/rotation/getAllKsaRotation`, config).then(res => {
            const items = res.data;
            setKsaRotation(items);
        });
    };

    const updateKsaRotation = async () => {
        const FormData = require("form-data");
        const data = new FormData();
        data.append("ksatriya_id", parseInt(ksatriyaId));
        data.append("start_date", convertedNewStartDate);
        data.append("end_date", convertedNewEndDate);

        const config = {
            method: "PUT",
            url: `/api/ksatriya/rotation/updateKsaRotation?ksatriya_rotation_id=${id}`,
            headers: {
                Authorization: "Bearer " + localStorage.getItem("auth"),
            },
            data: data,
        };

        await axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert("Item Updated!");
            })
            .catch((error) => {
                console.log(error);
                alert("Failed to Update!");
            });
        setTimeout(refreshList, 100);
    };

    return (
        <>
            <button className="btn btn-md btn-info" onClick={updateKsaRotation}>
                Update
            </button>
        </>
    );
};