import axios from 'axios'
import { GetKsaRotationList } from './ksaRotationGetRequest'

export const AddKsaRotation = ({ startDate, startTime, endDate, endTime, arrayData, setKsaRotation }) => {

    const convertStartDateToUtc = new Date(startDate + ' ' + startTime)
    const convertedStartDate = convertStartDateToUtc.toISOString().split('T').join(' ').replace('.000Z', '')
    const convertEndDateToUtc = new Date(endDate + ' ' + endTime)
    const convertedEndDate = convertEndDateToUtc.toISOString().split('T').join(' ').replace('.000Z', '')

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/ksatriya/rotation/getAllKsaRotation`, config).then(res => {
            const items = res.data;
            setKsaRotation(items);
        });
    }

    const arrayDataMapping = arrayData && arrayData.map((data, index) => {
        return { ksatriya_id: data.ksatriya_id }
    })

    const jsonData = {
        ksatriya_ids: arrayDataMapping
    }

    const addKsaRotation = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('ksatriya_id', JSON.stringify(jsonData))
        data.append('start_date', convertedStartDate);
        data.append('end_date', convertedEndDate)

        const config = {
            method: 'POST',
            url: `/api/ksatriya/rotation/addKsaRotation`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('KSA Added!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add!')
            });
        setTimeout(refreshList, 100)
    }
    return (
        <button className="btn btn-primary" onClick={addKsaRotation}>+ Add</button>
    )
}