import { useEffect } from "react"
import axios from "axios"

export const GetKsaRotationList = ({ setKsaRotation }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/ksatriya/rotation/getAllKsaRotation`, config).then(res => {
            const items = res.data;
            setKsaRotation(items);
        });
    }, [])
}

export const RefreshKsaRotationList = ({ setKsaRotation }) => {
    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/ksatriya/rotation/getAllKsaRotation`, config).then(res => {
            const items = res.data;
            setKsaRotation(items);
        });
    } 
    return refreshData
} 