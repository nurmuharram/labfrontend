import axios from "axios";

export const DeleteRotationPeriod = ({ setKsaRotation, setDeleteItemModal, id}) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/ksatriya/rotation/getAllKsaRotation`, config).then(res => {
            const items = res.data;
            setKsaRotation(items);
        });
    }

    const deleteKsaRotation = () => {
        const config = {
          method: "delete",
          url: `/api/ksatriya/rotation/deleteKsaRotation?ksatriya_rotation_id=${id}`,
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
        };

        axios(config)
          .then((response) => {
            console.log(JSON.stringify(response.data));
            alert("Item Removed!");
            setDeleteItemModal(false);
          })
          .catch((error) => {
            alert("Failed to remove item!");
          });
        setTimeout(refreshList, 100);
      };
    return (
        <><button className="btn btn-sm btn-danger" onClick={deleteKsaRotation} ><b>Delete</b></button></>
    )
}