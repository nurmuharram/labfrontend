import axios from "axios";
import { useEffect } from "react";

// Get Gacha Period
export const GetGachaPeriod = ({ setGachaList }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAll`, config).then(res => {
            const items = res.data;
            setGachaList(items);
        });
    }, [])
}

// Get Gacha Featured
export const GetGachaFeatured = ({ setGachaFeaturedList }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllFeatured`, config).then(res => {
            const items = res.data;
            setGachaFeaturedList(items);
        });
    }, [])
}

// Get Gacha Items
export const GetGachaItems = ({ setGachaItems }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllItem`, config).then(res => {
            const items = res.data;
            setGachaItems(items);
        });
    }, [])
}

// Get Gacha Loot
export const GetGachaLoot = ({ setGachaLoot }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllLoot`, config).then(res => {
            const items = res.data;
            setGachaLoot(items);
        });
    }, [])
}