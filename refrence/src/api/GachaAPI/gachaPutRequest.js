import { CButton } from "@coreui/react";
import axios from "axios";

export const UpdateGachaStartdate = ({ setGachaList, newstartdate, newstarttime, end_date, random_value, id }) => {

    const convertTime = new Date(newstartdate + ' ' + newstarttime)
    const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')


    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAll`, config).then(res => {
            const items = res.data;
            setGachaList(items);
        });
    }

    const updateStartDate = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_date', startingdate);
        data.append('end_date', end_date);
        data.append('random_value', random_value);

        const config = {
            method: 'PUT',
            url: `/api/gacha/update?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateStartDate}>Update</CButton></>
    )
}

export const UpdateGachaEnddate = ({ setGachaList, newenddate, newendtime, start_date, random_value, id }) => {

    const convertTimeEnd = new Date(newenddate + ' ' + newendtime)
    const endingdate = convertTimeEnd.toISOString().split('T').join(' ').replace('.000Z', '')


    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAll`, config).then(res => {
            const items = res.data;
            setGachaList(items);
        });
    }

    const updateEndDate = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_date', start_date);
        data.append('end_date', endingdate);
        data.append('random_value', random_value);

        const config = {
            method: 'PUT',
            url: `/api/gacha/update?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateEndDate}>Update</CButton></>
    )
}

export const UpdateRandomValue = ({ setGachaList, end_date, start_date, newrandomvalue, id }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAll`, config).then(res => {
            const items = res.data;
            setGachaList(items);
        });
    }

    const updateRandomvalue = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_date', start_date);
        data.append('end_date', end_date);
        data.append('random_value', parseInt(newrandomvalue));

        const config = {
            method: 'PUT',
            url: `/api/gacha/update?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateRandomvalue}>Update</CButton></>
    )
}

export const UpdateGachaIdFeatured = ({ setGachaFeaturedList, newgacha_id, gachaitemID, _priority, id }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllFeatured`, config).then(res => {
            const items = res.data;
            setGachaFeaturedList(items);
        });
    }

    const updateGachaID = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('gacha_id', newgacha_id);
        data.append('gacha_item_id', gachaitemID);
        data.append('priority', parseInt(_priority));

        const config = {
            method: 'PUT',
            url: `/api/gacha/updateFeatured?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateGachaID}>Update</CButton></>
    )
}

export const UpdateGachaItemIdFeatured = ({ setGachaFeaturedList, gachaID, newgacha_item_id, _priority, id }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllFeatured`, config).then(res => {
            const items = res.data;
            setGachaFeaturedList(items);
        });
    }

    const updateGachaItemID = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('gacha_id', gachaID);
        data.append('gacha_item_id', newgacha_item_id);
        data.append('priority', parseInt(_priority));

        const config = {
            method: 'PUT',
            url: `/api/gacha/updateFeatured?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateGachaItemID}>Update</CButton></>
    )
}

export const UpdateGachaPriorityFeatured = ({ setGachaFeaturedList, gachaID, gachaitemID, newpriority, id }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllFeatured`, config).then(res => {
            const items = res.data;
            setGachaFeaturedList(items);
        });
    }

    const updatePriority = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('gacha_id', gachaID);
        data.append('gacha_item_id', gachaitemID);
        data.append('priority', parseInt(newpriority));

        const config = {
            method: 'PUT',
            url: `/api/gacha/updateFeatured?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updatePriority}>Update</CButton></>
    )
}

export const UpdateGachaItemAmount = ({ setGachaItems, newamount, id }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllItem`, config).then(res => {
            const items = res.data;
            setGachaItems(items);
        });
    }

    const updateAmount = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('amount', newamount);

        const config = {
            method: 'PUT',
            url: `/api/gacha/updateItem?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateAmount}>Update</CButton></>
    )
}

export const UpdateGachaLootChance = ({ setGachaLoot, newchance, minValue, maxValue, id, gachaitemID }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllLoot`, config).then(res => {
            const items = res.data;
            setGachaLoot(items);
        });
    }

    const updateChance = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('chance', newchance)
        data.append('min_value', minValue)
        data.append('max_value', maxValue);

        const config = {
            method: 'PUT',
            url: `/api/gacha/updateLoot?gacha_id=${id}&gacha_item_id=${gachaitemID}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateChance}>Update</CButton></>
    )
}

export const UpdateGachaLootMinValue = ({ setGachaLoot, _chance, newmin_value, maxValue, id, gachaitemID }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllLoot`, config).then(res => {
            const items = res.data;
            setGachaLoot(items);
        });
    }

    const updateMinValue = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('chance', _chance)
        data.append('min_value', newmin_value)
        data.append('max_value', maxValue);

        const config = {
            method: 'PUT',
            url: `/api/gacha/updateLoot?gacha_id=${id}&gacha_item_id=${gachaitemID}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateMinValue}>Update</CButton></>
    )
}

export const UpdateGachaLootMaxValue = ({ setGachaLoot, _chance, minValue, newmax_value, id, gachaitemID }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllLoot`, config).then(res => {
            const items = res.data;
            setGachaLoot(items);
        });
    }

    const updateMaxValue = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('chance', _chance)
        data.append('min_value', minValue)
        data.append('max_value', newmax_value);

        const config = {
            method: 'PUT',
            url: `/api/gacha/updateLoot?gacha_id=${id}&gacha_item_id=${gachaitemID}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateMaxValue}>Update</CButton></>
    )
}