import { CButton } from "@coreui/react";
import axios from "axios";

export const PostGacha = ({ startdate, enddate, starttime, endtime, randomvalue, setAddItemModal, setGachaList }) => {

    const convertTime = new Date(startdate + ' ' + starttime)
    const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')
    const convertTimeEnd = new Date(enddate + ' ' + endtime)
    const endingdate = convertTimeEnd.toISOString().split('T').join(' ').replace('.000Z', '')

    const refreshGachaPeriod = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAll`, config).then(res => {
            const items = res.data;
            setGachaList(items);
        });
    }

    const addGachaPeriod = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_date', startingdate)
        data.append('end_date', endingdate)
        data.append('random_value', parseInt(randomvalue))

        const config = {
            method: 'POST',
            url: `/api/gacha/add`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Gacha Period Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add!')
            });
        setTimeout(refreshGachaPeriod, 100)

    }

    return (
        <><CButton color='primary' size='md' onClick={addGachaPeriod}>Add Gacha Period</CButton></>
    )
}

export const PostGachaFeatured = ({ gacha_id, gacha_item_id, priority, addItemModal, setAddItemModal, setGachaFeaturedList }) => {

    const refreshGachaFeatured = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllFeatured`, config).then(res => {
            const items = res.data;
            setGachaFeaturedList(items);
        });
    }

    const addGachaFeatured = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('gacha_id', parseInt(gacha_id))
        data.append('gacha_item_id', parseInt(gacha_item_id))
        data.append('priority', parseInt(priority))

        const config = {
            method: 'POST',
            url: `/api/gacha/addFeatured`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Gacha Added to Featured!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add!')
            });
        setTimeout(refreshGachaFeatured, 100)

    }

    return (
        <><CButton color='primary' size='md' onClick={addGachaFeatured}>Add Gacha to Featured</CButton></>
    )
}

export const PostItemstoGacha = ({ itemtype, itemID, amount, addItemModal, setAddItemModal, setGachaItems }) => {

    const refreshGachaItems = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllItem`, config).then(res => {
            const items = res.data;
            setGachaItems(items);
        });
    }

    const addGachaItems = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('item_type', parseInt(itemtype))
        data.append('item_id', parseInt(itemID))
        data.append('amount', parseInt(amount))

        const config = {
            method: 'POST',
            url: `/api/gacha/addItem`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Items Added to Gacha!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add!')
            });
        setTimeout(refreshGachaItems, 100)

    }

    return (
        <><CButton color='primary' size='md' onClick={addGachaItems}>Add Items to Gacha</CButton></>
    )
}

export const PostGachaLoot = ({ gacha_id, gacha_item_id, chance, min_value, max_value, setAddItemModal, setGachaLoot }) => {

    const refreshGachaLoot = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllLoot`, config).then(res => {
            const items = res.data;
            setGachaLoot(items);
        });
    }

    const addGachaLoot = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('gacha_id', parseInt(gacha_id))
        data.append('gacha_item_id', parseInt(gacha_item_id))
        data.append('chance', parseInt(chance))
        data.append('min_value', parseInt(min_value))
        data.append('max_value', parseInt(max_value))

        const config = {
            method: 'POST',
            url: `/api/gacha/addLoot`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Gacha Added to Loot!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add!')
            });
        setTimeout(refreshGachaLoot, 100)
    }

    return (
        <><CButton color='primary' size='md' onClick={addGachaLoot}>Add Gacha to Loot</CButton></>
    )
}