import { CButton } from "@coreui/react";
import axios from "axios";

export const DeleteGacha = ({ setGachaList, setDeleteItemModal, id }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAll`, config).then(res => {
            const items = res.data;
            setGachaList(items);
        });
    }

    const deleteItem = () => {

        const config = {
            method: 'delete',
            url: `/api/gacha/delete?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth')
            }
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Removed!');
                setDeleteItemModal(false)
            })
            .catch((error) => {
                alert('Failed!');
            });
        setTimeout(refreshList, 100);
    };
    return (
        <><CButton type="send" size="sm" color="danger" onClick={deleteItem} ><b>Delete</b></CButton></>
    )
}

export const DeleteGachaFeatured = ({ setGachaFeaturedList, setDeleteItemModal, id }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllFeatured`, config).then(res => {
            const items = res.data;
            setGachaFeaturedList(items);
        });
    }

    const deleteItemFeatured = () => {

        const config = {
            method: 'delete',
            url: `/api/gacha/deleteFeatured?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth')
            }
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Removed!');
                setDeleteItemModal(false)
            })
            .catch((error) => {
                alert('Failed!');
            });
        setTimeout(refreshList, 100);
    };
    return (
        <><CButton type="send" size="sm" color="danger" onClick={deleteItemFeatured} ><b>Delete</b></CButton></>
    )
}

export const DeleteGachaItem = ({ setGachaItems, setDeleteItemModal, id }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllItem`, config).then(res => {
            const items = res.data;
            setGachaItems(items);
        });
    }

    const deleteItem = () => {

        const config = {
            method: 'delete',
            url: `/api/gacha/deleteItem?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth')
            }
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Removed!');
                setDeleteItemModal(false)
            })
            .catch((error) => {
                alert('Failed!');
            });
        setTimeout(refreshList, 100);
    };
    return (
        <><CButton type="send" size="sm" color="danger" onClick={deleteItem} ><b>Delete</b></CButton></>
    )
}

export const DeleteGachaLoot = ({ setGachaLoot, setDeleteItemModal, id, gachaitemID }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/gacha/getAllLoot`, config).then(res => {
            const items = res.data;
            setGachaLoot(items);
        });
    }

    const deleteLoot = () => {

        const config = {
            method: 'delete',
            url: `/api/gacha/deleteLoot?gacha_id=${id}&gacha_item_id=${gachaitemID}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth')
            }
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Removed!');
                setDeleteItemModal(false)
            })
            .catch((error) => {
                alert('Failed!');
            });
        setTimeout(refreshList, 100);
    };
    return (
        <><CButton type="send" size="sm" color="danger" onClick={deleteLoot} ><b>Delete</b></CButton></>
    )
}