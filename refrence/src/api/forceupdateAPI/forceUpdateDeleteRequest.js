import axios from "axios";

export const DeleteForceUpdateItem = ({ setForceUpdateList, setDeleteItemModal, id}) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/version/getVersionList`, config).then(res => {
            const items = res.data;
            setForceUpdateList(items);
        });
    }

    const deleteItem = () => {

        const config = {
            method: 'delete',
            url: `/api/version/deleteVersion?version_id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth')
            }
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Removed!');
                setDeleteItemModal(false)
            })
            .catch((error) => {
                alert('Failed!');
            });
        setTimeout(refreshList, 100);
    };
    return (
        <><button className="btn btn-sm btn-danger" onClick={deleteItem} ><b>Delete</b></button></>
    )
}