import axios from 'axios'

export const AddForceUpdate = ({setAddItemModal, addForceUpdateForm, setForceUpdateList }) => {

    const convertTime = new Date(addForceUpdateForm.CreateDate + ' ' + addForceUpdateForm.CreateTime)
    const CreateDate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event/getVersionList`, config).then(res => {
            const items = res.data;
            setForceUpdateList(items);
        });
    }

    const addVersion = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('version_string', addForceUpdateForm.VersionString)
        data.append('code_version', parseInt(addForceUpdateForm.CodeVersion))
        data.append('create_time', CreateDate)
        data.append('platform', addForceUpdateForm.Platform)


        const config = {
            method: 'POST',
            url: `/api/version/addVersionUpdate `,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Force Update Version Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add Version!')
            });
        setTimeout(refreshList, 100)

    }

    return (
        <><button className="btn btn-primary btn-md" onClick={addVersion}>+ Add</button></>
    )
}