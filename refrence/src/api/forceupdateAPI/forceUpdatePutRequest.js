import axios from "axios";

export const UpdateForceUpdateCreateDate = ({setForceUpdateList, updateForceForm,
    id
}) => {
    const convertTime = new Date(updateForceForm.CreateDate + " " + updateForceForm.CreateTime);
    const createDate = convertTime
        .toISOString()
        .split("T")
        .join(" ")
        .replace(".000Z", "");

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("auth"),
            },
        };
        axios.get(`/api/version/getVersionList`, config).then((res) => {
            const items = res.data;
            setForceUpdateList(items);
        });
    };

    const updateCreateDate = (e) => {
        e.preventDefault();
        const FormData = require("form-data");
        const data = new FormData();
        data.append("create_time", createDate);

        const config = {
            method: "PUT",
            url: `/api/version/updateVersionCreateTime?version_id=${id}`,
            headers: {
                Authorization: "Bearer " + localStorage.getItem("auth"),
            },
            data: data,
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert("Item Updated!");
            })
            .catch((error) => {
                console.log(error);
                alert("Failed!");
            });
        setTimeout(refreshList, 100);
    };

    return (
        <>
            <button className="btn btn-info" onClick={updateCreateDate}>
                Update
            </button>
        </>
    );
};

export const UpdateForceUpdateVersionString = ({setForceUpdateList, updateForceForm,
    id
}) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("auth"),
            },
        };
        axios.get(`/api/version/getVersionList`, config).then((res) => {
            const items = res.data;
            setForceUpdateList(items);
        });
    };

    const updateVersionString = (e) => {
        e.preventDefault();
        const FormData = require("form-data");
        const data = new FormData();
        data.append("version_string",updateForceForm.VersionString);

        const config = {
            method: "PUT",
            url: `/api/version/updateVersionString?version_id=${id}`,
            headers: {
                Authorization: "Bearer " + localStorage.getItem("auth"),
            },
            data: data,
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert("Item Updated!");
            })
            .catch((error) => {
                console.log(error);
                alert("Failed!");
            });
        setTimeout(refreshList, 100);
    };

    return (
        <>
            <button className="btn btn-info" onClick={updateVersionString}>
                Update
            </button>
        </>
    );
};

export const UpdateForceUpdateCodeVersion = ({setForceUpdateList, updateForceForm,
    id
}) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("auth"),
            },
        };
        axios.get(`/api/version/getVersionList`, config).then((res) => {
            const items = res.data;
            setForceUpdateList(items);
        });
    };

    const updateCodeVersion = (e) => {
        e.preventDefault();
        const FormData = require("form-data");
        const data = new FormData();
        data.append("code_version", parseInt(updateForceForm.CodeVersion));

        const config = {
            method: "PUT",
            url: `/api/version/updateVersionCodeVersion?version_id=${id}`,
            headers: {
                Authorization: "Bearer " + localStorage.getItem("auth"),
            },
            data: data,
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert("Item Updated!");
            })
            .catch((error) => {
                console.log(error);
                alert("Failed!");
            });
        setTimeout(refreshList, 100);
    };

    return (
        <>
            <button className="btn btn-info" onClick={updateCodeVersion}>
                Update
            </button>
        </>
    );
};

export const UpdateForceUpdatePlatform = ({setForceUpdateList, updateForceForm,
    id
}) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("auth"),
            },
        };
        axios.get(`/api/version/getVersionList`, config).then((res) => {
            const items = res.data;
            setForceUpdateList(items);
        });
    };

    const updatePlatform = (e) => {
        e.preventDefault();
        const FormData = require("form-data");
        const data = new FormData();
        data.append("platform", updateForceForm.Platform);

        const config = {
            method: "PUT",
            url: `/api/version/updateVersionPlatform?version_id=${id}`,
            headers: {
                Authorization: "Bearer " + localStorage.getItem("auth"),
            },
            data: data,
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert("Item Updated!");
            })
            .catch((error) => {
                console.log(error);
                alert("Failed!");
            });
        setTimeout(refreshList, 100);
    };

    return (
        <>
            <button className="btn btn-info" onClick={updatePlatform}>
                Update
            </button>
        </>
    );
};