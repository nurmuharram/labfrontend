import { useEffect } from "react";
import axios from "axios";

export const GetForceUpdateList = ({ setForceUpdateList }) => {
  useEffect(() => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/version/getVersionList`, config).then((res) => {
      const items = res.data;
      setForceUpdateList(items);
    });
  }, []);
};
