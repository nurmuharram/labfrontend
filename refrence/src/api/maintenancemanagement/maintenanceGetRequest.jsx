import { useEffect } from "react";
import axios from "axios";

export const GetMaintenanceList = ({ setMaintenanceList }) => {
  useEffect(() => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/maintenance/getAllMaintenance`, config).then((res) => {
      const items = res.data;
      setMaintenanceList(items);
    });
  }, []);
};
