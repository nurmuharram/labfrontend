import axios from "axios";

export const UpdateMaintenanceStartdate = ({ setMaintenanceList, id, end_date, newstartdate, newstarttime, _reason }) => {

    const convertTime = new Date(newstartdate + ' ' + newstarttime)
    const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/maintenance/getAllMaintenance`, config).then(res => {
            const items = res.data;
            setMaintenanceList(items);
        });
    }

    const updateStartDate = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_date', startingdate);
        data.append('end_date', end_date);
        data.append('reason', _reason);

        const config = {
            method: 'PUT',
            url: `/api/maintenance/updateStart?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><button className="btn btn-md btn-primary" onClick={updateStartDate}>Update</button></>
    )
}

export const UpdateMaintenanceEnddate = ({ setMaintenanceList, start_date, newenddate, newendtime, id, _reason }) => {

    const convertTimeEnd = new Date(newenddate + ' ' + newendtime)
    const endingdate = convertTimeEnd.toISOString().split('T').join(' ').replace('.000Z', '')


    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/maintenance/getAllMaintenance`, config).then(res => {
            const items = res.data;
            setMaintenanceList(items);
        });
    }

    const updateEndDate = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_date', start_date);
        data.append('end_date', endingdate);
        data.append('reason', _reason);

        const config = {
            method: 'PUT',
            url: `/api/maintenance/updateEnd?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><button className="btn btn-md btn-primary" onClick={updateEndDate}>Update</button></>
    )
}

export const UpdateReason = ({ setMaintenanceList, start_date, end_date, newreason, id }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/maintenance/getAllMaintenance`, config).then(res => {
            const items = res.data;
            setMaintenanceList(items);
        });
    }

    const updateReason = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_date', start_date);
        data.append('end_date', end_date);
        data.append('reason', newreason);

        const config = {
            method: 'PUT',
            url: `/api/maintenance/updateReason?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Reason Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><button className="btn btn-md btn-primary" onClick={updateReason}>Update</button></>
    )
}