import { CButton } from "@coreui/react";
import axios from "axios";

export const PostMaintenance = ({ startdate, enddate, starttime, endtime, reason, setAddItemModal, setMaintenanceList }) => {

    const convertTime = new Date(startdate + ' ' + starttime)
    const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')
    const convertTimeEnd = new Date(enddate + ' ' + endtime)
    const endingdate = convertTimeEnd.toISOString().split('T').join(' ').replace('.000Z', '')

    const refreshMaintenancePeriod = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/maintenance/getAllMaintenance`, config).then(res => {
            const items = res.data;
            setMaintenanceList(items);
        });
    }


    const addMaintenancePeriod = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_date', startingdate)
        data.append('end_date', endingdate)
        data.append('reason', reason)

        const config = {
            method: 'POST',
            url: `/api/maintenance/addMaintenance`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Maintenance Period Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add!')
            });
        setTimeout(refreshMaintenancePeriod, 100)

    }

    return (
        <><CButton color='primary' size='md' onClick={addMaintenancePeriod}>Add Maintenance Period</CButton></>
    )
}
