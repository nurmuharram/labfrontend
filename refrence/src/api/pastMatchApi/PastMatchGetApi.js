import React,{useEffect} from 'react'

import axios from 'axios'

export const GetPastMatchList = ({match,setMatchData}) => {
  useEffect(() => {
    const config = {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
        }
    }
    axios.get(`/api/stats/getRoomList?room_id?&offset=0&count=1000`, config).then(res => {
        const matchData = res.data;
        setMatchData(matchData);
    });
}, []);
}



export const SearchRoomData = ({ inputChange, setMatchData }) => {  
  const searchRoom = ()=>{
    const config = {
      headers: {
          Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
  }
  axios.get(`/api/stats/getRoomList?room_id=${inputChange.current}?&offset=0&count=10`, config).then(res => {
      const matchData = res.data;
      setMatchData(matchData);
    });   
  }
  return searchRoom
  }

  
  export const RefreshData = ({ setMatchData }) => {  
    const refreshRoom = ()=>{
      const config = {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
        }
    }
    axios.get(`/api/stats/getRoomList?room_id?&offset=0&count=10000`, config).then(res => {
      const matchData = res.data;
      setMatchData(matchData);
      });   
    }
    return refreshRoom
    }
  