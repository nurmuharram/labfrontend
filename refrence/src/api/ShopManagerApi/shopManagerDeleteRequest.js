import axios from "axios";

export const DeleteItemShop = ({ setDeleteItemModal, item, setItems}) => {

    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllItems`, config).then(res => {
            const items = res.data;
            setItems(items);
        });
    }

    const deleteItem = () => {
        const config = {
          method: "delete",
          url: `/api/shop/deleteItem?id=${item.shop_id}`,
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
        };

        axios(config)
          .then((response) => {
            console.log(JSON.stringify(response.data));
            alert("Item Removed!");
            setDeleteItemModal(false);
          })
          .catch((error) => {
            alert("Failed to delete item!");
          });
        setTimeout(refreshData, 100);
      };
    return (
        <><button className="btn btn-sm btn-danger" onClick={deleteItem} ><b>Delete</b></button></>
    )
}