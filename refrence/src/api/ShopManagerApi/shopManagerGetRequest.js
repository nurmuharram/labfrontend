import axios from "axios";
import { useEffect } from "react";

export const GetAllShopItems = ({changeItemTypeList, setItems}) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllItems?item_type=${changeItemTypeList}`, config).then(res => {
            const items = res.data;
            setItems(items);
        });
    }, [changeItemTypeList])
}
