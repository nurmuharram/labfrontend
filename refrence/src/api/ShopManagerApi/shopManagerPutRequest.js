import axios from "axios";

export const UpdatePriceCoin = ({
    item, setItems, skipPriceCitrineIfNull, skipPriceLotusIfNull, newPriceCoin
  }) => {
    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllItems`, config).then(res => {
            const items = res.data;
            setItems(items);
        });
    }
  
    /* UPDATE Price Coin - PUT Request */
    const updatePriceCoin = async (e) => {
        e.preventDefault()
        const FormData = require("form-data");
        const data = new FormData();
        data.append("amount", item.amount);
        data.append("price_coin", newPriceCoin);
        data.append("price_citrine", skipPriceCitrineIfNull());
        data.append("price_lotus", skipPriceLotusIfNull());

        const config = {
          method: "PUT",
          url: `/api/shop/updateItemPrice?id=${item.shop_id}`,
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
          data: data,
        };

        await axios(config)
          .then((response) => {
            console.log(JSON.stringify(response.data));
            alert("Item Updated!");
          })
          .catch((error) => {
            console.log(error);
            alert("Failed to Update!");
          });
        setTimeout(refreshData, 100);
      };
  
    return (
      <>
        <button className="btn btn-md btn-info" onClick={updatePriceCoin}>
          Update
        </button>
      </>
    );
  };

  export const UpdatePriceCitrine = ({
    item, setItems, skipPriceCoinIfNull, skipPriceLotusIfNull, newPriceCitrine
  }) => {
    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllItems`, config).then(res => {
            const items = res.data;
            setItems(items);
        });
    }
  
    /* UPDATE Price Citrine - PUT Request */
    const updatePriceCitrine = async (e) => {
        e.preventDefault()
        const FormData = require("form-data");
        const data = new FormData();
        data.append("amount", item.amount);
        data.append("price_coin", skipPriceCoinIfNull());
        data.append("price_citrine", newPriceCitrine);
        data.append("price_lotus", skipPriceLotusIfNull());

        const config = {
          method: "PUT",
          url: `/api/shop/updateItemPrice?id=${item.shop_id}`,
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
          data: data,
        };
        await axios(config)
          .then((response) => {
            console.log(JSON.stringify(response.data));
            alert("Item Updated!");
          })
          .catch((error) => {
            console.log(error);
            alert("Failed to Update!");
          });
        setTimeout(refreshData, 100);
      };

    return (
      <>
        <button className="btn btn-md btn-info" onClick={updatePriceCitrine}>
          Update
        </button>
      </>
    );
  };

  export const UpdatePriceLotus = ({
    item, setItems, skipPriceCoinIfNull, skipPriceCitrineIfNull, newPriceLotus
  }) => {
    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllItems`, config).then(res => {
            const items = res.data;
            setItems(items);
        });
    }
  
    /* UPDATE Price Lotus - PUT Request */
    const updatePriceLotus = async (e) => {
        e.preventDefault()
        const FormData = require("form-data");
        const data = new FormData();
        data.append("amount", item.amount);
        data.append("price_coin", skipPriceCoinIfNull());
        data.append("price_citrine", skipPriceCitrineIfNull());
        data.append("price_lotus", newPriceLotus);

        const config = {
          method: "PUT",
          url: `/api/shop/updateItemPrice?id=${item.shop_id}`,
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
          data: data,
        };

        await axios(config)
          .then((response) => {
            console.log(JSON.stringify(response.data));
            alert("Item Updated!");
          })
          .catch((error) => {
            console.log(error);
            alert("Failed to Update!");
          });
        setTimeout(refreshData, 100);
      };

 
    return (
      <>
        <button className="btn btn-md btn-info" onClick={updatePriceLotus}>
          Update
        </button>
      </>
    );
  };

  export const UpdateAmount = ({
    item, setItems, skipPriceCoinIfNull, skipPriceCitrineIfNull, newAmount, skipPriceLotusIfNull
  }) => {
    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllItems`, config).then(res => {
            const items = res.data;
            setItems(items);
        });
    }
  
    /* UPDATE Amount - PUT Request */
    const updateAmount = async (e) => {
        e.preventDefault()
        const FormData = require("form-data");
        const data = new FormData();
        data.append("amount", newAmount);
        data.append("price_coin", skipPriceCoinIfNull());
        data.append("price_citrine", skipPriceCitrineIfNull());
        data.append("price_lotus", skipPriceLotusIfNull());

        const config = {
          method: "PUT",
          url: `/api/shop/updateItemPrice?id=${item.shop_id}`,
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
          data: data,
        };

        await axios(config)
          .then((response) => {
            console.log(JSON.stringify(response.data));
            alert("Item Updated!");
          })
          .catch((error) => {
            console.log(error);
            alert("Failed to Update!");
          });
        setTimeout(refreshData, 100);
      };

 
    return (
      <>
        <button className="btn btn-md btn-info" onClick={updateAmount}>
          Update
        </button>
      </>
    );
  };

  export const UpdateDescription = ({
    item, setItems, newDescription
  }) => {
    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllItems`, config).then(res => {
            const items = res.data;
            setItems(items);
        });
    }
  
    /* UPDATE Description - PUT Request */
    const updateDescription = async (e) => {
        e.preventDefault()
        const FormData = require("form-data");
        const data = new FormData();
        data.append("description", newDescription);

        const config = {
          method: "PUT",
          url: `/api/shop/updateItemDesc?id=${item.shop_id}`,
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
          data: data,
        };

        await axios(config)
          .then((response) => {
            console.log(JSON.stringify(response.data));
            alert("Item Updated!");
          })
          .catch((error) => {
            console.log(error);
            alert("Failed to Update!");
          });
        setTimeout(refreshData, 100);
      };
  
    return (
      <>
        <button className="btn btn-md btn-info" onClick={updateDescription}>
          Update
        </button>
      </>
    );
  };

  export const UpdateReleaseDate = ({
    item, setItems, newReleaseDate, newReleaseTime
  }) => {
    const dateToConvert = new Date(newReleaseDate + ' ' + newReleaseTime)
        const convertedDateToUtc = dateToConvert.toISOString().split('T').join(' ').replace('.000Z', '')
    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllItems`, config).then(res => {
            const items = res.data;
            setItems(items);
        });
    }
  
    /* UPDATE Description - PUT Request */
    const updateReleaseDate = async (e) => {
        e.preventDefault()
        const FormData = require("form-data");
        const data = new FormData();
        data.append("release_date", convertedDateToUtc);

        const config = {
          method: "PUT",
          url: `/api/shop/updateItemReleaseDate?id=${item.shop_id}`,
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
          data: data,
        };

        await axios(config)
          .then((response) => {
            console.log(JSON.stringify(response.data));
            alert("Item Updated!");
          })
          .catch((error) => {
            console.log(error);
            alert("Failed to Update!");
          });
        setTimeout(refreshData, 100);
      };
  
    return (
      <>
        <button className="btn btn-md btn-info" onClick={updateReleaseDate}>
          Update
        </button>
      </>
    );
  };