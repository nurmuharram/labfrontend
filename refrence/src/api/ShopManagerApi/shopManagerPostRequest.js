import axios from 'axios';
import React from 'react'

export const AddShopItem = () => {

    const addShopItem = (e, itemId, itemType, amount, priceCoin, priceLotus, priceCitrine, releaseDate, releaseTime, description, setAddItemModal, setItems) => {

        const dateToConvert = new Date(releaseDate + ' ' + releaseTime)
        const convertedDateToUtc = dateToConvert.toISOString().split('T').join(' ').replace('.000Z', '')

        const refreshData = () => {
            const config = {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('auth'),
                }
            }
            axios.get(`/api/shop/getAllItems`, config).then(res => {
                const items = res.data;
                setItems(items);
            });
        }
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('item_id', itemId)
        data.append('item_type', itemType)
        data.append('amount', amount)
        data.append('price_coin', priceCoin)
        data.append('price_citrine', priceCitrine)
        data.append('price_lotus', priceLotus)
        data.append('release_date', convertedDateToUtc)
        data.append('description', description)

        const config = {
            method: 'POST',
            url: `/api/shop/addItem`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert("Item Added!")
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to add item!')
            });
        setTimeout(refreshData, 100)
    };
    return (
        <><button className="btn btn-primary btn-md" onClick={addShopItem}>+ Add Item</button></>
    )
}
