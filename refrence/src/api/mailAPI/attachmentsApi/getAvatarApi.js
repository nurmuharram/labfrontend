const url = "/api/icon/avatar/getIconAvatars";
const header = {
            headers: {
                method: "GET",
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
                 /* "Access-Control-Allow-Origin": "*", */
               
            }
        }

const fetchGetAvatars = async () => {
  return await fetch(url, header)
    .then((response) => response.json())
    .catch((error) => {
      throw error;
    });
};

export default fetchGetAvatars;