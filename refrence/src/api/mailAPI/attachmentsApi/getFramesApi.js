const url = "/api/icon/frame/getIconFrames";
const header = {
            headers: {
                method: "GET",
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
                 /* "Access-Control-Allow-Origin": "*", */
               
            }
        }

const fetchGetFrames = async () => {
  return await fetch(url, header)
    .then((response) => response.json())
    .catch((error) => {
      throw error;
    });
};

export default fetchGetFrames;