import axios from "axios";
import { useEffect } from "react";

export const GetMailAttachments = ({ setMailAttachments }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get("api/mail/getAttachmentByTemplateId", config).then(res => {
            const items = res.data;
            setMailAttachments(items);
        });
    }, [])
}

export const GetMailCustomMessage = ({ setCustomMessages }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get("api/mail/getCustoms", config).then(res => {
            const items = res.data;
            setCustomMessages(items);
        });
    }, [])
}

export const GetLoginMail = ({ setLoginMails }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get("api/mail/getAllLoginMail", config).then(res => {
            const items = res.data;
            setLoginMails(items);
        });
    }, [])
}

export const GetMails = ({ setMails }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get("api/mail/get", config).then(res => {
            const items = res.data;
            setMails(items);
        });
    }, [])
}

export const GetMailTemplates = ({ setMailTemplates }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get("api/mail/getTemplates", config).then(res => {
            const items = res.data;
            setMailTemplates(items);
        });
    }, [])
}

export const GetPlayers = ({ setPlayers }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get("api/players/getPlayers", config).then(res => {
            const items = res.data;
            setPlayers(items);
        });
    }, [])
}