const url = "/api/mail/getAllLoginMail";
const header = {
            headers: {
                method: "GET",
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
                 /* "Access-Control-Allow-Origin": "*", */
               
            }
        }

const fetchGetLoginMail = async () => {
  return await fetch(url, header)
    .then((response) => response.json())
    .catch((error) => {
      throw error;
    });
};

export default fetchGetLoginMail;
