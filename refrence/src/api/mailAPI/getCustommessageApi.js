const url = "api/mail/getCustoms";
const header = {
            headers: {
                method: "GET",
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
                 /* "Access-Control-Allow-Origin": "*", */
               
            }
        }

const fetchGetCustoms = async () => {
  return await fetch(url, header)
    .then((response) => response.json())
    .catch((error) => {
      throw error;
    });
};

export default fetchGetCustoms;