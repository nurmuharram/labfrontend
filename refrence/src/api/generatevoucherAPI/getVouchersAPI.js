const url = "/api/voucher/getVouchers";
const header = {
            headers: {
                method: "GET",
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
                 /* "Access-Control-Allow-Origin": "*", */
               
            }
        }

const fetchGetVouchers = async () => {
  try {
    const response = await fetch(url, header);
    return await response.json();
  } catch (error) {
    throw error;
  }
};

export default fetchGetVouchers;
