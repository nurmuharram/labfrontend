import { useEffect } from "react"
import axios from "axios"

export const GetVoucherList = ({ setVouchers }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/voucher/queryVoucherExport?count=5000&offset=0`, config).then(res => {
            const items = res.data;
            setVouchers(items);
        });
    }, [])
}

export const GetVouchers = ({ setListVouchers,  countVoucher, offsetVoucher }) => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/voucher/queryVoucherExport?count=${countVoucher}&offset=${offsetVoucher}`, config).then(res => {
            const items = res.data;
            setListVouchers(items);
        });
}

export const GetVoucher1000List = ({ setOneThousandListVoucher }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/voucher/queryVoucherExport?count=1000&offset=0`, config).then(res => {
            const items = res.data;
            setOneThousandListVoucher(items);
        });
    }, [])
}

export const GetVoucherOne = ({ setVoucherOne }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/voucher/getAllVoucherOne`, config).then(res => {
            const items = res.data;
            setVoucherOne(items);
        });
    }, [])
}