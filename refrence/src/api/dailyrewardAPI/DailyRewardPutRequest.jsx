import axios from "axios";

export const UpdateReward = ({
  setDailyRewardList,
  newitemtype,
  setNewitemtype,
  newitemid,
  setNewitemid,
  newamount,
  setNewamount,
  id,
  _day,
}) => {
  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/dailyreward//getAllDailyReward`, config).then((res) => {
      const items = res.data;
      setDailyRewardList(items);
    });
  };

  const updateReward = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();
    data.append("item_type", parseInt(newitemtype));
    data.append("item_id", parseInt(newitemid));
    data.append("amount", parseInt(newamount));

    const config = {
      method: "PUT",
      url: `/api/dailyreward/updateDailyRewardItem?daily_id=${id}&day=${_day}`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Item Updated!");
      })
      .catch((error) => {
        console.log(error);
        alert("Failed!");
      });
    setTimeout(refreshList, 100);
  };

  return (
    <>
      <button className="btn btn-md btn-info" onClick={updateReward}>
        Update
      </button>
    </>
  );
};
