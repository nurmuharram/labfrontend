import { useEffect } from "react";
import axios from "axios";

export const GetDailyRewardList = ({ setDailyRewardList }) => {
  useEffect(() => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/dailyreward//getAllDailyReward`, config).then((res) => {
      const items = res.data;
      setDailyRewardList(items);
    });
  }, []);
};

export const GetDailyMonthYear = ({ setMonthYear }) => {
  useEffect(() => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/dailyreward/getAllYearMonth`, config).then((res) => {
      const items = res.data;
      setMonthYear(items);
    });
  }, []);
};