import axios from "axios";

export const DeleteReward = ({ setDailyRewardList, setDeleteItemModal, id, _day}) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/dailyreward//getAllDailyReward`, config).then(res => {
            const items = res.data;
            setDailyRewardList(items);
        });
    }

    const deleteItem = () => {

        const config = {
            method: 'delete',
            url: `/api/dailyreward/deleteDailyReward?daily_id=${id}&day=${_day}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth')
            }
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Removed!');
                setDeleteItemModal(false)
            })
            .catch((error) => {
                alert('Failed!');
            });
        setTimeout(refreshList, 100);
    };
    return (
        <><button className="btn btn-sm btn-danger" onClick={deleteItem} ><b>Delete</b></button></>
    )
}