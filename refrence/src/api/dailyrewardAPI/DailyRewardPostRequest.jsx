import axios from "axios";

export const AddDailyRewards = ({
  dailyRewardLoot,
  dailyid,
  setDailyRewardList,
  setAddItemModal,
}) => {
  const refreshRewardList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/dailyreward//getAllDailyReward`, config).then((res) => {
      const items = res.data;
      setDailyRewardList(items);
    });
  };

  const lootTable = {
    loot_tables: dailyRewardLoot,
  };

  const addRewards = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();
    data.append("daily_id", parseInt(dailyid));
    data.append("daily_loot", lootTable);

    const config = {
      method: "POST",
      url: `/api/dailyreward/AddDailyReward`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Rewards Added!");
        setAddItemModal(false);
      })
      .catch((error) => {
        console.log(error);
        alert("Failed to Add Rewards!");
      });
    setTimeout(refreshRewardList, 100);
  };

  return (
    <>
      <button className="btn btn-md btn-primary" onClick={addRewards}>
        Add Rewards
      </button>
    </>
  );
};
