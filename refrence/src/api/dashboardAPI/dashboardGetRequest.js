import { useEffect } from "react"
import axios from "axios"

export const GetKsaStatCount1 = ({ setCountsClassic, setGm1isLoading }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getKsaStatCount?game_mode=1`, config).then(res => {
            const count = res.data;
            setCountsClassic(count);
            setGm1isLoading(false);
        });
    }, [])
}

export const GetKsaStatCount2 = ({ setCountsRankedBlind }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getKsaStatCount?game_mode=2`, config).then(res => {
            const count = res.data;
            setCountsRankedBlind(count);
        });
    }, [])
}

export const GetKsaStatCount3 = ({ setCountsRankedDraft }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getKsaStatCount?game_mode=3`, config).then(res => {
            const count = res.data;
            setCountsRankedDraft(count);
        });
    }, [])
}

export const GetKsaTotalOwned = ({ setMostPurchase, setMostPurchaseisLoading }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getKsaTotalOwned`, config).then(res => {
            const count = res.data;
            setMostPurchase(count);
            setMostPurchaseisLoading(false);
        });
    }, [])
}

export const GetKsaTotalKda1 = ({ setKdaClassic, setKdaIsLoading }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getKsaTotalKda?game_mode=0`, config).then(res => {
            const count = res.data;
            setKdaClassic(count);
            setKdaIsLoading(false);
        });
    }, [])
}

export const GetKsaTotalKda2 = ({ setKdaRankedBlind }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getKsaTotalKda?game_mode=1`, config).then(res => {
            const count = res.data;
            setKdaRankedBlind(count);
        });
    }, [])
}

export const GetKsaTotalKda3 = ({ setKdaRankedDraft }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getKsaTotalKda?game_mode=2`, config).then(res => {
            const count = res.data;
            setKdaRankedDraft(count);
        });
    }, [])
}

export const refreshActiveChart = ({ setDailyUsers, setIsLoading, dateRangePick }) => {
    const refresh = () => {
        setIsLoading(true);
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getDailyUserUnique?start_date=${dateRangePick?.start?.format('YYYY-MM-DD')}&end_date=${dateRangePick?.end?.format('YYYY-MM-DD')}`, config).then(res => {
            const count = res.data;
            setDailyUsers(count);
            setIsLoading(false);
        });
    }
    return refresh
}

export const GetDailyUsers = ({ setDailyUsers, setIsLoading, year, month, day }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getDailyUserUnique?start_date=${year}-${month}-01&end_date=${year}-${month}-${day}`, config).then(res => {
            const count = res.data;
            setDailyUsers(count);
            setIsLoading(false);
        });
    }, [])
}

export const GetConcurrentUser = ({ setConcurrentUsers, setLoading, year, month, day }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getConcurrentUser?start_date=${year}-${month}-${day}%2000:00&end_date=${year}-${month}-${day}%2023:59`, config).then(res => {
            const count = res.data;
            setConcurrentUsers(count);
            setLoading(false)
        });
    }, [])
}

export const refreshConcurrentUser = ({ setConcurrentUsers, setLoading, dateRangePick }) => {
    const refresh = () => {
        setLoading(true);
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getConcurrentUser?start_date=${dateRangePick?.start?.format('YYYY-MM-DD')}%20${dateRangePick?.start?.format('HH:mm')}&end_date=${dateRangePick?.end?.format('YYYY-MM-DD')}%20${dateRangePick?.end?.format('HH:mm')}`, config).then(res => {
            const count = res.data;
            setConcurrentUsers(count);
            setLoading(false);
        });
    }
    return refresh
}

export const GetDailyUser = ({ setDailyUsers, setLoading, year, month, day }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getDailyUser?start_date=${year}-${month}-01&end_date=${year}-${month}-${day}`, config).then(res => {
            const count = res.data;
            setDailyUsers(count);
            setLoading(false);
        });
    }, [])
}

export const refreshDailyUsers = ({ setDailyUsers, setLoading, dateRangePick }) => {
    const refresh = () => {
        setLoading(true);
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getDailyUser?start_date=${dateRangePick?.start?.format('YYYY-MM-DD')}&end_date=${dateRangePick?.end?.format('YYYY-MM-DD')}`, config).then(res => {
            const count = res.data;
            setDailyUsers(count);
            setLoading(false);
        });
    }
    return refresh
}

export const GetRegisteredUsers = ({ setRegisterdUsers, setLoading }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getLoginUserCount`, config).then(res => {
            const count = res.data;
            setRegisterdUsers(count);
            setLoading(false);
        });
    }, [])
}