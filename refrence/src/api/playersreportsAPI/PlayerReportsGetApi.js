import React,{useEffect} from 'react'

import axios from 'axios'
export const GetGuildBlessing = ({setReports}) => {

  useEffect(() => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`api/report/getReports?count=10&offset=0`, config).then(res => {
      const items = res.data;
      setReports(items);
    });
  }, [])

}

export const GetGuildList = ({setGuildData}) => {

    useEffect(() => {
        const config = {
            headers: {
              Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
          }
          axios.get(`/api/guild/getAllGuilds?count=1000&offset=0`, config).then(res => {
            const data = res.data;
            setGuildData(data);
          });
    }, [])
  
  
  }
