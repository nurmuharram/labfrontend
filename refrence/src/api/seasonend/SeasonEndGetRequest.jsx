import { useEffect } from "react"
import axios from "axios"

export const GetSeasonEndList = ({ setSeasonEndList }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/season/getSeasons`, config).then(res => {
            const items = res.data;
            setSeasonEndList(items);
        });
    }, [])
}

export const GetSeasonMailRewards = ({ setSeasonMailReward }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/season/getSeasonMails`, config).then(res => {
            const items = res.data;
            setSeasonMailReward(items);
        });
    }, [])
}

export const GetSeasonRewards = ({ setSeasonReward }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/season/getSeasonRewards`, config).then(res => {
            const items = res.data;
            setSeasonReward(items);
        });
    }, [])
}

export const GetSeasonRankRewards = ({ setSeasonRankReward }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/season/getSeasonRankRewards`, config).then(res => {
            const items = res.data;
            setSeasonRankReward(items);
        });
    }, [])
}

export const GetMails = ({ setMails }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/mail/getTemplates`, config).then(res => {
            const items = res.data;
            setMails(items);
        });
    }, [])
}