import axios from "axios";

export const UpdateSeasonStartdate = ({
  setSeasonEndList,
  newstartdate,
  newstarttime,
  id,
  end_date,
}) => {
  const convertTime = new Date(newstartdate + " " + newstarttime);
  const startingdate = convertTime
    .toISOString()
    .split("T")
    .join(" ")
    .replace(".000Z", "");

  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasons`, config).then((res) => {
      const items = res.data;
      setSeasonEndList(items);
    });
  };

  const updateStartDate = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();
    data.append("start_date", startingdate);
    data.append("end_date", end_date);

    const config = {
      method: "PUT",
      url: `/api/season/updateSeason?season_id=${id}`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Item Updated!");
      })
      .catch((error) => {
        console.log(error);
        alert("Failed!");
      });
    setTimeout(refreshList, 100);
  };

  return (
    <>
      <button className="btn btn-info" onClick={updateStartDate}>
        Update
      </button>
    </>
  );
};

export const UpdateSeasonEnddate = ({
  setSeasonEndList,
  newenddate,
  newendtime,
  id,
  start_date,
}) => {
  const convertTimeEnd = new Date(newenddate + " " + newendtime);
  const endingdate = convertTimeEnd
    .toISOString()
    .split("T")
    .join(" ")
    .replace(".000Z", "");

  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasons`, config).then((res) => {
      const items = res.data;
      setSeasonEndList(items);
    });
  };

  const updateStartDate = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();
    data.append("start_time", start_date);
    data.append("end_time", endingdate);

    const config = {
      method: "PUT",
      url: `/api/season/updateSeason?season_id=${id}`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Item Updated!");
      })
      .catch((error) => {
        console.log(error);
        alert("Failed!");
      });
    setTimeout(refreshList, 100);
  };

  return (
    <>
      <button className="btn btn-info" onClick={updateStartDate}>
        Update
      </button>
    </>
  );
};

export const UpdateSeasonReward = ({
  newitemtype,
  setNewtemtype,
  newitemid,
  setNewitemid,
  newamount,
  setNewamount,
  newseasonid,
  setNeweasonid,
  setSeasonEndList,
  id,
  setSeasonReward,
}) => {
  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasonRewards`, config).then((res) => {
      const items = res.data;
      setSeasonReward(items);
    });
  };

  const updateReward = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();
    data.append("season_id", parseInt(newseasonid));
    data.append("item_id", parseInt(newitemid));
    data.append("item_type", parseInt(newitemtype));
    data.append("amount", parseInt(newamount));

    const config = {
      method: "PUT",
      url: `/api/season/updateSeasonReward?season_reward_id=${id}`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Item Updated!");
      })
      .catch((error) => {
        console.log(error);
        alert("Failed!");
      });
    setTimeout(refreshList, 100);
  };

  return (
    <>
      <button className="btn btn-info" onClick={updateReward}>
        Update
      </button>
    </>
  );
};

export const UpdateSeasonRankReward = ({
  newitemtype,
  setNewitemtype,
  newitemid,
  setNewitemid,
  newamount,
  setNewamount,
  newseasonid,
  setNewesasonid,
  setSeasonEndList,
  id,
  setSeasonRankReward,
  newrank,
}) => {
  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasonRankRewards`, config).then((res) => {
      const items = res.data;
      setSeasonRankReward(items);
    });
  };

  const updateReward = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();
    data.append("season_id", parseInt(newseasonid));
    data.append("rank", parseInt(newrank));
    data.append("item_id", parseInt(newitemid));
    data.append("item_type", parseInt(newitemtype));
    data.append("amount", parseInt(newamount));

    const config = {
      method: "PUT",
      url: `/api/season/updateSeasonRankReward?season_reward_id=${id}`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Item Updated!");
      })
      .catch((error) => {
        console.log(error);
        alert("Failed!");
      });
    setTimeout(refreshList, 100);
  };

  return (
    <>
      <button className="btn btn-info" onClick={updateReward}>
        Update
      </button>
    </>
  );
};

export const UpdateMail = ({
  setItemtype,
  setAmount,
  setItemid,
  amount,
  itemtype,
  itemid,
  seasonEndList,
  setSeasonid,
  setNewseasonid,
  id,
  setSeasonMailReward,
  newseasonid
}) => {
  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasonMails`, config).then((res) => {
      const items = res.data;
      setSeasonMailReward(items);
    });
  };

  const updateReward = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();
    data.append("season_id", parseInt(newseasonid));
    data.append("item_id", parseInt(itemid));
    data.append("item_type", parseInt(itemtype));
    data.append("amount", parseInt(amount));

    const config = {
      method: "PUT",
      url: `/api/season/updateSeasonMail?mail_id=${id}`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Item Updated!");
      })
      .catch((error) => {
        console.log(error);
        alert("Failed!");
      });
    setTimeout(refreshList, 100);
  };

  return (
    <>
      <button className="btn btn-info" onClick={updateReward}>
        Update
      </button>
    </>
  );
};
