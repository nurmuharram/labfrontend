import axios from "axios";

export const DeleteSeasonPeriod = ({
  setSeasonEndList,
  setDeleteItemModal,
  id,
}) => {
  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasons`, config).then((res) => {
      const items = res.data;
      setSeasonEndList(items);
    });
  };

  const deleteItem = () => {
    const config = {
      method: "delete",
      url: `/api/season/deleteSeason?season_id=${id}`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Item Removed!");
        setDeleteItemModal(false);
      })
      .catch((error) => {
        alert("Failed!");
      });
    setTimeout(refreshList, 100);
  };
  return (
    <>
      <button className="btn btn-danger" onClick={deleteItem}>
        <b>Delete</b>
      </button>
    </>
  );
};

export const DeleteSeasonRewardItem = ({
  setSeasonReward,
  setDeleteItemModal,
  id,
}) => {
  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasonRewards`, config).then((res) => {
      const items = res.data;
      setSeasonReward(items);
    });
  };

  const deleteItem = () => {
    const config = {
      method: "delete",
      url: `/api/season/deleteSeasonReward?season_id=${id}`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Item Removed!");
        setDeleteItemModal(false);
      })
      .catch((error) => {
        alert("Failed!");
      });
    setTimeout(refreshList, 100);
  };
  return (
    <>
      <button className="btn btn-danger" onClick={deleteItem}>
        <b>Delete</b>
      </button>
    </>
  );
};

export const DeleteSeasonRankRewardItem = ({
  setSeasonRankReward,
  setDeleteItemModal,
  id,
}) => {
  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasonRankRewards`, config).then((res) => {
      const items = res.data;
      setSeasonRankReward(items);
    });
  };

  const deleteItem = () => {
    const config = {
      method: "delete",
      url: `/api/season/deleteSeasonRankReward?season_rank_id=${id}`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Item Removed!");
        setDeleteItemModal(false);
      })
      .catch((error) => {
        alert("Failed!");
      });
    setTimeout(refreshList, 100);
  };
  return (
    <>
      <button className="btn btn-danger" onClick={deleteItem}>
        <b>Delete</b>
      </button>
    </>
  );
};

export const DeleteSeasonMail = ({
  setSeasonMailReward,
  setDeleteItemModal,
  id,
}) => {
  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasonMails`, config).then((res) => {
      const items = res.data;
      setSeasonMailReward(items);
    });
  };

  const deleteItem = () => {
    const config = {
      method: "delete",
      url: `/api/season/deleteSeasonMail?mail_id=${id}`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Item Removed!");
        setDeleteItemModal(false);
      })
      .catch((error) => {
        alert("Failed!");
      });
    setTimeout(refreshList, 100);
  };
  return (
    <>
      <button className="btn btn-danger" onClick={deleteItem}>
        <b>Delete</b>
      </button>
    </>
  );
};
