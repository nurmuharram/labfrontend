import axios from "axios";

export const AddSeasonPeriod = ({
  startdate,
  enddate,
  starttime,
  endtime,
  setSeasonEndList,
  setAddItemModal,
}) => {
  const convertTime = new Date(startdate + " " + starttime);
  const startingdate = convertTime
    .toISOString()
    .split("T")
    .join(" ")
    .replace(".000Z", "");
  const convertTimeEnd = new Date(enddate + " " + endtime);
  const endingdate = convertTimeEnd
    .toISOString()
    .split("T")
    .join(" ")
    .replace(".000Z", "");

  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasons`, config).then((res) => {
      const items = res.data;
      setSeasonEndList(items);
    });
  };

  const addPeriod = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();
    data.append("start_date", startingdate);
    data.append("end_date", endingdate);

    const config = {
      method: "POST",
      url: `/api/season/addSeason`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Season Period Added!");
        setAddItemModal(false);
      })
      .catch((error) => {
        console.log(error);
        alert("Failed to Add!");
      });
    setTimeout(refreshList, 100);
  };

  return (
    <>
      <button className="btn btn-info" onClick={addPeriod}>
        Add Season Period
      </button>
    </>
  );
};

export const AddSeasonReward = ({
  seasonid,
  amount,
  itemid,
  itemtype,
  setSeasonReward,
  setAddItemModal,
}) => {
  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasonRewards`, config).then((res) => {
      const items = res.data;
      setSeasonReward(items);
    });
  };

  const addSeasonreward = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();
    data.append("season_id", parseInt(seasonid));
    data.append("item_id", parseInt(itemid));
    data.append("item_type", parseInt(itemtype));
    data.append("amount", parseInt(amount));

    const config = {
      method: "POST",
      url: `/api/season/addSeasonReward`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Season Reward Added!");
        setAddItemModal(false);
      })
      .catch((error) => {
        console.log(error);
        alert("Failed to Add!");
      });
    setTimeout(refreshList, 100);
  };

  return (
    <>
      <button className="btn btn-info" onClick={addSeasonreward}>
        Add Season Reward
      </button>
    </>
  );
};

export const AddSeasonRankRewards = ({
  seasonid,
  amount,
  itemid,
  itemtype,
  rank,
  setSeasonRankReward,
  setAddItemModal,
}) => {
  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasonRankRewards`, config).then((res) => {
      const items = res.data;
      setSeasonRankReward(items);
    });
  };

  const addSeasonRankreward = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();
    data.append("season_id", parseInt(seasonid));
    data.append("rank", parseInt(rank));
    data.append("item_id", parseInt(itemid));
    data.append("item_type", parseInt(itemtype));
    data.append("amount", parseInt(amount));

    const config = {
      method: "POST",
      url: `/api/season/addSeasonRankReward`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Season Rank Reward Added!");
        setAddItemModal(false);
      })
      .catch((error) => {
        console.log(error);
        alert("Failed to Add!");
      });
    setTimeout(refreshList, 100);
  };

  return (
    <>
      <button className="btn btn-info" onClick={addSeasonRankreward}>
        Add Season Rank Reward
      </button>
    </>
  );
};

export const SendSeasonMail = ({
  seasonid,
  maildata,
  setSeasonMailReward,
  setAddItemModal,
}) => {
  const refreshList = () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/season/getSeasonMails`, config).then((res) => {
      const items = res.data;
      setSeasonMailReward(items);
    });
  };

  const rankmails = {
    rank_mails: maildata,
  };

  const sendmail = (e) => {
    e.preventDefault();
    const FormData = require("form-data");
    const data = new FormData();
    data.append("season_id", parseInt(seasonid));
    data.append("rank_mails", rankmails);

    const config = {
      method: "POST",
      url: `/api/season/senSeasonMail`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Season Mail Created!");
        setAddItemModal(false);
      })
      .catch((error) => {
        console.log(error);
        alert("Failed to Add!");
      });
    setTimeout(refreshList, 100);
  };

  return (
    <>
      <button className="btn btn-info" onClick={sendmail}>
        Send Season Mail Reward
      </button>
    </>
  );
};
