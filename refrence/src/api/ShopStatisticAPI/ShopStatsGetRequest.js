import {useEffect} from 'react'
import axios from 'axios'

export const GetMostBoughtItem = ({setMostpurchase}) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getMostBoughtItem`, config).then(res => {
            const count = res.data;
            setMostpurchase(count);
        });
    }, [])

}

export const GetTopUpHistory = ({setTopuplist, changelist}) => {

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getTopUpHistory?count=10&offset=${changelist}`, config).then(res => {
            const items = res.data;
            setTopuplist(items);
        });
    }, [changelist])
}