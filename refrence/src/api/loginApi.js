import axios from 'axios';

export const loginApi = (payload)=>{
    const formData = new FormData()
    formData.append ('email',payload.email)
    formData.append ('password',payload.password)
    return axios.post ('/api/login',formData,{ withCredentials: true })
    // return axios.post (process.env.REACT_APP_BE_URL+'/api/login',formData)
}
