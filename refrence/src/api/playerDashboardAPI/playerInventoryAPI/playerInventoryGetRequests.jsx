import { useEffect } from "react"
import axios from "axios"

export const GetInvBoxes = ({ user_id, setBoxes }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/players/getInvBox?user_id=${user_id}`, config).then(res => {
            const items = res.data;
            setBoxes(items);
        });
    }, [])
}

export const GetInvavatars = ({ user_id, setAvatars }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/players/getInvAvatars?user_id=${user_id}`, config).then(res => {
            const items = res.data;
            setAvatars(items);
        });
    }, [])
}

export const GetInvFrames = ({ user_id, setFrames }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/players/getInvFrames?user_id=${user_id}`, config).then(res => {
            const items = res.data;
            setFrames(items);
        });
    }, [])
}

export const GetInvKsatriyas = ({ user_id, setKsa }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/players/getInvKsatriyas?user_id=${user_id}`, config).then(res => {
            const items = res.data;
            setKsa(items);
        });
    }, [])
}

export const GetInvSkinFragments = ({ user_id, setSkinFragments }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/players/getInvSkinFragment?user_id=${user_id}`, config).then(res => {
            const items = res.data;
            setSkinFragments(items);
        });
    }, [])
}

export const GetInvMiscItems = ({ user_id, setMiscItems }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/players/getInvMiscItem?user_id=${user_id}`, config).then(res => {
            const items = res.data;
            setMiscItems(items);
        });
    }, [])
}

export const GetInvRunes = ({ user_id, setRunes }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/players/getInvRunes?user_id=${user_id}`, config).then(res => {
            const items = res.data;
            setRunes(items);
        });
    }, [])
}

export const GetInvVahana = ({ user_id, setVahana }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/players/getInvVahana?user_id=${user_id}`, config).then(res => {
            const items = res.data;
            setVahana(items);
        });
    }, [])
}