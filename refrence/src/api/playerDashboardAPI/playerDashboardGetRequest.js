import { useEffect } from "react"
import axios from "axios"

export const GetUserKsaKdaStats = ({ setKsakdalist, gamemode, id }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/stats/getUserKdaKsaStats?user_id=${id}&game_mode=${gamemode}`, config).then(res => {
            const items = res.data;
            setKsakdalist(items);
        });
    }, [gamemode])
}
