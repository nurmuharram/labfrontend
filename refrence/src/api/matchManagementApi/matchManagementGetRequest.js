import axios from 'axios';
import { useEffect } from 'react';

export const GetMatches = ({setMatches, page}) => {
    useEffect(() => {
        const config = {
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
        };
        axios
          .get(`/api/match/getMatches?count=10&offset=${page.current}`, config)
          .then((res) => {
            const matches = res.data;
            setMatches(matches);
          });
      }, [page.current]);
}

export const refreshMatches = ({setMatches, page}) => {
    const refreshMatchList = () => {
        const config = {
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
        };
        axios
          .get(`/api/match/getMatches?count=10&offset=${page.current}`, config)
          .then((res) => {
            const items = res.data;
            setMatches(items);
          });
      };  
      return refreshMatchList
}

export const KillRoom = ({item}) => {
    const killRoom = (e) => {
        e.preventDefault();
        const config = {
          method: "GET",
          url: `api/match/cancelmatch?room_id=${item.room_id}`,
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
        };

        axios(config)
        .then((response) => {
            console.log(JSON.stringify(response.data));
            alert('Success Kill Room')
        })
        .catch((error) => {
            console.log(error);
            alert('Failed to Kill Room!')
        });
      };
      return (
        <td>
          {item.can_timeout === "1" ? (
            <button onClick={killRoom} className="btn btn-danger">Kill</button>
          ) : (
            <div>
              
            </div>
          )}
        </td>
      );
}