import { CButton } from "@coreui/react";
import axios from "axios";

export const UpdateEventStartdate = ({ setEventList, newstartdate, newstarttime, endtime, _expireddate, id }) => {

    const convertTime = new Date(newstartdate + ' ' + newstarttime)
    const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')


    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event/getAllEvent`, config).then(res => {
            const items = res.data;
            setEventList(items);
        });
    }

    const updateStartDate = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_time', startingdate);
        data.append('end_time', endtime);
        data.append('expired_date', _expireddate);

        const config = {
            method: 'PUT',
            url: `/api/event/updateEventDate?event_id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateStartDate}>Update</CButton></>
    )
}

export const UpdateEventEnddate = ({ setEventList, newenddate, newendtime, starttime, _expireddate, id }) => {

    const convertTimeEnd = new Date(newenddate + ' ' + newendtime)
    const endingdate = convertTimeEnd.toISOString().split('T').join(' ').replace('.000Z', '')


    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event/getAllEvent`, config).then(res => {
            const items = res.data;
            setEventList(items);
        });
    }

    const updateEndDate = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_time', starttime);
        data.append('end_time', endingdate);
        data.append('expired_date', _expireddate);

        const config = {
            method: 'PUT',
            url: `/api/event/updateEventDate?event_id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateEndDate}>Update</CButton></>
    )
}

export const UpdateEventExpireddate = ({ setEventList, newexpired_date, newexpired_time, endtime, starttime, id }) => {

    const convertTimeExpired = new Date(newexpired_date + ' ' + newexpired_time)
    const expireddate = convertTimeExpired.toISOString().split('T').join(' ').replace('.000Z', '')


    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event/getAllEvent`, config).then(res => {
            const items = res.data;
            setEventList(items);
        });
    }

    const updateExpiredDate = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('start_time', starttime);
        data.append('end_time', endtime);
        data.append('expired_date', expireddate);

        const config = {
            method: 'PUT',
            url: `/api/event/updateEventDate?event_id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateExpiredDate}>Update</CButton></>
    )
}

export const UpdateEventName = ({ setEventList, neweventName, id }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event/getAllEvent`, config).then(res => {
            const items = res.data;
            setEventList(items);
        });
    }

    const updateName = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('event_name', neweventName);

        const config = {
            method: 'PUT',
            url: `/api/event/updateEventName?event_id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateName}>Update</CButton></>
    )
}

export const UpdateTargetenergy = ({ setEventEnergy, newtargetEnergy, id }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event/getAllEventEnergy`, config).then(res => {
            const items = res.data;
            setEventEnergy(items);
        });
    }

    const updateTarget = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('target_energy', parseInt(newtargetEnergy));

        const config = {
            method: 'PUT',
            url: `/api/event/updateEventEnergyTarget?event_energy_id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Updated!')
            })
            .catch((error) => {
                console.log(error);
                alert('Failed!')
            });
        setTimeout(refreshList, 100)
    };

    return (
        <><CButton color='primary' size='md' onClick={updateTarget}>Update</CButton></>
    )
}