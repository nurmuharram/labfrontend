import { CButton } from "@coreui/react";
import axios from "axios";

export const AddEvent = ({ startdate, enddate, starttime, endtime, expired_date, expired_time, energy_base, setAddItemModal, setEventList, ksatriyaid, boostrank, trial, boostpermanent, event_name }) => {

    const convertTime = new Date(startdate + ' ' + starttime)
    const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')
    const convertTimeEnd = new Date(enddate + ' ' + endtime)
    const endingdate = convertTimeEnd.toISOString().split('T').join(' ').replace('.000Z', '')
    const convertExpiredTime = new Date(expired_date + ' ' + expired_time)
    const expireddate = convertExpiredTime.toISOString().split('T').join(' ').replace('.000Z', '')

    const refreshEventList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event/getAllEvent`, config).then(res => {
            const items = res.data;
            setEventList(items);
        });
    }

    const addEvent = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('event_name', event_name)
        data.append('start_time', startingdate)
        data.append('end_time', endingdate)
        data.append('expired_date', expireddate)
        data.append('energy_base', energy_base)
        data.append('ksatriya_id', parseInt(ksatriyaid))
        data.append('rank', parseInt(boostrank))
        data.append('trial', parseInt(trial))
        data.append('permanent', parseInt(boostpermanent))

        const config = {
            method: 'POST',
            url: `/api/event/addEventEnergy`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Event Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add Event!')
            });
        setTimeout(refreshEventList, 100)

    }

    return (
        <><button className="btn btn-primary btn-md" onClick={addEvent}>+ Add Period</button></>
    )
}

export const AddEventEnergy = ({ startdate, enddate, starttime, endtime, setAddItemModal, setEventEnergy, eventId, maxEnergy, arrayData, target_energy, reward }) => {

    const convertTime = new Date(startdate + ' ' + starttime)
    const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')
    const convertTimeEnd = new Date(enddate + ' ' + endtime)
    const endingdate = convertTimeEnd.toISOString().split('T').join(' ').replace('.000Z', '')

    const refreshEventList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event/getAllEventEnergy`, config).then(res => {
            const items = res.data;
            setEventEnergy(items);
        });
    }

    const item_rewards = {
        item_reward: arrayData
    }

    const targetEnergy = {
        target_energy: target_energy
    }

    const addEvent = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('event_id', parseInt(eventId))
        data.append('start_date', startingdate)
        data.append('end_date', endingdate)
        data.append('max_energy', parseInt(maxEnergy))
        data.append('reward', parseInt(reward))
        data.append('target_energy', JSON.stringify(targetEnergy))
        data.append('item_reward', JSON.stringify(item_rewards))

        const config = {
            method: 'POST',
            url: `/api/event/addEventEnergyDetails`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Event Details Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add Event Details!')
            });
        setTimeout(refreshEventList, 100)

    }

    return (
        <><button className="btn btn-primary btn-md" onClick={addEvent}>+ Add Event Energy</button></>
    )
}

export const AddEventAnniversary = ({ anniversaryForm, setEventList, setAddAnniversaryModal  }) => {

    const convertTime = new Date(anniversaryForm.start_date + ' ' + anniversaryForm.start_time)
    const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')
    const convertTimeEnd = new Date(anniversaryForm.end_date + ' ' + anniversaryForm.end_time)
    const endingdate = convertTimeEnd.toISOString().split('T').join(' ').replace('.000Z', '')
    const convertTimeExpired = new Date(anniversaryForm.expired_date + ' ' + anniversaryForm.expired_time)
    const expiredDate = convertTimeExpired.toISOString().split('T').join(' ').replace('.000Z', '')

    const refreshEventList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event/getAllEvent`, config).then(res => {
            const items = res.data;
            setEventList(items);
        });
    }

    const addEvent = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('event_name', anniversaryForm.event_name)
        data.append('start_time', startingdate)
        data.append('end_time', endingdate)
        data.append('expired_date', expiredDate)

        const config = {
            method: 'POST',
            url: `/api/event_anniversary/addEventAnniversary`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Event Added!')
                setAddAnniversaryModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add Event!')
            });
        setTimeout(refreshEventList, 100)

    }

    return (
        <><button className="btn btn-primary btn-md" onClick={addEvent}>+ Add Event Anniversary</button></>
    )
}

export const AddEventAnniversaryMissionType = ({ missionType, setAnniversaryMissionType, setAddItemModal }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllMissionType`, config).then(res => {
            const items = res.data;
            setAnniversaryMissionType(items);
        });
    }

    const addEvent = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('description', missionType)

        const config = {
            method: 'POST',
            url: `/api/event_anniversary/addEventAnniversary`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Event Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add Event!')
            });
        setTimeout(refreshList, 100)

    }

    return (
        <><button className="btn btn-primary btn-md" onClick={addEvent}>+ Add</button></>
    )
}

export const AddEventAnniversaryMission = ({ addMissionForm, setMissionList, setAddItemModal }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllMissions`, config).then(res => {
            const items = res.data;
            setMissionList(items);
        });
    }

    const addMission = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('description', parseInt(addMissionForm.description))
        data.append('target', parseInt(addMissionForm.target))
        data.append('misison_type', parseInt(addMissionForm.missionType))

        const config = {
            method: 'POST',
            url: `/api/event_anniversary/addMission`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Mission Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add Mission!')
            });
        setTimeout(refreshList, 100)

    }

    return (
        <><button className="btn btn-primary btn-md" onClick={addMission}>+ Add</button></>
    )
}

export const AddEventAnniversaryMissionEvent = ({ setMissionEvent, setAddItemModal,  missionEventForm }) => {

    const convertTime = new Date(missionEventForm.start_date + ' ' + missionEventForm.start_time)
    const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')
    const convertTimeEnd = new Date(missionEventForm.end_date + ' ' + missionEventForm.end_time)
    const endingdate = convertTimeEnd.toISOString().split('T').join(' ').replace('.000Z', '')

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventMissions`, config).then(res => {
            const items = res.data;
            setMissionEvent(items);
        });
    }

    const addMission = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('event_id', parseInt(missionEventForm.eventId))
        data.append('start_date', startingdate)
        data.append('end_date', endingdate)

        const config = {
            method: 'POST',
            url: `/api/event_anniversary/addEventMission`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Mission Event Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add Mission Event!')
            });
        setTimeout(refreshList, 100)

    }

    return (
        <><button className="btn btn-primary btn-md" onClick={addMission}>+ Add</button></>
    )
}

export const AddEventAnniversaryMissionDetail = ({ setMissionEventDetail, setAddItemModal, eventDetailForm }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventMissionDetail`, config).then(res => {
            const items = res.data;
            setMissionEventDetail(items);
        });
    }

    const addMissionDetail = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('event_mission_id', parseInt(eventDetailForm.eventID))
        data.append('mission_id', parseInt(eventDetailForm.eventMissionId))

        const config = {
            method: 'POST',
            url: `/api/event_anniversary/addEventMissionDetails`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Mission Detail Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add Mission Detail!')
            });
        setTimeout(refreshList, 100)

    }

    return (
        <><button className="btn btn-primary btn-md" onClick={addMissionDetail}>+ Add</button></>
    )
}

export const AddEventMissionRewards = ({setAddItemModal, arrayData, setMissionReward, rewardForm }) => {

    const refreshEventList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventMissionRewards`, config).then(res => {
            const items = res.data;
            setMissionReward(items);
        });
    }

    const item_rewards = {
        item_rewards: arrayData
    }

    const addEventReward = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('mission_reward_id', parseInt(rewardForm.missionRewardID))
        data.append('item_rewards', JSON.stringify(item_rewards))

        const config = {
            method: 'POST',
            url: `/api/event_anniversary/addEventMissionRewardDetail`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Event Reward Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add Reward Details!')
            });
        setTimeout(refreshEventList, 100)

    }

    return (
        <><button className="btn btn-primary btn-md" onClick={addEventReward}>+ Add</button></>
    )
}

export const AddEventAnniversaryShop = ({ addEventShopForm, setAddItemModal, setShopList }) => {

    const convertTime = new Date(addEventShopForm.start_date + ' ' + addEventShopForm.start_time)
    const startingdate = convertTime.toISOString().split('T').join(' ').replace('.000Z', '')
    const convertTimeEnd = new Date(addEventShopForm.end_date + ' ' + addEventShopForm.end_time)
    const endingdate = convertTimeEnd.toISOString().split('T').join(' ').replace('.000Z', '')

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventShop`, config).then(res => {
            const items = res.data;
            setShopList(items);
        });
    }

    const addShopPeriod = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('event_id', parseInt(addEventShopForm.eventID))
        data.append('misc_id', parseInt(addEventShopForm.miscID))
        data.append('start_date', startingdate)
        data.append('end_date', endingdate)

        const config = {
            method: 'POST',
            url: `/api/event_anniversary/addEventShop`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Shop Period Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add!')
            });
        setTimeout(refreshList, 100)

    }

    return (
        <><button className="btn btn-primary btn-md" onClick={addShopPeriod}>+ Add</button></>
    )
}

export const AddAnniversaryShopItems = ({setAddItemModal, arrayData, setItemList }) => {

    const refreshEventList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllShopItems`, config).then(res => {
            const items = res.data;
            setItemList(items);
        });
    }

    const items = {
        shop_items: arrayData
    }

    const addShopItems = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('shop_items', items)

        const config = {
            method: 'POST',
            url: `/api/event_anniversary/addShopItem`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Event Shop Item(s) Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add Shop Item(s)!')
            });
        setTimeout(refreshEventList, 100)

    }

    return (
        <><button className="btn btn-primary btn-md" onClick={addShopItems}>+ Add</button></>
    )
}

export const AddAnniversaryShopItemDetails = ({setAddItemModal, arrayData, setItemDetails, itemDetailsForm }) => {

    const refreshEventList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventShopDetails`, config).then(res => {
            const items = res.data;
            setItemDetails(items);
        });
    }

    const items = {
        event_shop_items: arrayData
    }

    const addShopItems = (e) => {
        e.preventDefault()
        const FormData = require('form-data');
        const data = new FormData();
        data.append('event_shop_id', parseInt(itemDetailsForm.eventShopID))
        data.append('event_shop_items', items)

        const config = {
            method: 'POST',
            url: `/api/event_anniversary/addEventShopItem`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            },
            data: data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Event Shop Item(s) Added!')
                setAddItemModal(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to Add Shop Item(s)!')
            });
        setTimeout(refreshEventList, 100)

    }

    return (
        <><button className="btn btn-primary btn-md" onClick={addShopItems}>+ Add</button></>
    )
}