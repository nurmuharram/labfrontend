import { useEffect } from "react"
import axios from "axios"

export const GetEventList = ({ setEventList }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event/getAllEvent`, config).then(res => {
            const items = res.data;
            setEventList(items);
        });
    }, [])
}

export const GetEventEnergy = ({ setEventEnergy }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event/getAllEventEnergy`, config).then(res => {
            const items = res.data;
            setEventEnergy(items);
        });
    }, [])
}

export const GetEventAnniversaryMission = ({ setMissionList }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllMissions`, config).then(res => {
            const items = res.data;
            setMissionList(items);
        });
    }, [])
}

export const GetAnniversaryEventMission = ({ setMissionEvent }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventMissions`, config).then(res => {
            const items = res.data;
            setMissionEvent(items);
        });
    }, [])
}

export const GetAnniversaryShop = ({ setAnniversaryShop }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllShopItems`, config).then(res => {
            const items = res.data;
            setAnniversaryShop(items);
        });
    }, [])
}

export const GetAnniversaryEventShop = ({ setAnniversaryEventShop }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventShop`, config).then(res => {
            const items = res.data;
            setAnniversaryEventShop(items);
        });
    }, [])
}

export const GetAnniversaryMissionType = ({ setAnniversaryMissionType }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllMissionType`, config).then(res => {
            const items = res.data;
            setAnniversaryMissionType(items);
        });
    }, [])
}

export const GetAnniversaryMissionEvent = ({ setMissionEvent }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventMissions`, config).then(res => {
            const items = res.data;
            setMissionEvent(items);
        });
    }, [])
}

export const GetAnniversaryMissionEventDetail = ({ setMissionEventDetail }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventMissionDetail`, config).then(res => {
            const items = res.data;
            setMissionEventDetail(items);
        });
    }, [])
}

export const GetAnniversaryMissionEventDetailbyeventID = ({ setEventDetails, eventId }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventMissionDetailByEventId?event_id=${eventId}`, config).then(res => {
            const items = res.data;
            setEventDetails(items);
        });
    }, [])
}

export const GetAnniversaryMissionEventReward = ({ setMissionReward }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventMissionRewards`, config).then(res => {
            const items = res.data;
            setMissionReward(items);
        });
    }, [])
}

export const GetAnniversaryShopList = ({ setShopList }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventShop`, config).then(res => {
            const items = res.data;
            setShopList(items);
        });
    }, [])
}

export const GetAnniversaryShopItemList = ({ setItemList }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllShopItems`, config).then(res => {
            const items = res.data;
            setItemList(items);
        });
    }, [])
}

export const GetAnniversaryShopItemDetails = ({ setItemDetails }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event_anniversary/getAllEventShopDetails`, config).then(res => {
            const items = res.data;
            setItemDetails(items);
        });
    }, [])
}