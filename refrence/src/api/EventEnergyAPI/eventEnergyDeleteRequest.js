import { CButton } from "@coreui/react";
import axios from "axios";

export const DeleteEvent = ({ setEventList, setDeleteItemModal, id }) => {

    const refreshList = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/event/getAllEvent`, config).then(res => {
            const items = res.data;
            setEventList(items);
        });
    }

    const deleteItem = () => {

        const config = {
            method: 'delete',
            url: `/api/event/deleteEvent?id=${id}`,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth')
            }
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Item Removed!');
                setDeleteItemModal(false)
            })
            .catch((error) => {
                alert('Failed!');
            });
        setTimeout(refreshList, 100);
    };
    return (
        <><CButton type="send" size="sm" color="danger" onClick={deleteItem} ><b>Delete</b></CButton></>
    )
}