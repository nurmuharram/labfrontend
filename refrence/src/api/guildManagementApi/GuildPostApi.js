import React,{useEffect} from 'react'

import axios from 'axios'

export  const ChangeGuildName = ({match,guildName,setGuildName}) => {
const changeGuildName = ()=>{
    const data = new FormData();

    data.append('guild_name', guildName)

    const config = {
        method: 'put',
        url: `/api/guild/changeGuildName?guild_id=${match.params.guild_id}`,
        headers: {
            'Authorization': 'Bearer ' + localStorage.getItem('auth'),

        },
        data: data
    };

    axios(config)
        .then((response) => {
            console.log(JSON.stringify(response.data));
            alert('Guild Name Changed')
            setGuildName(false)
        })
        .catch((error) => {
            console.log(error);
            alert('Failed to change!')
        });
    setTimeout(500)
}
    return (
        <>
            <button className='btn btn-info btn-sm' onClick={changeGuildName}>
                Change Guild Name
            </button>{' '}
        </>
    )
}

export  const ChangeGuildInitial = ({match,guildInitial,setGuildInitial}) => {
    const ChangeGuildInitial = ()=>{
        const data = new FormData();
    
        data.append('guild_initial', guildInitial)
    
        const config = {
            method: 'put',
            url: `/api/guild/changeGuildInitial?guild_id=${match.params.guild_id}`,
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('auth'),
    
            },
            data: data
        };
    
        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert('Guild Initial Changed')
                setGuildInitial(false)
            })
            .catch((error) => {
                console.log(error);
                alert('Failed to change!')
            });
        setTimeout(500)
    }
        return (
            <>
                <button className='btn btn-info btn-sm' onClick={ChangeGuildInitial}>
                    Change Guild Initial
                </button>{' '}
            </>
        )
    }

    export  const ChangeGuildMotto = ({match,GuildMotto,setGuildMotto}) => {
        const ChangeGuildMotto = ()=>{
            const data = new FormData();
        
            data.append('guild_initial', GuildMotto)
        
            const config = {
                method: 'put',
                url: `/api/guild/changeGuildMotto?guild_id=${match.params.guild_id}`,
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('auth'),
        
                },
                data: data
            };
        
            axios(config)
                .then((response) => {
                    console.log(JSON.stringify(response.data));
                    alert('Guild Motto Changed')
                    setGuildMotto(false)
                })
                .catch((error) => {
                    console.log(error);
                    alert('Failed to change!')
                });
            setTimeout(500)
        }
            return (
                <>
                    <button className='btn btn-info btn-sm' onClick={ChangeGuildMotto}>
                        Change Guild Motto
                    </button>{' '}
                </>
            )
        }