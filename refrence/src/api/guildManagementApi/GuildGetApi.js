import {useEffect} from 'react'
import axios from 'axios'
export const GetGuildBlessing = ({match,setGuildBlessings}) => {

  useEffect(() => {
    const config = {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
        }
    }
    axios.get(`/api/guild/getGuildBlessingsLog?guild_id=${match.params.guild_id}&count=10&offset=0`, config).then(res => {
        const data = res.data;
        setGuildBlessings(data);
    });
}, [])


}

export const GetGuildList = ({setGuildData}) => {

    useEffect(() => {
        const config = {
            headers: {
              Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
          }
          axios.get(`/api/guild/getAllGuilds?count=1000&offset=0`, config).then(res => {
            const data = res.data;
            setGuildData(data);
          });
    }, [])
  
  
  }


export const GetGuildCitrineContributionList = ({match,setGetGuildCitrineContribution}) => {

  useEffect(() => {
    const config = {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
        }
    }
    axios.get(`/api/guild/getGuildCitrineContribution?guild_id=${match.params.guild_id}&count=10&offset=0`, config).then(res => {
        const data = res.data;
        setGetGuildCitrineContribution(data);
    });
}, [])

}

export const GetGuildMemberCheckinLogsList = ({match,setGuildMemberCheckinLogsList}) => {

  useEffect(() => {
    const config = {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
        }
    }
    axios.get(`/api/guild/getGuildMemberCheckInLogs?guild_id=${match.params.guild_id}&count=10&offset=0`, config).then(res => {
        const data = res.data;
        setGuildMemberCheckinLogsList(data);
    });
}, [])

}

export const GetGuildMemberLogsServices = ({match,setGuildMemberLogs}) => {

  useEffect(() => {
    const config = {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
        }
    }
    axios.get(`/api/guild/getGuildMemberLogs?guild_id=${match.params.guild_id}&count=10&offset=0`, config).then(res => {
        const data = res.data;
        setGuildMemberLogs(data);
    });
}, [])

}

export const GetGuildMemberRankLogsServices = ({match,setGuildRankLogs}) => {

  useEffect(() => {
    const config = {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
        }
    }
    axios.get(`/api/guild/getGuildMemberRankLogs?guild_id=${match.params.guild_id}&count=10&offset=0`, config).then(res => {
        const data = res.data;
        setGuildRankLogs(data);
    });
}, [])

}

export const GetGuildMembersServices = ({match,setGuildMembersServices}) => {

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/guild/getGuildMembers?guild_id=${match.params.guild_id}`, config).then(res => {
            const data = res.data;
            setGuildMembersServices(data);
        });
    }, [])
  }
  
  export const GetGuildMissionServices = ({match,setGuildMissionServices}) => {

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/guild/getGuildMisisons?guild_id=${match.params.guild_id}&count=10&offset=0`, config).then(res => {
            const data = res.data;
            setGuildMissionServices(data);
        });
    }, [])

  }
  