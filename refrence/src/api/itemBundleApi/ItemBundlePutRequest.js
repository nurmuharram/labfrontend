import axios from "axios";

export const UpdateItemBundle = ({
    item,
    itemType,
    newItemId,
    newAmount,
    setBundleList
  }) => {
    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllBundles`, config).then(res => {
            const items = res.data;
            setBundleList(items);
        });
    };
  
    const updateItem = async () => {
        const FormData = require("form-data");
        const data = new FormData();
        data.append("item_type_new", itemType);
        data.append("item_id_new", newItemId);
        data.append("amount", newAmount);

        const config = {
          method: "PUT",
          url: `/api/shop/updateBundle?shop_id=${item.shop_id}&item_id=${item.item_id}&item_type=${item.item_type}`,
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
          data: data,
        };

        await axios(config)
          .then((response) => {
            console.log(JSON.stringify(response.data));
            alert("Item Updated!");
          })
          .catch((error) => {
            console.log(error);
            alert("Failed to Update!");
          });
        setTimeout(refreshData, 100)
      };
  
    return (
      <>
        <button className="btn btn-md btn-info" onClick={updateItem}>
          Update
        </button>
      </>
    );
  };

  
export const UpdateFeaturedBundleStartDateEndDate = ({ setFeaturedBundleList, startDate, startTime, endDate, endTime, item }) => {

  const startDateTimeToConvert = new Date(startDate + " " + startTime);
  const convertedStartDateTimeToUtc = startDateTimeToConvert
    .toISOString()
    .split("T")
    .join(" ")
    .replace(".000Z", "");
    const endDateTimeToConvert = new Date(endDate + " " + endTime);
    const convertedEndDateTimeToUtc = endDateTimeToConvert
      .toISOString()
      .split("T")
      .join(" ")
      .replace(".000Z", "");

  const refreshBundleList = () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/shop/getAllFeaturedBundles`, config).then(res => {
      const items = res.data;
      setFeaturedBundleList(items);
    });
  }

  const updateItemStartEndDate = async () => {
    const FormData = require('form-data');
    const data = new FormData();
    data.append('start_date', convertedStartDateTimeToUtc);
    data.append('end_date', convertedEndDateTimeToUtc);

    const config = {
      method: 'PUT',
      url: `/api/shop/updateFeaturedBundleDate?shop_id=${item.shop_id}`,
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      },
      data: data
    };

    await axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert('Item Updated!')
      })
      .catch((error) => {
        console.log(error);
        alert('Failed to Update!')
      });
    setTimeout(refreshBundleList, 100)
  };
  return (
    <button className="btn btn-sm btn-info" onClick={updateItemStartEndDate} ><b>Update New Start Date & End Date</b></button>
  )
}
  
export const UpdateFeaturedBundlePriority = ({ setFeaturedBundleList, priority, item}) => {

  const refreshBundleList = () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/shop/getAllFeaturedBundles`, config).then(res => {
      const items = res.data;
      setFeaturedBundleList(items);
    });
  }

  const updateItemPriority = async () => {
    const FormData = require("form-data");
    const data = new FormData();
    data.append("priority", priority);

    const config = {
      method: "PUT",
      url: `/api/shop/updateFeaturedBundlePriority?shop_id=${item.shop_id}`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
      data: data,
    };

    await axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert("Item Updated!");
      })
      .catch((error) => {
        console.log(error);
        alert("Failed to Update!");
      });
    setTimeout(refreshBundleList, 100);
  };
  return (
    <button className="btn btn-sm btn-info" onClick={updateItemPriority} ><b>Update Priority</b></button>
  )
}