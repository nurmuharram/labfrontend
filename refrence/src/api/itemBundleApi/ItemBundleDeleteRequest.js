import axios from "axios";

export const DeleteItemBundle = ({ setBundleList, setDeleteItemModal, item }) => {

    const refreshData = () => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllBundles`, config).then(res => {
            const items = res.data;
            setBundleList(items);
        });
    };

    const deleteItem = () => {
        const config = {
            method: "delete",
            url: `/api/shop/deleteBundle?shop_id=${item.shop_id}&item_id=${item.item_id}&item_type=${item.item_type}`,
            headers: {
                Authorization: "Bearer " + localStorage.getItem("auth"),
            },
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                alert("Item Removed!");
                setDeleteItemModal(false);
            })
            .catch((error) => {
                alert("Failed to delete item!");
            });
        setTimeout(refreshData, 100);
    };
    return (
        <><button className="btn btn-sm btn-danger" onClick={deleteItem} ><b>Delete</b></button></>
    )
}

export const DeleteFeaturedBundle = ({ setFeaturedBundleList, setDeleteItemModal, item }) => {

    const refreshBundleList = () => {
        const config = {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
          }
        }
        axios.get(`/api/shop/getAllFeaturedBundles`, config).then(res => {
          const items = res.data;
          setFeaturedBundleList(items);
        });
      }

    const deleteItem = () => {
        const config = {
          method: "delete",
          url: `/api/shop/deleteFeaturedBundle?shop_id=${item.shop_id}`,
          headers: {
            Authorization: "Bearer " + localStorage.getItem("auth"),
          },
        };

        axios(config)
          .then((response) => {
            console.log(JSON.stringify(response.data));
            alert("Item Removed!");
            setDeleteItemModal(false);
          })
          .catch((error) => {
            alert("Failed to delete item!");
          });
        setTimeout(refreshBundleList, 100);
      };
    return (
        <><button className="btn btn-sm btn-danger" onClick={deleteItem} ><b>Delete</b></button></>
    )
}