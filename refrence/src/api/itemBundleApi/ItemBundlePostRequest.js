import axios from "axios";

export const AddItemBundle = ({
  setBundleList,
  arrayData,
  priceCitrine,
  priceCoin,
  priceLotus,
  description,
  releaseDate,
  releaseTime
}) => {
  const toConvert = new Date(releaseDate + ' ' + releaseTime)
  const convertedToUtc = toConvert.toISOString().split('T').join(' ').replace('.000Z', '')
  const refreshData = () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/shop/getAllBundles`, config).then(res => {
      const items = res.data;
      setBundleList(items);
    });
  };

  const jsonData = {
    item_bundle: arrayData
  }

  const addItemBundle = (e) => {
    e.preventDefault()
    const FormData = require('form-data');
    const data = new FormData();
    data.append('item_bundle', JSON.stringify(jsonData))
    data.append('price_coin', priceCoin)
    data.append('price_citrine', priceCitrine)
    data.append('price_lotus', priceLotus)
    data.append('release_date', convertedToUtc)
    data.append('description', description)

    const config = {
      method: 'POST',
      url: `/api/shop/addBundle`,
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      },
      data: data
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert('Bundle Created!')
      })
      .catch((error) => {
        console.log(error);
        alert('Failed to Create Bundle!')
      });
    setTimeout(refreshData, 100)
  }
  return (
    <>
      {jsonData.item_bundle.length < 1 ? <button className="btn btn-md btn-primary" disabled>
        Add Item
      </button> : <button className="btn btn-md btn-primary" onClick={addItemBundle}>
        Add Item
      </button>}
    </>
  );
};


export const AddFeaturedBundle = ({
  startDate, startTime, endDate, endTime, shopId, priority, maxBuy, setFeaturedBundleList
}) => {
  const startDateTimeToConvert = new Date(startDate + ' ' + startTime)
  const convertedStartDateTimeToUtc = startDateTimeToConvert.toISOString().split('T').join(' ').replace('.000Z', '')
  const endDateTimeToConvert = new Date(endDate + ' ' + endTime)
  const convertedEndDateTime = endDateTimeToConvert.toISOString().split('T').join(' ').replace('.000Z', '')

  const refreshBundleList = () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
    }
    axios.get(`/api/shop/getAllFeaturedBundles`, config).then(res => {
      const items = res.data;
      setFeaturedBundleList(items);
    });
  }

  const addFeaturedBundle = (e) => {
    e.preventDefault()
    const FormData = require('form-data');
    const data = new FormData();
    data.append('shop_id', parseInt(shopId))
    data.append('start_date', convertedStartDateTimeToUtc)
    data.append('end_date', convertedEndDateTime)
    data.append('priority', parseInt(priority))
    data.append('max_buy', parseInt(maxBuy))

    const config = {
      method: 'POST',
      url: `/api/shop/addFeaturedBundle`,
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('auth'),
      },
      data: data
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        alert('Bundle Created!')
      })
      .catch((error) => {
        console.log(error);
        alert('Failed to Create Bundle!')
      });
    setTimeout(refreshBundleList, 100)
  }

  return (
    <>
      <button className="btn btn-md btn-primary" onClick={addFeaturedBundle}>
        Add Item
      </button>
    </>
  );
};

