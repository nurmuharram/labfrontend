import axios from "axios";
import { useEffect } from "react";

export const GetItemBundle = ({ setBundleList }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllBundles`, config).then(res => {
            const items = res.data;
            setBundleList(items);
        });
    }, [])
}

export const GetFeaturedBundle = ({ setFeaturedBundleList }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllFeaturedBundles`, config).then(res => {
            const items = res.data;
            setFeaturedBundleList(items);
        });
    }, [])
}

export const GetBundles = ({ setBundles }) => {
    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`/api/shop/getAllItems?item_type=0`, config).then(res => {
            const items = res.data;
            setBundles(items);
        });
    }, [])
}
