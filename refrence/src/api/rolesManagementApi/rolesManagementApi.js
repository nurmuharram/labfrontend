const url = "api/role/getAll";
const header = {
            headers: {
                method: "GET",
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
                 /* "Access-Control-Allow-Origin": "*", */
               
            }
        }

const fetchGetRoles = async () => {
  try {
    const response = await fetch(url, header);
    return await response.json();
  } catch (error) {
    throw error;
  }
};

export default fetchGetRoles;

