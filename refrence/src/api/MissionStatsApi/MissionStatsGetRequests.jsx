import { useEffect } from "react";
import axios from "axios";

export const GetDailyReward = ({ setDailyRewardCount }) => {
  useEffect(() => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/stats/getDailyRewardsDoneCount`, config).then((res) => {
      const items = res.data;
      setDailyRewardCount(items);
    });
  }, []);
};

export const GetWeeklyReward = ({ setWeeklyRewardsCount }) => {
  useEffect(() => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/stats/getWeeklyRewardsDoneCount`, config).then((res) => {
      const items = res.data;
      setWeeklyRewardsCount(items);
    });
  }, []);
};

export const GetMostCompletedMission = ({ setMostCompletedMission }) => {
  useEffect(() => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/stats/getMostCompletedMission`, config).then((res) => {
      const items = res.data;
      setMostCompletedMission(items);
    });
  }, []);
};

export const GetMostNotCompletedMission = ({ setMostNotCompletedMission }) => {
  useEffect(() => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("auth"),
      },
    };
    axios.get(`/api/stats/getMostNotCompletedMission`, config).then((res) => {
      const items = res.data;
      setMostNotCompletedMission(items);
    });
  }, []);
};
